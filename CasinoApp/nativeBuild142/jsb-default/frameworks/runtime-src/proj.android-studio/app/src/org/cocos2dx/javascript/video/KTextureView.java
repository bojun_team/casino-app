package org.cocos2dx.javascript.video;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.IMediaPlayer.OnErrorListener;
import com.ksyun.media.player.IMediaPlayer.OnInfoListener;
import com.ksyun.media.player.IMediaPlayer.OnPreparedListener;
import com.ksyun.media.player.IMediaPlayer.OnVideoSizeChangedListener;
import com.ksyun.media.player.KSYMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer.KSYReloadMode;
import com.ksyun.media.player.KSYTextureView;

import org.cocos2dx.javascript.JSConnect;
import org.cocos2dx.lib.Cocos2dxActivity;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ifalo-jun on 2017/5/25.
 * Use KSYUN Live Streaming player SDK v1.9.1
 * @see <a href="https://github.com/ksvc/KSYMediaPlayer_Android/wiki/KSYTextureView">wiki</a>
 *
 */

// ===== KSYTextureView 介紹 =====
/**
 * KSYTextureView封装了TextureView与KSYMediaPlayer，接口与KSYMediaPlayer保持一致，
 * 但将setSurface和setDisplay两个接口去除，其相关逻辑已移至KSYTextureView内部处理，用户无需再做处理。
 * KSYTextureView在软解或硬解的情况下均可使用，
 * 并支持前后台切换不黑屏或花屏、切后台音频播放、旋转、缩放、截图等功能
 */

public class KTextureView {
    interface Listener {
        void onEvent();
    }

    private static boolean isShowLog = false;
    private static final String TAG = "jun KTextureView";
    private static Cocos2dxActivity m_activity;
    private static KSYTextureView m_videoView = null;
    @SuppressLint("StaticFieldLeak")
    private static ProgressBar m_progress = null;
    private static String m_videoPath;
    private static int m_screenWidth;
    private static int m_screenHeight;
    private static boolean m_isMediaPlayerStart = false;
    private static boolean m_isVideoViewVisible  = false;
    private static boolean m_isRunningVideoAction = false;
    private static ArrayList<VideoAction> m_videoActionQueue = new ArrayList<>();

    private static int m_videoOriginTop = 0;
    private static int m_videoOriginLeft = 0;

    @SuppressLint("StaticFieldLeak")
    private static FrameLayout m_frameLayout;

    // 初始化 video
    public static void setupCurrentActivity(Cocos2dxActivity activity) {
        m_activity = activity;
        calculateScreenSize(); // 計算螢幕大小
    }

    private static void createFrameLayout(final int topMargin, final int width, final int height, final int clipH) {
        if(m_frameLayout != null) {
            if(isShowLog == true)Log.d(TAG, "createFrameLayout m_frameLayout exist");
        }


        if(m_frameLayout == null) {
            m_frameLayout = new FrameLayout(m_activity);
            m_activity.addContentView(m_frameLayout, new FrameLayout.LayoutParams(width, height));
            m_frameLayout.setClipChildren(true);
        }
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) m_frameLayout.getLayoutParams();
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.topMargin = topMargin;
        params.width = width;
        params.height = clipH;
        m_frameLayout.setLayoutParams(params);

    }

    // 計算螢幕大小
    private static void calculateScreenSize(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            m_activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        else
            m_activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        m_screenWidth = displayMetrics.widthPixels;
        m_screenHeight = displayMetrics.heightPixels;
    }

    // 取得螢幕高度
    public static int getScreenWidth() { return m_screenWidth; }
    public static int getScreenHeight() { return m_screenHeight; }

    // 建立一個影片播放View
    private static void createVideoView(final int topMargin, final int width, final int height, final int clipH, final int offsetY, final boolean isVideoMute) {
        if(m_videoView != null) releaseVideoView(null); // 如果影片播放視窗有被建立，先釋放掉舊的

        createFrameLayout(topMargin, width, height, clipH);

        // 建立把VideoView並加到螢幕上
        m_videoView = new KSYTextureView(m_activity);
        m_videoView.setOnLogEventListener(new IMediaPlayer.OnLogEventListener(){
            @Override
            public void onLogEvent(IMediaPlayer iMediaPlayer, String s) {

            }
        });
        m_videoView.setPlayerMute((isVideoMute)?(1):(0)); // 設置播放器禁音
        m_videoView.setVolume(0.75f, 0.75f);
        m_videoView.setBufferTimeMax(2f); // 設定直播緩衝的最大閾值，單位為秒，最小值為 0.5(s)
        m_videoView.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_AUTO);
        m_frameLayout.addView(m_videoView);
        m_videoOriginTop = -offsetY;
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) m_videoView.getLayoutParams();
        params.topMargin = m_videoOriginTop;
        params.leftMargin = 0;
        params.width = width;
        params.height = height;
        m_videoView.setLayoutParams(params);
        m_videoOriginLeft = params.leftMargin;

        // 設定監聽器
        m_videoView.setOnPreparedListener(onPreparedListener);
        m_videoView.setOnInfoListener(onInfo);
        m_videoView.setOnErrorListener(onError);
        m_videoView.setOnVideoSizeChangedListener(onVideoSizeChanged);

        // 如果m_progress 沒有被建立,建立一個讀取條
        if(m_progress == null) createProgressBar(topMargin);
    }

    // 釋放影片播放View
    private static void releaseVideoView(final Listener listener) {
        if(m_videoView == null) {
            if(listener != null) listener.onEvent();
            return;
        }
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(m_videoView == null) {
                    if(listener != null) listener.onEvent();
                    return;
                }
                m_frameLayout.removeView(m_videoView);
                m_videoView.release();
                m_videoView = null;
                releaseProgressBar(); // 移除讀取條
                if(listener != null) listener.onEvent();
            }
        });
    }

    // 播放影片
    private static void playVideo(final int viewY, final int w, final int h, String url, final int clipH, final int offsetY, final boolean isVideoMute) {
        if(isShowLog == true)Log.w(TAG,"playVideo [ " + viewY + ", " + w + ", " + h + ", " + url + " ]");
        m_videoPath = url;
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createVideoView(viewY, w, h, clipH, offsetY, isVideoMute); // 建立一個播放器，並設定監聽器
                m_isMediaPlayerStart = true;
                m_isVideoViewVisible = true;
                updateProgressBar(false); // 更新讀取條
                try {
                    m_videoView.setDataSource(m_videoPath); // 設定影片網址
                    m_videoView.prepareAsync(); // 建立連接，讀取數據，創建解碼器等準備播放的工作
                } catch (IOException e) {
                    Log.d(TAG, "playVideo IOException [ " + e.getLocalizedMessage() + " ]");
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    Log.d(TAG, "playVideo IllegalStateException [ " + e.getLocalizedMessage() + " ]");
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    Log.d(TAG, "playVideo IllegalArgumentException [ " + e.getLocalizedMessage() + " ]");
                    e.printStackTrace();
                }
                KTextureView.m_isRunningVideoAction = false;
                KTextureView.nextAction();
            }
        });
    }

    public static void addAction(VideoAction action) {
        if(isShowLog == true)Log.w(TAG, "addAction:" + action.cmd);
        m_videoActionQueue.add(action);
        nextAction();
    }

    private static void nextAction() {
        if(m_isRunningVideoAction) {
            if(isShowLog == true)Log.w(TAG, "nextAction action running");
            return;
        }
        if(m_videoActionQueue.size() == 0) {
            if(isShowLog == true)Log.w(TAG, "nextAction no next action");
            return;
        }
        if(isShowLog == true)Log.w(TAG, "nextAction:" + m_videoActionQueue.get(0).cmd);

        m_isRunningVideoAction = true;
        VideoAction currAction = m_videoActionQueue.get(0);
        m_videoActionQueue.remove(0);

        if(currAction.cmd.equals("seekTo")) {
            int pos =  Integer.parseInt(currAction.params.get("pos"));
            seekTo(pos);
        }

        if(currAction.cmd.equals("playVideo")) {
            int viewY =  Integer.parseInt(currAction.params.get("viewY"));
            int w =  Integer.parseInt(currAction.params.get("w"));
            int h =  Integer.parseInt(currAction.params.get("h"));
            String url =  currAction.params.get("url");
            int clipH =  Integer.parseInt(currAction.params.get("clipH"));
            int offsetY =  Integer.parseInt(currAction.params.get("offsetY"));
            boolean isVideoMute = Boolean.parseBoolean(currAction.params.get("isVideoMute"));
            playVideo(viewY, w, h, url, clipH, offsetY, isVideoMute);
        }

        if(currAction.cmd.equals("pauseVideo")) {
            pauseVideo();
        }

        if(currAction.cmd.equals("setVideoVisible")) {
            boolean isVisible =  Boolean.parseBoolean(currAction.params.get("isVisible"));
            setVideoVisible(isVisible);
        }

        if(currAction.cmd.equals("setVideoMute")) {
            boolean isVideoMute =  Boolean.parseBoolean(currAction.params.get("isVideoMute"));
            setVideoMute(isVideoMute);
        }

        if(currAction.cmd.equals("replayVideo")) {
            replayVideo();
        }

        if(currAction.cmd.equals("continueVideo")) {
            continueVideo();
        }

        if(currAction.cmd.equals("stopVideo")) {
            stopVideo();
        }

        if(currAction.cmd.equals("setVideoScale")) {
            boolean isScaleToBig =  Boolean.parseBoolean(currAction.params.get("isScaleToBig"));
            String tableID =  currAction.params.get("tableID");
            float scale =  Float.parseFloat(currAction.params.get("scale"));
            float offsetX =  Float.parseFloat(currAction.params.get("offsetX"));
            float offsetY =  Float.parseFloat(currAction.params.get("offsetY"));
            setVideoScale(isScaleToBig, tableID, scale, offsetX, offsetY);
        }

    }
    // 暫停影片
    private static void pauseVideo() {
        if(!m_isMediaPlayerStart) {
            KTextureView.m_isRunningVideoAction = false;
            KTextureView.nextAction();
            return; // 沒有播放過影片，則不暫停影片
        }
        if(m_videoView == null) {
            KTextureView.m_isRunningVideoAction = false;
            KTextureView.nextAction();
            return;
        }
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(m_videoView == null) {
                    KTextureView.m_isRunningVideoAction = false;
                    KTextureView.nextAction();
                    return;
                }
                if(isShowLog == true)Log.d(TAG,"pauseVideo");
                m_isVideoViewVisible = false;
                updateProgressBar(false);
                if(!m_videoView.isPlaying()) {
                    KTextureView.m_isRunningVideoAction = false;
                    KTextureView.nextAction();
                    return; // 沒有正在播放影片，則不需要暫停影片
                }
                m_videoView.pause();
                KTextureView.m_isRunningVideoAction = false;
                KTextureView.nextAction();
            }
        });
    }

    // 繼續影片
    private static void continueVideo() {
        if(!m_isMediaPlayerStart) {
            KTextureView.m_isRunningVideoAction = false;
            KTextureView.nextAction();
            return; // 沒有播放過影片，則不繼續影片
        }
        if(m_videoView == null) {
            KTextureView.m_isRunningVideoAction = false;
            KTextureView.nextAction();
            return;
        }
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!m_isMediaPlayerStart) {
                    KTextureView.m_isRunningVideoAction = false;
                    KTextureView.nextAction();
                    return; // 沒有播放過影片，則不繼續影片
                }
                if(m_videoView == null) {
                    KTextureView.m_isRunningVideoAction = false;
                    KTextureView.nextAction();
                    return;
                }
                if(isShowLog == true)Log.d(TAG,"continueVideo");
                m_videoView.start();
                m_isVideoViewVisible = true;
                updateProgressBar(true);
                KTextureView.m_isRunningVideoAction = false;
                KTextureView.nextAction();
            }
        });
    }

    //重新播放影片
    private static void replayVideo() {
        if(!m_isMediaPlayerStart) return; // 沒有播放過影片，則不重讀影片
        reloadVideo();
    }

    // 停止影片
    private static void stopVideo() {
        if(isShowLog == true)Log.d(TAG,"stopVideo");
        m_isMediaPlayerStart = false;
        m_isVideoViewVisible = false;
        if(m_videoView != null) {
            m_videoView.stop();
        }
        releaseVideoView(new Listener() {
            @Override
            public void onEvent() {
                KTextureView.m_isRunningVideoAction = false;
                KTextureView.nextAction();
            }
        });
    }

    // 重讀影片
    private static void reloadVideo() {
        if(m_videoView == null) return;
        if(isShowLog == true)Log.d(TAG,"reloadVideo");
        KSYReloadMode mode = KSYReloadMode.KSY_RELOAD_MODE_ACCURATE; // 精確模式
        m_videoView.reload(m_videoPath, true, mode);
    }

    // m_mediaPlayer在準備完成，可以播放時會發出onPrepared
    private static OnPreparedListener onPreparedListener = new OnPreparedListener() {
        @Override
        public void onPrepared(IMediaPlayer mp) {
            if(isShowLog == true)Log.d(TAG, "onPrepared");
            int scalingMode = KSYMediaPlayer.VIDEO_SCALING_MODE_NOSCALE_TO_FIT;
            m_videoView.setVideoScalingMode(scalingMode); // 設定scalingMode 裁剪模式(不會有黑邊)
            m_videoView.start(); // 開始播放視頻
            updateProgressBar(true); // 更新讀取條
            sendVideoInfoToJS("didPrepared",""); //通知 creator 影片已準備好
        }
    };

    // 消息監聽器，會將關於播放器的消息通過此回調接口，例如：視頻渲染，音頻渲染等
    private static OnInfoListener onInfo = new OnInfoListener() {
        @Override
        public boolean onInfo(IMediaPlayer mp, int what, int extra) {
            switch (what) {
                case KSYMediaPlayer.MEDIA_INFO_VIDEO_ROTATION_CHANGED: // 視頻方向發生變化
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "視頻方向發生變化" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_SOFTWARE_DECODE: // 播放器使用軟解
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "播放器使用軟解" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_HARDWARE_DECODE: // 播放器使用硬解
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "播放器使用硬解" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: // 視頻開始渲染
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "視頻開始渲染" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_BUFFERING_START: // 緩存數據開始
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "緩存數據開始" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_BUFFERING_END: // 緩存數據
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "緩存數據結束" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_SUGGEST_RELOAD: // 建議調用 reload 接口
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "建議調用 reload 接口" + " " + extra + " ]");
                    reloadVideo(); // 重讀影片
                    break;
                case KSYMediaPlayer.MEDIA_INFO_RELOADED: // reload成功
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "reload成功" + " " + extra + " ]");
                    int scalingMode = KSYMediaPlayer.VIDEO_SCALING_MODE_NOSCALE_TO_FIT;
                    m_videoView.setVideoScalingMode(scalingMode);
                    m_videoView.start(); // 開始播放視頻
                    updateProgressBar(true); // 更新讀取條
                    sendVideoInfoToJS("didPrepared",""); //通知 creator 影片已準備好
                    break;
                case KSYMediaPlayer.MEDIA_INFO_NOT_SEEKABLE: // 該多媒體文件不可快進/快退
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + "該多媒體文件不可快進/快退" + " " + extra + " ]");
                    break;
                default:
                    if(isShowLog == true)Log.d(TAG, "onInfo = [ " + what + " " + extra + " ]");
            }
            return true;
        }
    };

    // 錯誤監聽器，播放器遇到錯誤時會將相應的錯誤碼通過此回調接口
    private static OnErrorListener onError = new OnErrorListener() {
        @Override
        public boolean onError(IMediaPlayer mp, int what, int extra) {
            switch (what) {
                case KSYMediaPlayer.MEDIA_ERROR_CONNECT_SERVER_FAILED: // 連接服務器失敗
                    Log.e(TAG, "onError = [" + "連接服務器失敗" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_ERROR_IO: // 網絡超時
                    Log.e(TAG, "onError = [" + "網絡超時" + " " + extra + " ]");
                    break;
                default:
                    Log.e(TAG, "onError = [" + what + " " + extra + " ]");
            }
            try {
                Thread.sleep(3000); // wait for 3 seconds
            } catch (Exception ex) {
                Log.e(TAG, "onError = [ " + ex.getMessage() + " ]");
                ex.printStackTrace();
            }
            reloadVideo(); // 重讀影片
            return true;
        }
    };

    // 視頻的寬高發生變化時會有相應的回調
    private static OnVideoSizeChangedListener onVideoSizeChanged = new OnVideoSizeChangedListener() {
        @Override
        public void onVideoSizeChanged(IMediaPlayer mp, int w, int h, int sar_num, int sar_den) {
            // Update Real Video Aspect Ratio
            m_activity.mFrameLayout.requestLayout();
            m_activity.mFrameLayout.invalidate();
        }
    };

    // 創造一個讀取條
    private static void createProgressBar(final int videoTopMargin) {
        m_progress = new ProgressBar(m_activity, null, android.R.attr.progressBarStyle);
        m_frameLayout.addView(m_progress);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) m_progress.getLayoutParams();
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.topMargin = videoTopMargin;
        params.width = 100;
        params.height = 100;
        m_progress.setLayoutParams(params);
    }

    // 移除讀取條
    private static void releaseProgressBar() {
        if(m_progress == null) return;
        m_frameLayout.removeView(m_progress);
        m_progress = null;
    }

    // 更新讀取條
    private static void updateProgressBar(boolean needReload) {
        m_frameLayout.bringChildToFront(m_progress); // 讀取條顯示在最上層
        // 如果 videoView 沒有被建立，則隱藏讀取條
        if(m_videoView == null) {
            m_progress.setVisibility(View.INVISIBLE);
            return;
        }

        // 如果不顯示影片，則隱藏讀取條及影片View
        if(!m_isVideoViewVisible) {
            m_videoView.setVisibility(View.INVISIBLE);
            m_progress.setVisibility(View.INVISIBLE);
            return;
        }

        // 如果開始播放影片，則隱藏讀取條，顯示影片View
        if(m_videoView.isPlaying()) {
            m_videoView.setVisibility(View.VISIBLE);
            m_progress.setVisibility(View.INVISIBLE);
        }
        // 顯示讀取條
        else {
            if(needReload) reloadVideo(); // 當m_mediaPlayer.start()後，沒有成功播放，則重讀影片
            m_progress.setVisibility(View.VISIBLE);
        }
    }

    // 將影片快轉/快退到指定位置
    private static void seekTo(long pos) {
        if(isShowLog == true)Log.d(TAG,"seekTo [ " + pos + " ]");
        // 直播視訊不支持seek
        KTextureView.m_isRunningVideoAction = false;
        KTextureView.nextAction();
    }

    // 當裝置進入背景時要做的事
    public static void onPause() {
        if(isShowLog == true)Log.d(TAG,"onPause");
        if (m_videoView == null) return;
        m_videoView.runInBackground(false); // true表示切换到后台后仍然播放音频
    }

    // 當裝置回到遊戲時要做的事
    public static void onResume() {
        if(isShowLog == true)Log.d(TAG,"onResume");
        if (m_videoView == null) return;
        m_videoView.runInForeground();
        if(m_isVideoViewVisible) reloadVideo();
    }

    // 設定是否顯示影片
    private static void setVideoVisible(final boolean isVisible) {
        if(isShowLog == true)Log.d(TAG,"setVideoVisible [ " + isVisible + " ]");
        m_isVideoViewVisible = isVisible;
        KTextureView.m_isRunningVideoAction = false;
        KTextureView.nextAction();
    }

    // 設定是否禁音影片
    private static void setVideoMute(final boolean isVideoMute) {
        if(isShowLog == true)Log.d(TAG,"setVideoMute [ " + isVideoMute + " ]");
        if (m_videoView != null) {
            m_videoView.setPlayerMute((isVideoMute)?(1):(0)); // 設置播放器禁音
        }
        KTextureView.m_isRunningVideoAction = false;
        KTextureView.nextAction();
    }

    // 傳 RTMP Video 資訊到 JS 層
    private static void sendVideoInfoToJS(final String strInfoName, final String strInfoData) {
        JSConnect.sendVideoInfo(strInfoName,strInfoData);
    }

    // 設定是否縮放影片
    private static void setVideoScale(boolean isScaleToBig, String tableID, float scale, float offsetX, float offsetY) {
        if(isShowLog == true)Log.w(TAG,"setVideoScale [ " + isScaleToBig + " ][ " + tableID + " ]");

        if(isScaleToBig == true) {
            videoScaleTo(scale, offsetY, offsetX);
        } else {
            videoScaleTo(1.0f, 0.0f, 0.0f);
        }
    }

    private static void videoScaleTo(final float scale, final float posY, final float posX) {
        if(isShowLog == true)Log.w(TAG,"videoScaleTo [ " + scale + " ][ " + posX + " ][ " + posY + " ]");

        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(m_videoView == null) {
                    KTextureView.m_isRunningVideoAction = false;
                    KTextureView.nextAction();
                    return;
                }

                m_videoView.setScaleX(scale);
                m_videoView.setScaleY(scale);

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) m_videoView.getLayoutParams();
                params.topMargin = ((int)(m_videoOriginTop-posY));
                params.leftMargin = (int)(m_videoOriginLeft + posX);
                m_videoView.setLayoutParams(params);

                KTextureView.m_isRunningVideoAction = false;
                KTextureView.nextAction();
            }
        });
    }

}
