package org.cocos2dx.javascript;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.cocos2d.CasinoApp.R;
import org.cocos2dx.javascript.video.KTextureView;
import org.cocos2dx.javascript.video.VideoAction;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHelper;
import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;


/**
 * Created by fan on 2017/1/24.
 */

public class JSConnect extends Cocos2dxActivity{
    private final static String TAG = "JSConnect";
    public static Cocos2dxActivity mActivity;
    public static final int COCOS_VIEW_WIDTH = 1080;
    public static final int COCOS_VIEW_HEIGHT = 1920;
    private static boolean mIsCocosCreatorRunning = false;
    private static Intent mIntent = null;

    public static void setupCurrentActivity(Cocos2dxActivity activity) {
        mActivity = activity;
    }
    //SignalR 串接
    public static void connectToServer(String connectUrl) {
        Log.w(TAG,"connectToServer");
        SignalRTool.connect(connectUrl);
    }

    // JS call native (Objective-C -IOS) Log
    public static void nlog(final String strTag,final String strInfo)
    {
//        Log.w(TAG,"javaScript: " + strTag + " : [" + strInfo + "]");
        Log.w(strTag,strInfo);
    }
    //單純傳字串
    public static void recvServerData(String message) throws UnsupportedEncodingException {
//        Log.w("ww ==",message);
        String encodeMessage1 = java.net.URLEncoder.encode(message,"UTF8");
        String encodeMessage2 = encodeMessage1.replace("+", "%20");
        final String sendMessage = "NativeConnect.onRecvServerData(\"" + encodeMessage2 + "\");";
//        Log.w(TAG,"sendMessage = " + sendMessage);
        Cocos2dxHelper.runOnGLThread(new Runnable() {
            @Override
            public void run() {
//                Log.w("ww sendMessage",sendMessage);
                Cocos2dxJavascriptJavaBridge.evalString(sendMessage);
            }
        });
    }
    //

    public static void onRecvRequireServerWtihCmd(String cmd, String response, String error, String cbName) throws UnsupportedEncodingException {
//        Log.w("ww","onRecvRequireServerWtihCmd");
//        Log.w("ww ===",response);
        // 因為Creator Cocos2dxJavascriptJavaBridge 不能含有特殊字元
        String encodeMessage1 = java.net.URLEncoder.encode(response,"UTF8");
        String encodeMessage2 = encodeMessage1.replace("+", "%20");
        final String sendMessage = "NativeConnect.onRecvRequireServer(\""+ cmd + "\",\"" + encodeMessage2 + "\",\""+ error+"\",\"" + cbName + "\");";
//        Log.w(TAG,"sendMessage = " + sendMessage);
        Cocos2dxHelper.runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(sendMessage);
            }
        });

    }

    public static void requireServerWithCmd(String cmd, String jsonS, int cbName) throws JSONException, ExecutionException, InterruptedException {
//        Log.w("ww","requireServerWithCmd");
        JSONArray arr = new JSONArray(jsonS);
        SignalRTool.requireServerWithCmd(cmd, jsonS, cbName);

    }

    public static void onRecvConnectionInfo(String state, String info){
//        Log.w("ww","onRecvConnectionInfo");
        final String sendMessage = "NativeConnect.onRecvConnectionInfo(\""+ state + "\",\"" + info + "\");";
        Cocos2dxHelper.runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(sendMessage);
            }
        });
    }
    // 視訊-----------
    public static void seekTo(int pos) {
        VideoAction action = new VideoAction();
        action.cmd = "seekTo";
        action.params.put("pos", Integer.toString(pos));
        KTextureView.addAction(action);
    }
    public static void sendVideoInfo(String cmd, String info){
        final String sendMessage = "NativeConnect.onVideoInfo(\""+ cmd + "\",\"" + info + "\");";
        Cocos2dxHelper.runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(sendMessage);
            }
        });
    }

    public static void playVideo(int x, int y, int width, int height, final String strPath, int clipW, int clipH, int offsetY, boolean isVideoMute) {
        float screenWidth = KTextureView.getScreenWidth();
        float screenHeight = KTextureView.getScreenHeight();
        float wFactor = screenWidth / (float)COCOS_VIEW_WIDTH;
        float hFactor = screenHeight / (float)COCOS_VIEW_HEIGHT;
        float _y = (float)(COCOS_VIEW_HEIGHT / 2 - y - height / 2 + (height-clipH)*0.5 );
        int videoW = (int)(wFactor * (float)width);
        int videoH = (int)(hFactor * (float)height);
        int videoY = (int)(_y * hFactor);

        VideoAction action = new VideoAction();
        action.cmd = "playVideo";
        action.params.put("viewY", Integer.toString(videoY));
        action.params.put("w", Integer.toString(videoW));
        action.params.put("h", Integer.toString(videoH));
        action.params.put("url", strPath);
        action.params.put("clipH", Integer.toString((int)(clipH*hFactor)));
        action.params.put("offsetY", Integer.toString((int)(offsetY*hFactor)));
        action.params.put("isVideoMute", Boolean.toString(isVideoMute));
        KTextureView.addAction(action);
    }

    public static void pauseVideo(){
        VideoAction action = new VideoAction();
        action.cmd = "pauseVideo";
        KTextureView.addAction(action);
    }

    public static void setVideoVisible(boolean isVisible) {
        VideoAction action = new VideoAction();
        action.cmd = "setVideoVisible";
        action.params.put("isVisible", Boolean.toString(isVisible));
        KTextureView.addAction(action);
    }

    public static void setVideoMute(boolean isVideoMute) {
        VideoAction action = new VideoAction();
        action.cmd = "setVideoMute";
        action.params.put("isVideoMute", Boolean.toString(isVideoMute));
        KTextureView.addAction(action);
    }

    public static void setVideoScale(boolean isScaleToBig, String tableID, float scale, float offsetX, float offsetY) {
        VideoAction action = new VideoAction();
        action.cmd = "setVideoScale";
        action.params.put("isScaleToBig", Boolean.toString(isScaleToBig));
        action.params.put("tableID", tableID);
        action.params.put("scale", Float.toString(scale));
        action.params.put("offsetX", Float.toString(offsetX*2));
        action.params.put("offsetY", Float.toString(offsetY*2));
        KTextureView.addAction(action);
    }

    public static void setVideoScale(boolean isScaleToBig, String tableID) {
        float scale;
        float offsetX;
        float offsetY;
        if(tableID.equals("A")) {
            scale = 2.0f;
            offsetX = 15.0f;
            offsetY = 100.0f;
        } else if(tableID.equals("B")) {
            scale = 3.5f;
            offsetX = 35.0f;
            offsetY = 190.0f;
        } else if(tableID.equals("C")) {
            scale = 2.0f;
            offsetX = 0.0f;
            offsetY = 75.0f;
        } else if(tableID.equals("D")) {
            scale = 1.8f;
            offsetX = 0.0f;
            offsetY = 100.0f;
        } else {
            scale = 1.3f;
            offsetX = 0.0f;
            offsetY = 22.5f;
        }
        JSConnect.setVideoScale(isScaleToBig, tableID, scale, offsetX, offsetY);
    }

    public static void replayVideo(){
        VideoAction action = new VideoAction();
        action.cmd = "replayVideo";
        KTextureView.addAction(action);
    }

    public static void continueVideo(){
        VideoAction action = new VideoAction();
        action.cmd = "continueVideo";
        KTextureView.addAction(action);
    }

    public static void stopVideo(){
        VideoAction action = new VideoAction();
        action.cmd = "stopVideo";
        KTextureView.addAction(action);
    }

    public static String getGameVersion(){
        return  getContext().getResources().getString(R.string.version);
    }

    public  static  String getCountry() {
        return  getContext().getResources().getString(R.string.country);
    }

    public  static  String getGamePlatform() {
        Log.w("Jun", "getGamePlatform " + getContext().getResources().getString(R.string.GamePlatform));
        return  getContext().getResources().getString(R.string.GamePlatform);
    }

    public static void openWeb(String url){
        Intent ie = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(ie);
    }
    public static void openWebInApp(float top, float down, float left, float right, String url){
        WebViewTool.enableWebView(url, top, down, left, right, "");
    }
    public static void openWebFullScreenInApp(String url,String title){
        WebViewTool.openWebFullScreen(url, title);
    }
    public static void closeWeb(){
        WebViewTool.disableWebView();
    }

    public static void exit(){
        System.exit(0);
    }


    public static void disconnect(){
        SignalRTool.disconnect();
    }

    public static void onReceiveIntent(Intent intent) {
        if(intent == null) return;
        if(mIsCocosCreatorRunning == false) {
            mIntent = intent;
            return;
        }

        Uri data = intent.getData();
        if(data == null) return;

        JSONObject json = new JSONObject();
        String jsonString = "";
        try {
            json.put("scheme", data.getScheme().toString());
            json.put("host", data.getHost());
            JSONObject queryList = new JSONObject();
            for (String itemKey : data.getQueryParameterNames()) {
                queryList.put(itemKey, data.getQueryParameter(itemKey));
            }
            json.put("queryList", queryList);

            jsonString = json.toString();
            // 因為Creator Cocos2dxJavascriptJavaBridge 不能含有特殊字元
            String encodeMessage1 = java.net.URLEncoder.encode(jsonString,"UTF8");
            jsonString = encodeMessage1.replace("+", "%20");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final String sendMessage = "NativeConnect.onRecvDeepLinkData(\"" + jsonString + "\");";
        Cocos2dxHelper.runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(sendMessage);
            }
        });
    }

    public static void setCocosCreatorRunning() {
        mIsCocosCreatorRunning = true;
        if(mIntent != null) {
            onReceiveIntent(mIntent);
            mIntent = null;
        }
    }

    public static void copyText(final String text) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ClipboardManager cmb = (ClipboardManager)mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                cmb.setPrimaryClip(ClipData.newPlainText(null, text));
            }
        });
    }

}
