package org.cocos2dx.javascript;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.util.List;

/**
 * Created by fan on 2017/3/8.
 */

public class IntentOtherApp {
    public static Cocos2dxActivity mActivity;
    public static List<ApplicationInfo> m_appList;

    public static void setupCurrentActivity(Cocos2dxActivity activity) {
        mActivity = activity;
    }

    public static void openLine(String url) {
        String appPackage = "jp.naver.line.android";
        String strURL = "";
        if (IntentOtherApp.checkLineInstalled(appPackage)){
            strURL = url;
        }else{
            strURL = "https://play.google.com/store/apps/details?id=jp.naver.line.android";
        }
        Intent ie = new Intent(Intent.ACTION_VIEW, Uri.parse(strURL));
        mActivity.startActivity(ie);
    }

    public static void openQQ(String company_id) {
        String appPackage = "com.tencent.mobileqqi";
        String strURL = "";
        Log.w("openQQ",String.valueOf(IntentOtherApp.checkLineInstalled(appPackage)));
        if (IntentOtherApp.checkLineInstalled(appPackage)){
            strURL = String.format("mqq://im/chat?chat_type=wpa&uin=%s&version=1&src_type=web", company_id);
        }else{
            strURL = "https://play.google.com/store/apps/details?id=com.tencent.mobileqqi";
        }
        Intent ie = new Intent(Intent.ACTION_VIEW, Uri.parse(strURL));
        mActivity.startActivity(ie);
    }

    public static void openFacebookMessager(String company_id) {
        String appPackage = "com.facebook.orca";
        String strURL = "";
        Log.w("openFacebookMessager",String.valueOf(IntentOtherApp.checkLineInstalled(appPackage)));
        if (IntentOtherApp.checkLineInstalled(appPackage)){
//            strURL = "https://www.facebook.com/messages/783834831702578";
            strURL = String.format("https://www.facebook.com/messages/%s", company_id);
        }else{
            strURL = "https://play.google.com/store/apps/details?id=com.facebook.orca";
        }
        Intent ie = new Intent(Intent.ACTION_VIEW, Uri.parse(strURL));
        mActivity.startActivity(ie);
    }

    private static boolean checkLineInstalled(String appPackage){
        PackageManager pm = mActivity.getPackageManager();
        m_appList = pm.getInstalledApplications(0);
        boolean lineInstallFlag = false;
        for (ApplicationInfo ai : m_appList) {
            if(ai.packageName.equals(appPackage)){
                lineInstallFlag = true;
                break;
            }
        }
        return lineInstallFlag;
    }




}
