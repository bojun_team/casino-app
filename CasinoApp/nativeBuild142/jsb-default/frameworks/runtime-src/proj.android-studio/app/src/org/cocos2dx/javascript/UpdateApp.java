package org.cocos2dx.javascript;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.io.File;

/**
 * Created by fan on 2017/3/8.
 */

public class UpdateApp {
    public static Cocos2dxActivity mActivity;
    private static long m_ldownloadId; // 下載的 id
    private static DownloadChangeObserver m_downloadObserver; // 下載狀況的觀察者
    private static Handler m_handler;
    private static ProgressDialog m_progressDialog = null;

    public static void setupCurrentActivity(Cocos2dxActivity activity) {
        mActivity = activity;
    }

    public static void UpdateAppDownload(String domainUrl)
    {
        // 產生顯示下載狀況的 Dialog
        createDownloadStatusDialog();

        Log.w("UpdateApp", domainUrl);
        Uri uri = Uri.parse(domainUrl);

        DownloadManager downloadManager = (DownloadManager) mActivity.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        // 设置允许使用的网络类型，这里是移动网络和wifi都可以
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
        // 设置是否允许漫游
        request.setAllowedOverRoaming(false);
        // 设置文件类型
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(domainUrl));
        request.setMimeType(mimeString);

        // 在通知栏中显示
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setTitle("download...");
        request.setVisibleInDownloadsUi(true);
        // sdcard目录下的download文件夹
        String fileName = domainUrl.substring(domainUrl.lastIndexOf('/') + 1,domainUrl.length());

        Log.w("UpdateApp", fileName);

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        // 将下载请求放入队列
        m_ldownloadId = downloadManager.enqueue(request);

        Log.w("UpdateApp", "after enqueue...");

        // 觀察下載狀況
        m_downloadObserver = new DownloadChangeObserver(m_handler);

        Log.w("UpdateApp", "after DownloadChangeObserver...");

        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        Log.w("UpdateApp", "after getExternalStoragePublicDirectory...");

        Uri downloadLocation = Uri.fromFile(dir);
        String strDownloadLocation = downloadLocation.toString() + "/";
        downloadLocation = Uri.parse(strDownloadLocation);

        Log.w("UpdateApp", downloadLocation.toString());
        // mActivity.getContentResolver().registerContentObserver(Uri.parse("content://downloads/my_downloads"),
        // true, m_downloadObserver);
        mActivity.getContentResolver().registerContentObserver(
                Uri.parse("content://downloads/my_downloads"), true,
                m_downloadObserver);

        Log.w("UpdateApp", "after registerContentObserver...");

        // 註冊下載完成的事件
        mActivity.registerReceiver(downloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        Log.w("UpdateApp", "after registerReceiver...");
    }

    private static BroadcastReceiver downloadCompleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /** 下载完成后安装APK **/
            installApk(context, intent);
        }
    };

    static class DownloadChangeObserver extends ContentObserver {
        public DownloadChangeObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            queryDownloadStatus();
        }
    };

    private static void queryDownloadStatus() {
        DownloadManager downloadManager = (DownloadManager) mActivity
                .getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(m_ldownloadId);
        Cursor c = downloadManager.query(query);
        if (c != null && c.moveToFirst()) {
            int status = c.getInt(c
                    .getColumnIndex(DownloadManager.COLUMN_STATUS));

            //int reasonIdx = c.getColumnIndex(DownloadManager.COLUMN_REASON);
            //int titleIdx = c.getColumnIndex(DownloadManager.COLUMN_TITLE);
            int fileSizeIdx = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
            int bytesDLIdx = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
            //String title = c.getString(titleIdx);
            int fileSize = c.getInt(fileSizeIdx);
            int bytesDL = c.getInt(bytesDLIdx);

            // Translate the pause reason to friendly text.
            //int reason = c.getInt(reasonIdx);
            StringBuilder sb = new StringBuilder();
            sb.append("Downloaded ").append(bytesDL).append(" / ").append(fileSize);

            // Display the status
            //Log.d("UpdateApp", sb.toString());
            switch (status) {
                case DownloadManager.STATUS_PAUSED:
                    Log.v("UpdateApp", "STATUS_PAUSED");
                    break;
                case DownloadManager.STATUS_PENDING:
                    Log.v("UpdateApp", "STATUS_PENDING");
                    break;
                case DownloadManager.STATUS_RUNNING:
                    // 正在下载，不做任何事情
                    updateDownloadProgress(bytesDL, fileSize);
                    Log.v("UpdateApp", "STATUS_RUNNING");
                    break;
                case DownloadManager.STATUS_SUCCESSFUL:
                    // 完成
                    Log.v("UpdateApp", "下載完成");
                    showFinishDialog( "提示", "離開應用程式" );
                    break;
                case DownloadManager.STATUS_FAILED:
                    // 清除已下载的内容，重新下载
                    Log.v("UpdateApp", "STATUS_FAILED");
                    showFinishDialog( "下載失敗", "請檢查網路，重新下載。" );
                    downloadManager.remove(m_ldownloadId);
                    break;
            }
        }
    };

    private static void installApk(Context context, Intent intent) {
        Log.w("UpdateApp", "installApk()");
        String action = intent.getAction();

        if (!(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action))) {
            Log.w("UpdateApp", "not ACTION_DOWNLOAD_COMPLETE");
            return;
        }

        DownloadManager downloadManager = (DownloadManager) mActivity
                .getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(m_ldownloadId);
        Cursor c = downloadManager.query(query);
        if (!(c.moveToFirst())) {
            Log.w("UpdateApp", "not moveToFirst");
            return;
        }

        String filePath;
        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
            filePath = c.getString(c
                    .getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
        } else {
            Log.w("UpdateApp", "not STATUS_SUCCESSFUL");
            return;
        }

        Intent i = new Intent(Intent.ACTION_VIEW);
        Log.w("UpdateApp", "installApk filePath");
        Log.w("UpdateApp", filePath);
        i.setDataAndType(Uri.parse("file://" + filePath),
                "application/vnd.android.package-archive");
        mActivity.startActivity(i);

        // 反註冊下載完成的事件
        mActivity.unregisterReceiver(downloadCompleteReceiver);
    };

    // 產生顯示下載狀況的 Dialog
    private static void createDownloadStatusDialog()
    {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( null == m_progressDialog )
                {
                    m_progressDialog = new ProgressDialog( mActivity );
                    m_progressDialog.setProgressStyle( ProgressDialog.STYLE_HORIZONTAL );
                    m_progressDialog.setCancelable( false );
                    m_progressDialog.setMessage( "更新中，請稍候。" );
                    m_progressDialog.show();
                }
            }
        });
    }

    private static void updateDownloadProgress( int iProgress, int iMax )
    {
        if( null == m_progressDialog )
        {
            return;
        }

        if( iMax <= 0 )
        {
            Log.w("UpdateApp", "updateDownloadProgress iMax <= 0");
            return;
        }

        if( iProgress > iMax )
        {
            Log.w("UpdateApp", " int iProgress, int iMax  iProgress > iMax");
            return;
        }

        m_progressDialog.setMax( iMax );
        m_progressDialog.setProgress( iProgress );
    };

    // 顯示下載完成或下載失敗的 dialog，按了 ok 以後會關閉 app
    private static void showFinishDialog(final String strTitle, final String strMessage)
    {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle(strTitle);
                builder.setMessage(strMessage);

                // 產生按鈕
                builder.setNeutralButton("離開", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        System.exit(0);
                    }
                });

                AlertDialog dialog = builder.show();

                // 標題置中
                TextView titleText = null;
                int titleID = mActivity.getResources().getIdentifier("alertTitle", "id", "android");
                if( 0 < titleID )
                {
                    titleText = (TextView)dialog.findViewById(titleID);
                }

                if( null != titleText )
                {
                    titleText.setGravity(Gravity.CENTER);
                }

                // 內容置中
                TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
                if( null != messageText )
                {
                    messageText.setGravity(Gravity.CENTER);
                }

                dialog.show();
            }
        });
    };
}
