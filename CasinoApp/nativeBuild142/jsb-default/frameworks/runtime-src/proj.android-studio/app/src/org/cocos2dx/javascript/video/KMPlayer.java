package org.cocos2dx.javascript.video;

import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import org.cocos2dx.javascript.JSConnect;
import org.cocos2dx.lib.Cocos2dxActivity;

import java.io.IOException;

import com.ksyun.media.player.IMediaPlayer;
import com.ksyun.media.player.IMediaPlayer.OnPreparedListener;
import com.ksyun.media.player.IMediaPlayer.OnInfoListener;
import com.ksyun.media.player.IMediaPlayer.OnErrorListener;
import com.ksyun.media.player.IMediaPlayer.OnVideoSizeChangedListener;
import com.ksyun.media.player.IMediaPlayer.OnSeekCompleteListener;
import com.ksyun.media.player.KSYMediaPlayer;
import com.ksyun.media.player.KSYMediaPlayer.KSYReloadMode;

/**
 * Created by ifalo-jun on 2017/5/17.
 * Use KSYUN Live Streaming player SDK v1.9.1
 */

public class KMPlayer {
    private static final String TAG = "KMPlayer";
    private static Cocos2dxActivity m_activity;
    private static KSYMediaPlayer m_mediaPlayer = null;
    private static ProgressBar m_progress = null;
    private static SurfaceView m_videoView = null;
    private static SurfaceHolder m_videoHolder;
    private static String m_videoPath;
    private static int m_viewY;
    private static int m_videoWidth;
    private static int m_videoHeight;
    private static int m_screenWidth;
    private static int m_screenHeight;
    private static boolean m_isMediaPlayerStart = false;
    private static boolean m_isVideoViewVisible  = false;


    // 初始化 video
    public static void setupCurrentActivity(Cocos2dxActivity activity) {
        m_activity = activity;
        calculateScreenSize(); // 計算螢幕大小
    }

    private static void calculateScreenSize(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            m_activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        else
            m_activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        m_screenWidth = displayMetrics.widthPixels;
        m_screenHeight = displayMetrics.heightPixels;
    }

    public static int getScreenHeight() {
        return m_screenHeight;
    }

    // 建立一個播放器，並設定監聽器
    private static void createMediaPlayer() {
        m_mediaPlayer = new KSYMediaPlayer.Builder(m_activity).build();
        m_mediaPlayer.setOnPreparedListener(onPreparedListener);
        m_mediaPlayer.setOnInfoListener(onInfoListener);
        m_mediaPlayer.setOnErrorListener(onErrorListener);
        m_mediaPlayer.setOnVideoSizeChangedListener(onVideoSizeChangedListener);
        m_mediaPlayer.setOnSeekCompleteListener(onSeekComplete);

        createVideoView(); // 建立一個用來播放影片的surface view
    }

    // 釋放播放器
    private static void releaseMediaPlayer() {
        // 先從m_activity.mFrameLayout移除view，再釋放播放器
        if(m_videoView != null) {
            m_activity.mFrameLayout.removeView(m_videoView);
            m_videoView = null;
        }

        if(m_mediaPlayer != null ){
            m_mediaPlayer.release();
            m_mediaPlayer = null;
        }
    }

    // 建立一個用來播放影片的surface view
    private static void createVideoView() {
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_videoView = new SurfaceView(m_activity);
                setVideoHolder(m_videoView); // 設定m_videoHolder，接收video view變化的資訊
                m_videoView.setBackgroundColor(Color.TRANSPARENT);
                m_activity.mFrameLayout.addView(m_videoView);

                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)m_videoView.getLayoutParams();
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                layoutParams.topMargin = m_viewY;
                layoutParams.width = m_videoWidth;
                layoutParams.height = m_videoHeight;
                m_videoView.setLayoutParams(layoutParams);

                m_activity.mFrameLayout.requestLayout();
                m_activity.mFrameLayout.invalidate();
            }
        });
    }

    // 設定m_videoHolder，接收video view變化的資訊
    private static void setVideoHolder(final SurfaceView view) {
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_videoHolder = view.getHolder();
                m_videoHolder.addCallback(videoHolderCallback);
                m_videoHolder.setFormat(PixelFormat.TRANSPARENT);
                m_videoHolder.setFixedSize(m_videoWidth, m_videoHeight);
            }
        });
    }

    // 视频渲染相关
    private static SurfaceHolder.Callback videoHolderCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if(m_mediaPlayer != null) {
                m_mediaPlayer.setDisplay(holder);
                // 设置在播放视频时是否保持屏幕常亮 必须在setDisplay(SurfaceHolder)之后调用方可生效
                m_mediaPlayer.setScreenOnWhilePlaying(true);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            m_activity.mFrameLayout.requestLayout();
            m_activity.mFrameLayout.invalidate();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if(m_mediaPlayer != null) {
                m_mediaPlayer.setDisplay(null);
            }
        }
    };

    // 播放影片
    public static void playVideo(final int centerY, final int w, final int h, String url) {
        Log.d(TAG,"playVideo [ " + centerY + ", " + w + ", " + h + ", " + url + " ]");
        m_isMediaPlayerStart = true;
        m_videoPath = url;
        m_viewY = centerY;
        m_videoWidth = w;
        m_videoHeight = h;
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(m_mediaPlayer != null) { // 如果播放器有被建立，先釋放掉舊的播放器
                    releaseMediaPlayer();
                }

                m_isVideoViewVisible = true;
                createMediaPlayer(); // 建立一個播放器，並設定監聽器
                updateProgressBar(false); // 更新讀取條

                try {
                    m_mediaPlayer.setDataSource(m_videoPath); // 設定影片網址
                    m_mediaPlayer.prepareAsync(); // 建立連接，讀取數據，創建解碼器等準備播放的工作
                } catch (IOException e) {
                    Log.d(TAG, "playVideo IOException ====");
                    e.printStackTrace();
                }
            }
        });
    }

    // 暫停播放
    public static void pauseVideo() {
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_mediaPlayer != null) {
                    if (m_mediaPlayer.isPlaying())
                        Log.d(TAG,"pauseVideo");
                    m_mediaPlayer.pause();
                    m_isVideoViewVisible = false;
                    updateProgressBar(false);
                }
            }
        });
    }

    // 繼續影片
    public static void continueVideo() {
        if(m_isMediaPlayerStart) {
            m_activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (m_mediaPlayer != null) {
                        Log.d(TAG,"continueVideo");
                        m_isVideoViewVisible = true;
                        m_mediaPlayer.start();
                        updateProgressBar(true);
                    }
                }
            });
        }
    }

    //重新播放
    public static void replayVideo() {
        if(m_isMediaPlayerStart) {
            Log.d(TAG,"replayVideo");
            playVideo(m_viewY,m_videoWidth,m_videoHeight,m_videoPath);
        }
    }

    // 停止影片
    public static void stopVideo() {
        m_isMediaPlayerStart = false;
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"stopVideo");
                if (m_mediaPlayer != null) {
                    m_mediaPlayer.stop();
                    releaseMediaPlayer();
                }
            }
        });
    }

    // 重讀影片
    private static void reloadVideo() {
        Log.d(TAG,"reloadVideo");
        KSYReloadMode mode = KSYReloadMode.KSY_RELOAD_MODE_ACCURATE; // 精確模式
        m_mediaPlayer.reload(m_videoPath, true, mode);
    }

    // m_mediaPlayer在準備完成，可以播放時會發出onPrepared
    private static OnPreparedListener onPreparedListener = new OnPreparedListener() {
        @Override
        public void onPrepared(IMediaPlayer mp) {
            // scalingMode 共有三種，目前選擇第三種
            // VIDEO_SCALING_MODE_NOSCALE_TO_FIT 拉伸模式，視頻完全填滿顯示窗口，畫面會有變形
            // VIDEO_SCALING_MODE_SCALE_TO_FIT 填充模式，在視頻寬高比例與手機寬高比例不一致時會有黑邊
            // VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING 裁剪模式(不會有黑邊)
            int scalingMode = KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
            m_mediaPlayer.setVideoScalingMode(scalingMode);
            m_mediaPlayer.start(); // 開始播放視頻
            updateProgressBar(true); // 更新讀取條
            sendVideoInfoToJS("didPrepared",""); //通知 creator 影片已準備好
            Log.d(TAG, "onPrepared");
        }
    };

    // 消息監聽器，會將關於播放器的消息通過此回調接口，例如：視頻渲染，音頻渲染等
    private static OnInfoListener onInfoListener = new OnInfoListener() {
        @Override
        public boolean onInfo(IMediaPlayer mp, int what, int extra) {
            switch (what) {
                case KSYMediaPlayer.MEDIA_INFO_SOFTWARE_DECODE: // 播放器使用軟解
                    Log.d(TAG, "onInfo = [ " + "播放器使用軟解" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_SUGGEST_RELOAD: // 建議調用 reload 接口
                    Log.d(TAG, "onInfo = [ " + "建議調用 reload 接口" + " " + extra + " ]");
                    reloadVideo(); // 重讀影片
                    break;
                case KSYMediaPlayer.MEDIA_INFO_BUFFERING_START: // 緩存數據開始
                    Log.d(TAG, "onInfo = [ " + "緩存數據開始" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_BUFFERING_END: // 緩存數據
                    Log.d(TAG, "onInfo = [ " + "緩存數據結束" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: // 視頻開始渲染
                    Log.d(TAG, "onInfo = [ " + "視頻開始渲染" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_INFO_RELOADED: // reload成功
                    Log.d(TAG, "onInfo = [ " + "reload成功" + " " + extra + " ]");
                    int scalingMode = KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
                    m_mediaPlayer.setVideoScalingMode(scalingMode);
                    m_mediaPlayer.start(); // 開始播放視頻
                    updateProgressBar(true); // 更新讀取條
                    sendVideoInfoToJS("didPrepared",""); //通知 creator 影片已準備好
                    break;
                case KSYMediaPlayer.MEDIA_INFO_NOT_SEEKABLE: // 該多媒體文件不可快進/快退
                    Log.d(TAG, "onInfo = [ " + "該多媒體文件不可快進/快退" + " " + extra + " ]");
                    break;
                default:
                    Log.d(TAG, "onInfo = [ " + what + " " + extra + " ]");
            }
            return true;
        }
    };

    // 錯誤監聽器，播放器遇到錯誤時會將相應的錯誤碼通過此回調接口
    private static OnErrorListener onErrorListener = new OnErrorListener() {
        @Override
        public boolean onError(IMediaPlayer mp, int what, int extra) {
            switch (what) {
                case KSYMediaPlayer.MEDIA_ERROR_CONNECT_SERVER_FAILED: // 連接服務器失敗
                    Log.d(TAG, "onError = [" + "連接服務器失敗" + " " + extra + " ]");
                    break;
                case KSYMediaPlayer.MEDIA_ERROR_IO: // 網絡超時
                    Log.d(TAG, "onError = [" + "網絡超時" + " " + extra + " ]");
                    break;
                default:
                    Log.d(TAG, "onError = [" + what + " " + extra + " ]");
            }
            try {
                // wait for 3 seconds
                Thread.sleep(3000);
            } catch (Exception ex) {
                Log.d(TAG, "onError = [ " + ex.getMessage() + " ]");
                ex.printStackTrace();
            }
            reloadVideo(); // 重讀影片
            return true;
        }
    };

    // 視頻的寬高發生變化時會有相應的回調
    private static OnVideoSizeChangedListener onVideoSizeChangedListener = new OnVideoSizeChangedListener() {
        @Override
        public void onVideoSizeChanged(IMediaPlayer mp, int width, int height, int sar_num, int sar_den) {
            if (mp != null) {
                // Update Real Video Aspect Ratio
                m_activity.mFrameLayout.requestLayout();
                m_activity.mFrameLayout.invalidate();
            }
        }
    };

    // 在seek成功後會發出此回調
    private static OnSeekCompleteListener onSeekComplete = new OnSeekCompleteListener() {
        @Override
        public void onSeekComplete(IMediaPlayer mp) {
            m_activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onSeekComplete");
                }
            });
        }
    };

    // 創造一個讀取條
    private static void createProgressBar() {
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_progress = new ProgressBar(m_activity, null, android.R.attr.progressBarStyle);
                m_activity.mFrameLayout.addView(m_progress);
                FrameLayout.LayoutParams barLp = (FrameLayout.LayoutParams) m_progress.getLayoutParams();
                barLp.gravity = Gravity.CENTER_HORIZONTAL;
                barLp.topMargin = m_viewY - 100 / 2 + m_videoHeight / 2;
                barLp.width = 100;
                barLp.height = 100;
                m_progress.setLayoutParams(barLp);
            }
        });
    }

    // 更新讀取條
    private static void updateProgressBar(boolean needReload) {
        // 如果 m_progress 沒有被建立
        if(m_progress == null) {
            createProgressBar(); // 創造一個讀取條
        }

        // 讀取條顯示在最上層
        m_activity.mFrameLayout.bringChildToFront(m_progress);

        // 如果 videoView 沒有被建立，則隱藏讀取條
        if(m_videoView == null) {
            if (m_progress != null) m_progress.setVisibility(View.INVISIBLE);
            return;
        }

        // 如果不顯示影片，則隱藏讀取條
        if(!m_isVideoViewVisible) {
            m_videoView.setBackgroundColor(Color.BLACK);
            if (m_progress != null) m_progress.setVisibility(View.INVISIBLE);
            return;
        }

        // 如果開始播放影片，則隱藏讀取條
        if(m_mediaPlayer.isPlaying()) {
            m_videoView.setBackgroundColor(Color.TRANSPARENT);
            if (m_progress != null) m_progress.setVisibility(View.INVISIBLE);
        }
        // 顯示讀取條
        else {
            if(needReload) reloadVideo(); // 當m_mediaPlayer.start()後，沒有成功播放，則重讀影片
            if (m_progress != null) m_progress.setVisibility(View.VISIBLE);
        }
    }

    public static void setVideoVisible(final boolean isVisible) {
        m_isVideoViewVisible = isVisible;
    }

    // 傳 RTMP Video 資訊到 JS 層
    private static void sendVideoInfoToJS(final String strInfoName, final String strInfoData) {
        JSConnect.sendVideoInfo(strInfoName,strInfoData);
    }

    public static void seekTo(long pos) {
        Log.d(TAG,"Video seekTo :" + pos);
        m_mediaPlayer.seekTo(pos, true);
    }
}
