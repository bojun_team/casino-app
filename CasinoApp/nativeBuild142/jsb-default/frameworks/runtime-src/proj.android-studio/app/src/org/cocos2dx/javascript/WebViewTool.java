package org.cocos2dx.javascript;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.http.SslError;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.cocos2d.CasinoApp.R;
import org.cocos2dx.lib.Cocos2dxActivity;
/**
 * Created by Bean on 2018/1/2.
 */

public class WebViewTool {
    private static Cocos2dxActivity m_activity = null;
    private static PopupWindow m_popupWindow = null;
    private static WebView m_webview = null;
    private static float m_screenWidth = 0;
    private static float m_screenHeight = 0;
    private static boolean m_isOpenWebView = false;
    public static void openWebFullScreen (final String url, final String title) {
        if(m_isOpenWebView != false) return;
        m_isOpenWebView = true;
        WebViewTool.enableWebView(url, m_screenHeight / 8, 0, 0, 0, title);
    }
    public static void enableWebView (final String url, final float top, final float down, final float left, final float right,final String title){
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_webview = new WebView(m_activity);
                m_webview.getSettings().setJavaScriptEnabled(true);
                m_webview.getSettings().setLoadWithOverviewMode(true);
                m_webview.getSettings().setUseWideViewPort(true);
                m_webview.getSettings().setBuiltInZoomControls(false);
                m_webview.getSettings().setSupportZoom(true);
                m_webview.setBackgroundColor(Color.WHITE);
                m_webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Log.w("BEan", "shouldOverrideUrlLoading: ");
                        view.loadUrl(url);
                        return true;
                    }

                    @Override
                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                        super.onReceivedError(view, errorCode, description, failingUrl);
                        Log.w("BEan", "onReceivedError: ");
                    }

                    @Override
                    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                        super.onReceivedSslError(view, handler, error);
                        Log.w("BEan", "onReceivedSslError: ");
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        Log.w("BEan", "onPageFinished: ");
                        super.onPageFinished(view, url);
                    }
                });
                DisplayMetrics dm = new DisplayMetrics();
                m_activity.getWindowManager().getDefaultDisplay().getRealMetrics(dm);

                float widthVar = m_screenWidth / 1080;
                float heightVar = m_screenHeight / 1920;
                float webY = top * heightVar;
                float webX = left * widthVar;
                float webWidth = m_screenWidth - right * widthVar - webX;
                float webHeight = m_screenHeight - down * heightVar - webY * 2;
                LayoutParams webLayoutParams = getFrameLayoutParams((int)webX, (int)webY, (int)webWidth, (int)webHeight);
                m_webview.setLayoutParams(webLayoutParams);
                m_webview.loadUrl(url);


                View view = new View(m_activity);
                view.setBackgroundColor(Color.BLACK);
                int viewWidth = (int)webWidth;
                int viewHeight = (int)m_screenHeight;
                LayoutParams viewLayoutParams = getFrameLayoutParams(0, 0, viewWidth, viewHeight);
                view.setLayoutParams(viewLayoutParams);

                ImageView imageView = new ImageView(m_activity);
                imageView.setImageResource(R.drawable.back);
                imageView.setScaleX(0.5f);
                imageView.setScaleY(0.5f);
                int imgWidth = (int)webY;
                int imgHidth = imgWidth;
                int imgX = (int)(webWidth - imgWidth);
                LayoutParams imgLayoutParams = getFrameLayoutParams(imgX, 0, imgWidth, imgHidth);
                imageView.setLayoutParams(imgLayoutParams);

                Button closeButton = new Button(m_activity);
                closeButton.setBackgroundColor(Color.BLACK);
                int btnWidth = (int)webY;
                int btnHidth = btnWidth;
                int btnX = (int)(webWidth - btnWidth);
                LayoutParams closeBtnLayoutParams = getFrameLayoutParams(btnX, 0, btnWidth, btnHidth);
                closeButton.setLayoutParams(closeBtnLayoutParams);

                TextView textView = new TextView(m_activity);
                LayoutParams textLayoutParams = getFrameLayoutParams(0, 0, viewWidth, btnHidth);
                textView.setLayoutParams(textLayoutParams);
                textView.setText(title);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25f);
                textView.setTextColor(Color.parseColor("#E5B200"));

                FrameLayout baseLayout = new FrameLayout(m_activity);
                baseLayout.addView(view);
                baseLayout.addView(closeButton);
                baseLayout.addView(imageView);
                baseLayout.addView(m_webview);
                baseLayout.addView(textView);


                m_popupWindow = new PopupWindow(baseLayout);
                m_popupWindow.setWidth((int)m_screenWidth);
                m_popupWindow.setHeight((int)m_screenHeight);
                m_popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
                m_popupWindow.setOutsideTouchable(false);
                m_popupWindow.setFocusable(true);
                m_popupWindow.setTouchable(true);
                m_popupWindow.showAtLocation(m_activity.getGLSurfaceView(),Gravity.TOP, 0, 0);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (m_popupWindow != null) {
                            m_popupWindow.dismiss();
                            m_popupWindow = null;
                            m_isOpenWebView = false;
                        }
                    }
                });
            }

            public LayoutParams getFrameLayoutParams(int x, int y, int w, int h){
                LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.TOP;
                layoutParams.leftMargin = x;
                layoutParams.topMargin = y;
                layoutParams.width = w;
                layoutParams.height = h;
                return layoutParams;
            }

        });
    }

    public static void disableWebView (){
        if (m_popupWindow == null) return;
        m_activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_popupWindow != null) {
                    m_popupWindow.dismiss();
                    m_popupWindow = null;
                    m_isOpenWebView = false;
                }
            }
        });
    }
    public static void setupCurrentActivity(Cocos2dxActivity activity) {
        m_activity = activity;
        calculateScreenSize(); // 計算螢幕大小
    }
    private static void calculateScreenSize(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            m_activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        else
            m_activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        m_screenWidth = displayMetrics.widthPixels;
        m_screenHeight = displayMetrics.heightPixels;
    }
}
