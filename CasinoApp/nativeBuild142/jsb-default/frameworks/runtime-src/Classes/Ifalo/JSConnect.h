//
//  JSConnect.h
//  CasinoApp
//
//  Created by Bean on 2017/1/23.
//
//



@interface JSConnect : NSObject
+ (JSConnect *)sharedJSConnect;
+ (void)nlogTag:(NSString *)strTag info:(NSString *)strInfo;
//-------------signal R
+ (void)recvServerData:(NSString*)json;
+ (void)onRecvRequireServerWtihCmd:(NSString*)cmd data:(NSString*)data error:(NSString*)errorMsg callBackName:(NSString*)cbName;
+ (void)sendConnectInfoWithState:(NSString*)state info:(NSString*)info;
//-------------視訊
+ (void)playVideoWithX:(NSNumber*)x y:(NSNumber*)y w:(NSNumber*)w h:(NSNumber*)h url:(NSString*)url clipW:(NSNumber*)clipW clipH:(NSNumber*)clipH offsetY:(NSNumber*)offsetY isVideoMute:(BOOL)isVideoMute;
+ (void)stopVideo;
+ (void)setVideoVisible:(BOOL)isVisible;
+ (void)setVideoScaleWithIsScale:(BOOL)isScaleToBig tableID:(NSString*)tableID;
+ (void)setVideoScaleWithIsScale:(BOOL)isScaleToBig tableID:(NSString*)tableID scale:(NSNumber*)scale offsetX:(NSNumber*)offsetX offsetY:(NSNumber*)offsetY;
+ (void)setVideoMute:(BOOL)isVideoMute;
+ (void)pauseVideo;
+ (void)continueVideo;
+ (void)sendVideoDataWithCmd:(NSString*)cmd andInfo:(NSString*)info ;
//-------------Web
+ (void)openWebWithTop:(NSNumber*)top
                  down:(NSNumber*)down
                  left:(NSNumber*)left
                 right:(NSNumber*)right
                   url:(NSString*)url;
+ (void)openWebWithX:(NSNumber*)x y:(NSNumber*)y w:(NSNumber*)w h:(NSNumber*)h url:(NSString*)url;
+ (void)closeWeb;
+ (void)openWebOnOtherApp:(NSString*)url;
+ (void)openWebFullScreenWithUrl:(NSString*)url title:(NSString*)title;
//----------------
+ (void)updataAppWithURL:(NSString*)url;
+ (NSString*)getGameVersion;
+ (NSString*)getCountry;
+ (NSString*)getGamePlatform;
+ (void)onRecvDeepLinkData:(NSURL *)url;
+ (void)exit;
+ (void)setCocosCreatorRunning;
@end
