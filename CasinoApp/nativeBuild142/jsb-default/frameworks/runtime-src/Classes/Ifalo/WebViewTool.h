//
//  WebViewTool.h
//  HoldemPokerApp
//
//  Created by Bean on 2016/11/16.
//
//

#import <Foundation/Foundation.h>

@interface WebViewTool : NSObject <UIWebViewDelegate>
{
    UIView* m_rootView;
    UIWebView* m_webView;
    UIView* m_fullScreenView;
}


+ (WebViewTool *)sharedWebView;
+ (void)openSafariWithURL:(NSString*)url;
+ (void)enableWebViewWithCreatorTop:(NSNumber*)top
                               down:(NSNumber*)down
                               left:(NSNumber*)left
                              right:(NSNumber*)right
                                url:(NSString*)url;
+ (void)enableWebViewWithX:(NSNumber*)x y:(NSNumber*)y width:(NSNumber*)w
                   height:(NSNumber*)h url:(NSString*)url;
+ (void)enableFullScreenWebViewWithUrl:(NSString*)url title:(NSString*)title;
+ (void)disableWebView;

- (void)setRootView:(UIView*)rootView;
@end
