//
//  AppUpdataTool.m
//  CasinoApp
//
//  Created by Bean on 2017/2/10.
//
//

#import "AppUpdataTool.h"

@implementation AppUpdataTool
static AppUpdataTool* _appUpdataTool = nil;
+ (AppUpdataTool *)sharedAppUpdataTool
{
    if (!_appUpdataTool) {
        
        if( [ [AppUpdataTool class] isEqual:[self class]] )
            _appUpdataTool = [[AppUpdataTool alloc] init];
        else
            _appUpdataTool = [[self alloc] init];
    }
    return _appUpdataTool;
}
- (void)dealloc{
    [super dealloc];
    [m_rootView release];
}
//-----------------------------public function-------------------------
- (void)setRootView:(UIView*)rootView
{
    m_rootView = [rootView retain];
}
- (void)updataAppWithURL:(NSString*)url{
    //轉成ios的大小
    CGRect rect = CGRectMake(1, 1, 1, 1);
    //做webView
    UIWebView* webView = [[[UIWebView alloc] initWithFrame:rect] autorelease];
    webView.center = CGPointMake(rect.size.width / 2, rect.size.height / 2);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [webView loadRequest:request];
    [m_rootView addSubview:webView];
    m_webView = webView;
    [m_webView setDelegate:self];
}
//-----------------------------delegate-------------------------
- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    NSLog(@"error: OpenWebView = %d", (int)[error code]);
    
    NSString* errMsg = [NSString stringWithFormat:@"更新失敗…請聯絡客服人員。Error Code:%d", (int)[error code]];
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:errMsg
                                                   delegate:self
                                          cancelButtonTitle:@"離開"
                                          otherButtonTitles:nil];
    [alert show];
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"Start");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Finish");
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:@"離開應用程式"
                                                   delegate:self
                                          cancelButtonTitle:@"離開"
                                          otherButtonTitles:nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    exit(0);
}

@end
