//
//  AppUpdataTool.h
//  CasinoApp
//
//  Created by Bean on 2017/2/10.
//
//

#import <Foundation/Foundation.h>

@interface AppUpdataTool : NSObject<UIWebViewDelegate>
{
    UIView* m_rootView;
    UIWebView* m_webView;
}
+ (AppUpdataTool *)sharedAppUpdataTool;
- (void)setRootView:(UIView*)rootView;
- (void)updataAppWithURL:(NSString*)url;
@end
