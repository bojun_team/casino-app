import Rx from 'rxjs' 
export default class Pokers {
    constructor () { 
        this._sequence = 0
        this._sequence$ = new Rx.BehaviorSubject(this._sequence)
        this._pokers = ["","","","","",""]
        this._pokers$ = [
            new Rx.BehaviorSubject(this._pokers[0]),
            new Rx.BehaviorSubject(this._pokers[1]),
            new Rx.BehaviorSubject(this._pokers[2]),
            new Rx.BehaviorSubject(this._pokers[3]),
            new Rx.BehaviorSubject(this._pokers[4]),
            new Rx.BehaviorSubject(this._pokers[5]),
        ]
    }
    setPoker(index, value){
        this.getPoker(index).next(value)
        this._pokers[index] = value
    }
    getPoker$(index){
        return this._pokers$[index]
    }
    
    setSequence(value){
        this.sequence$.next(value)
        this._sequence = value
    }
    get sequence(){
        return this._sequence
    }
    getSequence$(){
        return this._sequence$
    }
}