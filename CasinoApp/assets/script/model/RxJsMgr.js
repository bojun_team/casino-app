import Rx from 'rxjs' 
import Config from '../constants/Config'
import SignalR from '../native/SignalR'

/*
    功能描述：用來做遊戲中目前的UI溝通
    實作內容：
        1 建立UI的參數並使其可以被訂閱及修改
*/ 
class LobbyUIState {
    constructor () { 
        this._propChange$ = {
            isHideLoading: new Rx.BehaviorSubject(false), 
            loadingBarProgress: new Rx.BehaviorSubject(0), 
        }
    } 
    reset() {
        this.isHideLoading = false
        this.loadingBarProgress = 0
    }
    get propChange$() {
        return this._propChange$ 
    }  
    get isHideLoading () {
        return this._propChange$.isHideLoading.getValue()
    }
    set isHideLoading (isHideLoading) {
        this._propChange$.isHideLoading.next(isHideLoading)
    } 
    get loadingBarProgress(){
        return this._propChange$.loadingBarProgress.getValue()
    }
    set loadingBarProgress (loadingBarProgress) {
        this._propChange$.loadingBarProgress.next(loadingBarProgress)
    }    
}

/*
    功能描述：用來做遊戲中目前的UI溝通
    實作內容：
        1 建立UI的參數並使其可以被訂閱及修改
*/ 

var _videoButtonCallback = {
    onBigBtnClickEvent(){},
    onSmallBtnClickEvent(){},
    oncloseBtnClickEvent(){},
    onReflashBtnClickEvent(){},
    onCloseNodeClickEvent(){},
}

class GameUIState {
    constructor () {
        this._propChange$ = {
            isHideChipPicker: new Rx.BehaviorSubject(true),
            isHideLimitPicker: new Rx.BehaviorSubject(true),
            isHideLoading: new Rx.BehaviorSubject(false), 
            isHideVideoButtonPannel: new Rx.BehaviorSubject(true),
            loadingBarProgress: new Rx.BehaviorSubject(0), 
        }
    }
    reset() {
        this.isHideChipPicker = true
        this.isHideLimitPicker = true
        this.isHideLoading = false
        this.isHideVideoButtonPannel = true
        this.loadingBarProgress = 0
    }
    get propChange$() {
        return this._propChange$
    }
    get isHideChipPicker () {
        return this._propChange$.isHideChipPicker.getValue()
    }
    set isHideChipPicker (isHideChipPicker) {  
        this._propChange$.isHideChipPicker.next(isHideChipPicker)
    }
    get isHideLimitPicker () {
        return this._propChange$.isHideLimitPicker.getValue()
    }
    set isHideLimitPicker (isHideLimitPicker) {  
        this._propChange$.isHideLimitPicker.next(isHideLimitPicker)
    }
    get isHideLoading () {
        return this._propChange$.isHideLoading.getValue()
    }
    set isHideLoading (isHideLoading) {  
        this._propChange$.isHideLoading.next(isHideLoading)
    }
    get loadingBarProgress(){
        return this._propChange$.loadingBarProgress.getValue()
    }
    set loadingBarProgress (loadingBarProgress) {    
        this._propChange$.loadingBarProgress.next(loadingBarProgress)
    }
    get isHideVideoButtonPannel() {
        return this._propChange$.isHideVideoButtonPannel.getValue()
    }
    set isHideVideoButtonPannel(isHideVideoButtonPannel) {
        this._propChange$.isHideVideoButtonPannel.next(isHideVideoButtonPannel)
    }
    get videoButtonCallback() {
        return _videoButtonCallback
    }
}

class ConnectData {
    constructor () {  
        this._connectState = Config.ConnectState.DISCONNECTED
        this._info = "" 
        this._propChange$ = {
            connectState: new Rx.BehaviorSubject(this._connectState), 
        }
        SignalR.listenRecvConnectInfo(function(state, info){
            this.connectState = state
            this._info = info
        }.bind(this))
    } 
    reset() {
        SignalR.disconnect()
        this._connectState = Config.ConnectState.DISCONNECTED
        this._info = ""
        SignalR.listenRecvConnectInfo(function(state, info){
            this.connectState = state
            this._info = info
        }.bind(this))
    }
    get connectState (){
        return this._connectState
    }
    set connectState (connectState) {
        this._connectState = connectState 
        this._propChange$.connectState.next(Number(connectState))
    }
    get propChange$() {
        return this._propChange$
    }

    disconnect(fun){
        SignalR.listenRecvConnectInfo(null)
        SignalR.disconnect()
    }
}

const lobbyUIState = new LobbyUIState()
const gameUIState = new GameUIState()
const connectData = new ConnectData()
var RxJsMgr = {
    get lobbyUIState() {
        return lobbyUIState
    },
    get gameUIState() {
        return gameUIState
    },
    get connectData() {
        return connectData
    }
}

module.exports = RxJsMgr
