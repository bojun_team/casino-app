import Rx from 'rxjs'
import WebApiTool from '../tool/WebApiTool'
import Tool from "../tool/Tool"
import DebugFunction from "../debugPanel/DebugFunction"
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import RxJsMgr from '../model/RxJsMgr'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'
import JackpotAlert from '../tool/JackpotAlert'
import JackpotWinView from '../tool/JackpotWinView'

export const MAX_CHIPS_NUM = 3

const ACCEPT_JACKPOT_BIG_WIN_RESULT = {
    ErrorCode:0, // 領取BigWin獎金錯誤碼
    ContinueWin:0, // 連贏累積次數
    Kind:0, // 中獎種類
    WinAmount:0, // 中獎金額
    TotalBetAmount:0 // 當前累積的下注金額(已結算後)
}

export default class User {
    constructor () { 
        this.is5RunNobet = false 
        this.is5MinuteNobet = false
        this.inBackGroundDisConnect = false
 
        this.isFakeLogin = false
        //讀取玩家基本資料 
        this.companyId = Config.CompanyId.Pharaoh //預設法老王
        this.isAutoLogin = false // 是否為自動登入  
        this.userPassId = '' // 用來判斷玩家身份的ID
        this.userToken = '' // 法老版用來登入遊戲伺服器 (signalR login)
        this.cashType = Config.CashType.CREDIT  // CashType 1:信用(CREDIT) 2:現金(CASH)
        this.comingMaintainMessage = '' //維護偵測訊息
         
        // 讀取存檔資料 
        let musicEnabled = (Tool.loadData(Config.saveAudio.music, "true") == "true") ? true : false
        let effectEnabled = (Tool.loadData(Config.saveAudio.effect, "true") == "true") ? true : false
        let videoEnabled = (Tool.loadData(Config.saveName.videoEnable, "true") == "true") ? true : false
        let dToken = String(Tool.loadData(Config.saveName.dToken, '')) // 用來當作串接版永久登入用的參數
 
        // Jackpot 累積資訊 
        let jackpotWinInfo = { 
            continueWin: 0, // 連贏累積次數 
            kind: Config.JackpotKind.NON_JACKPOT , // 中獎種類 
            winAmount: 0, // 中獎金額 
            totalBetAmount: 0, // 當前累積的下注金額(已結算後) 
            nextBigWinRatio: 0, // 下一個BigWin倍率 (非可已領取狀態下均為0.0) 
            baseBigWinAmount: 0, // 首局達BigWin時取的jp金額 
            expectedWinAmount: 0, // 預期該局會贏的jp金額 
            nextWinExpectedWinAmount: 0 
        } 
  
        this._propChange$ = { 
            username: new Rx.BehaviorSubject(''),
            password: new Rx.BehaviorSubject(''),
            dToken: new Rx.BehaviorSubject(dToken),
            usercredit: new Rx.BehaviorSubject(0),
            isOpenSetting: new Rx.BehaviorSubject(false),
            kickUser: new Rx.BehaviorSubject(Config.kickState.default),
            restoreCredit: new Rx.BehaviorSubject(0),
            comingMaintainCount:new Rx.BehaviorSubject(0),
            isRecvCashType:new Rx.BehaviorSubject(false),
            isReflashVideo:new Rx.BehaviorSubject(false), 
            musicEnabled:new Rx.BehaviorSubject(musicEnabled),
            effectEnabled:new Rx.BehaviorSubject(effectEnabled),
            videoEnabled:new Rx.BehaviorSubject(videoEnabled),
            // Jackpot 目前累積金額 (note 每3秒會推送一次)
            jpPoolAmount:new Rx.BehaviorSubject(0),
            jackpotWinInfo:new Rx.BehaviorSubject(jackpotWinInfo),
            idleRunNeverBet: new Rx.BehaviorSubject(0), // 未下注局數
        } 
        this.recvServerDataEvtId = NativeConnect.listenRecvServerData((data) => {
            if (data.M == "getUserCredit") { 
                this.usercredit = Number(data.A[0]["UserCredit"].Casino) 
                this.userPassId = String(data.A[0]["UserPassId"])
                this.setCashType(data.A[0].CashType)
            }
            else if (data.M == "updateUserCredit") { 
                this.usercredit = Number(data.A[0].RemainCredit) 
            }
            else if (data.M == "kickUser"){
                this.kickUser = data.A[0] 
            }
            else if (data.M == "restoreCredit"){ 
                this.restoreCredit = data.A[0]
            } 
            else if (data.M == "idleRunNeverBet") {
                this._propChange$.idleRunNeverBet.next(data.A[0])
            }
            else if (data.M == "comingMaintain"){
                this.comingMaintainMessage = String(data.A[0]["Message"])
                let count = this._propChange$.comingMaintainCount.getValue() + 1
                this._propChange$.comingMaintainCount.next(count)
            }
            else if (data.M == 'updateJackpotPoolAmount') {
                this.jpPoolAmount = data.A[0]
            }
            else if (data.M == 'updateJackpotInfo') {
                this.updateJackpotInfo(
                    data.A[0].ContinueWin, 
                    data.A[0].Kind,
                    Math.floor(data.A[0].WinAmount),
                    data.A[0].TotalBetAmount,
                    data.A[0].NextBigWinRatio,
                    Math.floor(data.A[0].BaseBigWinAmount),
                    Math.floor(data.A[0].ExpectedWinAmount),
                    Math.floor(data.A[0].NextWinExpectedWinAmount)
                )
            }
        })
    }

    // ==================================================
    // Get property change observable stream
    get propChange$() {
        return this._propChange$
    } 
    get username () { 
        return this._propChange$.username.getValue() 
    } 
    set username (username) { 
        this._propChange$.username.next(username) 
    } 
    get password () { 
        return this._propChange$.password.getValue() 
    } 
    set password (password) { 
        this._propChange$.password.next(password) 
    } 
    get dToken() { 
        return this._propChange$.dToken.getValue() 
    } 
    set dToken(dToken) { 
        this._propChange$.dToken.next(dToken)
        if(this.isFakeLogin == true) {
            // 清除記憶登入訊息
            Tool.saveData(Config.saveName.dToken, "");
        } else {
            Tool.saveData(Config.saveName.dToken, dToken) 
        }
    } 
    get usercredit () { 
        return this._propChange$.usercredit.getValue() 
    } 
    set usercredit (usercredit) { 
        this._propChange$.usercredit.next(usercredit) 
    }  
    get isOpenSetting () { 
        return this._propChange$.isOpenSetting.getValue() 
    } 
    set isOpenSetting (isOpenSetting) { 
        this._propChange$.isOpenSetting.next(isOpenSetting) 
    } 
    get kickUser(){ 
        return this._propChange$.kickUser.getValue() 
    } 
    set kickUser(kickUser) {
        this._propChange$.kickUser.next(kickUser) 
    }  
    get restoreCredit () {
        return this._propChange$.restoreCredit.getValue() 
    } 
    set restoreCredit(restoreCredit) {
        this._propChange$.restoreCredit.next(restoreCredit) 
    } 
    get isRecvCashType() { 
        return this._propChange$.isRecvCashType.getValue() 
    } 
    get isReflashVideo() { 
        return this._propChange$.isReflashVideo.getValue() 
    } 
    set isReflashVideo(isReflashVideo) { 
        this._propChange$.isReflashVideo.next(isReflashVideo) 
    } 
    get musicEnabled()  { 
        return this._propChange$.musicEnabled.getValue() 
    }  
    set musicEnabled(musicEnabled) { 
        Tool.saveData(Config.saveAudio.music, String(musicEnabled)) 
        this._propChange$.musicEnabled.next(musicEnabled) 
    }  
    get effectEnabled() { 
        return this._propChange$.effectEnabled.getValue() 
    }  
    set effectEnabled(effectEnabled) { 
        Tool.saveData(Config.saveAudio.effect, String(effectEnabled)) 
        this._propChange$.effectEnabled.next(effectEnabled)         
    }  
    get videoEnabled() { 
        return this._propChange$.videoEnabled.getValue() 
    } 
    set videoEnabled(videoEnabled) { 
        Tool.saveData(Config.saveName.videoEnable, String(videoEnabled)) 
        this._propChange$.videoEnabled.next(videoEnabled) 
    } 
    get jpPoolAmount() { 
        return this._propChange$.jpPoolAmount.getValue() 
    }  
    set jpPoolAmount(jpPoolAmount) { 
        this._propChange$.jpPoolAmount.next(jpPoolAmount) 
    } 
    get jackpotWinInfo() { 
        return this._propChange$.jackpotWinInfo.getValue() 
    } 
    set jackpotWinInfo(jackpotWinInfo) { 
        this._propChange$.jackpotWinInfo.next(jackpotWinInfo) 
    } 
    // ==================================================
    updateJackpotInfo(continueWin=0, kind=0, winAmount=0, totalBetAmount=0, nextBigWinRatio=0, baseBigWinAmount=0, expectedWinAmount=0, nextWinExpectedWinAmount=0) {
        this.jackpotWinInfo = {
            continueWin: continueWin,
            kind: kind,
            winAmount: winAmount,
            totalBetAmount: totalBetAmount,
            nextBigWinRatio: nextBigWinRatio,
            baseBigWinAmount: baseBigWinAmount,
            expectedWinAmount: expectedWinAmount,
            nextWinExpectedWinAmount: nextWinExpectedWinAmount
        }

        // 10局連贏且達下注條件
        if(kind == Config.JackpotKind.JACKPOT_BONUS) {
            JackpotAlert.showNormalWin(continueWin, winAmount)
        }

        // 20局連贏且達下注條件
        if(kind == Config.JackpotKind.FINAL_JACKPOT_BIG_WIN) {
            Casino.User.updateJackpotInfo(0, Config.JackpotKind.NON_JACKPOT, 0, 0, 0)            
            JackpotAlert.showSuperBigWin(continueWin, winAmount, ()=>{
                JackpotWinView.playAnimation(winAmount)
            })
        }
    }
    setCashType(cashType=0) { 
        let isDataError = (cashType != Config.CashType.CASH && cashType != Config.CashType.CREDIT) 
        // 如果發佈時沒有啟用Jackpot或是資料異常，則不會執行 
        if(Config.JACKPOT_ENABLE_BUILD == true && isDataError == false) { 
            this.cashType = cashType 
            Config.JACKPOT_ENABLE = (this.cashType == Config.CashType.CASH) 
        } 
        this._propChange$.isRecvCashType.next(true) 
    }       
    resetIdleRunNeverBet() {
        this._propChange$.idleRunNeverBet.next(0)
    }
    //-------------  
    // 單桌入桌
    seat (tableName, tableId, callBack) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser)
            SignalR.seat(tableName, tableId, callBack)
        else {
            DebugFunction.seat(tableName, tableId, callBack) 
        }
    }
    // 單桌離桌
    leave(tableName, fromTableId, toTableId, callBack) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser)
            SignalR.leave(tableName, fromTableId, toTableId, callBack)
        else {
            callBack("leave", 0)
        }
    } 
    // 多台入桌
    multipleTableSeat (tableNameInfos, callBack) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser)
            SignalR.multipleTableSeat(tableNameInfos, callBack)
        else {
            callBack("multipleTableSeat", this.multipleSeatTableInfoResp())
        }
    }
    // 多台離桌
    multipleTableLeave (callBack) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser)
            SignalR.multipleTableLeave(callBack)
        else {
            callBack("multipleTableSeat", 0)
        }
    }
    // 改變下注玩法
    updateBaccaratGameMode (tableName, tableID, betZoneType, callBack) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser)
            SignalR.updateBaccaratGameMode(tableName, tableID, betZoneType, callBack)
        else {
            let resp = {}
            resp.GameCenterErrorCode = 0
            resp.GameMode = Number(betZoneType)
            callBack("updateBaccaratGameMode", resp)
        }
    }
    // 設定玩家關注的注區ID
    setPushBettingPoolSpotId (callBack) {
        let spotIDList = [
            // 標準
            Config.spotID.Banker, 
            Config.spotID.Player, 
            // 免水
            Config.spotID.BankerNoWater, 
            Config.spotID.PlayerNoWater,
        ]
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser)
            SignalR.setPushBettingPoolSpotId(spotIDList, callBack)
    }
    // 確認是否繼續比倍或是領取BigWin獎金
    acceptJackpotBigWin(acceptBigWinResponse=0, callBack=function(cmd='', resp=ACCEPT_JACKPOT_BIG_WIN_RESULT, error=''){}) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser) {
            SignalR.acceptJackpotBigWin(acceptBigWinResponse, callBack)
        } else {
            let resp = {
                "ErrorCode" : 0,
                "ContinueWin": 15,
                "Kind": 2,
                "WinAmount": 100000,
                "TotalBetAmount": 100000
            }
            callBack(Config.SignalRCommand.ACCEPT_JACKPOT_BIG_WIN, resp, null)
        }
    } 
    /** 傳送現場驗證碼 */
    liveAuth(tableName="", tableId="", authCode="", callBack=function(cmd='', resp=SEND_VERIFICATION_CODE_RESULT, error=''){}) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser) {
            SignalR.liveAuth(tableName, tableId, authCode, callBack);
        } else {
            callBack(Config.SignalRCommand.LIVE_AUTH, 0, null)
        }
    }
    /** 取得桌檯維護狀態 */
    getBaccaratTableStatus(callBack=function(cmd='', resp=0, error=''){}) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser) {
            SignalR.getBaccaratTableStatus(callBack);
        } else {
            callBack(Config.SignalRCommand.GET_BACCARAT_TABLE_STATUS, {
                A:0,B:1,C:0,D:2,E:0
            }, null)
        }
    } 
    /** 傳送現場驗證碼 */
    getOpeningTimes(callBack=function(cmd='', resp=0, error=''){}) {
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID || cc.sys.isBrowser) {
            SignalR.getOpeningTimes(callBack);
        } else {
            callBack(Config.SignalRCommand.GET_OPENING_TIMES, 0, null)
            Casino.Tables.updateOpeningTimes([
                {"TableName":"Baccarat","TableId":"D","OpeningHours":{
                    "StartAt":"2019-01-17T01:02:00","EndAt":"2019-01-17T09:09:00"
                },"Type":0},
	            {"TableName":"Baccarat","TableId":"HE","OpeningHours":{
                    "StartAt":"2019-01-17T01:02:00","EndAt":"2019-01-17T09:09:00"
                },"Type":0}
            ])
        }
    } 

    onLogout() {
        this.logOut() 
        Tool.goToLoginScene(function() {            
            Casino.Game = null
        })
    }
    stoplistenRecvServerData() {
        if(this.recvServerDataEvtId == null) return
        NativeConnect.stoplistenRecvServerData(this.recvServerDataEvtId)
        this.recvServerDataEvtId = null
    }
    logOut(isClearPassword, isClearAutoLogin){  
        if (cc.sys.isBrowser) {
            NativeConnect.exit()
            return
        }
        if (isClearPassword == null) isClearPassword = true
        if (isClearAutoLogin == null) isClearAutoLogin = true
        if (isClearPassword) Tool.saveData(Config.saveName.password, "") 
        if (isClearAutoLogin) Tool.saveData(Config.saveName.isAutoLogin, "false") 
         
        RxJsMgr.connectData.disconnect()
        Casino.setTablesNull()
    }
    login(userName, token, callBack){
        const loginData = {
            "Token":token,
            "Username":userName,
            "PlatformId":Config.platFormId,
            'CompanyId':this.companyId
        }  
        SignalR.login(loginData, callBack)
    }
    // 存入資料
    save(){
        Tool.saveData(Config.saveName.userName, this.username)
        Tool.saveData(Config.saveName.password, this.password)
        Tool.saveData(Config.saveName.isAutoLogin, this.isAutoLogin) 
        Tool.saveData(Config.saveName.origin, this.companyId)
    }
    isWebApiSuccess(readyState, status){
        return readyState == Config.HttpRequestReadyState.DONE || status == Config.HttpRequestStatus.SUCCESS 
    }

    // 測試資料
    multipleSeatTableInfoResp() {
        let resp = {}
            resp["ErrorCode"] = 0

            resp["TableInfoList"] = [
                {TableName:"Baccarat", TableId:"A", VideoInfo:{UniversalVideoUrl:{MobileVideoUrl:"stream.wm77.asia/alive2/stream3"}}, BaccaratGameStats:{Poker1:"H1", Poker2:"H8", Poker3:"D8",Poker4:"H6", Poker5:null, Poker6:null} },
                {TableName:"Baccarat", TableId:"B", VideoInfo:{UniversalVideoUrl:{MobileVideoUrl:"stream.wm77.asia/alive8/stream3"}}, BaccaratGameStats:{Poker1:"H1", Poker2:"H8", Poker3:"D8",Poker4:"H6", Poker5:null, Poker6:null}},
                {TableName:"Roulette", TableId:"C", VideoInfo:{UniversalVideoUrl:{MobileVideoUrl:"stream.wm77.asia/alive6/stream3"}}, BaccaratGameStats:{Poker1:"H1", Poker2:"H8", Poker3:"D8",Poker4:"H6", Poker5:null, Poker6:null}},
                {TableName:"Baccarat", TableId:"D", VideoInfo:{UniversalVideoUrl:{MobileVideoUrl:"stream.wm77.asia/alive7/stream3"}}, BaccaratGameStats:{Poker1:"H1", Poker2:"H8", Poker3:"D8",Poker4:"H6", Poker5:null, Poker6:null}},
                {TableName:"Baccarat", TableId:"E", VideoInfo:{UniversalVideoUrl:{MobileVideoUrl:"stream.wm77.asia/alive7/stream3"}}, BaccaratGameStats:{Poker1:"H1", Poker2:"H8", Poker3:"D8",Poker4:"H6", Poker5:null, Poker6:null}},
            ]
            
            resp["GameLimitStake"] = {
                TableName:"MultipleTable"
            }
            
            resp["UserLimitStakes"] = [
                {
                    Chip:[2000,5000,10000],
                    MinLimit:500,
                    GameName:"MultipleTable",
                    Selected:1,
                    MaxLimit:20000,
                    LimitId:4
                },
                {
                    Chip:[5000,10000,50000],
                    MinLimit:1000,
                    GameName:"MultipleTable",
                    Selected:0,
                    MaxLimit:50000,
                    LimitId:5
                }
            ]
        return resp
    }
}