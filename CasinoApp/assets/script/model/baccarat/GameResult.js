import Rx from 'rxjs' 
export default class GameResult {
    constructor () { 
        let hitSpots = ['']
        hitSpots.length = 0

        this._propChange$ = {
            hitSpots: new Rx.BehaviorSubject(hitSpots),
            totalAmount: new Rx.BehaviorSubject(0),
        }
    }
    get propChange$()  {
        return this._propChange$
    }
    get hitSpots() {
        return this._propChange$.hitSpots.getValue()
    }
    set hitSpots(hitSpots) {
        this._propChange$.hitSpots.next(hitSpots)
    }
    get totalAmount() {
        return this._propChange$.totalAmount.getValue() 
    }
    set totalAmount(totalAmount) {  
        this._propChange$.totalAmount.next(totalAmount)        
    }
    setGameResult(data){ 
        let totalAmount = 0
        let playerHitSummary = data.PlayerHitSummary
        if(playerHitSummary != null) {
            for(let index in playerHitSummary) {
                let loseWinAmount = Number(playerHitSummary[index].LoseWinAmount)
                if(loseWinAmount <= 0) continue
                totalAmount += loseWinAmount
            }
        }
        this.totalAmount = totalAmount
        this.hitSpots = data.HitSpots
    }
}