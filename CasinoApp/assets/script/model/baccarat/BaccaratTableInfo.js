import Rx from 'rxjs'
import Config from '../../constants/Config'

const DEFAULT_POKER_DATA = ['', '', '', '' , '', '', '']
const DO_COUNTDOWN_SEC = 20

export default class BaccaratTableInfo {
    constructor () { 
        this.countDownEndDate = 0                                   //倒數結束的時間
        
        this._propChange$ = { 
            tableID: new Rx.BehaviorSubject(''), //桌子id
            tableName: new Rx.BehaviorSubject(''), //遊戲類別:baccarat,scibo
            run: new Rx.BehaviorSubject(0), //幾局
            round: new Rx.BehaviorSubject(0), //幾輪     
            roadmap: new Rx.BehaviorSubject(''), //路圖
            gameStatus: new Rx.BehaviorSubject(0), //遊戲狀態
            countDownSec: new Rx.BehaviorSubject(0), //目前剩餘時間
            totalTime: new Rx.BehaviorSubject(0), //倒數總時間
            tableMode: new Rx.BehaviorSubject(''), //遊戲類型:"BaccaratSpeed" "BaccaratStandard" "BaccaratMiCard" 
            player: new Rx.BehaviorSubject(0), //閒的下注人數
            banker: new Rx.BehaviorSubject(0), //莊的下注人數
            tie: new Rx.BehaviorSubject(0), //和的下注人數
            pokers: new Rx.BehaviorSubject(DEFAULT_POKER_DATA.slice(0)), //撲克牌資料
            playerTotal: new Rx.BehaviorSubject(0), //閒家的撲克牌總點數
            bankerTotal: new Rx.BehaviorSubject(0), //莊家的撲克牌總點數
            videoProvider: new Rx.BehaviorSubject(Config.VideoProvider.Vivo), //視頻提供商
            isR18: new Rx.BehaviorSubject(false), //是否18禁
            liveAuth: new Rx.BehaviorSubject(false), //是否開放現場驗證
            /** 現場開放時間資訊 */
            openingTimes:new Rx.BehaviorSubject({
                /** 開放時間 */
                startingAt: "1987-03-18T23:00:00",
                /** 關閉時間 */
                closingAt: "1987-03-18T04:00:00"
            }),
            tableStatus: new Rx.BehaviorSubject(0), //桌檯維護狀態 0:正常 1:維護 2:關閉
            // ===== Online Setting Start =====
            pokerDelayTime: new Rx.BehaviorSubject(0), //視訊延遲開牌的時間 毫秒
            videoZoomIn: new Rx.BehaviorSubject({
                "scale": 2.5,     // 視訊放大時倍率
                "offsetX": 0,   // 視訊放大時X位移量
                "offsetY": 0    // 視訊放大時Y位移量
            }),
            baccaratVideo: new Rx.BehaviorSubject({ // 視訊參數 , m_xx多台參數
                x: 0, 
                y: 435, 
                w: 1080, 
                h: 651,
                clipW: 1080, 
                clipH: 600, 
                offsetY: 0, 
                isVideoMute: true,
                m_y: 525,
                m_h: 550,
                m_clipH: 550
            }),
            /** 輪盤歷史開球紀錄 */
            history: new Rx.BehaviorSubject([0]),
        }
    }
    
    // ======
    get propChange$() {
        return this._propChange$
    }
    get countDownSec() {
        return this._propChange$.countDownSec.getValue()
    }
    set countDownSec(countDownSec) {
        this._propChange$.countDownSec.next(countDownSec)
    }
    get status() {
        return this._propChange$.gameStatus.getValue()
    }
    set status(status) {
        this._propChange$.gameStatus.next(status)
    }
    get roadmap() {
        return this._propChange$.roadmap.getValue()
    }
    set roadmap(roadmap) {
        this._propChange$.roadmap.next(roadmap)
    }
    get round() {
        return this._propChange$.round.getValue()
    }
    get run() {
        return this._propChange$.run.getValue()
    }
    get tableID() {
        return this._propChange$.tableID.getValue()
    }
    get tableMode() {
        return this._propChange$.tableMode.getValue()
    }
    get tableName() {
        return this._propChange$.tableName.getValue()
    }
    get totalTime() {
        return this._propChange$.totalTime.getValue()
    }
    get playerTotal() {
        return this._propChange$.playerTotal.getValue()
    }
    set playerTotal(playerTotal) {
        this._propChange$.playerTotal.next(playerTotal)
    }
    get bankerTotal() {
        return this._propChange$.bankerTotal.getValue()
    }
    set bankerTotal(bankerTotal) {
        this._propChange$.bankerTotal.next(bankerTotal)
    }
    get videoProvider() {
        return this._propChange$.videoProvider.getValue()
    }
    set videoProvider(videoProvider) {
        this._propChange$.videoProvider.next(videoProvider)
    }
    get isR18() {
        return this._propChange$.isR18.getValue()
    }
    get liveAuth() {
        return this._propChange$.liveAuth.getValue()
    }
    get tableStatus() {
        return this._propChange$.tableStatus.getValue();
    }
    get pokerDelayTime() {
        return this._propChange$.pokerDelayTime.getValue();
    } 
    get videoZoomIn() {
        return this._propChange$.videoZoomIn.getValue();
    }
    setData(data) {
         for(var key in data) {   
            var newKey = key.charAt(0).toLowerCase() + key.substring(1, key.length)
            if(this._propChange$.hasOwnProperty(newKey) == false) continue
            this._propChange$[newKey].next(data[key])
        }  
    }
    // 解析setInitTableInfo封包
    setInitTableInfo(data) {         
        // 初始化歷史紀錄
        let history = this._propChange$.history.getValue()
        history.length = 0
        this._propChange$.history.next(history)
        
        this.setData(data) 
        let gameStatus = Number(data.GameStatus)
        
        // 如果狀態為CountDown，設定倒數時間 
        if(gameStatus == Config.gameState.CountDown) { 
            this.updateCountDown() 
        } 
 
        //動態賠率 
        if(gameStatus == Config.gameState.OpenBankerCardBet) { 
            this.updateCountDown(false) 
        } 
 
        if(gameStatus == Config.gameState.OpenBankerCardBet) { 
            this.updateCountDown(false) 
        } 
        
    }
    // 解析updateGameStatus封包並更新遊戲狀態
    updateGameStatus(data) {
        //維修清除路圖
        let gameStatus = Number(data.Status)
        if(gameStatus == Config.gameState.Maintenance) {
            this.clearRoadMapInfo()
        }
        else if (gameStatus == Config.gameState.CountDown) {
            this.countDownSec = this.totalTime
            this.updateCountDown()
        }
        else if (gameStatus == Config.gameState.OpenBankerCardBet ||
                 gameStatus == Config.gameState.OpenPlayerCardBet) {
            this.countDownSec = this.totalTime
            this.updateCountDown(false)
        }
        else if(gameStatus == Config.gameState.NewRoundRun){
            this.updateNewRoundRun(data)
        }
        else if(gameStatus == Config.gameState.OpenBankerCardBetEnd || 
                gameStatus ==  Config.gameState.OpenPlayerCardBetEnd) {
            this.updateEndBetTime(false)
        }
        else if(gameStatus == Config.gameState.EndBetTime){
            this.updateEndBetTime()
        }
        else if(gameStatus == Config.gameState.Dealing){
            this.updateDealing(data)
        }
        else if(gameStatus == Config.gameState.Computing){
            this.updateComputing(data)
        }
        
        this.setData(data)
        this.status = data.Status
    } 

    //更新 狀態為倒數中
    updateCountDown(isClearPoker = true) {
        this.countDownEndDate = (+new Date()) + this.countDownSec * 1000 // 以毫秒為單位
        if (isClearPoker == false) return
        this._propChange$.pokers.next(DEFAULT_POKER_DATA.slice(0))
    }
    //更新 狀態為新輪新局
    updateNewRoundRun(data) {
        if(this.round == data.Round) return
        //處理新輪:清除路圖, 清除莊閒和
        this.clearRoadMapInfo()
        this.clearBankerPlayerTie()
    }
    //更新狀態 下注時間結束
    updateEndBetTime(isClearPoker = true) {
        if(isClearPoker == false) return
        this.bankerTotal = 0 
        this.playerTotal = 0

        this._propChange$.pokers.next(DEFAULT_POKER_DATA.slice(0))
    }
    //更新狀態 開牌中
    updateDealing(data) {
        this.setPoker(data, true)        
    }
    //更新狀態 結算中
    updateComputing(data) {
        if(Number(this.run) != Number(data.Run)) return
        this.bankerTotal = data.Stats.BankerTotal
        this.playerTotal = data.Stats.PlayerTotal
        this.setPoker(data.Stats)
        this.setHistory(data.Stats)
    }
    //更新彩池
    setHistory(data) {
        this._propChange$.player.next(data.Player)
        this._propChange$.banker.next(data.Banker)
        this._propChange$.tie.next(data.Tie)
    }
    setPokerData(data = {}, index = 1) {
        if(data["BankerTotal"] != null)
            this.bankerTotal = data.BankerTotal            
        if(data["PlayerTotal"] != null)
            this.playerTotal = data.PlayerTotal

        let pokers = this._propChange$.pokers.getValue()
        pokers[index] = data['Poker'+index]
        if(++index<7) {
            this.setPokerData(data, index)
        } else {
            // VIVI視訊 閒家前兩張位置互換，莊家前兩張牌位置互換
            const VIVO = Config.VideoProvider.Vivo
            const VIVO_BIG_TABLE = Config.VideoProvider.VivoBigTable
            if(this.videoProvider == VIVO || this.videoProvider == VIVO_BIG_TABLE) {
                let tempPoker = pokers[1]
                pokers[1] = pokers[3]
                pokers[3] = tempPoker
                tempPoker = pokers[2]
                pokers[2] = pokers[4]
                pokers[4] = tempPoker
            }
            this._propChange$.pokers.next(pokers)
        }
    }

    // 延遲設定撲克牌
    delaySetPokerData(data = {}) {
        setTimeout(() => {
            if(Number(this.status) != Config.gameState.Dealing) {
                return
            }
            
            this.setPokerData(data)
        }, this.pokerDelayTime)
    }

    setPoker(data = {}, isNeedDelay = false) {
        const VIVO = Config.VideoProvider.Vivo
        const VIVO_BIG_TABLE = Config.VideoProvider.VivoBigTable
        let isVivo = (this.videoProvider == VIVO || this.videoProvider == VIVO_BIG_TABLE)

        if(isNeedDelay == false || isVivo == false) {
            this.setPokerData(data)
            return
        }

        this.delaySetPokerData(data)
    }
    
    //初始化所有桌子的資訊
    setInitialGambleTable(data) {
        this.setPoker(data)
        this.setHistory(data)
    }
    //加入一個路圖的資料
    setRoadMapInfo(data) { 
        if (this.roadmap == '')
            this.roadmap = this.roadmap + data.Roadmap
        else
            this.roadmap = this.roadmap + "," + data.Roadmap
    }
    //清礎所有路圖資訊
    clearRoadMapInfo(){ 
        this.roadmap = ''
    } 
    //清除莊閒和
    clearBankerPlayerTie() {
        this._propChange$.player.next(0)
        this._propChange$.banker.next(0)
        this._propChange$.tie.next(0)
    }
    updateOpeningTimes(data) {
        let openingTimes = {
            startingAt: data.OpeningHours.StartAt,
            closingAt: data.OpeningHours.EndAt
        }
        this._propChange$.openingTimes.next(openingTimes);
    }
    updateTableStatus(status=0) {
        this._propChange$.tableStatus.next(status);
    }
}