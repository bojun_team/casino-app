import Rx from 'rxjs' 
import BetingPool from '../BetingPool'
import GameResult from './GameResult'
import SpotMgr from '../SpotMgr'
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

export default class BaccaratGame {
    constructor (tableName, tableID, spotInfo) {
        // 注區管理
        this._spotMgr = new SpotMgr (spotInfo) 
        // 存放桌檯Id
        this._tableID = tableID
        // 存放桌檯名稱
        this._tableName = tableName
        // 存放即時彩池資料的字典，key為注區ID  
        this._betingPool = {} 
        // 存放遊戲結果
        this._result = new GameResult()
        // 存放影片網址
        this._videoUrl = ""  

    // ===== RXJS        
        this._propChange$ = {
            isReady: new Rx.BehaviorSubject(false), // 確認是否收到所有資料
            betZoneType: new Rx.BehaviorSubject(Config.betZoneType.Standard), // 百家樂下注模式 Standard:標準, West:西洋, NoWater:免水
        }

    // ===== Server Data
        this.recvServerDataEvtId = Casino.NativeConnect.listenRecvServerData((data) => {
            this.processReady(data)
            // 入桌資料 initialGambleTable, getSelfBettingList, getUserLimitStake
            if (data.M == "initialGambleTable") {
                this.initialGambleTable(data.A) 
            }
            if (data.M == "getSelfBettingList") {
                 this.setSelfBettingList(data.A[0])
            }

            // 大廳就可以收的資料, 為了清除新局時清除下注資料
            if(data.M == "gameStatusUpdateMessageReceived"){
                this.updateGameStatus(data.A[0])
            }
                
            // 入桌後收的資料, 
            if (data.M == "updateBettingPool") {
                this.updateBettingPool(data.A[0])
            } 
            if (data.M == "updateComputingResult") {
                this.updateComputingResult(data.A[0])
            } 
            if (data.M == "getBaccaratGameMode"){
                this.betZoneType = Number(data.A)
            }
        })
        
    }
    get propChange$() {
        return this._propChange$
    }
    set tableID(id){
        this._tableID = id
    }
    get tableID(){
        return this._tableID
    }
    set tableName(name){
        this._tableName = name
    }
    get tableName(){
        return this._tableName
    }
    set videoURL(videoURL) {
        this._videoUrl = videoURL
    }
    get videoURL(){
        return this._videoUrl
    }
    get tableInfo() {
        return Casino.Tables.getTableInfo(this.tableName, this.tableID)
    }
// --------- SpotMgr -----------
    get spotMgr () {
        return this._spotMgr
    }
    set betSum (betSum) {
        this._spotMgr.betSum = betSum
    }
    get betSum () {
        return this._spotMgr.betSum
    }
    // 分析封包 getSelfBettingList, 中場時不分析封包
    parseSelfBettingList(data){
        let table = Casino.Tables.getTableInfo(this._tableName, this._tableID)
        if(Number(table.status) == Config.gameState.HalfTime) return
        this._spotMgr.parseSelfBettingList(data, table.round, table.run)
    }
    // 分析封包 gameStatusUpdateMessageReceived
    parseGameStatus(status){
        // 當新輪新局時，清除下注金額並暫存這次下注金額
        if (Config.gameState.NewRoundRun == status ){
            this._spotMgr.cleanAndSaveChipAmount()
        }
        // 當中場時，清除下注金額及上一次下注金額
        else if(Config.gameState.HalfTime == status) {
            this._spotMgr.clearLastChipAmount()
        }
        // 當取消此局時時，清除下注金額及上一次下注金額
        else if(Config.gameState.CancelRun == status) {
            this._spotMgr.clearLastChipAmount()
        }
    }
    // 更新注區的限額
    updateGameLimitStake(gameLimitStake) {
        this._spotMgr.parserLimitStake(gameLimitStake)
    }
// --------- 百家樂下注模式 -----------    
    set betZoneType(betZoneType){
        this._propChange$.betZoneType.next(betZoneType)
    }
    get betZoneType() {
        return this._propChange$.betZoneType.getValue()
    }
    // 改變下注玩法
    changeBetZoneType(betZoneType, callBack) {
        Casino.User.updateBaccaratGameMode(this._tableName, this._tableID, betZoneType, function(cmd, respData, error){
            if (error != null){
                callBack(respData, error)
                return
            }
            
            const errorCode = Number(respData["GameCenterErrorCode"])
            if (errorCode == 0) {
                // 設定改變的下注玩法
                const mode = Number(respData["GameMode"])
                this.betZoneType = mode
                // 切換玩法時重設所有下注資料()
                this._spotMgr.clearLastChipAmount()
            }
            callBack(respData, errorCode)
        }.bind(this))
    }
// --------- 即時彩池 -----------
    // 取得即時彩池資料
    getBetingPool(spotId) {
        let betingPool = new BetingPool()
        // 如果 betingPool == null，則建立一個 BetingPool
        if(this._betingPool[String(spotId)] == null) {
            this._betingPool[String(spotId)] = betingPool
        } else {
            betingPool = this._betingPool[String(spotId)]
        }
        return betingPool
    }    
    // 更新即時彩池狀態
    updateBettingPool(data){
        if(Casino.Game.tableName == Config.TableName.MultipleTable) return
        
        let tableInfo = data.TableInfo
        // 如果是別桌資料則不處理
        if(tableInfo.TableName != this._tableName || tableInfo.TableId != this._tableID) return
        // 彩池資料
        this.parseBetingnStatisticsList(data.BetingnStatisticsList)
    }
    parseBetingnStatisticsList(betingnStatisticsList) {
        for(var index in betingnStatisticsList) {
            // 玩家該注區的下注明細
            let bettingStatisticsItem = betingnStatisticsList[index]
            // 注區編號
            let spotId = String(bettingStatisticsItem.SpotId)
            // 設定彩池資料
            this.getBetingPool(spotId).setData(bettingStatisticsItem)
        } 
    }
// --------- 遊戲結果 -----------
    // 取得遊戲結果
    get result () {
        return this._result
    }
    // 更新計算結果
    updateComputingResult(data){
        // 如果是別桌資料則不處理
        if(data.TableName != this._tableName || data.TableId != this._tableID) return
        this._result.setGameResult(data)
    }
// --------- 資料接收 -----------
    // 判斷是否接收到開啟遊戲所需資料
    processReady(data){
        if(this._propChange$.isReady.getValue() == true) return // 如果準備完成，則不會執行
        
        // 如果沒有判斷資料，全部預設為false
        if(this._isGetServerData == null) {
            this._isGetServerData = {
                isGetUserLimitStake : false,
                isInitialGambleTable : false,
                isGetSelfBettingList : false
            }
        }
        
        if(data.M == "getUserLimitStake") 
            this._isGetServerData.isGetUserLimitStake = true
        else if(data.M == "initialGambleTable")
            this._isGetServerData.isInitialGambleTable = true
        else if(data.M == "getSelfBettingList")
            this._isGetServerData.isGetSelfBettingList = true

        // 如果全部資料都有拿到，設定 isReady = true    
        if(this._isGetServerData.isGetSelfBettingList && 
           this._isGetServerData.isInitialGambleTable &&
           this._isGetServerData.isGetUserLimitStake 
           ) {
            this._propChange$.isReady.next(true)
        }
    }
    // 分析封包 initialGambleTable
    initialGambleTable(data){ 
        // 更新注區的限額
        this.updateGameLimitStake(data[0]["BaccaratGameLimitStake"]["Limits"])
        // 更新彩池資料
        this.parseBetingnStatisticsList(data[0]["BetingnStatisticsList"])
        // 取得視訊源
        this._videoUrl = data[0]["VideoInfo"]["UniversalVideoUrl"]["MobileVideoUrl"]
    }
    // 設定下注資料 
    setSelfBettingList(data) {
        var gameState = Casino.Tables.getTableInfo(this.tableName, this.tableID).status

        // 如果為中場或是維護中或是data==null，則不處理
        if(gameState == Config.gameState.HalfTime || gameState == Config.gameState.Maintenance)return
        this.parseSelfBettingList(data) 
    }
    // 更新遊戲狀態
    updateGameStatus(data) {
        if (data["TableName"] == this.tableName && data["TableID"] == this.tableID){
            const state = data["Status"]
            this.parseGameStatus(state) 
        }
    } 
    // 停止接收Server資料
    stoplistenRecvServerData(){
        if(this.recvServerDataEvtId == null) return
        Casino.NativeConnect.stoplistenRecvServerData(this.recvServerDataEvtId)
        this.recvServerDataEvtId = null
    }
    
}
