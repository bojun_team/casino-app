import Rx from 'rxjs'
import BaccaratInfo from './baccarat/BaccaratTableInfo'
import Casino from '../model/Casino'

export default class Tables {
    constructor () { 
        this._baccaratInfos = [new BaccaratInfo()]
        this._baccaratInfos.length = 0

        this._propChange$ = {
            isRecvInitTables: new Rx.BehaviorSubject(false), 
        }

        this.recvServerDataEvtId = Casino.NativeConnect.listenRecvServerData((data) => {
            if (data.M == "initialCasinoTable") {
                this.initialCasinoTable(data.A[0]) 
                this.getBaccaratTableStatus();
                this.getOpeningTimes();
                this.updateBaccaratOnlineSetting();
            }
            if (data.M == "roadMapMessageReceived") {
                this.setRoadData(data.A[0]) 
            }
            if(data.M == "gameStatusUpdateMessageReceived"){
                this.updateGameStatus(data.A[0])
            }
            if (data.M == "initialGambleTable") {
                this.initialGambleTable(data.A[0]) 
            } 
            if(data.M == "updateCasinoTable") { //維修後收到
                this.setRoadData(data.A[0])
                data.A[0].Status = data.A[0].GameStatus
                this.updateGameStatus(data.A[0])
            }
            if(data.M == "updateOpeningTimes") {
                this.updateOpeningTimes(data.A[0]);
            } 
            if(data.M == "updateGameStatus") {
                this.updateBaccaratTableStatus(data.A[0]);
            } 
        })

    }
    
    // Get property change observable stream
    get propChange$() {
        return this._propChange$
    }

    get isRecvInitTables() {
        return this._propChange$.isRecvInitTables.getValue()
    }
    
    //停止監聽Server封包，在玩家登出時會執行
    stoplistenRecvServerData(){
        if(this.recvServerDataEvtId == null) return
        NativeConnect.stoplistenRecvServerData(this.recvServerDataEvtId)
        this.recvServerDataEvtId = null
    }
    
    //分析封包initTableData
    initialCasinoTable (data){  
        for (let index in data) { 
            this._baccaratInfos[index] = new BaccaratInfo()
            this._baccaratInfos[index].setInitTableInfo(data[index])
        }
        this._propChange$.isRecvInitTables.next(true)
    }
    //分析封包 setRoadData
    setRoadData (data) {
        this.getTableInfo(data.TableName, data.TableID).setRoadMapInfo(data)
    } 
    // 取得 單一桌的資料(TableInfo)
    getTableInfo (tableName='', tableID='') {
        return this._baccaratInfos.find(info => info.tableName == tableName && info.tableID == tableID)
    }
    //取得所有table的ID
    getTableId(tableName){
        return this._baccaratInfos.filter(info => info.tableName == tableName).map(info => info.tableID)
    }
    //分析封包 updateGameStatus
    updateGameStatus(data) {  
        this.getTableInfo(data.TableName, data.TableID).updateGameStatus(data) 
    }
    //分析封包 initialGambleTable
    initialGambleTable(data) {
        this.getTableInfo(data.TableName, data.TableId).setInitialGambleTable(data.BaccaratGameStats)
    }
     /** 取得桌檯維護狀態 */
     getOpeningTimes() {
         Casino.User.getOpeningTimes((cmd, resp, error) =>{

         });
     }
    /** 更新桌檯開放時間 */
    updateOpeningTimes(data) {
        for (let index in data) {
            let openingTime = data[index];
            let table = this.getTableInfo(openingTime.TableName, openingTime.TableId);
            if(table != null) {
                table.updateOpeningTimes(openingTime);
            }
        }
    }
    /** 取得桌檯維護狀態 */
    getBaccaratTableStatus() {
        Casino.User.getBaccaratTableStatus((cmd, resp, error) => {
            if (error == null) {
                this.updateBaccaratTableStatus(resp);
            }
        });
    }
    /** 更新桌檯維護狀態 */
    updateBaccaratTableStatus(data) {
        for(let key in data) {
            let table = this.getTableInfo("Baccarat", key);
            if(table != null) {
                table.updateTableStatus(data[key]);
            }
        }
    }
    /** 更新桌檯網路參數 */
    updateBaccaratOnlineSetting() {
        Casino.AppWebAPI.getOnlineSetting((value)=>{
            if(value == null) { console.log("jun updateBaccaratOnlineSetting value == null "); return;}

            let baccaratSetting = value["baccaratSetting"];
            if(baccaratSetting == null) { console.log("jun baccaratSetting == null "); return;}

            for(let index in baccaratSetting) {
                let data = baccaratSetting[index];
                let info = this.getTableInfo("Baccarat", data.tableID);
                if(info != null) {
                    info.setData(data);
                }
            }
        });
    }
}
