import Config from '../constants/Config'
import Rx from 'rxjs'

const _propChange$ = {
    isHideKeyboard: new Rx.BehaviorSubject(true),
    text: new Rx.BehaviorSubject(''),
    eventMaskerEnable: new Rx.BehaviorSubject(true),
}

var _node = null
var _isLoadingPrefab = false

var NumberKeyboard = {
    loadPrefab() {
        if(_node != null || _isLoadingPrefab == true) return
        _isLoadingPrefab = true
        cc.loader.loadRes("prefab/common/NumberKeyboard", (err, prefab) => {
            _node = cc.instantiate(prefab)
            cc.director.getScene().addChild(_node, Config.GlobalZ_Index.NUMBER_KEYBOARD)
            cc.game.addPersistRootNode(_node)
        })
    },

    set isHideKeyboard(isHideKeyboard) {
        _propChange$.isHideKeyboard.next(isHideKeyboard)
    },

    get isHideKeyboard() {
        return _propChange$.isHideKeyboard.getValue()
    },

    get isHideKeyboard$() {
        return _propChange$.isHideKeyboard
    },

    set text(text) {
        _propChange$.text.next(text)
    },

    get text() {
        return _propChange$.text.getValue()
    },

    get text$() {
        return _propChange$.text
    },

    addLastText(text='') {
        if(this.text.length > 3) return
        // this.text = String(Number(this.text + text))
        this.text = String((this.text + text))
    },

    deleteLastText() {
        if(this.text.length == 0) return
        this.text = this.text.substring(0, this.text.length-1)
    },
}

module.exports = NumberKeyboard