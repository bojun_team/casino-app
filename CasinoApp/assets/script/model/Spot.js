import Rx from 'rxjs' 
import NativeConnect from '../native/NativeConnect'

export default class Spot {
    constructor (spotInfo) { 
        if(spotInfo == null) return
        this._spot_id = spotInfo["spot_id"]
        this._description = spotInfo["description"]
        this._decriptionChinese = spotInfo["decriptionChinese"]
        this._gameLimitStakeList = [] //注區的上下限額
        this._lastChipAmount = 0 //上一次下注金額
        this._propChange$ = {
            chipAmount: new Rx.BehaviorSubject(0), //目前下注金額
        }
    }
    // 設定下注金額
    set chipAmount (chipAmount) {  
        this._propChange$.chipAmount.next(chipAmount)
    }    
    // 取得下注金額
    get chipAmount () {
        return this._propChange$.chipAmount.getValue()
    }
    // 上一次下注的金額
    get lastChipAmount (){
        return this._lastChipAmount
    }
    // 取得可訂閱的成員
    get propChange$() {
        return this._propChange$
    }
    // 清除下注金額並暫存這次下注金額
    cleanAndSaveChipAmount (){
        this._lastChipAmount = this.chipAmount
        this.chipAmount = 0
    }
    // 清除下注金額及上一次下注金額
    clearLastChipAmount(){
        this.chipAmount = 0
        this._lastChipAmount = 0
    } 
    // 設定注區的上下限額
    // data: 封包 initialGambleTable 內的 ["BaccaratGameLimitStake"]["Limits"][ApplySpotIds][n]
    setLimitStake(data){
        var gameLimitStake = {} 
        gameLimitStake.limitMax = data["LimitMax"]
        gameLimitStake.limitMin = data["LimitMin"]
        gameLimitStake.Odds = data["Odds"]
        this._gameLimitStakeList[data["LimitId"]] = gameLimitStake 
    } 
    // 取得注區最大的限額
    // id: 限額 id
    getGameLimitMaxStake(id){ 
        if (this._gameLimitStakeList.length == 0 ) return 0
        else return Number(this._gameLimitStakeList[id].limitMax)
    }
    //取得注區最小的限額
    // id: 限額 id
    getGameLimitMiniStake(id){ 
        if (this._gameLimitStakeList.length == 0 ) return 0
        else return Number(this._gameLimitStakeList[id].limitMin)
    }
    //取得注區的賠率
    // id: 限額 id
    getGameLimitOddStake(id){
        if (this._gameLimitStakeList.length == 0 ) return 0
        else return Number(this._gameLimitStakeList[id].Odds)
    }
}