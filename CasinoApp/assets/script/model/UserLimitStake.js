export default class UserLimitStake {
    constructor () { 
        this._gameName = ""
        this._limitId = 1
        this._maxLimit = 1
        this._minLimit = 0
        this._selected = 0
    }

    get gameName () {
        return this._gameName
    }
    get limitId () {
        return this._limitId
    }
    get maxLimit () {
        return this._maxLimit
    }
    get minLimit () {
        return this._minLimit
    }
    get selected () {
        return this._selected == 1
    }
      
    setData(data){
        for(var key in data) {   
            var newKey = "_" + key.charAt(0).toLowerCase() + key.substring(1, key.length)
            // 這裡不存籌碼資訊
            if(newKey == "_chip") continue
            this[newKey] = data[key]
        }  
    }
    
}