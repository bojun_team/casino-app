import Rx from 'rxjs' 
import UserChip from 'UserChip'
import Localize from '../constants/Localize'
import localizeMgr from '../tool/localize/LocalizeMgr'
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import SignalR from '../native/SignalR'

var _lastUseChip = {
    Baccarat: {
        limitId: 0,
        index: 0,
        chips: [0,0,0]
    },
    MultipleTable: {
        limitId: 0,
        index: 0,
        chips: [0,0,0]
    },
    Roulette: {
        limitId: 0,
        index: 0,
        chips: [0,0,0]
    }
}

export default class ChipManager {
    constructor (tableName) {
        this._currentLimitId = "" // 目前使用籌碼組的限額Id
        this._userChips = {}  // 籌碼組，使用限額的limitId當作key
        
        // ----- RXJS
        
        this._tableName = tableName
        let useChipIndex = 0
        if(tableName != null) {
            useChipIndex = _lastUseChip[tableName].index  // 目前使用籌碼組的籌碼index
        }

        let choseUseChips = [0, 0, 0]
        choseUseChips.length = 0

        this._propChange$ = {
            choseUseChips: new Rx.BehaviorSubject(choseUseChips), 
            useChipIndex: new Rx.BehaviorSubject(useChipIndex),
        }
    }
    // 取得正在使用的籌碼組
    get currentChips () {
        return this._userChips[this._currentLimitId].chip
    }
    // 取得正在使用籌碼組的籌碼index
    get useChipIndex () {
        return Number(this._propChange$.useChipIndex.getValue())
    }
    // 取得正在使用的籌碼
    get currentChip () {
        return Number(this.currentChips[this.useChipIndex])
    }
    // 判斷是否更換過籌碼
    isChangeChips(choseUseChips) {
        let isChange = false
        if(choseUseChips.length != _lastUseChip[this._tableName].chips.length) return true
        for(let index in choseUseChips) {
            if(choseUseChips[index] != _lastUseChip[this._tableName].chips[index]) {
                isChange = true
                _lastUseChip[this._tableName].chips = choseUseChips
                break
            }
        }
        return isChange
    }
    // 設定使用者正在使用的籌碼組
    set choseUseChips (choseUseChips) {
        // 判斷是否更換過限額
        let isChangeLimit = (this._currentLimitId != _lastUseChip[this._tableName].limitId)
        this._propChange$.choseUseChips.next(choseUseChips)
        
        if(this.isChangeChips(choseUseChips) == true || isChangeLimit == true) {
            _lastUseChip[this._tableName].limitId = this._currentLimitId
            this.useChipIndex = 0
        } else {
            this.useChipIndex = _lastUseChip[this._tableName].index
        }
    }
    // 設定籌碼組正在使用的籌碼index
    set useChipIndex(useChipIndex){
        _lastUseChip[this._tableName].index = useChipIndex
        this._propChange$.useChipIndex.next(useChipIndex)
    }
    get propChange$() {
        return this._propChange$
    }
    //分析封包 UserLimitStakes
    setUserLimitStakes(tableName, data) {
        // 設定所有籌碼資料
        for(let i=0; i<data.length; i++) {
            let userChip = new UserChip(data[i])
            // 處理使用中的籌碼組
            if(userChip.selected) {
                // 如果是不適合的籌碼，重新設定籌碼
                if (userChip.isHaveSelectNotAllowChip()) { 
                    userChip.resetChips()
                }
                // 設定選擇的籌碼組
                this._currentLimitId = userChip.limitId
                this.choseUseChips = userChip.chip
            }
            this._userChips[userChip.limitId] = userChip
        }

        // 收到限額範本時，重送目前使用籌碼給Sever
        this.sendChipsChose(tableName, this.currentChips, null)
    }

    // 資料檢查：傳送選擇的籌碼
    isDataError(tableName='', choseChips=[]) {
        if(tableName == null) {
            console.log('ChipManager sendChipsChose tableName == null')
            return true
        }

        if(choseChips == null) {
            console.log('ChipManager sendChipsChose choseChips == null')
            return true
        }

        if(choseChips.length == 0) {
            console.log('ChipManager sendChipsChose choseChips.length == 0')
            return true
        }

        return false
    }

    // 傳送選擇的籌碼
    sendChipsChose(tableName, choseChips, onSendComplete){
        // 傳送選擇的籌碼前，先檢查資料，若資料有誤，則不傳送
        if(this.isDataError(tableName, choseChips) == true) return

        // 更新玩家的籌碼設定到Server
        SignalR.updatePersonalChipSetting(Config.platFormId, tableName, this._currentLimitId, String(choseChips), function(cmd, resp, error) {
            if (error != null){
                if(onSendComplete != null) onSendComplete(resp, error)
                return
            }

            if (resp == 0) {
                // 改變目前存取籌碼資料
                this._userChips[this._currentLimitId].chip = choseChips
                this.choseUseChips = choseChips
            }

            if(onSendComplete != null) {
                onSendComplete(resp, error)
            }
        }.bind(this))
    }
}
