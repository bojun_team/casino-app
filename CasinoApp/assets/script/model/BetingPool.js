import Rx from 'rxjs' 
export default class BetingPool {
    constructor () { 
        this._propChange$ = {
            spotId: new Rx.BehaviorSubject(0), //注區ID 
            spotPlayerCount: new Rx.BehaviorSubject(0), //此注區下注人數
            spotTotal: new Rx.BehaviorSubject(0), //此注區下注總金額 
        }
    }
    get propChange$() {
        return this._propChange$
    }  
    setData(data) {
        this._propChange$.spotId.next(data.SpotId) 
        this._propChange$.spotPlayerCount.next(data.SpotPlayerCount)
        this._propChange$.spotTotal.next(data.SpotTotal)
    } 
}