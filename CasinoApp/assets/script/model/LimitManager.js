import Rx from 'rxjs' 
import UserLimitStake from './UserLimitStake'
import Localize from '../constants/Localize'
import localizeMgr from '../tool/localize/LocalizeMgr'
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

export default class LimitManager {
    constructor () {
        this._userLimitStakes = []  // 限額範本是一個資料陣列

        this._propChange$ = {
            currentLimitStakeIndex: new Rx.BehaviorSubject(0),
        }
    }
    
    get currentLimitStakeIndex () {
        return this._propChange$.currentLimitStakeIndex.getValue()
    }

    set currentLimitStakeIndex (currentLimitStakeIndex) { 
        this._propChange$.currentLimitStakeIndex.next(currentLimitStakeIndex)
    }

    get propChange$() {
        return this._propChange$
    }

    // 取得正在使用的限額資訊
    getCurrentLimitStake () {
        return this._userLimitStakes[this.currentLimitStakeIndex]
    }

    

    // 分析封包 UserLimitStakes
    setUserLimitStakes (data) {    
        // 設定所有限額範本資料
        let currentLimitStakeIndex = 0
        for(let i=0; i<data.length; i++) {
            this._userLimitStakes[i] = new UserLimitStake() 
            this._userLimitStakes[i].setData(data[i])
            if(this._userLimitStakes[i].selected) {
                currentLimitStakeIndex = i
            }
        }

        // 設定選擇的範本
        this.currentLimitStakeIndex = currentLimitStakeIndex
    }
    // 傳送使用的限額範本ID給Server
    sendLimitChoseIndex(index, callBack){
        let tableName = Casino.Game.tableName
        let limitId = this._userLimitStakes[index]["_limitId"]
        if(tableName == Config.TableName.MultipleTable)
            SignalR.updateMultipleTableUserLimitStake(limitId, callBack)
        else
            SignalR.updateUserLimitStake(tableName, limitId, callBack)
    }
}
