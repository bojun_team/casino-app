import Rx from 'rxjs'

import LimitManager from './LimitManager'
import ChipManager from './ChipManager'
import SpotMgr from './SpotMgr'

import BaccaratGame from './baccarat/BaccaratGame' 

import UserLimitStake from './UserLimitStake'
import UserChip from './UserChip'
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import Tool from '../tool/Tool'
import Casino from '../model/Casino'

export default class Game {
    constructor (tableName, tableID, spotInfo) {
        // 限額管理
        this._limitMgr = new LimitManager ()
        // 籌碼管理
        this._chipMgr = new ChipManager (tableName)
        // 存放桌檯類型
        this._tableName = tableName
        // 存放桌檯類型
        this._tableID = tableID
        // 存放注區資訊
        this._spotInfo = spotInfo
        // 存放各遊戲
        this._games = [new BaccaratGame()]
        this._games[0].stoplistenRecvServerData()
        this._games.length = 0
        // 處理單台
        this.processSingleTable(tableName, tableID) 
        // ===== Server Data
        this.recvServerDataEvtId = NativeConnect.listenRecvServerData(function(data){
            if(data.M == "getUserLimitStake") {
                this._limitMgr.setUserLimitStakes(data.A[0])
                this._chipMgr.setUserLimitStakes(this._tableName, data.A[0])
            }
        }.bind(this))

        this._propChange$ = {
            resetNoBetTimeCount: new Rx.BehaviorSubject(0), // 當有下注行為時+1, 重置踢人時間
        }
    }
    get propChange$() {
        return this._propChange$
    }
    set resetNoBetTimeCount(resetNoBetTimeCount) {
        this._propChange$.resetNoBetTimeCount.next(resetNoBetTimeCount)
    }
    get resetNoBetTimeCount() {
        return this._propChange$.resetNoBetTimeCount.getValue()
    }
    get games (){
        return this._games
    }
    // 單台取game
    get singleGame () {
        return this._games[0]
    }
    // 單台取tableName
    get tableName(){
        return this._tableName
    }
    // 單台取tableId
    get tableID () {
        return this._tableID
    }
    // 多台取game
    getGame (tableName, tableID) {
        for (let index in this._games) {
            let game = this._games[index]
            if(game.tableName == tableName && game.tableID == tableID) {
                return game
            }
        }
        return null
    }
    /** 排序列表 */
    sortList () {
        let first = [];
        let normal = [];
        let atfer = [];
        for (var i = 0; i < this._games.length; i++) {
            let info = this._games[i].tableInfo;
            if (info.isR18 == true && info.tableStatus == Config.tableStatus.Normal) {
                first.push(this._games[i]);
            } else if(info.tableStatus == Config.tableStatus.Normal){
                normal.push(this._games[i]);
            } else {
                atfer.push(this._games[i]);
            }
        }
        this._games = first.concat(normal).concat(atfer);
    }
    // 解析入桌資訊:遊戲的限注, 玩家限注範本設定, 各桌資訊
    parseMultipleSeatTableInfo (data) {
        // 解析各桌資訊:即時彩池狀態, 視訊源, 自己下注資料, 百家樂下注模式
        this.parseTableInfoList(data.TableInfoList)
        // 解析遊戲的限注
        this.parseGameLimitStake(data.GameLimitStake)
        // 解析玩家限注範本
        this.parseUserLimitStakes(data.UserLimitStakes)
    }
    // 解析各桌資訊:視訊源, 自己下注資料, 百家樂下注模式
    parseTableInfoList(tableInfoList) {
        for(let index in tableInfoList) {
            let tableInfo = tableInfoList[index]
            if(tableInfo.TableName == Config.TableName.Baccarat) {
                this.createBaccaratGame(tableInfo)
            }
        }
    }
    // 創造百家樂遊戲
    createBaccaratGame(tableInfo) {
        let baccaratGame = new BaccaratGame (
            tableInfo.TableName,
            tableInfo.TableId,
            this._spotInfo,
        )
        // 更新視訊源
        baccaratGame.videoURL = tableInfo.VideoInfo.UniversalVideoUrl.MobileVideoUrl
        // 更新自己下注資料
        baccaratGame.parseSelfBettingList(tableInfo.SelfBettingList)
        // 更新百家樂下注模式
        baccaratGame.betZoneType = tableInfo.GameMode
        // 更新牌局狀態
        let table = Casino.Tables.getTableInfo(tableInfo.TableName, tableInfo.TableId)
        table.setInitialGambleTable(tableInfo.BaccaratGameStats)

        this._games[this._games.length] = baccaratGame
    }
    // 解析遊戲的限注
    parseGameLimitStake(gameLimitStake) {
        for (let index in this._games) {
            let game = this._games[index]
            if(game.tableName != gameLimitStake.TableName) continue
            game.updateGameLimitStake(gameLimitStake.Limits)
        }
    }
    // 解析玩家限注範本
    parseUserLimitStakes(userLimitStakes) {
        this._limitMgr.setUserLimitStakes(userLimitStakes)
        this._chipMgr.setUserLimitStakes(Config.TableName.MultipleTable, userLimitStakes)
    }
    // 處理單台
    processSingleTable (tableName, tableID) {
        if(this._tableName == Config.TableName.MultipleTable) return

        if(tableName == Config.TableName.Baccarat) {
            this._games[0] = new BaccaratGame(
                String(tableName),
                String(tableID), 
                this._spotInfo,
            )
        } else if(tableName == Config.TableName.Roulette) {
            this._games[0] = new BaccaratGame(
                String(tableName),
                String(tableID), 
                this._spotInfo,
            )
        }
    }
    // --------- LimitManager -----------
    get limitMgr () {
        return this._limitMgr
    }
    get currentLimitId () {
        return this._limitMgr.getCurrentLimitStake().limitId
    }
    // 取得所有的限額資訊
    getUserLimitStakes(data){    
        return this._limitMgr._userLimitStakes
    }
    // 取得正在使用的限額資訊
    getCurrentLimitStak(){
        return this._limitMgr.getCurrentLimitStake()
    }
    // 傳送使用的限額範本ID給Server
    sendLimitChoseIndex(index, callBack) {
        this._limitMgr.sendLimitChoseIndex(index, callBack)
    }
    // --------- ChipManager -----------
    get chipMgr () {
        return this._chipMgr
    }
    // 設定籌碼組正在使用的籌碼index
    set useChipIndex (useChipIndex) {
        this._chipMgr.useChipIndex = useChipIndex
    }
    // 取得正在使用的籌碼
    get choseChip () {
        return this._chipMgr.currentChip
    }
    // 取得使用者正在使用的籌碼組
    getChoseUseChips () {
        return this._chipMgr.currentChips
    }
    // 取得籌碼組正在使用的籌碼index
    getUseChipIndex () {
        return this._chipMgr.useChipIndex
    }
    // 傳送選擇的籌碼
    sendChipsChose (choseChips, onSendComplete) {
        this._chipMgr.sendChipsChose(this._tableName, choseChips, onSendComplete)
    }
    // --------- SpotMgr -----------
    // 單台用
    get spotMgr () {
        return this._games[0].spotMgr
    }
    set betSum (betSum) {
        this._games[0].spotMgr.betSum = betSum
    }
    get betSum () {
        return this._games[0].spotMgr.betSum
    }
    // 多台用
    getSpotMgr (tableName, tableID) {
        return this.getGame(tableName, tableID).spotMgr
    }
    setBetSum (tableName, tableID, betSum) {
        this.getGame(tableName, tableID).spotMgr.betSum = betSum
    }
    getBetSum (tableName, tableID) {
        return this.getGame(tableName, tableID).spotMgr.betSum
    }
    // ===== 下注
    bet (tableName, tableID, spotId, betAmount, callBack) {
        this.getGame(tableName, tableID).spotMgr.bet(tableName, tableID, this.currentLimitId, spotId, betAmount, function(resp, error){
            ++this.resetNoBetTimeCount
            if(callBack != null) callBack(resp, error)
        }.bind(this))
    }
    // ===== 重新下注
    rebet (tableName, tableID, callBack) {
        let game = this.getGame(tableName, tableID)
        let betZoneType = game.betZoneType
        game.spotMgr.rebet(tableName, tableID, this.currentLimitId, betZoneType, function(resp){
            ++this.resetNoBetTimeCount
            if(callBack != null) callBack(resp)
        }.bind(this))
    }
    // ===== 取消下注
    cancelBet (tableName, tableID, callBack) {
        this.getGame(tableName, tableID).spotMgr.cancelBet(tableName, tableID, function(resp){
            ++this.resetNoBetTimeCount
            if(callBack != null) callBack(resp)
        }.bind(this))
    }
    // --------- 離桌 -----------
    leaveGame (successFun, failFun){
        // 單台離桌
        if(this._tableName != Config.TableName.MultipleTable) {
            let toTableId = "" //這參數可以用來決定換桌
            Casino.User.leave(this._tableName, this._tableID, toTableId, function(cmd, resp, error) {
                this.leaveGameCallback(resp, error, successFun, failFun)
            }.bind(this))
            return
        }
        // 多台離桌
        Casino.User.multipleTableLeave(function(cmd, resp, error) {
            // 移除所有物桌台後 ， 再轉換場景
            let canvas = cc.director.getScene().getChildByName('Canvas')
            let multipleGamesContainer = canvas.getComponentInChildren('multipleGamesContainer')
            if(multipleGamesContainer != null) { 
                Tool.getLoadingView().progressBar.progress = 0
                Tool.getLoadingView().loadingLabel.dataID = ""
                Tool.openLoadingView()
                multipleGamesContainer.removeTable(function() {
                    this.leaveGameCallback(resp, error, successFun, failFun) 
                }.bind(this))
            } else {
                this.leaveGameCallback(resp, error, successFun, failFun)            
            }
        }.bind(this))
    }
    leaveGameCallback(resp, error, successFun, failFun) {
        if (error != null){
            failFun(resp, error)
            return
        }

        if (resp == 0) {   
            // 停止接收Server資料
            this.stoplistenRecvServerData()
            successFun()
        } else {
            failFun(resp, error)
        }
    }
    // --------- 停止接收Server資料 -----------
    stoplistenRecvServerData () {
        for (let index in this._games) {
            let game = this._games[index]
            game.stoplistenRecvServerData()
        }
        if(this.recvServerDataEvtId == null) return
        NativeConnect.stoplistenRecvServerData(this.recvServerDataEvtId)
        this.recvServerDataEvtId = null
    }
    // --------- ------ -----------
    // 變更標準，免水或西洋
    changeBetZoneType (tableName, tableID, betZoneType, callBack) {
        this.getGame(tableName, tableID).changeBetZoneType(betZoneType, function(cmd, respData, error){
            ++this.resetNoBetTimeCount
            if(callBack != null) callBack(cmd, respData, error)
        }.bind(this))
    }
    // 取得即時彩池資料
    getBetingPool (tableName, tableID, spotId) {
        return this.getGame(tableName, tableID).getBetingPool(spotId)
    }
    // 取得遊戲結果
    getResult (tableName, tableID) {
        return this.getGame(tableName, tableID).result
    }
    // 取得影片網址
    getVideoURL (tableName, tableID) {
        return this.getGame(tableName, tableID).videoURL
    }
    // --------- ------ -----------
    // 回傳現在是不是水立方百家樂桌檯
    // 判斷是否為水立方
    static get isWaterCube() {
        // 當不在Game Scene時，一定不是水立方
        if(cc.director.getScene().name != Config.SceneName.Game) {
            return false
        }

        let game = Casino.Game
        // 當不在遊戲桌時，一定不是水立方
        if(game == null) return false
        // 多台也當作不是水立方
        if(game.tableName == Config.TableName.MultipleTable) return false
        // 沒有登入時，一定不是水立方
        if(Casino.Tables == null) return false

        let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
        // 找不到桌檯資料，當作不是水立方
        if(table == null) return false

        let videoProvider = table.videoProvider
        return (videoProvider == Config.VideoProvider.WaterCube)
    }

    static get isPlayingWaterCubeVideoEffect() {
        return false
        // if(Casino.User == null) return false
        // return (Game.isWaterCube && Casino.User.videoEnabled == true)
    }
}