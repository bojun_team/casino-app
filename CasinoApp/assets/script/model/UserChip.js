import Config from '../constants/Config'

export default class UserChip {
    constructor (data) { 
        this._gameName = data["GameName"]
        this._limitId = data["LimitId"]
        this._maxLimit = data["MaxLimit"]
        this._minLimit = data["MinLimit"]
        this._selected = data["Selected"]
        this._chip = data["Chip"]
    }
    get gameName () {
        return this._gameName
    }
    get limitId () {
        return this._limitId
    }
    get maxLimit () {
        return this._maxLimit
    }
    get minLimit () {
        return this._minLimit
    }
    get selected () {
        return this._selected == 1
    }
    set chip (chip) {
        this._chip = chip
    }
    get chip () {
        return this._chip
    }

    isChipAllow(chip) {
        const maxLimitNum = Number(this._maxLimit)
        const minLimitNum = Number(this._minLimit)
        var chipNum = Number(chip)
        return (chipNum <= maxLimitNum && chipNum >= minLimitNum)
    }
    isHaveSelectNotAllowChip(){
        for (var key in this.chip){
            let chipNum = Number(this.chip[key])
            if (!this.isChipAllow(chipNum)) {
                return true
            }
        }
        return false
    }
    getAllowChips () {
        const maxLimitNum = Number(this._maxLimit)
        const minLimitNum = Number(this._minLimit)
        this._allowChips = []
        let index = 0
        for (var key in Config.chips){
            let chipNum = Number(Config.chips[key])
            if (chipNum <= maxLimitNum && chipNum >= minLimitNum) {
                this._allowChips[index] = chipNum
                index++
            }
        }
        return this._allowChips
    }
    resetChips(){
        const allowChips = this.getAllowChips()
        
        this.chip = []
        for (var index = 0; index < allowChips.length; index++) {
            this.chip[index] = allowChips[index]
            if (index >= 2) {
                return this.chip
            }
        }
        return this.chip
    }
}
