import User from './User'
import Tables from './Tables'
import Game from './Game'
import AppWebAPI from '../login/AppWebAPI'
import BrowserAPI from '../login/BrowserAPI'
import Tool from '../tool/Tool'
import ReturnMoneyAlert from '../popupView/ReturnMoneyAlert'
import HotUpdate from '../../hotUpdate/script/module/HotUpdate'
import NativeConnect from '../native/NativeConnect'
import MusicControl from '../tool/MusicControl'
import Localize  from '../constants/Localize'
import NumberKeyboard from '../model/NumberKeyboard'

import Rx from 'rxjs'

var _isResizeVideo = new Rx.BehaviorSubject(false)
var _isFullScreen = new Rx.BehaviorSubject(false)
var _isPortrait = new Rx.BehaviorSubject(false)

export default class Casino {
    constructor() {
        this._user = new User() // User : 用者資訊
        this._tables = new Tables() // Table: 桌檯資訊
        this._game = new Game() // Game : 遊戲資訊
    }

    static get User() {
        return _casino._user
    }

    static NewUser() {
        if(_casino._user != null) {
            _casino._user.stoplistenRecvServerData()
        }
        _casino._user = new User()
    }

    static NewTables() {
        if(_casino._tables != null) {
            _casino._tables.stoplistenRecvServerData()
        }
        _casino._tables = new Tables()
    }

    static setTablesNull() {
        _casino._tables = null
    }

    static get Tables() {
        return _casino._tables
    }

    static set Game(game) {
        if(_casino._game != null) {
            _casino._game.stoplistenRecvServerData()
        }
        _casino._game = game
    }

    static get Game() {
        return _casino._game
    }

    static get isPlayingWaterCubeVideoEffect() {
        return Game.isPlayingWaterCubeVideoEffect
    }

    // 訊號不穩-退錢提示視窗 (2018.03.24)
    static showReturnMoneyAlert() {
        ReturnMoneyAlert.show()
    }

    static get AppWebAPI() { return AppWebAPI }
    static get BrowserAPI() { return BrowserAPI }
    static get Tool() { return Tool }
    static get HotUpdate() {return HotUpdate}
    static get NativeConnect() {return NativeConnect}
    static get MusicControl() {return MusicControl}

    static get isResizeVideo$() {return _isResizeVideo}
    static get isFullScreen$() {return _isFullScreen}
    static get isPortrait$() {return _isPortrait}
    static get Localize() {return Localize}
    static get NumberKeyboard() { return NumberKeyboard }
}

var _casino = new Casino()
window.Casino = _casino