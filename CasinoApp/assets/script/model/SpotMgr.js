import Spot from './Spot'
import Rx from 'rxjs' 
import Localize from '../constants/Localize'
import localizeMgr from '../tool/localize/LocalizeMgr'
import Tool from "../tool/Tool"
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

export default class SpotMgr {
    //spotsInfo: 從http get上取得的資料
    constructor (spotsInfo) { 
        this._spots = [new Spot()]
        this._spots.length = 0

        this._propChange$ = {
            canRebet: new Rx.BehaviorSubject(false),
            betSum: new Rx.BehaviorSubject(0), // 下注總額
        }

        this.createSpots(spotsInfo)
    }
    setCanRebet(canRebet) {
        this._propChange$.canRebet.next(canRebet)
    }
    set betSum(betSum){
        this._propChange$.betSum.next(betSum)
    }
    get betSum () {
        return this._propChange$.betSum.getValue()
    }
    get canRebet(){
        for(var key in this._spots) { 
            if (this._spots[key].lastChipAmount > 0) return true
        }
        return false 
    }
    get propChange$() {
        return this._propChange$
    } 
    createSpots(spotsInfo){
        for(var key in spotsInfo) {  
            let spot = new Spot(spotsInfo[key]) 
            this._spots[spotsInfo[key].spot_id] = spot
            
        } 
    }
    // 更新注區的限額
    parserLimitStake(gameLimitStake) {
        for (var key in gameLimitStake) {
            var limitData = gameLimitStake[key] 
            var applySpotIds = limitData["ApplySpotIds"]
            // 設定注區的限額
            for (var index in applySpotIds) {
                if (this._spots[applySpotIds[index]] != null)
                    this._spots[applySpotIds[index]].setLimitStake(limitData)  
            }
        }
    }
    // 分析封包 getSelfBettingList
    parseSelfBettingList(data, currentRound, currentRun){
        // 清除下注金額及上一次下注金額
        this.clearLastChipAmount ()
        
        // 如果沒有資料，則將下注金額設成零
        if (data == null) {
            this.betSum = 0
            return 
        }

        // 加總相對應注區ID的金額
        data.forEach(function(bettingInfo){
            if(currentRound != bettingInfo.Round || currentRun != bettingInfo.Run) return
            this._spots[bettingInfo.SpotId].chipAmount += bettingInfo.SpotAmount
        }.bind(this))

        // 加總下注金額
        this.sumAllbet()
    }
    // 加總下注區域籌碼
    getAllAmount(amounts) {
        let amount = 0
        for(let key in amounts) {
            let data = amounts[key]
            amount += Number(data["Item2"])
        }
        return amount
    } 
    //取得注區資訊
    // spotId: 注區 id
    getSpot(spotId){
        return this._spots[spotId]
    }
    cleanAndSaveChipAmount (){
        for(var key in this._spots){
            this._spots[key].cleanAndSaveChipAmount()
        }
        this.setCanRebet(this.canRebet)
        this.betSum = 0
    }
    
    clearLastChipAmount(){
        for(var key in this._spots){
            this._spots[key].clearLastChipAmount()
        }
        this.setCanRebet(false)
        this.betSum = 0
    }
    // 玩家再投注是否超過注區限額上限
    // spotId: 注區 id
    isAddAmountOverSpotMaxLimit(spotId, betAmount, currentLimitId){
        const maxLimit = this._spots[spotId].getGameLimitMaxStake(currentLimitId)
        return (this._spots[spotId].chipAmount + betAmount) > maxLimit
    }
    // 取得注區還剩多少可投注金額
    // spotId: 注區 id
    getSpotRemainMaxLimit(spotId, currentLimitId){
        const maxLimit = this._spots[spotId].getGameLimitMaxStake(currentLimitId)
        return maxLimit - this._spots[spotId].chipAmount
    }
    // 目前注區是否高於或等於注區上限
    // spotId: 注區 id
    isOverSpotMaxLimit(spotId, currentLimitId){
        const maxLimit = this._spots[spotId].getGameLimitMaxStake(currentLimitId)
        return this._spots[spotId].chipAmount >= maxLimit
    }
    // 是否低於注區下限
    // spotId: 注區 id
    isLowerSpotMiniLimit(spotId, betAmount, currentLimitId){
        const miniLimit = this._spots[spotId].getGameLimitMiniStake(currentLimitId)
        const total = this._spots[spotId].chipAmount + betAmount 
        return total < miniLimit
    } 
    // ====== 下注
    bet (tableName, tableID, limitId, spotId, betAmount, callBack) {
        // 判斷目前注區是否高於或等於注區上限  
        if (this.isOverSpotMaxLimit(spotId, limitId)) {
            callBack(Config.betErrorId.betIsMax)
            return
        }
        
        // 判斷目前注區是否低於注區下限
        if (this.isLowerSpotMiniLimit(spotId, betAmount, limitId)) {
            callBack(Config.betErrorId.lowerBetMiniLimit)
            return
        }
        // 如果玩家再投注超過注區限額上限，將設定金額為剩下可下注的金額
        const isOverMaxLimit = this.isAddAmountOverSpotMaxLimit(spotId, betAmount, limitId)
        if (isOverMaxLimit){
            betAmount = this.getSpotRemainMaxLimit(spotId, limitId)
        } 
        
        // 傳送下注資料給Server
        SignalR.bet(tableName, tableID, spotId, betAmount, function(cmd, respData, error) {
            if (error != null){
                callBack(respData, error)
                return
            }
            let errorCode = Number(respData["GameCenterErrorCode"])
            if(Number.isNaN(errorCode) && respData != Config.requireToServerError.timeOut) {  
                errorCode = 1;
            }
            // 如果 errorCode = 0 或是超過最大下注上限
            if (errorCode == Config.GameCenterErrorCode.OperationSuccess || errorCode == Config.betErrorId.overBetMaxLimit) {
                // 放籌碼到注區上
                let respSpotId = String(respData.SpotId)
                let respAmount = Number(respData.Amount)
                this._spots[respSpotId].chipAmount += respAmount
                // 加總所有下注區域下注金額
                this.sumAllbet()

                if (isOverMaxLimit) {
                    callBack(Config.betErrorId.betOverLimit)
                    return
                }
            }
            callBack(errorCode)
        }.bind(this))
    }
    
    rebet(tableName, tableID, currentLimitId, betZoneType, callBack){
        let betInfos = []
        var index = 0
        var isNoBet = true // 目前是判斷有無資料
        let isOverMaxLimit = false
        let isLowerMiniLimit = false
        let isAddAmountOverSpotMaxLimit = false
        let totalLastChipAmount = 0

        let table = Casino.Tables.getTableInfo(tableName, tableID)

        // 處理重新下注資料
        for (var key in this._spots){ 
            // 如果無下注金額，處理下一筆
            if (this._spots[key].lastChipAmount <= 0 ) continue

            
            const spotID = key
            let lastChipAmount = this._spots[key].lastChipAmount
            // 如果標準玩法超過30局，則不能重複下注大小單雙，處理下一筆
            if(betZoneType == Config.betZoneType.Standard && table.run > 30) {
                if( spotID == Config.spotID.Big ||
                    spotID == Config.spotID.Small ||
                    spotID == Config.spotID.PlayerOdd ||
                    spotID == Config.spotID.BankerOdd || 
                    spotID == Config.spotID.PlayerEven ||
                    spotID == Config.spotID.BankerEven
                ) {
                    continue
                }
            }

            // 如果目前注區是否高於或等於注區上限，處理下一筆
            if (this.isOverSpotMaxLimit(spotID, currentLimitId)){
                isOverMaxLimit = true
                continue
            }
 
            // 如果目前注區是否低於注區下限，處理下一筆
            if (this.isLowerSpotMiniLimit(spotID, lastChipAmount, currentLimitId)){
                isLowerMiniLimit = true
                continue
            }
 
            // 如果玩家再投注超過注區限額上限，將設定金額為剩下可下注的金額
            if (this.isAddAmountOverSpotMaxLimit(spotID, lastChipAmount, currentLimitId)){
                lastChipAmount = this.getSpotRemainMaxLimit(spotID, currentLimitId)
                isAddAmountOverSpotMaxLimit = true
            }
 
            totalLastChipAmount = totalLastChipAmount + lastChipAmount
            betInfos[index] = {"SpotId" : spotID, AmountList:[lastChipAmount]}
            isNoBet = false
            index++
 
        }
        
        // 如果帳戶額度不足，顯示額度不足訊息
        if (Number(Casino.User.usercredit) < totalLastChipAmount) {
            let errorCode = Config.GameCenterErrorCode.NotSufficientCredit
            let dataID = Localize.betFailMsg(errorCode)
            Tool.showToast(dataID, Config.TOAST_SHOW_TIME_DEFAULT, [errorCode])
            callBack(Config.betErrorId.betOverCredit)
            return
        }
        
        // 如果沒有下注資料，回傳相對應錯誤訊息
        if (isNoBet) {
            // 該注區限額已滿
            if (isOverMaxLimit) callBack(Config.betErrorId.betIsMax)
            // 下注金額低於注區押注限額
            if (isLowerMiniLimit) callBack(Config.betErrorId.rebetLowerBetMiniLimit)
            return
        }
        // 傳送重新下注給Server
        SignalR.multipBet(tableName, tableID, betInfos, function(cmd, respData, error) {
            if (error != null){
                callBack(respData, error)
                return
            }

            let errorCode =  0
            for(let index in respData) {
                let bettingSlip = respData[index]
                errorCode = Number(bettingSlip.GameCenterErrorCode)

                if(errorCode != Config.GameCenterErrorCode.OperationSuccess) continue
                // 放籌碼到注區上
                let respSpotId = String(bettingSlip.SpotId)
                let respAmount = Number(bettingSlip.Amount)
                this._spots[respSpotId].chipAmount += respAmount
                // 加總所有下注區域下注金額
                this.sumAllbet()
            }

            if (Number(errorCode) == 0) {
                // 如果玩家再投注超過注區限額上限
                if (isAddAmountOverSpotMaxLimit){
                    callBack(Config.betErrorId.betOverLimit)
                    return
                }
                // 如果目前注區是否高於或等於注區上限
                else if (isOverMaxLimit){
                    callBack(Config.betErrorId.betIsMax)
                    return
                }
                // 如果目前注區是否低於注區下限
                else if(isLowerMiniLimit ){
                    callBack(Config.betErrorId.rebetLowerBetMiniLimit)
                    return
                }
            }
            callBack(errorCode)
        }.bind(this))
    }
    // ====== 取消下注
    cancelBet(tableName, tableID, callBack){
        // 傳送取消下注給Server
        SignalR.revokeAllBet(tableName, tableID, function(cmd, resp, error) {
            
            if (error != null){
                callBack(resp, error)
                return
            }           
            if (resp == 0) {
                 // 清空下注區域籌碼及下注總額
                for (var key in this._spots){ 
                    this._spots[key].chipAmount = 0
                }
                this.betSum = 0
            } else {
                callBack(resp)
            }
        }.bind(this))
    }
    // 加總所有下注區域下注金額
    sumAllbet(){
        var sum = 0
        for (var spotId in this._spots){
            sum = sum + this._spots[spotId].chipAmount
        }
        this.betSum = sum
    }
}