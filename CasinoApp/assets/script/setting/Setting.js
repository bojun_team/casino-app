
import Game from '../model/Game'
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import Casino from '../model/Casino'
import Tool from '../tool/Tool'
import localizeMgr from '../tool/localize/LocalizeMgr'
cc.Class({
    extends: cc.Component,

    properties: {
        view: cc.Node,

        bgMusicCheckBox:require("CheckBox"),
        effectCheckBox:require("CheckBox"),
        videoCheckBox:require("CheckBox"),
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }  
    },
    // use this for initialization
    onLoad: function () { 
        this.subscribes = []

        let isOpenSetting$ = Casino.User.propChange$.isOpenSetting
        this.subscribes[this.subscribes.length] = isOpenSetting$.subscribe({
            next: isOpenSetting => {
                this.view.active = isOpenSetting
                this.setAllCheckBox()                  
            }
        }) 
    }, 
    onHistory:function(){  
        let url = Config.getReportURL(Casino.User.dToken)
        let labelString = localizeMgr.getLocalizedString("HistoryBetList", localizeMgr.getCurrentLanguage())

        if(cc.sys.isBrowser) {
            url = `http://${casino.domain}/player/report?token=${casino.rToken}`
            console.log('jun onHistory url ', url)
        }

        NativeConnect.openFullScreenWebView(url, labelString)
    },
    onRule:function(){ 
        let url = Config.getRuleURL(Casino.User.cashType)
        let labelString = localizeMgr.getLocalizedString("PlayRule", localizeMgr.getCurrentLanguage())
        NativeConnect.openFullScreenWebView(url, labelString)
    },
    onCustomer:function(){
        let username = "";
        if(Casino.User && Casino.User.username) {
            username = "&username=" + Casino.User.username;
        }
        let url =  Config.getCustomerURL(Casino.User.cashType) + username;
        let labelString = localizeMgr.getLocalizedString("CustomerService", localizeMgr.getCurrentLanguage())
        NativeConnect.openFullScreenWebView(url, labelString) 
    },
    onMusic:function(isCheck){
        Casino.User.musicEnabled = isCheck
    },
    onEffect:function(isCheck){
        Casino.User.effectEnabled = isCheck
    },
    onVideo:function(isCheck){
        Casino.User.videoEnabled = isCheck
    },
    onVideoEffect:function(isCheck){
        Casino.User.videoEffectEnabled = isCheck
    },
    onBackBtn:function(){
        Casino.User.isOpenSetting = false
    },
    setAllCheckBox:function(){
        this.bgMusicCheckBox.setCallBackFun(this.onMusic.bind(this))
        this.effectCheckBox.setCallBackFun(this.onEffect.bind(this))
        this.videoCheckBox.setCallBackFun(this.onVideo.bind(this))

        this.bgMusicCheckBox.setIsCheck(Casino.User.musicEnabled)
        this.effectCheckBox.setIsCheck(Casino.User.effectEnabled)
        this.videoCheckBox.setIsCheck(Casino.User.videoEnabled)
    }
    
});
