import $ from 'jquery'
import Config from '../constants/Config'

module.exports = {
    connectToServer(url = '') {
        window.jQuery = $
        require('ms-signalr-client')
        this.hubConnection = $.hubConnection(url)
        this.proxy = this.hubConnection.createHubProxy("casinoHub")
        Config.SIGNAL_R_PROXY_LIST.forEach((method) => {
            this.proxy.on(method, (data) =>  {
                let jsonData = { "M": method, "A": [data] }
                let json = JSON.stringify(jsonData)
                window.NativeConnect.onRecvServerData(json)
            })
        })
        this.hubConnection.start()
        .done(() => {
            window.NativeConnect.onRecvConnectionInfo(Config.ConnectState.CONNECTED, null)
        })
        .fail((err) => {
            window.NativeConnect.onRecvConnectionInfo(Config.ConnectState.ERROR, error)
        })
        this.hubConnection.disconnected(()=>{
            window.NativeConnect.onRecvConnectionInfo(Config.ConnectState.DISCONNECTED, null)
        })
        this.hubConnection.reconnecting(() => {
            window.NativeConnect.onRecvConnectionInfo(Config.ConnectState.RECONNECTING, null)
        })
    },
    disconnect() {
        this.proxy = null
        if(this.hubConnection == null) return
        this.hubConnection.stop()
        this.hubConnection = null
    },
    listenRecvConnectInfo(recvConnectInfoEvent=function(state='', info=''){}) {
        window.NativeConnect.listenRecvConnectInfo(recvConnectInfoEvent)
    },
    requireServer(callBack=function(cmd, resp, error){}, ...data) {
        if(this.proxy == null) return
        let timeOutNum = 0
        let isTimeOut = false
        timeOutNum = setTimeout(() => {
            isTimeOut = true
            callBack(data[0], Config.requireToServerError.timeOut, null)
        }, Config.apiBreakTime)

        let invoke = this.proxy.invoke.apply(this.proxy, data)
        invoke.done((resp) => {
            clearTimeout(timeOutNum)
            if (isTimeOut) return
            console.log('requireServer done cmd:' + data[0], ' resp:', resp)
            callBack(data[0], resp, null)
        })
        .fail((error) => {
            clearTimeout(timeOutNum)
            if (isTimeOut) return
            console.log('requireServer error cmd:' + data[0], ' error:', error)
            callBack(data[0], null, error)
        })
    },
    // 登入
    login(loginData, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.LOGIN, loginData)
    },
    // 設定玩家關注的注區ID
    setPushBettingPoolSpotId(spotIDList=[], callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.SET_PUSH_BETTING_POOL_SPOT_ID, spotIDList)        
    },
    // 單桌入桌
    seat(tableName='', tableId='', callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.SEAT, tableName, tableId)
    },
    // 單桌離桌
    leave(tableName='', fromTableId='', toTableId='', callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.LEAVE, tableName, fromTableId, toTableId)
    },
    // 多台入桌
    multipleTableSeat(tableNameInfos, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.MULTIPLE_TABLE_SEAT, tableNameInfos)
    },
    // 多台離桌
    multipleTableLeave(callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.MULTIPLE_TABLE_LEAVE)
    },
    // 單桌換限額
    updateUserLimitStake(tableName='', limitId=0, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.UPDATE_USER_LIMIT_STAKE, tableName, limitId)
    },
    // 多桌換限額
    updateMultipleTableUserLimitStake(limitId=0, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.UPDATE_MULTIPLE_TABLE_USER_LIMIT_STAKE, limitId)
    },
    // 更新籌碼
    updatePersonalChipSetting(platFormId=1, tableName='', limitId=0, choseChips='', callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.UPDATE_PERSONAL_CHIPS_SETTING, platFormId, tableName, limitId, choseChips)
    },
    // 改變下注玩法
    updateBaccaratGameMode(tableName='', tableID='', betZoneType=0, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.UPDATE_BACCARAT_GAME_MODE, tableName, tableID, betZoneType)
    },
    // 單筆下注
    bet(tableName='', tableID='', spotId='', betAmount=0, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.BET, tableName, tableID, spotId, betAmount)
    },
    // 多筆下注
    multipBet(tableName='', tableID='', betInfos=[{SpotId:'', AmountList:[0]}], callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.MULTIP_BET, tableName, tableID, betInfos)
    },
    // 取消全部下注
    revokeAllBet(tableName='', tableID='', callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.REVOKE_ALL_BET, tableName, tableID)
    },
    // 玩家傳回是否繼續比倍的決定
    acceptJackpotBigWin(acceptBigWinResponse=0, callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.ACCEPT_JACKPOT_BIG_WIN, acceptBigWinResponse)
    },
    /** 傳送現場驗證碼 */
    liveAuth(tableName="", tableId="", authCode="", callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.LIVE_AUTH, tableName, tableId, authCode);
    },
    /** 取得開放時間 */
    getOpeningTimes(callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.GET_OPENING_TIMES);
    },
    /** 取得桌檯狀態 */
    getBaccaratTableStatus(callBack=function(cmd='', resp=0, error=''){}) {
        this.requireServer(callBack, Config.SignalRCommand.GET_BACCARAT_TABLE_STATUS);
    }
}
