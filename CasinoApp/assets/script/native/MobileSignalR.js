import Config from '../constants/Config'

module.exports = {
    connectToServer(url = '') {
        window.NativeConnect.connect(url)
    },
    disconnect() {
        window.NativeConnect.disconnect()
    },
    listenRecvConnectInfo(recvConnectInfoEvent=function(state='', info=''){}) {
        window.NativeConnect.listenRecvConnectInfo(recvConnectInfoEvent)
    },
    requireServer:function(cmd='', data=[], callBack) {
        let timeOutNum = 0
        let isTimeOut = false
        timeOutNum = setTimeout(() => {
            isTimeOut = true
            callBack(data[0], Config.requireToServerError.timeOut, null)
        }, Config.apiBreakTime)

        let json = JSON.stringify(data)
        window.NativeConnect.requireServer(cmd, json, (callbackCmd, resp, error)=> {
            clearTimeout(timeOutNum)
            callBack(callbackCmd, resp, error)
        })
    },
    // 登入
    login(loginData, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [loginData]
        this.requireServer(Config.SignalRCommand.LOGIN, data, callBack)        
    },
    // 設定玩家關注的注區ID
    setPushBettingPoolSpotId(spotIDList=[], callBack=function(cmd='', resp=0, error=''){}) {
        let data = [spotIDList]
        this.requireServer(Config.SignalRCommand.SET_PUSH_BETTING_POOL_SPOT_ID, data, callBack)
    },
    // 單桌入桌
    seat(tableName='', tableId='', callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, tableId]
        this.requireServer(Config.SignalRCommand.SEAT, data, callBack)
    },
    // 單桌離桌
    leave(tableName='', fromTableId='', toTableId='', callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, fromTableId, toTableId]
        this.requireServer(Config.SignalRCommand.LEAVE, data, callBack)
    },
    // 多台入桌
    multipleTableSeat(tableNameInfos, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableNameInfos]
        this.requireServer(Config.SignalRCommand.MULTIPLE_TABLE_SEAT, data, callBack)
    },
    // 多台離桌
    multipleTableLeave(callBack=function(cmd='', resp=0, error=''){}) {
        let data = []
        this.requireServer(Config.SignalRCommand.MULTIPLE_TABLE_LEAVE, data, callBack)
    },
    // 單桌換限額
    updateUserLimitStake(tableName='', limitId=0, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, limitId]
        this.requireServer(Config.SignalRCommand.UPDATE_USER_LIMIT_STAKE, data, callBack)
    },
    // 多桌換限額
    updateMultipleTableUserLimitStake(limitId=0, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [limitId]
        this.requireServer(Config.SignalRCommand.UPDATE_MULTIPLE_TABLE_USER_LIMIT_STAKE, data, callBack)
    },
    // 更新籌碼
    updatePersonalChipSetting(platFormId=1, tableName='', limitId=0, choseChips='', callBack=function(cmd='', resp=0, error=''){}) {
        let data = [platFormId, tableName, limitId, choseChips]
        this.requireServer(Config.SignalRCommand.UPDATE_PERSONAL_CHIPS_SETTING, data, callBack)
    },
    // 改變下注玩法
    updateBaccaratGameMode(tableName='', tableID='', betZoneType=0, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, tableID, betZoneType]
        this.requireServer(Config.SignalRCommand.UPDATE_BACCARAT_GAME_MODE, data, callBack)
    },
    // 單筆下注
    bet(tableName='', tableID='', spotId='', betAmount=0, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, tableID, spotId, betAmount]
        this.requireServer(Config.SignalRCommand.BET, data, callBack)
    },
    // 多筆下注
    multipBet(tableName='', tableID='', betInfos=[{SpotId:'', AmountList:[0]}], callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, tableID, betInfos]
        this.requireServer(Config.SignalRCommand.MULTIP_BET, data, callBack)
    },
    // 取消全部下注
    revokeAllBet(tableName='', tableID='', callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, tableID]
        this.requireServer(Config.SignalRCommand.REVOKE_ALL_BET, data, callBack)
    },
    // 玩家傳回是否繼續比倍的決定
    acceptJackpotBigWin(acceptBigWinResponse=0, callBack=function(cmd='', resp=0, error=''){}) {
        let data = [acceptBigWinResponse]
        this.requireServer(Config.SignalRCommand.ACCEPT_JACKPOT_BIG_WIN, data, callBack)
    },
    /** 傳送現場驗證碼 */
    liveAuth(tableName="", tableId="", authCode="", callBack=function(cmd='', resp=0, error=''){}) {
        let data = [tableName, tableId, authCode]
        this.requireServer(Config.SignalRCommand.LIVE_AUTH, data, callBack)
    },
    /** 取得開放時間 */
    getOpeningTimes(callBack=function(cmd='', resp=0, error=''){}) {
        let data = []
        this.requireServer(Config.SignalRCommand.GET_OPENING_TIMES, data, callBack)
    },
    /** 取得桌檯維護狀態 */
    getBaccaratTableStatus(callBack=function(cmd='', resp=0, error=''){}) {
        let data = []
        this.requireServer(Config.SignalRCommand.GET_BACCARAT_TABLE_STATUS, data, callBack)
    }
}
