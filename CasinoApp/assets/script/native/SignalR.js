

import MobileSignalR from './MobileSignalR'
import WebSignalR from './WebSignalR'

if(!cc.sys.isBrowser) {
    module.exports = MobileSignalR
} else {
    module.exports = WebSignalR
}