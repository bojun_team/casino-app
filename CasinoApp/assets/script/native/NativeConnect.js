import Config from '../constants/Config'
 
 window.NativeConnect = {   
    //-----------
    exit:function(){
        if (cc.sys.isBrowser) {
            window.close()
            window.location.href = null
            return
        }

         if (cc.sys.os == cc.sys.OS_IOS) 
            jsb.reflection.callStaticMethod("JSConnect", "exit");
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "exit","()V")
        else {  
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        } 
    },
    //------------signal R
    connect:function(url){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS) 
            jsb.reflection.callStaticMethod("JSConnect", "connectToServerWithURL:",url);
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "connectToServer","(Ljava/lang/String;)V",url);
        else { 
            // this.onRecvConnectionInfo("ConnectionDidClose")
            this.onRecvConnectionInfo(1)
            this.nlog("connect 不支持平台 ：", cc.sys.os)
        } 
    },
    //------------signal R
    disconnect:function(){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS) 
            jsb.reflection.callStaticMethod("JSConnect", "disconnect");
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "disconnect","()V");
        else {  
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        } 
    },
    //------------------------
    // 監聽 SignalR的資訊
    listenRecvConnectInfo:function(fun){
        this.recvConnectInfoEvent = fun
    },
    // 收到 SignalR的資訊
    onRecvConnectionInfo:function(state, info){
        if (this.recvConnectInfoEvent != null) this.recvConnectInfoEvent(state, info)
    },
    //------------server 廣播的資訊------------
    // 收server 廣播的資訊
    onRecvServerData:function(json){ 
        let decMsg = json
		if(json	!= "NULL")
		{ 
        	if (cc.sys.os == cc.sys.OS_IOS) 
            	decMsg = decodeURI(json); 
        	else if (cc.sys.os == cc.sys.OS_ANDROID) 
            	decMsg =  decodeURIComponent(json); 
		}
        let data = JSON.parse(decMsg) 
        if (data == null)
            this.nlog("data == null json = ", json)
        else
        {
            for (let key in this.recvDataCallBack){
                this.recvDataCallBack[key](data)
            }
        }
    },
    // 監聽 廣播的資訊
    listenRecvServerData:function(fun)
    {
        if (this.recvDataCallBack == null) {
            this.recvDataCallBack = {}
            this.recvDataSN = 1  
        } 
        this.recvDataCallBack[this.recvDataSN] = fun
        this.recvDataSN++ 
        return this.recvDataSN - 1
    },
    // 停止 監聽 廣播的資訊
    stoplistenRecvServerData:function(evtID)
    { 
        delete this.recvDataCallBack[evtID]
    },
    //----------對server 發送資訊-------------- 
    // 對server 發送資訊
    requireServer:function(cmd, json, callBack)
    {
        if (this.requireServerCallBack == null) {
            this.requireServerCallBack = {}
            this.requireServerSN = 1
        }
        this.requireServerCallBack["cb" + this.requireServerSN] = callBack 
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "requireServerWithCmd:jsonData:callBackName:", cmd, json, this.requireServerSN); 
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "requireServerWithCmd", "(Ljava/lang/String;Ljava/lang/String;I)V", cmd, json, this.requireServerSN);
        else {  
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
            if (cmd == "bet" || cmd == "MultipBet"){
                this.onRecvRequireServer(cmd, "{\"data\":{\"GameCenterErrorCode\":0,\"Amount\":1000}}", "null", this.requireServerSN)
            }
            else
                this.onRecvRequireServer(cmd, "{\"data\":0}", "null", this.requireServerSN)
        } 
        this.requireServerSN ++ 
    }, 
    // 收到 server 回應的資訊
    onRecvRequireServer:function(cmd, json, errorMsg, cbSN){
        let fun = this.requireServerCallBack["cb"+cbSN] 
        if (errorMsg != "null" ){
            fun(cmd, json, errorMsg)
        }
        var decMsg = json
        if(json != "NULL")
        { 
            if (cc.sys.os == cc.sys.OS_IOS)
                decMsg = decodeURI(json)
            else if (cc.sys.os == cc.sys.OS_ANDROID)
                decMsg =  decodeURIComponent(json); 
        }
        var data = JSON.parse(decMsg)
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID)
            fun(cmd, data["data"])
        else
            fun(cmd, data["data"])
        delete this.requireServerCallBack["cb"+cbSN]
    }, 
    
    //------------video
    // 監聽 Video的資訊
    listenVideoInfo:function(fun)
    {
        this.recvVideoInfoCallBack = fun  
    },
    // 收到 底層的資訊
    onVideoInfo:function(cmd, info){
        if (this.recvVideoInfoCallBack != null)
            this.recvVideoInfoCallBack(cmd,info)
    },
    playVideo: function(x, y, w, h, url, clipW, clipH, offsetY, isVideoMute) { 
        if (cc.sys.isBrowser){
            if(window.playVideo == null) return
            window.playVideo(x, y, w, h, url, clipW, clipH, offsetY, isVideoMute)
            return
        }
        
        if (cc.sys.os == cc.sys.OS_IOS) 
            jsb.reflection.callStaticMethod("JSConnect", "playVideoWithX:y:w:h:url:clipW:clipH:offsetY:isVideoMute:", x, y, w, h, url, clipW, clipH, offsetY, isVideoMute); 
        else if (cc.sys.os == cc.sys.OS_ANDROID) 
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "playVideo", "(IIIILjava/lang/String;IIIZ)V", x, y, w, h, url, clipW, clipH, offsetY, isVideoMute); 
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os) 
        } 
    }, 
    pauseVideo:function(){
        if (cc.sys.isBrowser){
            if(window.pauseVideo == null) return
            window.pauseVideo()
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "pauseVideo"); 
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "pauseVideo", "()V");
        else {  
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        } 
    },
    setVideoVisbile:function(isVisible)
    {
        if (cc.sys.isBrowser) {
            if(window.setVideoVisbile == null) return
            window.setVideoVisbile(isVisible)
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "setVideoVisible:", isVisible); 
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "setVideoVisible", "(Z)V", isVisible);
        } else {  
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        } 
    },
    setVideoMute:function (isVideoMute) {
        if (cc.sys.isBrowser){
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "setVideoMute:", isVideoMute); 
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "setVideoMute", "(Z)V", isVideoMute);
        } else {  
            this.nlog("setVideoMute 不支持平台 ：", cc.sys.os)
        } 
    },
    stopVideo:function(){
        if (cc.sys.isBrowser){
            if(window.stopVideo == null) return
            window.stopVideo()
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "stopVideo");
            else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "stopVideo", "()V");
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        } 
    },
    continueVideo:function(){
        if (cc.sys.isBrowser){
            if(window.continueVideo == null) return
            window.continueVideo()
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "continueVideo");
            else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "continueVideo", "()V");
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        } 
    },
    setVideoScale(isScaleToBig=false, tableID='A') {
        if (cc.sys.isBrowser){
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "setVideoScaleWithIsScale:tableID:", isScaleToBig, tableID); 
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "setVideoScale", "(ZLjava/lang/String;)V", isScaleToBig, tableID);
        } else {  
            this.nlog("setVideoScale 不支持平台 ：", cc.sys.os)
        }
    },
    setVideoScale2(isScaleToBig=false, tableID='A', scale=2.0, offsetX=0, offsetY=70) {
        if (cc.sys.isBrowser){
            return
        }

        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "setVideoScaleWithIsScale:tableID:scale:offsetX:offsetY:", isScaleToBig, tableID, scale, offsetX, offsetY); 
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "setVideoScale", "(ZLjava/lang/String;FFF)V", isScaleToBig, tableID, scale, offsetX, offsetY);
        } else {  
            this.nlog("setVideoScale 不支持平台 ：", cc.sys.os)
        }
    },
    //------------web
    openWebOnOtherApp:function(url){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "openWebOnOtherApp:",url);
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "openWeb", "(Ljava/lang/String;)V", url);
        else { 
            this.nlog("openWebOnOtherApp 不支持平台:", cc.sys.os + ' url:' + url)
        }
    },
    //------------appUpdata
    updateApp:function(url){ 
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "updataAppWithURL:", url);
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/UpdateApp", "UpdateAppDownload", "(Ljava/lang/String;)V", url);
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },
    //------------other
    openLine:function(company_id){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "openLineWithCompanyId:",company_id);
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/IntentOtherApp", "openLine", "(Ljava/lang/String;)V",String(company_id));
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },
    openFacebookMessager:function(company_id){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "openFacebookMessagerWithCompanyId:",company_id);
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/IntentOtherApp", "openFacebookMessager", "(Ljava/lang/String;)V",String(company_id));
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },
    openQQ:function(company_id){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "openQQWithCompanyId:",company_id);
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/IntentOtherApp", "openQQ", "(Ljava/lang/String;)V",String(company_id));
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },
    openWebView:function(top = 0, down = 0, left = 0, right = 0, url = ''){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "openWebWithTop:down:left:right:url:", top, down, left, right, url);
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "openWebInApp", "(FFFFLjava/lang/String;)V", top, down, left, right, url);
        }
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },
    openFullScreenWebView:function(url = '', title = ''){ 
        if (cc.sys.isBrowser) {
            window.open(url)
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS){ 
            jsb.reflection.callStaticMethod("JSConnect", "openWebFullScreenWithUrl:title:", url, title); 
        }
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "openWebFullScreenInApp", "(Ljava/lang/String;Ljava/lang/String;)V", url, title);
        }
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },
    closeWebView:function(){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "closeWeb");
        else if (cc.sys.os == cc.sys.OS_ANDROID){
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "closeWeb", "()V");
        }
        else { 
            this.nlog("sendSFSRequestWtihCmd 不支持平台 ：", cc.sys.os)
        }
    },

    getGameVersion:function(){
        if (cc.sys.isBrowser) {
            return Config.gameVersion
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            return jsb.reflection.callStaticMethod("JSConnect", "getGameVersion");
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "getGameVersion","()Ljava/lang/String;");
        else { 
            return Config.gameVersion
        }
    },
    getCountry:function(){
        if (cc.sys.isBrowser) {
            return Config.gameCountry
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            return jsb.reflection.callStaticMethod("JSConnect", "getCountry");
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "getCountry","()Ljava/lang/String;");
        else { 
            return Config.gameCountry
        }
    },
    nlog:function(strTag, strInfo){
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "nlogTag:info:", strTag, String(strInfo)); 
        else if (cc.sys.os == cc.sys.OS_ANDROID) 
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "nlog", "(Ljava/lang/String;Ljava/lang/String;)V",strTag,String(strInfo));
        else 
            cc.log(strTag, strInfo) 
    },
    //------------ deepLink
    newDeepLinkDataCallBackList: function() {
        if (this.recvDeepLinkDataCallBack == null) {
            this.recvDeepLinkDataCallBack = {}
            this.recvDeepLinkDataSN = 1  
        } 
    },

    getDecMsg: function(msg) {
        if(msg == null) return null
        if (cc.sys.os == cc.sys.OS_IOS) return decodeURI(msg)
        else if (cc.sys.os == cc.sys.OS_ANDROID) return decodeURIComponent(msg)
    },   
 
    onRecvDeepLinkData: function(jsonString) {
        let decMsg = this.getDecMsg(jsonString)
        let data = JSON.parse(decMsg)
        if (data == null) return
        this.sentDeepLinkData(data) 
    }, 
    sentDeepLinkData: function(data) {
        this.newDeepLinkDataCallBackList()
        for (let key in this.recvDeepLinkDataCallBack) {
            this.recvDeepLinkDataCallBack[key](data)
        }
    },
    // 監聽 deeplink的資訊
    listenRecvDeepLinkData:function(fun) {
       this.newDeepLinkDataCallBackList()
       this.recvDeepLinkDataCallBack[this.recvDeepLinkDataSN++] = fun
       return this.recvDeepLinkDataSN - 1
    },
    // 停止 監聽 deeplink的資訊的資訊
    stoplistenDeepLinkData:function(evtID) { 
        delete this.recvDeepLinkDataCallBack[evtID]
    },

    setCocosCreatorRunning: function() {
        if (cc.sys.isBrowser) {
            return
        }
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "setCocosCreatorRunning");
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "setCocosCreatorRunning", "()V");
        else { 
            this.nlog("setCocosCreatorRunning 不支持平台 ：", cc.sys.os)
        }
    }
    ,
    /** 修復 古老的function失聯 2019.04.02 發現 */
    /** 因年久失修，功能已不可考，為了避免錯誤訊息過度出現，所以添加回來 */
    onChangeWebUrl: function() {

    }
}

const DEEP_LINK_DATA = {
    scheme:'',
    host:'',
    queryList:{
        lang:'',
        token:'',
        platformURL:''
    }
}

module.exports = {
    //------------ server ---------------------------------------------------
    listenRecvServerData(callBack=function(data={}){}) {
        let evtID = 0
        evtID = window.NativeConnect.listenRecvServerData(callBack)
        return evtID
    }
    , stoplistenRecvServerData(evtID=0) {
        window.NativeConnect.stoplistenRecvServerData(evtID)
    }

    //------------ video ----------------------------------------------------
    , listenVideoInfo(callBack=function(cmd='',info=''){}) {
        window.NativeConnect.listenVideoInfo(callBack)
    }
    , playVideo(x=0, y=0, w=0, h=0, url='', clipW=0, clipH=0, offsetY=0, isVideoMute=true) {
        window.NativeConnect.playVideo(x, y, w, h, url, clipW, clipH, offsetY, isVideoMute)
    }
    , pauseVideo() {
        window.NativeConnect.pauseVideo()
    }
    , setVideoVisbile(isVisible=false) {
        window.NativeConnect.setVideoVisbile(isVisible)
    }
    , setVideoMute(isVideoMute=true) {
        window.NativeConnect.setVideoMute(isVideoMute)
    }
    , stopVideo() {
        window.NativeConnect.stopVideo()
    }
    , continueVideo() {
        window.NativeConnect.continueVideo()
    }
    , setVideoScale(isScaleToBig=false, tableID='A', scale=2.0, offsetX=0, offsetY=70) {
        if(window.NativeConnect.getGameVersion() == "2.2.43") {
            window.NativeConnect.setVideoScale(isScaleToBig, tableID);
        } else {
            window.NativeConnect.setVideoScale2(isScaleToBig, tableID, scale, offsetX, offsetY);
        }
    }

    //------------ web ------------------------------------------------------
    , openWebOnOtherApp(url='') {
        window.NativeConnect.openWebOnOtherApp(url)
    }
    , updateApp(url='') {
        window.NativeConnect.updateApp(url)
    }
    , openLine(companyId='') {
        window.NativeConnect.openLine(companyId)
    }
    , openFacebookMessager(companyId='') {
        window.NativeConnect.openFacebookMessager(companyId)
    }
    , openQQ(companyId='') {
        window.NativeConnect.openQQ(companyId)
    }
    , openWebView(top = 0, down = 0, left = 0, right = 0, url = '') {
        window.NativeConnect.openWebView(top, down, left, right, url)
    }
    , openFullScreenWebView(url = '', title = '') {
        window.NativeConnect.openFullScreenWebView(url, title)
    }
    ,closeWebView() {
        window.NativeConnect.closeWebView()
    }
    //------------ other ----------------------------------------------------
    , get gameVersion() {
        let version = ''
        version = window.NativeConnect.getGameVersion()
        return version
    }
    , get country() {
        let appCountry = ''
        appCountry = window.NativeConnect.getCountry()
        return appCountry
    }
    , get gamePlatform() {
        let appPlatform = "";
        if (cc.sys.isBrowser) {
            appPlatform = Config.gamePlatform;
        }else if (cc.sys.os == cc.sys.OS_IOS)
            appPlatform = jsb.reflection.callStaticMethod("JSConnect", "getGamePlatform");
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            appPlatform = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "getGamePlatform","()Ljava/lang/String;");
        else { 
            appPlatform = Config.gamePlatform
        }
        
        return appPlatform;
    }
    , nlog(tag='', message='', ...values) { 
        let formatMessage = message.replace(/\{(\d+)\}/g, function (format='', i=0) {
            if(values[i] == null) return ""
            return values[i]
        })
        window.NativeConnect.nlog(tag, formatMessage) 
    }
    , exit() {
        window.NativeConnect.exit()
    }

    , copyText(text) {
        if (cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod("JSConnect", "copyText:", String(text));
        else if (cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JSConnect", "copyText", "(Ljava/lang/String;)V", String(text));
        else { 
            this.nlog("Native copyText 不支持平台 ：", cc.sys.os + ' str= ' + String(text))
        }
    }

    //------------ deep link ------------------------------------------------
    , sentDeepLinkData(data={}) {
        window.NativeConnect.sentDeepLinkData(data)
    }
    , listenRecvDeepLinkData(callback=function(data=DEEP_LINK_DATA){}) {
        let evtID = 0
        evtID = window.NativeConnect.listenRecvDeepLinkData(callback)
        return evtID
    }
    , stoplistenDeepLinkData(evtID='') {
        window.NativeConnect.stoplistenDeepLinkData(evtID)
    }
    , setCocosCreatorRunning() {
        window.NativeConnect.setCocosCreatorRunning()
    }
}