import Config from '../constants/Config'
import WebApiTool from '../tool/WebApiTool'
import WebServerApiTool from '../tool/WebServerApiTool'
import Localize from '../constants/Localize'
import Tool from "../tool/Tool"

var HTTP_GET_SUCCESS = function(responseText='') {}
var HTTP_GET_FAIL = function(readyState=0) {}

var HttpType = {
    GET: 'get',
    POST: 'post'
}

// http api 附加 timeout 功能
var httpTool = function(httpType=HttpType.GET, url='', params='', successFun=HTTP_GET_SUCCESS, failFun=HTTP_GET_FAIL) {
    let isTimeOut = false
    let timeOutNum = setTimeout(() => { 
        isTimeOut = true
        failFun(Config.HttpRequestReadyState.TIME_OUT)
    }, Config.apiBreakTime)
    let callback = function(cbStatus=0, cbResponseText='', cbReadyState=0) {
        if(isTimeOut == true) return
        clearTimeout(timeOutNum)
        if(cbStatus == Config.HttpRequestStatus.SUCCESS && cbReadyState == Config.HttpRequestReadyState.DONE)
            successFun(cbResponseText)
        else
            failFun(Config.HttpRequestReadyState.DONE)
    }
    if(httpType == HttpType.GET)
        WebApiTool.get(url, callback)
    else
        WebApiTool.post(url, params, callback)
}

var httpGet = function(url='', successFun=HTTP_GET_SUCCESS, failFun=HTTP_GET_FAIL) {
    httpTool(HttpType.GET, url, '', successFun, failFun)
}

// http post 附加 timeout 功能
var httpPost = function(url='', params='', successFun=HTTP_GET_SUCCESS, failFun=HTTP_GET_FAIL) {
    httpTool(HttpType.POST, url, params, successFun, failFun)
}

var API_SUCCESS = function(jsonData='') {}
var API_FAIL = function() {}

// ============================ 遊戲伺服器 API
// 取得注區資料
var getSpotInfo = function(successFun=API_SUCCESS, failFun=API_FAIL) {
    httpGet(Config.getSpotURL(), (responseText)=> {
        let data = JSON.parse(responseText)
        successFun(data)
    }, (readyState)=> {
        let titleDataID = Localize.AlertTitleError
        let msgDataID = Localize.NoSpotInfo
        if(readyState == Config.HttpRequestReadyState.TIME_OUT)
            msgDataID = Localize.GetSpotInfoTimeOut
        Tool.showAlert(titleDataID, msgDataID, failFun)        
    })
}

// 取得籌碼資料
var getChipInfo = function(successFun=API_SUCCESS, failFun=API_FAIL) {
    httpGet(Config.getSpotURL(), (responseText)=> {
        let data = JSON.parse(responseText)
        successFun(data)
    }, (readyState)=> {
        let titleDataID = Localize.AlertTitleError
        let msgDataID = Localize.NoChipInfo
        if(readyState == Config.HttpRequestReadyState.TIME_OUT)
            msgDataID = Localize.GetChipInfoTimeOut
        Tool.showAlert(titleDataID, msgDataID, failFun)        
    })
}

// ============================ 平台伺服器 API
// 取得線上參數
var getOnlineSetting = function(callback=(value={})=>{}) {
    let cbSuccessFun = function(responseText) {
        let data;
        try {
            data = JSON.parse(responseText)
            let setting = data["manifest"];
            if(setting != null) {
                callback(setting);
            } else {
                callback();
            }
        } catch(e) {
            console.log(e);
            callback();
            return;
        }
    }
    let cbFailFun = function(readyState) {
        callback();
    }
    let url = Config.getVersionCheckURL();
    httpGet(url, cbSuccessFun, cbFailFun)
}

// 檢查遊戲版本
var API_GET_VERSION_SUCCESS = function(haveNewVersion=false, newVersion='', newURL='') {}
var checkGameVersion = function(currentGameVersion='', successFun=API_GET_VERSION_SUCCESS, failFun=API_FAIL) {
    let cbSuccessFun = function(responseText) {
        let data;
        try {
            data = JSON.parse(responseText)
        } catch(e) {
            let errorCode = 10;
            let titleDataID = Localize.AlertTitleError
            let msgDataID = Localize.NoGetVersion
            // service not available 
            Tool.showAlert(titleDataID, msgDataID, failFun, [errorCode]);
            return;
        }

        let dataKey = ((cc.sys.os == cc.sys.OS_IOS) ? ("iOS") : ("Android"));
        if(data[dataKey]) {
            let appVersion = data[dataKey]["mobile"]["ver"];
            let appUrl = data[dataKey]["mobile"]["file_url"];

            if(currentGameVersion != appVersion) {
                successFun(true, appVersion, appUrl);
            } else {
                successFun(false);
            }
        } else {
            successFun(false);
        }
    }
    let cbFailFun = function(readyState) {
        let errorCode = readyState 
        let titleDataID = Localize.AlertTitleError
        let msgDataID = Localize.NoGetVersion
        if(readyState == Config.HttpRequestReadyState.TIME_OUT)
            msgDataID = Localize.GetVersionTimeOut
        Tool.showAlert(titleDataID, msgDataID, failFun, [errorCode]) 
    }
    
    let url = Config.getVersionCheckURL();
    httpGet(url, cbSuccessFun, cbFailFun)
}

// 取得伺服器狀態
var getServerState = function (companyId = "", successFun = API_SUCCESS, failFun = API_FAIL) {
    let url = `${Config.getServerStateURL()}?companyId=${companyId}`;
    httpGet(url, (responseText) => {
        let data = JSON.parse(responseText);
        let returnCode = data.ReturnCode;
        let msgDataID = Localize.NoLocalize;
        if (returnCode == Config.MaintainCheck.SERVER_UP) {
            successFun(data);
        }
        else if (returnCode == Config.MaintainCheck.SERVER_DOWN) {
            Tool.showAlert("", msgDataID, failFun, [data.ReturnMessage]);
        }
        else {
            let code = (returnCode != null) ? ("\n(Code:" + returnCode + ")") : ("");
            let message = `平台伺服器維修中，請稍後再試${code}`;
            Tool.showAlert("", msgDataID, failFun, [message]);
        }
    }, (readyState) => {
        let titleDataID = Localize.AlertTitleError;
        let msgDataID = Localize.NoServerState;
        if (readyState == Config.HttpRequestReadyState.TIME_OUT)
            msgDataID = Localize.GetServerStateTimeOut;
        Tool.showAlert(titleDataID, msgDataID, failFun);
    });
} 

// 顯示登入失敗訊息
var showLoginFailMessage = function(readyState, failFun) {
    let titleDataID = Localize.AlertTitleError
    let msgDataID = Localize.LoginWebApiFail
    if(readyState == Config.HttpRequestReadyState.TIME_OUT)
        msgDataID = Localize.GetLoginInfoTimeOut
    Tool.showAlert(titleDataID, msgDataID, failFun)
}

// 取得串接版登入 signalR token (利用deep link token)
var loginGame = function(token='', successFun=API_SUCCESS, failFun=API_FAIL) {
    let url = Config.getLoginGameURL() + "token=" + token
    httpGet(url, (responseText)=> {
        let data = JSON.parse(responseText)
        successFun(data)
    }, (readyState)=> {
        showLoginFailMessage(readyState, failFun)      
    })
}

// 取得串接版登入 signalR token (利用上次登入所儲存的 dToken)
var directLoginGame = function(dToken='', successFun=API_SUCCESS, failFun=API_FAIL) {
    let url = Config.getDirectLoginGameURL() + dToken
    httpGet(url, (responseText)=> {
        let data = JSON.parse(responseText)
        successFun(data)
    }, (readyState)=> {
        showLoginFailMessage(readyState, failFun)      
    })
}

// 取得串接版登入 signalR token (利用上次登入所儲存的 dToken)
var fakeLoginGame = function(successFun=API_SUCCESS, failFun=API_FAIL) {
    let url = Config.getFakeLoginGameURL()
    httpGet(url, (responseText)=> {
        let data = JSON.parse(responseText)
        successFun(data)
    }, (readyState)=> {
        showLoginFailMessage(readyState, failFun)      
    })
}

var logRecord = function(infoList, message) {
    console.log("jun logRecord infoList ", infoList);
    console.log("jun logRecord message ", message);
    httpPost(
        Config.getLogRecordURL(),
        "infoList="+infoList+"&message="+message
    )
    
}

module.exports = {
    checkGameVersion: checkGameVersion,
    getServerState: getServerState,
    getSpotInfo: getSpotInfo,
    getChipInfo: getChipInfo,
    loginGame: loginGame,
    directLoginGame: directLoginGame,
    fakeLoginGame: fakeLoginGame,
    getOnlineSetting: getOnlineSetting,
    logRecord: logRecord,
}

