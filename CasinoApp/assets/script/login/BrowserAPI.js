import $ from 'jquery'

var BrowserAPI = {
    processGetSpotInfo(getSpotUrl='', successFun=function(spotInfo={}){}, failFun=function(error=''){}) {
        $.get(getSpotUrl)
        .done((data) => {
            successFun(data)
        })
        .fail((err) => {
            failFun()
        })
    },

    processGetChipInfo(getChipUrl= '', successFun=function(chipInfo={}){}, failFun=function(error=''){}) {
        $.get(getChipUrl)
        .done((data) => {
            successFun(data)
        })
        .fail((err) => {
            failFun()
        })
    }

}

module.exports = BrowserAPI