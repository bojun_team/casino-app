/*





*/

import localizeMgr from '../tool/localize/LocalizeMgr'
import DebugFunction from "../debugPanel/DebugFunction"
import Config from '../constants/Config'
import RxJsMgr from '../model/RxJsMgr'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

// 手機網頁版
import common from '../tool/common'
var { decode, getQuery } = common

var _tokenList = {
    test: 'eyJpdiI6Im42SnNPVmZjODZMK2IwalwvWFpGMG9RPT0iLCJ2YWx1ZSI6Ilc2TFpkV21vMWJqMnY0NlJvU2NNMEJGN1Nkbkh0SjVSa3RydmN0ZkpnQklkTGV0eDJDNWpRTzBPNVY1Z2JEUmd0TUlnRk1ic2FTSVdiMlkrUnhrZFo0XC9HQVhySThPaHFcL3dTZUE5VGZpSzFlM1hWblZsYXhsdXpRK05pNGh6bE5MUWUyZjRXYWRjREFoc0JRN0pPVzFqc2dwbVdoWmlON0I3K1lCcHlxT1dTXC9cL1ZRTFhrRzROV3FpMVRSSjN5TFBUNVhCdXQ3M1lTR3lITVM0b2NOYlhZQVhaeUloK3B4aVdHY0Y3WHFkeFNad2MwcjJzOE1kYis0Mnd3bTV5RkZPbjdkXC9vemlhblQ0UkppMjZBZGVaNFwveWx0YzJsUHdMbU5HT2E5U01qMFdMb1NVdFlzMzJDWHNPeUNxaXpvWnNlRmVNbGZzV1F3blpOVGpIelwvZ1JlMm95Y0VBK1VadVU3YnZUclFLa0NkNmJwR0s5amh0cVNHZHNBVFwvdWdxSkQ3VFdETXRPVklXVnVVVnpzaVc2UXhLT1dCTDJUTzNLZnRKdUNXcmVEMDFveDFydXVEUDBMVlJQYVpqQVRoMUxnMG9yMXFtMnozYVhVZnFTeDZMVDJyYXZPRCtITU9WMkE3cjlRdXo4Mk9SQnhCandBUW02R0RzRTJJSjk1T0llYndyUTJ4ajBkcGJ5ajZCZmdaMEhYSTVDQ1kwMnB6SFdoU0VaaXNBZEVhMU9ybnBnY01UY25wbEloOEFHNGZ2NG9pIiwibWFjIjoiN2E0NTBhZmM2YWRmOTAzYzNiZjEzNjIyMDlmZjJmYmNjNTBjMDViZGEwOTY2NjBjNDA0OGZjY2I2ZTg5MjYwNCJ9',
    stage: 'eyJpdiI6IktCR2c5VmZpQkQ4U3hxdUd1amVxTVE9PSIsInZhbHVlIjoiT29qVjhFY01PV1k5QytxNTFmNXRYd0dSNTN6QitzZlVnUm5WMVVKdzh3Y0xPUGdKYVl3cGI3aGVpRTdXOFVoZ2pBRWtDbDVWMXpPSm1kT3J6NEdWYllwUm5QQzZVNytEM0FvV2JyaDVUNkFUeG53MEtVaUdObkhZRHJZcjd2ZVwvSFg0K3dTNllQSUNpWVZiMFpiSnhjeFVcL054clB4R3ErMHUydUZSeitBVnp3ZFBqUU1kV1JpSUxjNjJ3WXFxRzBXelpNeStYaEZ5YXgxRTh2dzRRMVkxbURRTk1OZ0wzVDVacDJ2Y09ycXgwSkZBTFhwVEF2ekJZS0xjdUJzck9mVm91R0lvWnNvdjFJU2ROUlwvbVJqQ1NGZXNkMlwvekJnNVZCb2xtQk81cjJZRnZvYytOMTdLYVE1bUlTb0c0eDBzMGNnQ0ZrUitDclp1dGM4cFBqZHpFZFM4ZjBUZVp1bHJJNnhLNW5GdGhxanVxUGJCaHRYRGlDM05UNUtSRlJyK2hoWTFPa0o4QVd0anZOVE1PcGE2bGU4R2V6RWdsb2dCQzU5M3NOQytLc2txWW5KeXI5OHVKa3Q1SW8rbkdnSzBYZlpmbUFpWlR1djZ0YjNwcjNTUDRjMVhBOGZnRUNTRTNaS3dNRENcL0wzcTB6RU5qOG5aM2FXajVkK01pU0I4NFdOMkVCanZod1IwNDVRVzhRMXZQSWtIeDZVeDErUk83ampUZ1VsRnROemgrRW9GRm92XC9DMkZlQUIwb2dNa0IxTkpVQmtnY1wva0Y2d1V1Y0Ryb2I5Z0E9PSIsIm1hYyI6IjZjMTYxODE5ODk3OWI5NGY0OTc0OGZlNmU0NjA3MjE2YmU0ZmEwZTk3YzQ3NTFkNWM3YzJiYzI4YTRkNmUyZWIifQ==',
    release: 'eyJpdiI6Im9mMTByamlmOG90dG1jbnRqQUw1RFE9PSIsInZhbHVlIjoiYkdmamxKcGwzcEtSZlwvcjA2V092N05odHRFbGkwd2FXMG1MZW1GYjQzMVc0dGFRMFhZNHdIVnRudXM3ZVM0MWdreFpqYzRMYTJSRXFWVFFkZXpZbDBrN0FCcVdZYVwvNVlKRHNGVTN1SFNjSkkwXC8rY2V0dG03Y1NHZjVmaUx1WkFGK2FLNHZsMTJQVjE0Vkp2MWVRK09DRmhVNUJLbWttYXQ0cnF0Z1VtQlI4ZlpJQUlmd1FzNW52RWhYVllxNmNEdXpVZnpveUkwQlNEU05QT2x1RTRtN1lRaVJBWnFJbE5JTngzR0hGVkczYVVtSk1SODRFeklCMUJCeWVEYVR3OXBhTEc3OGp5alRIaU1qUTlhNjhLdnpma3N5OWY5K2RUcWZwaWJFck0wNzFLdEMrdzl5T296UlJSOHZrWk5EbWdZXC9cL3lRdjVFZW16TlwvSDIrMWs5MVY1aXBNTEZRTHk2MCt3dkxyRDlyUWJPZXRnZDNJNSt0QnlQYzJwTTR1Z2xFKytNbFZRT283dnd3ZXZsTURINVVTTWk5cWRTUjNGRnVXMnZlNFAzRW5ER1dqNXJyTEoyWWkwMVRLVk9CMWFlTG5zc1MwcnowakJIbFN3RGFSRERuUStaSUFGWEs1M2tmWE45Rkt4dnIweklWa0pBQ0NtbmlobUVsWU9zYnNwcXRSRzhBUlFtZUpQTHQxWDlWRVZUYUFmTDFDc1JOaUE2Q01teXFRXC9tYUlEc0s1R0lXK2p5dUoyZUZRZXlUNE50TVZCdnptUFwvR1kxVGc4cytEU0hCMEFRPT0iLCJtYWMiOiJmMmVjMTIwYWMwNjhhNDEwYTdmMzAzOTdiOWFlYWQ0MjhmODJmZTdjMTlkZTg0YTg2NzVkNDAzNmJjMzk0YmFjIn0='
}

var _creatorLoginData = {
    scheme: "pharao-casino-mobile",
    host: "launch-game",
    queryList: {
        lang: "zh_TW",
        token: _tokenList[Config.gameOutput]
    }
}

cc.Class({
    extends: cc.Component,

    properties: {
        button: cc.Node
    }, 
    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
        this.stopDeepLinklisten()
        this.button.off('click', this.fakeLoginGame, this);
    }, 
    stopDeepLinklisten: function() {
        if(this.recvDeepLinkDataEvtId != null) {
            NativeConnect.stoplistenDeepLinkData(this.recvDeepLinkDataEvtId) 
            this.recvDeepLinkDataEvtId == null 
        }
        clearTimeout(this.timeOutNum) 
        this.timeOutNum = null 
        cc.game.off(cc.game.EVENT_SHOW, this.onAppResume, this) 
    },  
    onLoad: function () {
        // 手機網頁版 不跑熱更新
        if(cc.sys.isBrowser) {
            this.initLoginView()
            return 
        }
        
        // Creator 不能跑熱更新
        if (cc.sys.os != cc.sys.OS_IOS && cc.sys.os != cc.sys.OS_ANDROID) {
            this.initLoginView()
            return 
        }

        // 執行登入前先檢查熱更新版本
        Casino.HotUpdate.isNewVersion((eventCode, remotelVersion) => {
            let isError = false
            let isNew = false
            switch(eventCode) {
                case Casino.HotUpdate.UpdateState.ERROR_NO_LOCAL_MANIFEST:
                case Casino.HotUpdate.UpdateState.ERROR_DOWNLOAD_MANIFEST:
                case Casino.HotUpdate.UpdateState.ERROR_PARSE_MANIFEST:
                    isError = true
                    break
                case Casino.HotUpdate.UpdateState.ALREADY_UP_TO_DATE:
                    break
                case Casino.HotUpdate.UpdateState.NEW_VERSION_FOUND:
                    isNew = true
                    break
            }

            let titleDataID = ''
            let msgDataID = ''
            Casino.Tool.closeAllAlert()

            if(isError == true) {
                console.log('jun show error alert')
                titleDataID = Casino.Localize.AlertTitleError
                msgDataID = Casino.Localize.NoGetVersion
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {  
                    Casino.NativeConnect.exit()
                }.bind(this), [eventCode])
                return
            }


            if(isNew == true) {
                console.log('jun show new alert')
                titleDataID = Casino.Localize.AlertTitleError
                msgDataID = Casino.Localize.VersionIsOldHotUpdate
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {  
                    Casino.NativeConnect.exit()
                }.bind(this), [remotelVersion])
                return
            }
            
            this.initLoginView()
        })
    }, 

    initLoginView: function() {
        // 設定現在跑的版本
        Config.gameCountry = Casino.NativeConnect.country

        /** 設定現在跑的平台 */
        Config.gamePlatform = Casino.NativeConnect.gamePlatform;

        this.LoadingLabelDataID = ""
        this.LoadingViewVisible = true
        this.LoadingProgress = 0
        Casino.Tool.openLoadingView(()=>{
            this.setLoadingLabel(this.LoadingLabelDataID)
            this.setVisibleLoadingView(this.LoadingViewVisible)
            this.setLoadingProgress(this.LoadingProgress)
        })
        
        Casino.Tool.setKeepScreenOn(true)

        // 初始化遊戲資料
        this.isWaitingConnect = false
        RxJsMgr.connectData.reset()
        Casino.NewUser()
        Casino.NewTables()

        // 初始化音效控制
        Casino.Tool.addMusicControl()

        // 初始化數字鍵盤
        Casino.NumberKeyboard.loadPrefab();

        // 訂閱
        this.subscribes = []

        // 訂閱 登入成功完成
        let checkLogin = function() {
            if(Casino.Tables.isRecvInitTables == true && Casino.User.isRecvCashType == true)
            Casino.Tool.goToLobbyScene()
        }
        let isRecvInitTables$ = Casino.Tables.propChange$.isRecvInitTables 
        this.subscribes[this.subscribes.length] = isRecvInitTables$.subscribe({ 
            next: isRecvInitTables => {  
                checkLogin() 
            } 
        }) 
        let isRecvCashType$ = Casino.User.propChange$.isRecvCashType 
        this.subscribes[this.subscribes.length] = isRecvCashType$.subscribe({ 
            next: isRecvCashType => {  
                checkLogin() 
            } 
        }) 

        // 訂閱 連線狀態
        let connectState$ = RxJsMgr.connectData.propChange$.connectState
        this.subscribes[this.subscribes.length] = connectState$.subscribe({
            next: connectState => {     
                if (this.isWaitingConnect == false) return  
                if (connectState == Config.ConnectState.DISCONNECTED) {
                    let titleDataID = Casino.Localize.AlertTitleError
                    let msgDataID = Casino.Localize.ConnectClose
                    Casino.Tool.showAlert(titleDataID, msgDataID, function() { 
                        this.setVisibleLoadingView(false)
                        Casino.Tool.returnToPlatform()
                    }.bind(this))
                } else if (connectState == Config.ConnectState.CONNECTED) {   
                    this.runNextCommand()
                }
            },  
        })

        // 初始化登入流程(命令模式）
        this.commandRunIndex = -1

        // 手機網頁版
        if(cc.sys.isBrowser) {
            this.commandList = [
                "getSpotInfoBrowser",
                "initBrowserlogin",
                "connectSignalR",
                "loginUser"
            ]
            this.runNextCommand()
            return
        }

        this.commandList = [
            "checkGameVersion",
            "getSpotInfo",
            "getChipInfo",
            "initAppLogin",
            "loginGame",
            "getServerState",
            "connectSignalR",
            "loginUser"
        ]

        this.runNextCommand()
    }, 
    onAppResume: function() {
        if(this.timeOutNum != null) return
        this.addTimeOut()
    },
    addTimeOut: function() {        
        clearTimeout(this.timeOutNum)
        this.timeOutNum = setTimeout(function() { 
            // 如果有dToken則嘗試做dToken登入  
            if(Casino.User.dToken != null && Casino.User.dToken.length > 0) { 
                this.directLoginGame(); 
                return 
            } 
            this.showLoginTimeoutAlert()
        }.bind(this), Config.deepLinkBreakTime)
    },
    //更新讀取條文字
    setLoadingLabel: function(dataID) {
        this.LoadingLabelDataID = dataID
        if(Casino.Tool.getLoadingView() == null) return
        Casino.Tool.getLoadingView().loadingLabel.dataID = dataID
    },
    //更新讀取條進度
    setLoadingProgress: function(progress) {
        this.LoadingProgress = progress
        if(Casino.Tool.getLoadingView() == null) return
        Casino.Tool.getLoadingView().progressBar.progress = progress
    },
    setVisibleLoadingView:function(isVisible) {
        this.LoadingViewVisible = isVisible
        if(Casino.Tool.getLoadingView() == null) return
        if(isVisible) {
            Casino.Tool.openLoadingView()
        } else {
            Casino.Tool.closeLoadingView()
        }
    },
    runCommand: function(name) {
        let commandIndex = this.commandList.indexOf(name)
        if(commandIndex != -1) {
            this.commandRunIndex = commandIndex - 1
            this.runNextCommand()
        }
    },
    runNextCommand: function(runFrom) {
        let nextCommandRunIndex = this.commandRunIndex + 1
        if(nextCommandRunIndex >= this.commandList.length) return
        let commandName = this.commandList[++this.commandRunIndex]
        let progress = (this.commandRunIndex+1)/this.commandList.length
        this.setLoadingProgress(progress)
        if(typeof this[commandName] != "function") return
        this[commandName]()
    },
    // 版本檢查
    checkGameVersion: function() {
        let versionUpdate = function(strVersion, url) {
            let titleDataID = Casino.Localize.AlertTitleError
            let msgDataID = Casino.Localize.VersionIsOld
            Casino.Tool.closeAllAlert()
            Casino.Tool.showAlert(titleDataID, msgDataID, function() {  
                Casino.NativeConnect.updateApp(url)
            }.bind(this), [strVersion])
        }
        let success = function(haveNewVersion, newVersion, newURL) {
            if(haveNewVersion == true)
                versionUpdate(newVersion, newURL)
            else
                this.runNextCommand()
        }
        let fail = function(){ 
            Casino.NativeConnect.exit()
        }
        this.setLoadingLabel(Casino.Localize.LoadingGetVersion)
        Casino.AppWebAPI.checkGameVersion(Casino.NativeConnect.gameVersion, success.bind(this), fail.bind(this))
    },
    // 取得SERVER資訊
    getServerState: function() { 
        let success = function(data){
            this.runNextCommand()
        }
        let fail = function(){
            Casino.NativeConnect.exit()
        }
        this.setLoadingLabel(Casino.Localize.LoadingGetServer)
        Casino.AppWebAPI.getServerState(Casino.User.companyId, success.bind(this), fail.bind(this))
    }, 
    // 取得注區資訊
    getSpotInfo: function() { 
        let success = function(data){
            Casino.spotInfo = data
            this.runNextCommand()
        }
        let fail = function(){
            Casino.NativeConnect.exit()
        }
        this.setLoadingLabel(Casino.Localize.LoadingGetSpot)
        Casino.AppWebAPI.getSpotInfo(success.bind(this), fail.bind(this))   
    }, 
    // 取得籌碼資訊
    getChipInfo: function() { 
        let success = function(data) {
            this.runNextCommand()
        }
        let fail = function(){
            Casino.NativeConnect.exit()
        }
        this.setLoadingLabel(Casino.Localize.LoadingGetChip)
        Casino.AppWebAPI.getChipInfo(success.bind(this), fail.bind(this)) 
    },
    // 登入 SignalR
    connectSignalR: function() {
        if(RxJsMgr.connectData.connectState != Config.ConnectState.CONNECTED) {
            this.isWaitingConnect = true
            SignalR.connectToServer(Config.getSignalrURL())
        } else {
            this.runNextCommand()
        }
    },
    // 登入使用者
    loginUser: function() {
        this.isWaitingConnect = false
        Casino.User.login(Casino.User.username, Casino.User.userToken, function(cmd, resp, error){ 
            if (error != null){
                let errorCode = String(resp)
                let errorMessage = String(error)                        
                let titleDataID = Casino.Localize.AlertTitleError
                let msgDataID = Casino.Localize.UnknownError
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.NativeConnect.exit()
                }, [errorCode, errorMessage])
                return
            }
            if (resp == "0"){
                Casino.Tool.isLoginGame = true
                this.setLoadingProgress(0)
                this.setVisibleLoadingView(true)

                // 手機網頁版
                if(cc.sys.isBrowser) return

                if (cc.sys.os != cc.sys.OS_IOS && cc.sys.os != cc.sys.OS_ANDROID) {
                    DebugFunction.sendGetUserCredit()
                    DebugFunction.sendInitTables()   
                }
            } else {
                let titleDataID = Casino.Localize.AlertTitleError
                let msgDataID = Casino.Localize.LoginFail
                if(Number(resp) == Config.GameCenterErrorCode.ServerIsSuspend) {
                    msgDataID = Casino.Localize.GameServerMaintain
                }
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    this.setVisibleLoadingView(false)
                    Casino.Tool.returnToPlatform()
                }.bind(this))
            }
        }.bind(this)) 
    },
    // ============ 串接版本 ============
    initAppLogin: function() {
        this.setVisibleLoadingView(false)
        // 設定預設語言
        let lang = Casino.Tool.getAppLanguage()
        localizeMgr.changeLanguage(lang)

        // 設定監聽 appResume
        cc.game.on(cc.game.EVENT_SHOW, this.onAppResume, this)

        // 設定監聽 deeplink
        this.recvDeepLinkDataEvtId = Casino.NativeConnect.listenRecvDeepLinkData((data)=> {
            if(data.host == Config.DeepLinkHost.LAUNCH_GAME) {
                clearTimeout(this.timeOutNum)
                Casino.Tool.closeAllAlert()

                let lang = Config.webApiLanguage(data.queryList.lang)
                localizeMgr.changeLanguage(lang)
                Casino.Tool.saveAppLanguage(lang)

                Casino.Tool.saveplatformURL(data.queryList.platformURL)
                this.token = data.queryList.token 
                Casino.User.isFakeLogin = false
                this.runCommand("loginGame")
            }
        })
        // 設定deeplink timeout
        this.addTimeOut()
        // 通知native, cocos creator 已經開始跑了
        Casino.NativeConnect.setCocosCreatorRunning()
        // Creator 送假登入資訊
        if(cc.sys.os != cc.sys.OS_IOS && cc.sys.os != cc.sys.OS_ANDROID) {
            Casino.NativeConnect.sentDeepLinkData(_creatorLoginData)
        }
    },

    fakeLoginGame: function() {
        let success = function(data) {
            this.token = data.token 
            Casino.User.isFakeLogin = true 
            this.runCommand("loginGame")
        }
        let fail = function() {
            this.showLoginFailAlert()
        }
        Casino.AppWebAPI.fakeLoginGame(success.bind(this), fail.bind(this))
    },
 
    // 串接版本 login
    loginGame: function() { 
        this.setLoadingLabel(Casino.Localize.LoadingLogin)

        let success = function(data) {
            // Token or DToken 錯誤，顯示錯誤訊息
            if(data.ReturnCode != null) {
                // show alert
                this.showTokenFailAlert(data.ReturnCode) 
                return
            }

            // 不同平台帳號，顯示錯誤訊息
            if(Config.isRightAppEnv(data.appEnv) == false) {
                this.showLoginAppEnvErrorAlert()
                return
            }
            
            Casino.User.username = data.username 
            Casino.User.userToken = data.token
            Casino.User.dToken = data.dToken
            if(Config.gamePlatform == Config.Platform.JHONG_FA) {
                Casino.Tool.saveData(Config.saveName.dToken, "");
            }
            Casino.User.companyId = data.platformId
            this.runNextCommand()
        }
        let fail = function() {
            this.showLoginFailAlert()
        }
        
        Casino.AppWebAPI.loginGame(this.token, success.bind(this), fail.bind(this))
    },
    // 直接登入
    directLoginGame: function() {
        this.setLoadingLabel(Casino.Localize.LoadingLogin)

        let success = function(data) {
            // Token or DToken 錯誤，顯示錯誤訊息
            if(data.ReturnCode != null) {
                // show alert
                this.showTokenFailAlert(data.ReturnCode)
                return
            }

            // 不同平台帳號，顯示錯誤訊息
            if(Config.isRightAppEnv(data.appEnv) == false) {
                this.showLoginAppEnvErrorAlert()
                return
            }
            
            Casino.User.username = data.username 
            Casino.User.userToken = data.token
            Casino.User.dToken = data.dToken
            if(Config.gamePlatform == Config.Platform.JHONG_FA) {
                Casino.Tool.saveData(Config.saveName.dToken, "");
            }
            Casino.User.companyId = data.platformId
            this.runCommand('getServerState');
        }
        let fail = function() {
            this.showLoginFailAlert()
        }
        Casino.AppWebAPI.directLoginGame(Casino.User.dToken,  success.bind(this), fail.bind(this))
    },
    showLoginTimeoutAlert: function()  {
        // 眾發目前沒有Stage體驗功能
        if(Config.gamePlatform == Config.Platform.JHONG_FA) {

        } else if(Config.gameOutput == Config.Output.STAGE) {
            if(Casino.User.dToken.length > 0) return
            this.button.active = true
            this.button.on('click', this.fakeLoginGame, this);
            return
        }

        let titleDataID = Casino.Localize.NoLocalize
        let msgDataID = Casino.Localize.CooperationLogout
        if(Casino.Tool.isLoginGame == false) {
            msgDataID = Casino.Localize.CooperationFirstLogin
        }
        this.timeOutNum = null
        Casino.Tool.closeAllAlert()
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            if(Casino.Tool.returnToPlatform() == false) {
                Casino.NativeConnect.exit()
            }
        })
    }, 
    showLoginFailAlert:function() {
        let titleDataID = Casino.Localize.AlertTitleError
        let msgDataID = Casino.Localize.CooperationTokenError
        this.timeOutNum = null
        Casino.Tool.closeAllAlert()
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            if(Casino.Tool.returnToPlatform() == false) {
                Casino.NativeConnect.exit()
            }
        })
    },
    showLoginAppEnvErrorAlert:function() {
        Casino.Tool.closeAllAlert()
        let titleDataID = Casino.Localize.AlertTitleError
        let msgDataID = Casino.Localize.CoopertaionAppEnvError
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            Casino.NativeConnect.exit()
        })
    },
    // token or dToken error alert
    showTokenFailAlert: function(errorCode) {
        Casino.Tool.closeAllAlert()
        let titleDataID = Casino.Localize.AlertTitleError
        let msgDataID = Casino.Localize.CoopertaionTokenFail
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            Casino.NativeConnect.exit()
        }, [errorCode])
    }

    // 手機網頁版 取得注區資訊
    , getSpotInfoBrowser: function() { 
        let success = function(spotInfo){
            Casino.spotInfo = spotInfo
            this.runNextCommand()
        }
        let fail = function(){
            let titleDataID = Localize.AlertTitleError
            let msgDataID = Localize.NoSpotInfo
            Tool.showAlert(titleDataID, msgDataID, ()=>{
                Casino.NativeConnect.exit()
            }) 
        }
        this.setLoadingLabel(Casino.Localize.LoadingGetSpot)
        Casino.BrowserAPI.processGetSpotInfo(Config.getSpotURL(), success.bind(this), fail.bind(this))
    }
    
    // 手機網頁版 登入用者
    , initBrowserlogin: function() {
        window.casino = {}

        const originalUrl = document.referrer
        if(getQuery('d', originalUrl)) window.casino.domain = getQuery('d',originalUrl)

        const sid = getQuery('e', originalUrl) ? getQuery('e', originalUrl):false
        if(sid){
            const data = decode(sid)
            let loginData = {
                Username: data.u,
                Token: data.t,
                Password:   null,
                PlatformId: Config.platFormId,
                CompanyId:  data.c
            }

            if(data.username) {
                loginData.Username = data.username
                loginData.Token = data.token
                loginData.Password = data.password
            }
            console.log('jun initBrowserlogin data:', data)
            
            // 設定預設語言
            let lang = Config.webApiLanguage(data.l)
            localizeMgr.changeLanguage(lang)

            Casino.User.username = loginData.Username 
            Casino.User.userToken = loginData.Token
            window.casino.rToken = data.r
            Casino.User.companyId = loginData.CompanyId
            window.platformURL = 'http://' + getQuery('p', originalUrl)            
        } else {
            console.log('jun 登入失敗 無 sid')
            let titleDataID = Casino.Localize.AlertTitleError
            let msgDataID = Casino.Localize.LoginFail
            Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                Casino.Tool.returnToPlatform()
            }.bind(this))
            return
        }
        this.runNextCommand()
    }

});
