import Rx from 'rxjs'
import Casino from '../model/Casino'
import Localize from '../constants/Localize'

var docElm = null

if(cc.sys.isBrowser && cc.sys.os == cc.sys.OS_ANDROID) {
    docElm = document.documentElement

    document.addEventListener("fullscreenchange", function( event ) {
        if ( document.fullscreen ) {
            Casino.isFullScreen$.next(true)
        } else {
            Casino.isFullScreen$.next(false)
        }
    });
    
    document.addEventListener("webkitfullscreenchange", function( event ) {
        if ( document.webkitIsFullScreen == true) {
            Casino.isFullScreen$.next(true)
        } else {
            Casino.isFullScreen$.next(false)
        }
    });
}

cc.Class({
    extends: cc.Component,

    properties: {
        AlertView: cc.Node,
    },

    // use this for initialization
    onLoad: function () {
        this.node.zIndex = 99999
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node

        this.subscribes = []

        // if (cc.sys.isBrowser && cc.sys.os == cc.sys.OS_ANDROID) this.subscribesFunc()
    },

    subscribesFunc: function() {
        this.subscribes[this.subscribes.length] = Casino.isFullScreen$.subscribe({
            next: isFullScreen => {
                if(isFullScreen == false) {
                    this.showAlert()
                } else {
                    this.AlertView.active = false
                }
            }
        })
    },

    onDestroy:function() {
        if(this.subscribes.length == 0) return
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    showAlert: function() {
        this.AlertView.active = true
    },
    
    toggleFullscreen: function () {
        if (docElm.requestFullscreen) {
          docElm.requestFullscreen()
        } else if (docElm.webkitRequestFullScreen) {
          docElm.webkitRequestFullScreen()
        }   
    }
});
