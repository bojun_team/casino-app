import Localize from '../constants/Localize'

cc.Class({
    extends: cc.Component,
    properties: {
        titleLab:require("LocalizedLabel"),
        msgLab:require("LocalizedLabel"),
        acceptBtn:cc.Button
    },
    onLoad: function () {
        this.acceptBtn.node.on("click", (function () {
            cc.director.getScene().removeChild(this.node)
            if (this.acceptEvent != null) {
                this.acceptEvent()
            }
        }), this);
    },
    addAlert: function(titleDataID, msgDataID, acceptFun, dataArguments) {
        this.titleLab.dataID = titleDataID
        
        if(dataArguments == null) dataArguments = []
        this.msgLab.dataArguments = dataArguments
        this.msgLab.dataID = msgDataID

        this.acceptEvent = acceptFun
    }
});
