import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        mainOriginBtn:cc.Button,
        mainOriginLabel:require("LocalizedLabel"),
        originBtns:[require("IFButton")],
    }, 
     
    onLoad: function () {
        this.node.active = false     

        //選擇端口順序
        let companyList = {
            Pharaoh:this.originBtns[0],
            SignatureClub:this.originBtns[1],
            Sands:this.originBtns[2],
            UbenClub:this.originBtns[3]
        }

        for(let key in companyList) {
            let btn = companyList[key]
            let companyId = Config.CompanyId[key]
            //設定選擇端口點擊事件
            btn.addClickEvent(function() {
                Casino.User.companyId = companyId
                this.mainOriginLabel.dataID = key
                this.node.active = false
            }.bind(this))

            if(Casino.User.companyId == companyId) {
                this.mainOriginLabel.dataID = key
            }
        }

        //設定端口點擊事件
        this.mainOriginBtn.node.on("click", function(){
            this.node.active = true
        }.bind(this), this); 
    }
});
