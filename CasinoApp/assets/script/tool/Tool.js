import Config from '../constants/Config'
import Localize from '../constants/Localize'
import NativeConnect from '../native/NativeConnect'
import MusicControl from './MusicControl'

const Tool = {
    printData:function(data, spaceCount)
    { 
        if (spaceCount == null) spaceCount = 0
        var space = ""
        for (var i = 0; i < spaceCount; i++) 
            space = "    " + space 
        var str = "[\n"
        for (var key in data)
        {
            if (typeof(data[key]) == "object"){
                str = str + space + key + ":" + this.printData(data[key], spaceCount + 1) + ",\n"
            } 
            else{
                str = str + space + key + ":" + data[key] + ",\n"
            }
        }
        var space = ""
        for (var i = 0; i < spaceCount - 1; i++) 
            space = "    " + space 
        return str + space +"]"
    },

    printAll:function(data, spaceCount)
    { 
        if (spaceCount == null) spaceCount = 0
        var space = ""
        for (var i = 0; i < spaceCount; i++) 
            space = "    " + space 
        var str = ""
        for (var key in data)
        {
            if (typeof(data[key]) == "object"){
                this.printAll(data[key], spaceCount + 1)
            }
            else{
                return (key + ":" + data[key])
            }
        } 
    },

    setSpritFrame:function(_sprit, plistUrl, pngName){
        cc.loader.loadRes(plistUrl, cc.SpriteAtlas, function (err, atlas) {
            var frame = atlas.getSpriteFrame(pngName)
            _sprit.spriteFrame = frame;
            cc.loader.releaseRes(plistUrl);
        });
    },
    //用來取數字轉成字串
    conversionNumber:function(number){
        var strNumber = ""
        var number_integer = Math.floor(number)  // 取整數部分
        
        var getStrSecond = function(str_second){
            for (var i = 3 ; i > 0 ; i -- ){
                var isReturn = false
                if (0 != Number(str_second.substring(i - 1, i))){
                    str_second = str_second.substring(0, i)
                    isReturn = true
                } 
                if (isReturn) break
            }

            if (0 == Number(str_second)) str_second = ""
            return str_second
        }

        var strN = String(number_integer)
        var iLen = strN.length
        if (number_integer %10 == 0 && 3 < iLen){
            if (number_integer % 10000 == 0){
                if (6 < iLen){
                    var str_first = strN.substring(0, iLen-6)
                    var str_second = strN.substring(str_first.length, iLen-3)
                    str_second = getStrSecond(str_second)
                    if (0 != str_second.length){
                        strNumber = str_first + "." + str_second + "M"
                    }else{
                        strNumber = str_first + "M"
                    }
                }else{
                    var str_first = strN.substring(0, iLen-3)
                    strNumber = str_first + "K"
                }
            }else{
                var str_first = strN.substring(0, iLen-3)
                var str_second = strN.substring(str_first.length, iLen)
                str_second = getStrSecond(str_second)
                if (0 != str_second.length) {
                    strNumber = str_first + "." + str_second + "K"
                }else{
                    strNumber = str_first + "K"
                }
            }
        }else{
            strNumber = String(number_integer) 
        }
        return strNumber
    },
    showWebView: function(url = "", title = "") {
        if (Tool.webViewPrefab == null) {
            cc.loader.loadRes("prefab/common/WebView", function (err, prefab) {
                Tool.webViewPrefab = prefab
                Tool.showWebView(url, title)
            })
        } else {
            var newNode = cc.instantiate(Tool.webViewPrefab);
            var webView = newNode.getComponent("IFWebView")
            webView.setTitle(title)
            let isAndroid = (cc.sys.os == cc.sys.OS_ANDROID)
            webView.openWebView(url, isAndroid)
            cc.director.getScene().addChild(newNode, Config.GlobalZ_Index.WEB_VIEW)
        }
    },
    showAlert: function(titleDataID, msgDataID, acceptFun, dataArguments) {
        if (Tool.alertPrefab == null) {
            cc.loader.loadRes("/base_res/prefab/AlertView", function (err, prefab) {
                Tool.alertPrefab = prefab
                Tool.showAlert(titleDataID, msgDataID, acceptFun, dataArguments)
            })
        } else {
            var newNode = cc.instantiate(Tool.alertPrefab);
            var alert = newNode.getComponent("AlertView")
            alert.addAlert(titleDataID, msgDataID, acceptFun, dataArguments)
            cc.director.getScene().addChild(newNode, Config.GlobalZ_Index.ALERT)
        }
    },
    closeAllAlert: function() {
        let child = cc.director.getScene().getChildByName("AlertView")
        while(child != null) {
            child.removeFromParent(true)
            child = cc.director.getScene().getChildByName("AlertView")
        }
    },
    // 使用後會跟上一個場景一起移除(轉場景使用)
    showNextSceneLockView: function(callBack) { 
        if (Tool.lockViewPrefab == null) {  
            cc.loader.loadRes("prefab/common/LockView", function (err, prefab) {
                Tool.lockViewPrefab = prefab
                Tool.showNextSceneLockView(callBack)
            }) 
        } else { 
            var view = cc.instantiate(Tool.lockViewPrefab); 
            cc.director.getScene().addChild(view)
            if(callBack!=null) callBack(view) 
        } 
    }, 
    saveData:function(key, value){
        cc.sys.localStorage.setItem(key, value);
    },
    loadData:function(key, defaultValue){
        const value = cc.sys.localStorage.getItem(key)
        if (value == null) return defaultValue
        else return value
    },

    showToast: function(strMessage, toastTime, dataArguments) {
        if (Tool.toastPrefab == null) {
            cc.loader.loadRes("prefab/common/ToastView", function (err, prefab) {
                Tool.toastPrefab = prefab
                Tool.showToast(strMessage, toastTime, dataArguments)
            })
        } else {
            var newNode = cc.instantiate(Tool.toastPrefab);
            var toast = newNode.getComponent("ToastView")
            toast.addToast(strMessage, toastTime, dataArguments)
            cc.director.getScene().addChild(newNode)
        }
    },

    showDailyQuotaUpdate: function() {
        if (Tool.dailyQuotaUpdatePrefab == null) {
            cc.loader.loadRes("prefab/common/DailyQuotaUpdateView", function (err, prefab) {
                Tool.dailyQuotaUpdatePrefab = prefab
                Tool.showDailyQuotaUpdate()
            })
        } else {
            Tool.dailyQuotaUpdateNode = cc.instantiate(Tool.dailyQuotaUpdatePrefab);
            cc.director.getScene().addChild(Tool.dailyQuotaUpdateNode, Config.GlobalZ_Index.DAILY_UPDATE)
        }
    },

    hideDailyQuotaUpdate: function() {
        if (Tool.dailyQuotaUpdateNode == null) return
        cc.director.getScene().removeChild(Tool.dailyQuotaUpdateNode)
    },

    addMusicControl: function() {
        MusicControl.init()
    },

    playEffect: function() {
        MusicControl.playBtnEffect()
    },

    getMusicControl: function() {
        return MusicControl.musicControl
    },

    openLoadingView: function(callBack) {
        Tool.isOpenLoading = true
        if (Tool.loadingView == null) {
            cc.loader.loadRes("prefab/common/LoadingView", function (err, prefab) {
                if(prefab == null) {
                    return                    
                }
                var newNode = cc.instantiate(prefab);
                Tool.loadingView = newNode.getComponent("LoadingView")
                cc.director.getScene().addChild(newNode, 318)
                cc.game.addPersistRootNode(newNode) // 跳場景不會關閉node
                Tool.loadingView.node.active = Tool.isOpenLoading
                if(callBack == null) return
                callBack(Tool.loadingView)                
            })
        } else {
            Tool.loadingView.node.active = true
            if(callBack == null) return
            callBack(Tool.loadingView)
        }
    },

    closeLoadingView: function() {
        Tool.getLoadingView().loadingLabel.dataID = ""
        Tool.isOpenLoading = false
        if(Tool.loadingView == null) return
        Tool.loadingView.node.active = false
    },

    getLoadingView: function() {
        return Tool.loadingView
    },

    goToLobbyScene: function(onLeave) {
        Tool.openLoadingView()
        cc.director.loadScene("Lobby", function(){
            if(onLeave!= null) onLeave()
        })
    },

    // 取得應用程式語言
    getAppLanguage: function() {
        let lang = (Config.gameCountry == Config.Country.TW) ? (Config.Language.ZH) : (Config.Language.CN)
        lang = Tool.loadData(Config.saveData.appLanguage, lang)
        return lang
    },

    // 儲存應用程式語言
    saveAppLanguage: function(lang) {
        Tool.saveData(Config.saveData.appLanguage, lang)
    },

    // 串接版 儲存返回平台的網址
    saveplatformURL: function(platformURL) {
        let decodeUrl = decodeURIComponent(platformURL)   
        Tool.saveData(Config.saveData.platformURL, decodeUrl)
    },

    // 串接版 返回到平台
    get havePlatformURL() {
        let _havePlatformURL = false
        let platformURL = Tool.loadData(Config.saveData.platformURL)
        _havePlatformURL = (platformURL != null && platformURL.length > 0)
        return _havePlatformURL
    },

    returnToPlatform: function() {
        if(cc.sys.isBrowser) {
            if(window.platformURL == '') {
                window.location.href = null
                return
            }
            window.location.href = window.platformURL
            return
        }
        
        let isReturnToPlatform = false
        let platformURL = Tool.loadData(Config.saveData.platformURL)
        if(platformURL != null && platformURL.length > 0) {
            cc.sys.openURL(platformURL)
            isReturnToPlatform = true
        }
        return isReturnToPlatform
    },

    goToLoginScene: function(onLeave=function(){}) {
        Tool.openLoadingView()
        Tool.getLoadingView().loadingLabel.dataID = Localize.LoadingLogout
        cc.director.loadScene("Login", function(){
            onLeave() 
        })
    },

    getThousandsOfBits:function(credit){
        //3為千分位
        const digital = 3
        const creditString = "" + credit
        const digtalCount = creditString.length 
        var finalStr = ""
        for(var n = creditString.length; n > 0; n-=digital)
        {
            if (n - digital <= 0)
                finalStr = creditString.substring(n - digital, n) + finalStr
            else
                finalStr =  "," + creditString.substring(n - digital, n) + finalStr
        }
        return "$" + finalStr
    }
    
    // 設定禁用裝置自動休眠功能
    , setKeepScreenOn(keepScreenOn=true) {
        if(cc.sys.isBrowser) return
        if(cc.sys.os != cc.sys.OS_IOS && cc.sys.os != cc.sys.OS_ANDROID) return
        cc.Device.setKeepScreenOn(keepScreenOn)
    }    
    
    // 是否登入過遊戲
    , get isLoginGame() {
        let isLoginGame = false
        let data = Tool.loadData(Config.saveData.isLoginGame, 'false')
        isLoginGame = ((data == 'true') ? (true) : (false))
        return isLoginGame
    }

    , set isLoginGame(isLoginGame) {
        let data = ((isLoginGame == true) ? ('true') : ('false'))
        Tool.saveData(Config.saveData.isLoginGame, data)
    }
} 

module.exports = Tool
