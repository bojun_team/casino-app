import NativeConnect from '../native/NativeConnect'

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
    },

    // use this for initialization
    onLoad: function () {
        // stream.wm77.asia/live2/stream2
        // this.playVideo()
    },
    onDestroy: function(){ 
        // NativeConnect.pauseVideo() 
        // this.stopVideo() 
    },
    playVideo:function (){
        NativeConnect.playVideo(0, this.node.getPositionY(), this.node.width, this.node.height, "rtmp://stream.wm77.asia/live2/stream3")
    },
    stopVideo:function(){
        NativeConnect.stopVideo()
    },
});
