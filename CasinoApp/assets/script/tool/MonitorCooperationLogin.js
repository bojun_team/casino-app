import Config from '../constants/Config'
import Casino from '../model/Casino'
import LocalizeMgr from '../tool/localize/LocalizeMgr'

cc.Class({
    extends: cc.Component,

    properties: {

    },
    
    onDestroy: function() {
        Casino.NativeConnect.stoplistenDeepLinkData(this.recvDeepLinkDataEvtId)
        clearTimeout(this.timeOutNum)
    }, 

    onLoad: function () {
        this.recvDeepLinkDataEvtId = Casino.NativeConnect.listenRecvDeepLinkData(this.onRecvDeepLinkData.bind(this))
    },

    logout:function(callback){
        Casino.User.logOut() 
        Casino.Tool.openLoadingView()
        cc.director.loadScene("Login", function(){
            Casino.Game = null
        })
    },

    loginSuccess: function(apiData, deepLinkData) {
        // Token or DToken 錯誤，顯示錯誤訊息
        if(apiData.ReturnCode != null) {
            // show alert
            this.showTokenFailAlert(apiData.ReturnCode)
            return
        }

        // 不同平台帳號，顯示錯誤訊息
        if(Config.isRightAppEnv(apiData.appEnv) == false) {
            this.showLoginAppEnvErrorAlert()
            return
        }

        // 重新登錄
        Casino.User.isFakeLogin = false;
        Casino.User.dToken = apiData.dToken  
        this.logout()
    },

    loginFail: function() {
        
    },

    onRecvDeepLinkData: function(deepLinkData) {
        if(deepLinkData.host != Config.DeepLinkHost.LAUNCH_GAME) return
        Casino.Tool.closeAllAlert();

        let lang = Config.webApiLanguage(deepLinkData.queryList.lang)
        LocalizeMgr.changeLanguage(lang)
        Casino.Tool.saveAppLanguage(lang)
        Casino.Tool.saveplatformURL(deepLinkData.queryList.platformURL)

        let token = deepLinkData.queryList.token
        let success = function(apiData) {
            this.loginSuccess(apiData, deepLinkData)
        }
        Casino.AppWebAPI.loginGame(token, success.bind(this), this.loginFail.bind(this))
    }
    ,
    showLoginAppEnvErrorAlert:function() {
        let titleDataID = Casino.Localize.AlertTitleError
        let msgDataID = Casino.Localize.CoopertaionAppEnvError
        Casino.Tool.closeAllAlert()
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            Casino.NativeConnect.exit()
        })
    }
    ,
    // token or dToken error alert
    showTokenFailAlert: function(errorCode) {
        let titleDataID = Casino.Localize.AlertTitleError
        let msgDataID = Casino.Localize.CoopertaionTokenFail
        Casino.Tool.closeAllAlert()
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            Casino.NativeConnect.exit()
        }, [errorCode])
    }
});
