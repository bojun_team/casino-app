cc.Class({
    extends: cc.Component,

    properties: {
        webViewWidget:cc.Widget,
        titleLabel:require("LocalizedLabel")
    },  
    
    onDestroy: function() {
        this.onCloseWebView()
    },

    setTitle: function(dataID = 0){
        this.titleLabel.dataID = dataID
    },

    openWebView:function (url, isAndroid){
        NativeConnect.openWebView( 
            this.webViewWidget.top, 
            this.webViewWidget.down, 
            this.webViewWidget.left, 
            this.webViewWidget.right, url)
        if(isAndroid == true) {
            this.node.active = false
        }
    },
    onCloseWebView:function(){
        this.node.active = false
        NativeConnect.closeWebView()
    },
    addCloseWebEventListener:function(closeWebEvent = () =>{}){
        this.closeWebEvent = closeWebEvent
    },
    removeCloseWebEventListener:function(){
        this.closeWebEvent = null
    }
});
