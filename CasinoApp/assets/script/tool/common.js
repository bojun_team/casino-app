import Ramda from 'ramda'
var { all, clone, concat, filter, head, invertObj, keys, last, mapObjIndexed, prop, propEq, pipe, repeat, sort, splitEvery, times, values } = Ramda

const countContinuous = (value, data) => {
  let count = 0
  for (var index in data) {
    if (data[index].type != value) {
      break
    }

    count++
  }

  return (count == data.length) ? count - 1 : count
}

const fill = (number, data) => (target) => {
  const count = number - target.length
  return concat(target, repeat(data, (count > 0) ? count : 0))
}

const twoDimensionalArray = (inner, outter, init) => {
  let unit = []
  let result = []
  times(() => unit.push(init), inner)
  times(() => result.push(clone(unit)), outter)

  return result
}

const capitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1)

const lowercase = (string) => string.charAt(0).toLowerCase() + string.slice(1)

const allTypeEmpty = all((x) => !(x.type || x.value))

const invertObject = (key) => pipe(mapObjIndexed(prop(key)), invertObj)

const search = (key, value) => pipe(filter(propEq(key, value)), head)

const findNumberCombine = (number, units) => {
  units = sort((a, b) => { return b - a },units)

  return units.reduce((previous, current) => {
    if (previous.remain == 0) return previous

    const count = Math.floor(previous.remain / current)
    previous.remain = previous.remain % current
    times(() => {
      previous.combine.push(current)
    }, count)

    return previous
  }, { combine: [], remain: number})
}

const lastValue = (obj) => pipe(values, last)(obj)
const lastKey = (obj) => pipe(keys, last)(obj)

const numberFormat = (n, x) => {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+$)';
    return n.toFixed(0).replace(new RegExp(re, 'g'), '$&,');
}

const caculatePoint = (deck) => {
  return deck.reduce((previous, current, index) => {
    const isPlayer = index % 2 == 0
    const number = current.number

    previous[isPlayer ? 0 : 1] += (number >= 10) ? 10 : number

    return previous
  }, [0, 0]).map((point) => {
    return point % 10
  })
}

const decode = (s) => {
  let splitNumber

  if (s.length < 100) {
    splitNumber = 5
  } else if (s.length >= 100 && s.length < 160) {
    splitNumber = 47
  } else if (s.length >= 160 && s.length < 200) {
    splitNumber = 79
  } else if (s.length >= 200) {
    splitNumber = 97
  }

  const splits = splitEvery(splitNumber, s)
  const first = splits.shift()
  const second = splits.shift()

  splits.unshift(second, first)

  return JSON.parse(atob(splits.join('')))
}

const getQuery = (field, url) => {
  var href = url ? url : window.location.href
  var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' )
  var string = reg.exec(href)
  return string ? string[1] : null
}

export default {
  allTypeEmpty: allTypeEmpty,
  countContinuous: countContinuous,
  fill: fill,
  twoDimensionalArray: twoDimensionalArray,
  capitalize: capitalize,
  lowercase: lowercase,
  invertObject: invertObject,
  search: search,
  findNumberCombine: findNumberCombine,
  lastValue: lastValue,
  lastKey: lastKey,
  numberFormat: numberFormat,
  caculatePoint: caculatePoint,
  decode: decode,
  getQuery: getQuery
}
