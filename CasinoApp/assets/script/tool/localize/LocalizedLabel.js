import localizeMgr from './LocalizeMgr'

// LocalizedLabel
cc.Class({
    extends: cc.Component,

    properties: {
        dataID: {
            get () {
                return this._dataID;
            },
            set (val) {
                if (this._dataID !== val) {
                    this._dataID = val;
                    let labelString = localizeMgr.getLocalizedString(val, localizeMgr.getCurrentLanguage())                    
                    this.updateLabel(labelString);
                }
            }
        },
        _dataID: '',
        dataArguments:[cc.String]
    },

    onDestroy () {
        localizeMgr.unRegisterLocalizeItem(this.node)
    }, 
    
    onLoad () {  
        this.fetchRender();
        localizeMgr.registerLocalizeItem(this.node)
    },

    fetchRender () {
        let label = this.node.getComponent(cc.Label);
        if (label != null) {
            this.label = label;
        }

        label = this.node.getComponent(cc.RichText);
        if (label != null) {
            this.label = label;
        }

        let editBox = this.node.getComponent(cc.EditBox);
        if(editBox != null) {
            this.editBox = editBox;
        }
    },

    updateLabel (labelString) {
        this.updateEditBox(labelString)
        if (this.label == null) return;
        if (labelString == null) labelString = ""
        labelString = labelString.replace(/\\n/g,"\n");
        this.label.string = this.stringFormat(labelString)
    },

    updateEditBox (placeholder) {
        if (this.editBox == null) return;
        if (placeholder == null) return
        this.editBox.placeholder = this.stringFormat(placeholder)
    },

    stringFormat (src) {
        return src.replace(/\{(\d+)\}/g, function (m, i) {
            if(this.dataArguments[i] == null) return ""
            return this.dataArguments[i]
        }.bind(this))
    }
});