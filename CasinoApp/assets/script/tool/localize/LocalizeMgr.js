
import Pada from './papaparse'

let isInit = false
let loadIndex = 0
let localizeDataList = {}
let atlasList = {}
let allLocalizedLabels = []
let allLocalizedSprites = {}
let currentLanguage = ""

const LocalizedLabel = "LocalizedLabel"
const LocalizedSprite = "LocalizedSprite"
const LocalizedButton = "LocalizedButton"
const LocalizedAtlas = "LocalizedAtlas"

function init(language, languageList, csvPathList) {
    if(isInit) {
        changeLanguage(language)
        return
    }

    if(currentLanguage != null) {
        currentLanguage = language
    }

    if(languageList == null) languageList = []

    if(csvPathList == null || csvPathList.length == 0){
        return
    }

    loadCSV(csvPathList, function(data){
        parseCSV(data, languageList)
    }, loadIndex, languageList)
}

function loadCSV(csvPathList, callback, loadIndex, languageList) {
    cc.loader.loadRes(csvPathList[loadIndex], function (err, data) { 
        callback(data)
        if(++loadIndex < csvPathList.length) {
            loadCSV(csvPathList, function(data){
                parseCSV(data, languageList)
            }, loadIndex, languageList)
        } else {
            isInit = true        
            updateAllLabels(currentLanguage)
        }
    });
}

function parseCSV(data, languageList) {
    let parsedCSV = Pada.parse(data)

    for(let index in parsedCSV.data) {
        let dataID = parsedCSV.data[index][0]
        localizeDataList[dataID] = {}

        for(let listIndex in languageList) {
            let languageKey = languageList[listIndex]
            localizeDataList[dataID][languageKey] = parsedCSV.data[index][(Number(listIndex)+1)]
        }
    }
    localizeDataList[""] = ""
}

function getLocalizedString(dataID, language) {
    if(localizeDataList[dataID] == null) {
        return ""
    }
    return localizeDataList[dataID][language]
}

function getLocalizedAtlas(dataID, language) {
    let  atlasItem = atlasList[dataID]
    
    if(atlasItem == null) {
        return null
    }

    let atlas = atlasItem.atlas[language]
    if(atlas == null) {
        return
    }
    return atlas
}

function changeLanguage(language) {
    if(language == currentLanguage) {
        return
    }
    currentLanguage = language
    updateAllLabels(currentLanguage)
    updateAllSprites(currentLanguage)
}

function updateAllLabels(language) {
    if(!isInit) return
        
    for (let i = 0; i < allLocalizedLabels.length; ++i) {
        let label = allLocalizedLabels[i];
        let dataID = label.dataID
        let labelString = getLocalizedString(dataID, language)
        label.updateLabel(labelString);
    }
}

function updateAllSprites(language) {
    for(let dataID in allLocalizedSprites) {
        updateSprite(dataID, null)
    }
}

function registerLocalizeItem(item) {
    let label = item.getComponent(LocalizedLabel)
    if(label!= null) {
        allLocalizedLabels.push(label)
        if(!isInit) return
        let dataID = label.dataID
        let labelString = getLocalizedString(dataID, currentLanguage)
        label.updateLabel(labelString);
    }

    let sprite = item.getComponent(LocalizedSprite)
    if(sprite!= null) {
        let dataID = sprite.dataID
        if(allLocalizedSprites[dataID] == null) {
            allLocalizedSprites[dataID] = []
        }
        allLocalizedSprites[dataID].push(sprite)
        updateSprite(dataID, sprite)        
    }

    let button = item.getComponent(LocalizedButton)
    if(button!= null) {
        let dataID = button.dataID
        if(allLocalizedSprites[dataID] == null) {
            allLocalizedSprites[dataID] = []
        }
        allLocalizedSprites[dataID].push(button)
        updateSprite(dataID, button)        
    }

    let atlas = item.getComponent(LocalizedAtlas)
    if(atlas!= null) {
        let dataID = atlas.dataID
        if(atlasList[dataID] == null) {
            atlasList[dataID] = {
                registerCount: 1,
                atlas:{}
            }
            let spriteAtlasSet = atlas.spriteAtlasSet
            spriteAtlasSet.forEach((spriteAtlasItem)=>{
                let language = spriteAtlasItem.language
                let spriteAtlas = spriteAtlasItem.spriteAtlas
                atlasList[dataID].atlas[language] = spriteAtlas
            })
        } else {
            atlasList[dataID].registerCount++
        }
        
        updateSprite(dataID, null)
    }
}

function updateSprite(dataID, localizedSprite) {
    let localizedSpriteList = allLocalizedSprites[dataID]
    if(localizedSprite != null) {
        localizedSpriteList = [localizedSprite]
    }

    if(localizedSpriteList == null) {
        return
    }

    localizedSpriteList.forEach((localizedSprite) => {
        let atlas = getLocalizedAtlas(dataID, currentLanguage)
        localizedSprite.updateSprite(atlas)
    })
}

function unRegisterLocalizeItem(item) {
    let label = item.getComponent(LocalizedLabel)
    if(label!= null) {
        let index = allLocalizedLabels.indexOf(label)
        if(index == -1) return
        allLocalizedLabels.splice(index, 1);
    }

    let sprite = item.getComponent(LocalizedSprite)
    if(sprite!= null) {

        for(let dataID in allLocalizedSprites) {
            let index = allLocalizedSprites[dataID].indexOf(sprite)
            if(index == -1) {
                continue
            }
            allLocalizedSprites[dataID].splice(index, 1);
        }
        
    }

    let button = item.getComponent(LocalizedButton)
    if(button!= null) {
        for(let dataID in allLocalizedSprites) {
            let index = allLocalizedSprites[dataID].indexOf(button)
            if(index == -1) {
                continue
            }
            allLocalizedSprites[dataID].splice(index, 1);
        }
    }

    let atlas = item.getComponent(LocalizedAtlas)
    if(atlas!= null) {
        let dataID = atlas.dataID
        let registerCount = --atlasList[dataID].registerCount
        if(registerCount == 0) {
            delete atlasList[dataID]            
        }
    }
}

function getCurrentLanguage() {
    return currentLanguage
}

module.exports = {
    init:init,
    changeLanguage:changeLanguage,
    getCurrentLanguage:getCurrentLanguage,    
    getLocalizedString:getLocalizedString,
    registerLocalizeItem:registerLocalizeItem,
    unRegisterLocalizeItem:unRegisterLocalizeItem,
}