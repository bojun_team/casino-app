import localizeMgr from './LocalizeMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        dataID: cc.String,
        spriteAtlasSet: {
            default:[],
            type:require("SpriteAtlasSet")
        }
    },

    onDestroy () {
        localizeMgr.unRegisterLocalizeItem(this.node)
    }, 

    onLoad () {
        localizeMgr.registerLocalizeItem(this.node)  
        
    }
});
