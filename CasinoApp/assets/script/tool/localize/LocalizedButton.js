import localizeMgr from './LocalizeMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        dataID: cc.String,
    },

    onDestroy () {
        localizeMgr.unRegisterLocalizeItem(this.node)
    }, 

    onLoad () {
        this.fetchRender();
        if(this.button != null) {
            localizeMgr.registerLocalizeItem(this.node)  
        }  
    },

    fetchRender () {
        let button = this.getComponent(cc.Button);
        if (button != null) {
            this.button = button;
        }
    },

    updateSprite (atlas) {
        if (this.button == null) {
            return
        }

        if(atlas == null) {
            return
        }
        
        if(this.button.normalSprite != null) {
            let name = this.button.normalSprite.name
            let spriteFrame = atlas.getSpriteFrame(name)
            if(spriteFrame != null) {
                this.button.normalSprite = spriteFrame
            }
        }

        if(this.button.pressedSprite != null) {
            let name = this.button.pressedSprite.name
            let spriteFrame = atlas.getSpriteFrame(name)
            if(spriteFrame != null) {
                this.button.pressedSprite = spriteFrame
            }
        }

        if(this.button.hoverSprite != null) {
            let name = this.button.hoverSprite.name
            let spriteFrame = atlas.getSpriteFrame(name)
            if(spriteFrame != null) {
                this.button.hoverSprite = spriteFrame
            }
        }

        if(this.button.disabledSprite != null) {
            let name = this.button.disabledSprite.name
            let spriteFrame = atlas.getSpriteFrame(name)
            if(spriteFrame != null) {
                this.button.disabledSprite = spriteFrame
            }
        }
    }
});
