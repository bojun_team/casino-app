import localizeMgr from './LocalizeMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        dataID: cc.String,
    },

    onDestroy () {
        localizeMgr.unRegisterLocalizeItem(this.node)
    }, 

    onLoad () {
        this.fetchRender();
        if(this.sprite != null) {
            localizeMgr.registerLocalizeItem(this.node)  
        }  
    },

    fetchRender () {
        let sprite = this.getComponent(cc.Sprite);
        if (sprite != null) {
            this.sprite = sprite;
        }
    },

    updateSprite (atlas) {
        if (this.sprite == null) {
            return
        }

        if(atlas == null) {
            return
        }

        let spriteFrame = atlas.getSpriteFrame(this.sprite.spriteFrame.name)
        if(spriteFrame == null) {
            return
        }
        this.sprite.spriteFrame = spriteFrame;
    }
});