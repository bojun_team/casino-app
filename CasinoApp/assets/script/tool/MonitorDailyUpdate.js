import Tool from "../tool/Tool"
import Config from '../constants/Config'
import RxJsMgr from '../model/RxJsMgr'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {

    },

    onDestroy: function(){
        for(let key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },

    // use this for initialization
    onLoad: function () {
        this.subscribes = []
        let restoreCredit$ = Casino.User.propChange$.restoreCredit
        this.subscribes[this.subscribes.length] = restoreCredit$.subscribe({
            next: restoreCreditType => {
                if (1 == restoreCreditType) {   
                    Tool.showDailyQuotaUpdate()
                } else {
                    Tool.hideDailyQuotaUpdate()
                }
            },  
        });

        let connectState$ = RxJsMgr.connectData.propChange$.connectState
        this.subscribes[this.subscribes.length] = connectState$.subscribe({
            next: type => {
                if(Config.ConnectState.DISCONNECTED == type || Config.ConnectState.ERROR == type) {
                    Tool.hideDailyQuotaUpdate()
                }
            }
        });
    },

});
