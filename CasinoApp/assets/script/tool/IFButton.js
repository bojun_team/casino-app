cc.Class({
    extends: cc.Component,

    editor: {
        executeInEditMode: true,
    },
    properties: {
        normalNode: cc.Node,
        pressedNode: cc.Node,
        disabledNode: cc.Node,

        clickEvents:{
            default: [],
            type: cc.Component.EventHandler,

        },
 
        isCancelWhenSlideX: false,
        isCancelWhenSlideY: false,
        _interactable : {
            default : true,
            visible : false,
        },
        interactable: {
            get: function () {
                return this._interactable;
            },
            set: function (interactable) {
                this._interactable = interactable;
                if(this.disabledNode != null)
                    this.disabledNode.active = !interactable
                if(this.normalNode != null)
                    this.normalNode.active = interactable
                if(this.pressedNode != null)
                    this.pressedNode.active = false 
            }
        }
    },

    // use this for initialization
    onLoad: function () { 

        this.isCancel = false
        this.interactable = this.interactable

        this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
            this.isCancel = false
            this.setToPressed()
            this.startLocation = new cc.v2(event.getLocationX(), event.getLocationY())
            return true;
        }.bind(this))

        this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) { 
            if (this.isCancel) return
            const cancelGap = 10
            let location = new cc.v2(event.getLocationX(), event.getLocationY()) 
            if (this.isCancelWhenSlideY || this.isCancelWhenSlideX){ 
                if (this.lastLocation ==  null){
                    this.lastLocation = location
                }
                else if (this.isCancelWhenSlideY && Math.abs(this.startLocation.y - location.y) > cancelGap){
                    this.isCancel = true  
                    this.setToNormal()
                    return
                } 
                else if (this.isCancelWhenSlideX && Math.abs(this.startLocation.x - location.x) > cancelGap){
                    this.isCancel = true 
                    this.setToNormal()
                    return
                } 
                this.lastLocation = location
            } 
            if( this.isTouchInNode(event) ) {
                this.setToPressed()
            } else {
                this.setToNormal()
            }
        }.bind(this))

        this.node.on(cc.Node.EventType.TOUCH_END, function (event) { 
            this.setToNormal()
            if (this._interactable){
                this.sendEvent(event)
            }
            this.isCancel = false
        }.bind(this))

        this.node.on(cc.Node.EventType.TOUCH_CANCEL, function () { 
            this.setToNormal() 
            this.isCancel = false
        }.bind(this))
    },

    setToPressed: function () {
        if(!this.interactable) return
        this.normalNode.active = false
        this.pressedNode.active = true
    },
    setToNormal : function () {
        if(!this.interactable) return
        this.normalNode.active = true
        this.pressedNode.active = false
    },
    addClickEvent:function(clickEvent){
        this.clickEvent = clickEvent
    },
    isTouchInNode:function(event){
        let location = new cc.v2(event.getLocationX(), event.getLocationY()) 
        location = this.node.convertToNodeSpace(location) 
        return (location.x > 0 && location.x < this.node.width && 
                location.y > 0 && location.y < this.node.height)  
    },
    sendEvent:function(event){  
        if (this.isCancel) return
        if (this.isTouchInNode(event)){
            if (this.clickEvent != null) this.clickEvent(this)
            cc.Component.EventHandler.emitEvents(this.clickEvents, event)
        } 
        
    }
});
