import Localize from '../constants/Localize'
import Tool from "../tool/Tool"
import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },

    onLoad: function () {
        this.subscribes = []
        
        let game = Casino.Game
        let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                if(gameStatus == Config.gameState.CancelRun) {
                    Casino.showReturnMoneyAlert()
                }
            },
        });
    },
    
});
