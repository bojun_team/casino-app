cc.Class({
    extends: cc.Component, 
    properties: {
        checkSprite:cc.Sprite,
        touchBtn:cc.Button,
        isCheck:false,
        checkOffSprite:cc.Sprite,
        interactable:true  
    },    
 
    // use this for initialization
    onLoad: function () {
        this.checkSprite.node.active = this.isCheck
        this.createEventHandler("touchCheckBtn", this.touchBtn, "CheckBox")

    },
    createEventHandler:function(strFunName, btn, componentName) {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;
        clickEventHandler.component = componentName;
        clickEventHandler.handler = strFunName; 
        btn.clickEvents.push(clickEventHandler);
    },
    touchCheckBtn:function()
    { 
        this.isCheck = !this.isCheck
        this.checkSprite.node.active = this.isCheck
        if (this.callBackFun != null){
            this.callBackFun (this.isCheck)
        }
    },
    
    setCallBackFun:function(evn){
        this.callBackFun = evn
    },

    setIsCheck:function(isCheck) {
        this.isCheck = isCheck 
        this.checkSprite.node.active = (isCheck && this.interactable)
    },

    setButtonInteractable: function(interactable) {
        this.interactable = interactable
        this.touchBtn.interactable = interactable

        if(interactable == false) {            
            this.checkOffSprite.node.active = false
            this.checkSprite.node.active = false
        } else {            
            this.checkOffSprite.node.active = true
            this.checkSprite.node.active = this.isCheck
        }
    }    
});
