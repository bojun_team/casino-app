
cc.Class({
    extends: cc.Component,

    properties: {

    },

    onLoad: function () {
        // 只需要在Android執行
        if (cc.sys.os != cc.sys.OS_ANDROID) return

        let label = this.node.getComponent(cc.Label)
        if(label == null) return

        cc.game.on(cc.game.EVENT_SHOW, ()=>{
            this.scheduleOnce(()=>{
                this.reflashLabel(label, this.node.anchorX)
            })
        })
    },

    reflashLabel: function(label, anchorX) {
        const LEFT_ANCHOR = 0
        let RIGHT_ANCHOR = 1
        let {string} = label
        if(anchorX == LEFT_ANCHOR) {
            label.string = string + " "
        } else if (anchorX == RIGHT_ANCHOR) {
            label.string = " " + string
        } else {
            label.string = " " + string + " "
        }
    }

});
