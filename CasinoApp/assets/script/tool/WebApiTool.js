import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'

export default class WebApiTool {
    static get(url, callback) {
        let xhr = new XMLHttpRequest()
        xhr.open('GET', url, true)        
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4)
            callback(xhr.status, xhr.responseText, xhr.readyState)
        }
        xhr.send()
    }

    // params 請求參數 ("id=1&id=2&id=3")
    static post(url, params, callback) {
        let xhr = new XMLHttpRequest()
        xhr.withCredentials = true
        xhr.open("POST", url)
        xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8")
        xhr["onloadend"] = function() {
            callback(xhr.status, xhr.responseText, xhr.readyState)
        }
        xhr.send(params)
    }
    
}
