import Tool from "../tool/Tool"

cc.Class({
    extends: cc.Component,

    properties: {
    },
    betEffect: function() {
        Tool.getMusicControl().betEffect()
    },
    onLoad: function () {
        this.node.on("click", this.betEffect, this)

        let iFButton = this.node.getComponent("IFButton")
        if(iFButton != null) {
            let event = new cc.Component.EventHandler()
            event.target = this.node;
            event.component = "BetEffect";
            event.handler = "betEffect";
            iFButton.clickEvents.push(event)
        }
    },

});
