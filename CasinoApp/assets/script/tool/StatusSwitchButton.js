cc.Class({
    extends: cc.Component,

    editor: {
        executeInEditMode: true,
    },
    properties: {
        button: cc.Button,
        normalNode: cc.Node,
        pressedNode: cc.Node,
        disabledNode: cc.Node,

        _interactable : {
            default : true,
            visible : false,
        },
        interactable: {
            get: function () {
                return this._interactable;
            },
            set: function (interactable) {
                this._interactable = interactable;
                this.disabledNode.active = !interactable
                this.normalNode.active = interactable
                this.pressedNode.active = false
                this.button.interactable = interactable
            }
        }
    },

    // use this for initialization
    onLoad: function () { 
        this.interactable = this.button.interactable

        this.button.node.on(cc.Node.EventType.TOUCH_START, function () {
            this.setToPressed()
            return true;
        }.bind(this))

        this.button.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            let location = this.node.convertToNodeSpaceAR(new cc.v2(event.getLocationX(), event.getLocationY())) 
            if( Math.abs(location.x) > this.button.node.width*0.5 ||
                Math.abs(location.y) > this.button.node.height*0.5
            ) {
                this.setToNormal()
            } else {
                this.setToPressed()
            }
        }.bind(this))

        this.button.node.on(cc.Node.EventType.TOUCH_END, function () {
            this.setToNormal()
        }.bind(this))

        this.button.node.on(cc.Node.EventType.TOUCH_CANCE, function () {
            this.setToNormal()
        }.bind(this))
    },

    setToPressed: function () {
        if(!this.interactable) return
        this.normalNode.active = false
        this.pressedNode.active = true
    },
    setToNormal : function () {
        if(!this.interactable) return
        this.normalNode.active = true
        this.pressedNode.active = false
    },
    testButton : function () {
        this.button.interactable = !this.button.interactable
        this.interactable = this.button.interactable
    }
});
