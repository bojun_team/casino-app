
// IFBlinkButton.js

cc.Class({
    extends: require("IFButton"),

    properties: {
        blinkNode: cc.Node,
    },

    onLoad: function () {
        this._super()
        this.startBlink()
    },

    setToPressed: function () {
        this._super()
        this.stopBlink()
    },

    setToNormal : function () {
        this._super()
        this.startBlink()
    },

    startBlink: function() {
        if(this.blinkNode == null) return
        this.blinkNode.stopAllActions()
        this.blinkNode.opacity = 0

        // 閃爍的週期時間
        const fadeDuration = 1
        var fadeIn = cc.fadeIn(fadeDuration / 2)
        var delay = cc.delayTime(0.2)
        var fadeOut = cc.fadeOut(fadeDuration / 2)
        var seq = cc.sequence(fadeIn, delay, fadeOut)
        var fadeInOutForever = cc.repeatForever(seq)
        this.blinkNode.runAction(fadeInOutForever)
    },

    stopBlink: function() {
        if(this.blinkNode == null) return
        this.blinkNode.stopAllActions()
        this.blinkNode.opacity = 0
    }

});
