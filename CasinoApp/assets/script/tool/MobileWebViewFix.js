

import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // use this for initialization
    onLoad: function () {
        this.subscribes = []

        if (!cc.sys.isBrowser) return
        this.subscribes[this.subscribes.length] = Casino.isFullScreen$.subscribe({
            next: isFullScreen => {
                this.unscheduleAllCallbacks()
                this.scheduleOnce(()=>this.sort(), 1.2)
            }
        })

        this.subscribes[this.subscribes.length] = Casino.isPortrait$.subscribe({
            next: isPortrait => {
                this.unscheduleAllCallbacks()
                this.scheduleOnce(()=>this.sort(), 0.2)
            }
        })
    },
    
    onDestroy:function() {
        if(this.subscribes.length == 0) return
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    // 排版
    sort: function() {
        let widget = this.node.getComponent(cc.Widget)
        if(widget != null) {
            widget.enabled = true
        }
        // 更新下一層的對齊
        this.updateSubNodeWidget(this.node)
        
        // 大約等 widget 排好 在排 IFPercentLayout
        this.scheduleOnce(()=>this.sortPercentLayouts(this.node), 0.2)
    },

    sortPercentLayouts: function(node) {
        let percentLayouts  = node.getComponentsInChildren('IFPercentLayout')
        for(let index in percentLayouts) {
            percentLayouts[index].sortChildren()
        }
    },

    // 更新下一層的對齊
    updateSubNodeWidget(node) {
        let children = node.children;
        for (var i = 0; i < children.length; ++i) {
            let subNode =  children[i]
            let widget = subNode.getComponent(cc.Widget)
            if(widget != null) {
                widget.enabled = true
            }
            this.updateSubNodeWidget(subNode)
        }
    },

});
