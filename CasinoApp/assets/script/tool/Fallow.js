cc.Class({
    extends: cc.Component,

    properties: {
        target:cc.Node,
        
    },

    // use this for initialization
    onLoad: function () {

    }, 
    
    update:function(){
        if (!this.node.active)return
        const targetPos = this.target.getPosition()
        const targetWorldPos = this.target.parent.convertToWorldSpaceAR(targetPos) 
        const pos = this.node.parent.convertToNodeSpaceAR(targetWorldPos)
        this.node.setPosition(cc.v2(pos.x, pos.y))
    }
});
