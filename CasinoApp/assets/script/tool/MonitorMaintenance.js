import Tool from "../tool/Tool"
import Config from '../constants/Config'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
        this.unschedule(this.upateEverySec);
    }, 
    onLoad: function () {
        this.subscribes = []

        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        let leaveGame = () => {
            Casino.Game.leaveGame(()=>{
                Tool.goToLobbyScene();
            }, () => {
                SignalR.disconnect
            });
        }
        let showAlert = (msgDataID) => {
            setTimeout(()=>{
                leaveGame();
            }, 2000);
            Casino.Tool.showToast(msgDataID, 2);
        }
        let checkStatus = (gameStatus, tableStatus) => {
            if(gameStatus == Config.gameState.Maintenance) {
                showAlert("MaintenanceToast");
            } else if(gameStatus == Config.gameState.HalfTime) {
                if((tableStatus & Config.tableStatus.Closed) == Config.tableStatus.Closed) {
                    showAlert("MaintenanceClose");
                } 
            }  
        }
        this.subscribes[this.subscribes.length] = table.propChange$.gameStatus.subscribe({
            next: gameStatus => { 
                checkStatus(gameStatus, table.tableStatus);
            }, 
        }); 
        this.subscribes[this.subscribes.length] = table.propChange$.tableStatus.subscribe({
            next: tableStatus => { 
                checkStatus(table.status, tableStatus);
            }, 
        }); 
    },

});
