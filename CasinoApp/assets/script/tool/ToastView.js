import Localize from '../constants/Localize'
import Config from '../constants/Config'

cc.Class({
    extends: cc.Component,

    properties: {
        toastNode:cc.Node,
        toastBg:cc.Sprite,
        toastLab:require("LocalizedLabel"),
    },

    // use this for initialization
    onLoad: function () {

    },

    //------------------------------------------------------------
    // 顯示 Toast
    // @param #integer iType Toast 類型，請參考 self.TOAST_TYPE_
    // @param #string strMessage 顯示訊息
    // @param #float fPositionX X 座標位置
    // @param #float fPositionY Y 座標位置
    //------------------------------------------------------------
    addToast:function(dataID, toastTime, dataArguments){
        var runningScene = cc.director.getScene()

        if (null == runningScene) return
        if (null == dataID || "" == dataID) return
        this.toastNode.setPosition(new cc.Vec2(0, 0)) // 設定位置

        // 多國語言文字設定
        if(dataArguments == null) dataArguments = []
        this.toastLab.dataArguments = dataArguments
        this.toastLab.dataID = dataID // 設定說明文字ID        

        // todo 依說明文字長度，調整背景圖片寬度
        // 注意：要在runningScene:addChild(baseNode)設定才有效果
        // imgBackground:getCustomSize() 可以取得背景圖片原圖的高度跟寬度
        // imgBackground:getCapInsets() 可以取得背景圖片九宮圖中間那格的高度跟寬度
        // lblText:getVirtualRendererSize() 可以取得文字寬度 
        // var iNewWidth = this.toastBg.getCustomSize().width - this.toastBg.getCapInsets().width + this.toastLab.size.width
        // -- 設定背景圖片寬度只能用比例的方式去算
        // 要加上左右九宮格邊界的長度
        this.toastBg.node.width = this.toastLab.node.width + this.toastBg.getInsetLeft() + this.toastBg.getInsetRight()
        // this.toastBg.node.height = this.toastLab.node.height + 100
        // 依 Toast 類型設定背景圖片
        // if self.TOAST_TYPE_WARNING == self.m_toastType then
        //     self.m_imgBackground:loadTexture("res/Component/IFCToastLayer/ifc_toast_background_warning.png")
        // elseif self.TOAST_TYPE_GAMESTATUS == self.m_toastType then
        //     self.m_imgBackground:loadTexture("res/Component/IFCToastLayer/ifc_toast_background_gameToast.png")
        // end
        this.toastNode.active = false
        this.toastNode.runAction(this.choseActionSequence(toastTime))
    },

    //------------------------------------------------------------
    // 要實現哪種類型的動作
    // return actionSequence 要實現的動作
    //------------------------------------------------------------
    choseActionSequence:function(toastTime){
        if(toastTime == null) {
            toastTime = Config.TOAST_SHOW_TIME_DEFAULT
        }
        // 從 parent 中移除
        var callback = function(){
             this.toastNode.removeFromParent()
        }

        var actionSequence = null
        var actionCallback = cc.callFunc(callback, this)
        // if self.TOAST_TYPE_GAMESTATUS == self.m_toastType then
            this.toastNode.active = true
            // 延遲兩秒
            actionSequence = cc.sequence(cc.delayTime(toastTime), actionCallback)
        // // elseif self.TOAST_TYPE_WARNING == self.m_toastType then
        //     -- 持續 3 秒，閃動 3 次
        //     local actionBlink = cc.Blink:create( 3 , 3 )
        //     actionSequence = cc.Sequence:create(actionBlink, actionCallback)
        // end
        return actionSequence
    }
       
    
});
