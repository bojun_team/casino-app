
// JackpotWinView.js
import Tool from '../tool/Tool'

const CHANGE_TIME = 0.3

cc.Class({
    extends: cc.Component,

    properties: {
        jpWin: cc.Node,
        winMoneyLabel: cc.Label,
        exploAnimation: cc.Animation
    },

    onLoad: function () {
        this.jpWin.opacity = 0
        this.jpWin.scaleX = 0
        this.jpWin.scaleY = 0
        this.node.active = false
    },
    
    playAnimation: function(winAmount) {
        this.node.active = true
        this.winMoneyLabel.string = Tool.getThousandsOfBits(winAmount)

        if (cc.sys.isBrowser) {
            this.jpWin.opacity = 255
            this.jpWin.scale = 1
            this.jpWin.runAction(
                cc.sequence(
                    cc.delayTime(1),
                    cc.callFunc(()=> { 
                        this.node.removeFromParent(true)
                        this.destroy()
                    })
                )
            )
            return
        }

        let exploBefore = cc.sequence(
            cc.spawn(
                cc.fadeIn(CHANGE_TIME),
                cc.scaleTo(CHANGE_TIME, 1)
            ),
            cc.callFunc(() => {
                this.exploAnimation.play()
            }, this)
        )
        let exploAfter = cc.sequence(
            cc.spawn(
                cc.fadeOut(CHANGE_TIME),
                cc.scaleTo(CHANGE_TIME, 0)
            ),
            cc.callFunc(() => {
                this.node.removeFromParent(true)
                this.destroy()
            }, this)
        )
        this.exploAnimation.on('finished', ()=> {
            this.node.runAction(exploAfter)
        }, this)
        this.jpWin.runAction(exploBefore)
    }

});

var JackpotWinView = {
    getWinView(callback=function(winView){}) {
        if(this.prefab == null) {
            cc.loader.loadRes("prefab/common/JackpotWinView", (err, prefab) => {
                this.prefab = prefab
                this.getWinView(callback)
            })
        } else {
            let newNode = cc.instantiate(this.prefab)
            cc.director.getScene().addChild(newNode, 998)
            let winView = newNode.getComponent("JackpotWinView")
            callback(winView)
        }
    },

    playAnimation(winAmount) {
        this.getWinView((winView) => {
            winView.playAnimation(winAmount)
        })
    }
}

module.exports = JackpotWinView
