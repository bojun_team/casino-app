import Tool from "../tool/Tool"

cc.Class({
    extends: cc.Component,

    properties: {
    },
    playEffect: function() {
        Tool.playEffect()
    },
    onLoad: function () {
        this.node.on("click", this.playEffect , this)

        let iFButton = this.node.getComponent("IFButton")
        if(iFButton != null) {
            let event = new cc.Component.EventHandler()
            event.target = this.node;
            event.component = "ButtonEffectEvn";
            event.handler = "playEffect";
            iFButton.clickEvents.push(event)
        }
    },

});
