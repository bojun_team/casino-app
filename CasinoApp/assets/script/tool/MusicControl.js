import Config from '../constants/Config'
import Casino from '../model/Casino'

const MUSIC_CONTROL = 'MusicControl'
const PLAY_MUSIC = 1
const CLOSE_MUSIC = 0
const DELAY_SEC = 2
cc.Class({
    extends: cc.Component,

    properties: {

    },

    // use this for initialization
    onLoad: function () {
        this.subscribes = [] // 訂閱清單
        this.noPlayEffect = false //不可播音效
        
        // 音樂初始化
        this.BgMusic = cc.audioEngine.playMusic(cc.url.raw(Config.Audio.BG_MUSIC))
        cc.audioEngine.setLoop(this.BgMusic, true);
        // 背景音樂控制
        cc.game.on(cc.game.EVENT_HIDE, function () { 
            this.noPlayEffect = true
            this.setMusicEnabled(false)
        }.bind(this)); 
        cc.game.on(cc.game.EVENT_SHOW , function () {  
            this.noPlayEffect = false
            this.setMusicEnabled(Casino.User.musicEnabled)
        }.bind(this));
    },

    subscribeMusicEnabled:function() {
        // 存檔音樂控制
        let musicEnabled$ = Casino.User.propChange$.musicEnabled
        this.subscribes.push(musicEnabled$.subscribe({
            next: musicEnabled => {
                this.setMusicEnabled(musicEnabled)
            }
        }))
    },

    closeAction:function(){
        this.node.stopAllActions()
    },

    setMusicEnabled: function(musicEnabled){
        let volume = musicEnabled ? PLAY_MUSIC : CLOSE_MUSIC
        cc.audioEngine.setVolume(this.BgMusic, volume)
    },

    playBtnEffect: function() {
        if(!Casino.User.effectEnabled || this.noPlayEffect) return
        cc.audioEngine.playEffect(cc.url.raw(Config.Audio.BUTTON_CLICK))
    }, 
 
    gameStatus: function(status) {
        if(!Casino.User.effectEnabled || this.noPlayEffect) return 
        if(Casino.isPlayingWaterCubeVideoEffect) return
        var effect = Config.gameStatusEffect(status)
        if(effect == '') return
        cc.audioEngine.playEffect(cc.url.raw(effect))
    },  
 
    countDown:function(sec){
        if(!Casino.User.effectEnabled || this.noPlayEffect) return 
        if(Casino.isPlayingWaterCubeVideoEffect) return
        if(sec <= Config.LAST_5_SEC && sec >= 0)
            cc.audioEngine.playEffect(cc.url.raw(Config.countDownEffect(sec)))
    },

    baccaratWhichWin:function(which, pokerPoint){
        if(!Casino.User.effectEnabled || this.noPlayEffect) return 
        if(Casino.isPlayingWaterCubeVideoEffect) return
        cc.audioEngine.playEffect(cc.url.raw(Config.baccaratWhichWinEffect(which)))
        let playEffect = cc.callFunc(function() {
            cc.audioEngine.playEffect(cc.url.raw(Config.pointEffect(pokerPoint)))                        
        }, this)
        this.node.runAction(cc.sequence(cc.delayTime(DELAY_SEC), playEffect))
    },

    betEffect:function() {
        if(!Casino.User.effectEnabled || this.noPlayEffect) return
        cc.audioEngine.playEffect(cc.url.raw(Config.Audio.CLICK_CHIP))
    }, 

    winMoney:function(){
        if(!Casino.User.effectEnabled || this.noPlayEffect) return
        let playEffect = cc.callFunc(function() {
            cc.audioEngine.playEffect(cc.url.raw(Config.Audio.WIN_MONEY))                        
        }, this)
        this.node.runAction(cc.sequence(cc.delayTime(DELAY_SEC), playEffect))
    }
});

var MusicControl = {
    get musicControl() {
        return this._musicControl
    },

    getMusicControl(callback=function(musicControl){}) {
        if(this._musicControl == null) {
            cc.loader.loadRes(Config.Prefab.MUSIC_NODE, function (err, prefab) {
                let newNode = cc.instantiate(prefab)
                cc.director.getScene().addChild(newNode)
                cc.game.addPersistRootNode(newNode) // 跳場景不會關閉node
                this._musicControl = newNode.getComponent(MUSIC_CONTROL)
                callback(this._musicControl)
            }.bind(this))
        } else {
            callback(this._musicControl)
        }
    },

    init() {
        this.getMusicControl((musicControl) => {
            musicControl.subscribeMusicEnabled()
        })
    },

    playBtnEffect() {
        this.getMusicControl((musicControl) => {
            musicControl.playBtnEffect()
        })
    },

    baccaratWhichWin(which='', pokerPoint=0) {
        this.getMusicControl((mc) => {
            mc.baccaratWhichWin(which, pokerPoint)
        })
    },

    closeAction() {
        this.getMusicControl((mc) => {
            mc.closeAction()
        })
    },

    winMoney() {
        this.getMusicControl((mc) => {
            mc.winMoney()
        })
    }
}

module.exports = MusicControl