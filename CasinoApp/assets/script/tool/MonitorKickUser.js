import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component, 
    onLoad: function () {
        this.subscribes = []
        let kickUser$ = Casino.User.propChange$.kickUser
        this.subscribes[this.subscribes.length] = kickUser$.subscribe({
            next: type => {
                if (Config.kickState.default == type) return
                Casino.Tool.hideDailyQuotaUpdate()
                switch (type) {
                    case Config.kickState.noAuthority:
                        this.showAlert(Casino.Localize.KickUserNoAuthority)
                        break;
                    case Config.kickState.relogin:
                        this.showAlert(Casino.Localize.KickUserTypeRelogin)
                        break;  
                    default:
                        this.showAlert(Casino.Localize.KickUser)
                        break; 
                }
            }
        });
    }, 
    onDestroy: function(){
        for(let key in this.subscribes){  
            this.subscribes[key].unsubscribe()
        } 
    },

    showAlert:function(msgDataID){ 
        let titleDataID = ""
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            Casino.User.logOut()
            Casino.Tool.goToLoginScene() 
        }.bind(this))
    },
});
