
import Game from '../model/Game'
import Casino from '../model/Casino'
import SoundControlTool from './SoundControlTool'
var { EffectNameTool, EffectName, LAST_5_SEC, DELAY_SEC } = SoundControlTool

cc.Class({
    extends: cc.Component,

    properties: {
        bgMusic: cc.AudioSource,
        buttonClick: cc.AudioClip,
        startBet: cc.AudioClip,
        endBet: cc.AudioClip,
        halfTime: cc.AudioClip,
        last5: cc.AudioClip,
        dindin: cc.AudioClip,
        playerWin: cc.AudioClip,
        bankerWin: cc.AudioClip,
        tie: cc.AudioClip,
        clickChip: cc.AudioClip,
        winMoney: cc.AudioClip,
        point0: cc.AudioClip,
        point1: cc.AudioClip,
        point2: cc.AudioClip,
        point3: cc.AudioClip,
        point4: cc.AudioClip,
        point5: cc.AudioClip,
        point6: cc.AudioClip,
        point7: cc.AudioClip,
        point8: cc.AudioClip,
        point9: cc.AudioClip
    },

    onLoad: function () {
        
    },

    closeAction: function() {
        this.node.stopAllActions()
    },

    setBgMusicMute: function(isMute) {
        this.bgMusic.mute = isMute
    },
    
    playEffect: function(effectName) {
        if(this[effectName] == null) return
        cc.audioEngine.play(this[effectName], false, 1);
    },

    delayToPlayEffect: function(effectName, delayTimeSec) {
        let playEffect = cc.callFunc(function() {
            this.playEffect(effectName)
        }, this)
        this.node.runAction(cc.sequence(cc.delayTime(delayTimeSec), playEffect))
    }
});


var SoundControl = {
    getSoundControl(callback=function(soundControl){}) {
        if(this._soundControl == null) {
            cc.loader.loadRes('prefab/common/SoundControl', function (err, prefab) {
                let newNode = cc.instantiate(prefab)
                cc.director.getScene().addChild(newNode)
                cc.game.addPersistRootNode(newNode) // 跳場景不會關閉node
                this._soundControl = newNode.getComponent('SoundControl')
                callback(this._soundControl)
            }.bind(this))
        } else {
            callback(this._soundControl)
        }
    },

    closeAction() {
        this.getSoundControl((soundControl) => {
            soundControl.closeAction()
        })
    },

    setBgMusicMute(isMute=false) {
        this.getSoundControl((soundControl) => {
            soundControl.setBgMusicMute(isMute)
        })
    },

    playEffect(effectName='') {
        this.getSoundControl((soundControl) => {
            soundControl.playEffect(effectName)
        })
    },

    delayToPlayEffect: function(effectName='', delayTimeSec=0) {
        this.getSoundControl((soundControl) => {
            soundControl.delayToPlayEffect(effectName, delayTimeSec)
        })
    },

    playBtnEffect() {
        this.playEffect(EffectName.BUTTON_CLICK)
    },

    gameStatus(status=0) {
        this.playEffect(EffectNameTool.gameStatus(status))
    },

    countDown(sec=0) {
        if(sec > LAST_5_SEC || sec < 0) return
        this.playEffect(EffectNameTool.countDown(sec))
    },

    baccaratWhichWin(which='', pokerPoint=0) {
        this.playEffect(EffectNameTool.baccaratWhichWin(which))
        this.delayToPlayEffect(EffectNameTool.point(pokerPoint), DELAY_SEC)
    },

    betEffect() {
        this.playEffect(EffectName.CLICK_CHIP)
    },

    winMoney() {
        this.delayToPlayEffect(EffectName.WIN_MONEY, DELAY_SEC)
    },

    init() {
        this.getSoundControl()
    }
}

module.exports = SoundControl
