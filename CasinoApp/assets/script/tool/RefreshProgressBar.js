cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // use this for initialization
    onLoad: function () {
        // 調整BetingPool ProgressBar，多解析度
        let progressBar = this.getComponent(cc.ProgressBar)
        progressBar.totalLength = cc.director.getWinSize().width
    },
    
});
