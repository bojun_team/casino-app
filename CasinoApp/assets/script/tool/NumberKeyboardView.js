import Casino from '../model/Casino'

// NumberKeyboardView

cc.Class({
    extends: cc.Component,

    properties: {
        view: cc.Node,
        eventMasker: cc.Button,
        numberButtons: [cc.Node],
        deleteButton: cc.Node,
        panel: cc.Node
    },

    onDestroy: function(){
        for (let index in this.subscribes) {
            this.subscribes[index].unsubscribe()
        }
    },

    onLoad: function () {
        let node = new cc.Node()
        let button = new cc.Button()
        
        for(let index in this.numberButtons) {
            let button = this.numberButtons[index]
            button.on('click', ()=>{
                Casino.NumberKeyboard.addLastText(String(index))
            })
        }

        this.eventMasker.node.on('click', ()=>{
            Casino.NumberKeyboard.isHideKeyboard = true
        })

        this.deleteButton.on('click', ()=>{
            Casino.NumberKeyboard.deleteLastText()
        })

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        //------ 訂閱
        this.subscribes = []

        //------ 訂閱 isHideKeyboard ， 控制View的開關
        this.subscribes[this.subscribes.length] = Casino.NumberKeyboard.isHideKeyboard$.subscribe({
            next: isHideKeyboard => {
                this.showAnimation(!isHideKeyboard);
            },  
        })
    },

    onKeyDown: function (event) {
        if(event.keyCode >= cc.KEY.num0 && event.keyCode <= cc.KEY.num9) {
            Casino.NumberKeyboard.addLastText(String(event.keyCode-cc.KEY.num0))
        } else if(event.keyCode == cc.KEY.backspace) {
            Casino.NumberKeyboard.deleteLastText()
        } else if(event.keyCode == cc.KEY.enter) {
            Casino.NumberKeyboard.isHideKeyboard = true
        }
    },

    showAnimation: function(active) {
        if(active) this.view.active = true;
        
        this.panel.stopAllActions();
        this.panel.y = (active) ? (-960-730): (-960);
        let moveToY = (active) ? (-960): (-960-730);
        let pos = new cc.Vec2(this.panel.x, moveToY);

        let finished = cc.callFunc(() => {
            this.view.active = active
        }, this);

        let action = cc.sequence(cc.moveTo(0.1, pos), finished)
        this.panel.runAction(action)
    }

});
