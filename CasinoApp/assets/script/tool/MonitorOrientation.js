import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // use this for initialization
    onLoad: function () {
        this.node.zIndex = 999999
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node
        
        if(window.addEventListener == null) return       
        
        window.detectOrientationchange = function(event) {
            if ( window.orientation == 180 || window.orientation == 0 ) {
                Casino.isPortrait$.next(true)
            } else {
                Casino.isPortrait$.next(false)
            }
        }
        window.detectOrientationchange()
        window.addEventListener("orientationchange", window.detectOrientationchange);
    },
    
});
