cc.Class({
    extends: cc.Component,

    properties: {
        blinkDuration: 1
    },

    // use this for initialization
    onLoad: function () {
    },

    startBlink: function(){
        var blinkForever = cc.repeatForever(cc.blink(this.blinkDuration, 1))
        this.node.runAction(blinkForever)
    },
    stopBlink: function(){ 
        this.node.stopAllActions()
    }
});