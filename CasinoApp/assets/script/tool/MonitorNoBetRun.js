import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // use this for initialization
    onLoad: function () {
        this.subscribes = []
        this.subscribes[this.subscribes.length] = Casino.User.propChange$.idleRunNeverBet.subscribe({
            next: idleRunNeverBet => {  
                this.showAlert(idleRunNeverBet)
            }
        });
    },
    
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },

    showAlert:function(idleRunNeverBet){
        const toastRunCount = 4
        const alertRunCount = 5
        let succcessFun = function() {
            Casino.User.is5RunNobet = true
            Casino.Tool.goToLobbyScene()
        }
        let failFun = function() {
            SignalR.disconnect()
        } 
        if (toastRunCount == idleRunNeverBet){
            Casino.Tool.showToast(Casino.Localize.RemindNoBetFiveRun)
        }else if (alertRunCount == idleRunNeverBet){ 
            Casino.Game.leaveGame(succcessFun, failFun) 
        }
    },
});
