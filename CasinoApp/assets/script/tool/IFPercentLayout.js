
cc.Class({
    extends: cc.Component,
    properties: {
        type : {
            type: cc.Layout.Type,
            default: cc.Layout.Type.HORIZONTAL,
            readonly: true
        },
        itemPercentList : [cc.Float],
        isResetChildren: {
            default: true,
        },
    },
    onLoad: function () {
        // 等待下一幀再執行排版
        this.scheduleOnce(this.sortChildren, 0)
    },
    // 排版
    sortChildren: function() {
        let children = this.node.children;
        let posX = -this.node.width*0.5
        for (var i = 0; i < children.length; ++i) {
            let node =  children[i]
            if(this.itemPercentList[i] == 0) {
                node.active = (this.isResetChildren) ? false : node.active
                continue
            } else {
                node.active = (this.isResetChildren) ? true : node.active
            }
            node.width = this.node.width*this.itemPercentList[i]/100
            node.setPositionX(posX+node.width*0.5)
            posX = posX + node.width

            // 更新下一層的對齊
            this.updateSubNodeWidget(node)
        }
    },
    // 更新下一層的對齊
    updateSubNodeWidget(node) {
        let children = node.children;
        for (var i = 0; i < children.length; ++i) {
            let subNode =  children[i]
            let widget = subNode.getComponent(cc.Widget)
            if(widget != null) {
                widget.enabled = true
            }
            this.updateSubNodeWidget(subNode)
        }
    },
});
