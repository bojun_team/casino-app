
import Config from '../constants/Config'

const LAST_5_SEC = 5
const DELAY_SEC = 2
const EffectName = {
    BUTTON_CLICK: 'buttonClick',
    START_BET: 'startBet',
    END_BET: 'endBet',
    HALF_TIME: 'halfTime',
    LAST_5: 'last5',
    DINDIN: 'dindin',
    PLAYER_WIN: 'playerWin',
    BANKER_WIN: 'bankerWin',
    TIE: 'tie',
    CLICK_CHIP: 'clickChip',
    WIN_MONEY: 'winMoney'
}

var EffectNameTool = {
    gameStatus(gameStatus=0) {
        switch (gameStatus) {
            case Config.gameState.NewRoundRun: return EffectName.START_BET
            case Config.gameState.EndBetTime: return EffectName.END_BET
            case Config.gameState.HalfTime: return EffectName.HALF_TIME
            default: return ''
        } 
    },

    countDown(sec=0) {
        switch (sec) {
            case LAST_5_SEC: return EffectName.LAST_5
            default: return EffectName.DINDIN
        }
    },

    baccaratWhichWin(which='') {
        switch (which) {
            case 'player': return EffectName.PLAYER_WIN
            case 'banker': return EffectName.BANKER_WIN
            case 'tie': return EffectName.TIE
            default: return ''
        }
    },

    point(pokerPoint=0) {
        return 'point' + String(pokerPoint)
    },
}

module.exports = {
    LAST_5_SEC: LAST_5_SEC,
    DELAY_SEC: DELAY_SEC,
    EffectName: EffectName,
    EffectNameTool: EffectNameTool,
}