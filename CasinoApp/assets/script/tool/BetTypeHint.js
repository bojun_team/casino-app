/*
    label提示新玩法

    功能列表：
        觸發方式：
            1.更新此功能後，玩家第一次入桌

        暫時停止與停止：
            1.玩家只要點按該鈕，則提示停止
            2.若玩家未點按，遇到停止下注期間則暫停閃爍，待下注時間開始繼續閃爍
            3.閃爍時間共計一分鐘
*/ 
import Tool from "../tool/Tool"
import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        hintNode:cc.Node,
    },

    // use this for initialization
    onLoad: function () {
        // 觸發方式：
        // 1.更新此功能後，玩家第一次入桌
        let isShowFirstHint = String(Tool.loadData(Config.saveData.isShowFirstBetingTypeHint, "false"))
        if(isShowFirstHint == "true") return
        Tool.saveData(Config.saveData.isShowFirstBetingTypeHint, "true")

        this.isStop = false
        this.playHint()

        this.subscribes = []
        // 訂閱注區玩法狀態， 1.玩家只要點按該鈕，則提示停止
        let betZoneTypeFirst = Casino.Game.singleGame.propChange$.betZoneType.getValue()
        let betZoneType$ = Casino.Game.singleGame.propChange$.betZoneType
        this.subscribes[this.subscribes.length] = betZoneType$.subscribe({
            next: betZoneType => {
                // 如果有切換玩法，則停止提示
                if(betZoneType != betZoneTypeFirst) {
                    this.isStop = true
                    this.stopHint()
                }
            }
        });

        if(this.isStop) return
        // 訂閱遊戲狀態， 2.若玩家未點按，遇到停止下注期間則暫停閃爍，待下注時間開始繼續閃爍
        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {  
                // 如果遊戲狀態不是為倒數中，則暫停hint          
                if(gameStatus != Config.gameState.CountDown) { 
                    this.pauseHint()
                } else if(!this.isStop) {
                    this.resumeHint()
                }
            },  
        });

        if(this.isStop) return
        // 3.閃爍時間共計一分鐘
        let actTime = 60
        this.scheduleOnce(function() {
            this.stopHint();
        }, actTime);
    },
    // label提示新玩法
    playHint: function () {
        this.hintNode.active = true
        const moveDuration = 1
        const moveY = 50
        let moveDown = cc.moveBy(moveDuration*0.5, cc.p(0, -moveY))
        let moveUp = cc.moveBy(moveDuration*0.5, cc.p(0, moveY))
        var delay = cc.delayTime(0.2)
        var seq = cc.sequence(moveDown, moveUp, delay)
        var forever = cc.repeatForever(seq)
        this.hintNode.runAction(forever)
    },

    pauseHint: function () {
        cc.director.getActionManager().pauseTarget(this.hintNode)
        this.hintNode.active = false
    },
    
    resumeHint: function () {
        this.hintNode.active = true
        cc.director.getActionManager().resumeTarget(this.hintNode)
    },
    
    stopHint: function () {
        this.unsubscribeAll()
        this.hintNode.stopAllActions()
        this.hintNode.active = false
    },

    unsubscribeAll: function() {
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    onDestroy: function(){
        this.unsubscribeAll()
    },
});
