
import Ramda from 'ramda'
var { append, clone, defaultTo, drop, findIndex, head, last, pipe, prop, reduce, zipObj, split, map } = Ramda
import Common from './common'
var { countContinuous, twoDimensionalArray } = Common
import Type from '../constants/type'
var { BANKER, PLAYER, TIE } = Type

const mapToBigRoad = (results) => {
  const firstTies = countContinuous(TIE, results)
  return pipe(clone, drop(firstTies), reduce(bigRoadReducer(firstTies), []))(results)
}

// NOTICE: bigRoadReducer is NOT a pure function
const bigRoadReducer = (initTie = 0) => (now, current) => {
  current.value = 0

  if (initTie != 0) {
    current.value = initTie
    initTie = 0
  }

  const lastRoad = now[now.length - 1]
  const lastRoadType = lastRoad ? lastRoad[0].type : 0
  const lastCircle = lastRoad ? lastRoad[lastRoad.length - 1] : current

  if (current.type == TIE) {
    lastCircle.value++

    if (now.length > 0) return now
  }

  if (lastRoadType == current.type) {
    lastRoad.push(current)
  } else {
    now.push([current])
  } 
  return now
}
const putRoadMapContainer = (maxY, roadData) => {
  maxY = maxY
  var roadMapContainer = []
  const putRoadMap = function(container, data, x, y)
  {
      if (container[x] == null){
          container[x] = []
          roadMapContainer[x].length = maxY
      } 
      container[x][y] = data 
      return container
  }
  var posTemp = new cc.Vec2(0, 0) 
  let startPointX = 0
  for(var x = 0; x < roadData.length; x++)
  {
      
    posTemp = new cc.Vec2(startPointX, 0)
    let isTurn = false
    for(var y = 0; y < roadData[x].length; y++)
    { 
        if (isTurn){
            posTemp = new cc.Vec2(posTemp.x + 1, posTemp.y)
        }
        // 下一格不是空的往右邊畫一格
        else if (roadMapContainer[posTemp.x] != null &&
                 roadMapContainer[posTemp.x][posTemp.y + 1] != null){
            posTemp = new cc.Vec2(posTemp.x + 1, posTemp.y)
            isTurn = true
        }
        else if (posTemp.y + 1 > maxY)//下一行是空的往右邊劃
        { 
            posTemp = new cc.Vec2(posTemp.x + 1, posTemp.y)
        }
        else{ 
            posTemp = new cc.Vec2(posTemp.x, posTemp.y + 1)
        } 
        roadMapContainer = putRoadMap(roadMapContainer, roadData[x][y], posTemp.x, posTemp.y) 
        
        if (posTemp.y == 1 && posTemp.x >= startPointX){
            startPointX++;
        }
    } 
    isTurn = false
  }
  return roadMapContainer
}
const putCuteRoadMapContainer = (maxY, roadData) => {
    maxY = maxY + 1
    var newRoad = []
    for(var i = 0; i < roadData.length; i++)
    {
        const x = Math.floor(i / maxY)
        const y = i % maxY + 1
        if (newRoad[x] == null) newRoad[x] = []
        newRoad[x][y] = roadData[i]
    }
    return newRoad
}
const mapToDownThreeRoad = (padding) => (results) => {
  const roads = []

  results.forEach((col, x) => {
    if (x <= padding) return

    col.forEach((result, y) => {
      if (x <= 1 + padding && y < 1) return

      const lastRoad = roads[roads.length - 1]
      const lastRoadType = lastRoad ? lastRoad[0].type : 0
      let type = 0

      if (y == 0) {
        type = (results[x - 1].length == results[x - 2 - padding].length) ? BANKER : PLAYER
      } else {
        type = (results[x - 1 - padding][y] || !results[x - 1 - padding][y - 1]) ? BANKER : PLAYER
      }

      if (lastRoadType == type) {
        lastRoad.push({ type })
      } else {
        roads.push([{ type }])
      }
    })
  })

  return roads
}

const predictDownThreeRoadTypes = (bigRoad, type) => {
  const getLastType = pipe(last, defaultTo([[{}]]), head, prop('type'))
  const getDownThreeRoadLastType = (padding) => pipe(mapToDownThreeRoad(padding), getLastType)

  return {
    bigRoad: getLastType(bigRoad),
    bigEyeRoad: getDownThreeRoadLastType(0)(bigRoad),
    smallRoad: getDownThreeRoadLastType(1)(bigRoad),
    cockroachRoad: getDownThreeRoadLastType(2)(bigRoad)
  }
}


const mapToDragonRoad = (height, width, unitData) => (results) => {
  const road = twoDimensionalArray(height--, width, unitData)

  results.forEach((col, x) => {
    if (!road[x]) return

    let ceil = findIndex((data) => data.type)(road[x]) - 1
    ceil = (ceil > 0 && ceil < height) ? ceil : height

    col.forEach((result, y) => {
      const over = (y - ceil > 0) ? y - ceil : 0
      road[x + over][y - over] = result
    })
  })

  return road
}
const parserRoadMap = (s) => {
  const formatResultString = zipObj(['type', 'value','pairs'])
  var resolveResult = pipe(defaultTo(''), split(','), map(formatResultString))
  return resolveResult(s)
}
export default {
  mapToBigRoad: mapToBigRoad,
  mapToDragonRoad: mapToDragonRoad,
  mapToDownThreeRoad: mapToDownThreeRoad,
  predictDownThreeRoadTypes: predictDownThreeRoadTypes,
  parserRoadMap: parserRoadMap,
  putRoadMapContainer: putRoadMapContainer,
  putCuteRoadMapContainer: putCuteRoadMapContainer
}
