cc.Class({
    extends: cc.Component,

    properties: {
        
    },
    
    onLoad: function () {
        const duration = 1
        let fadeIn = cc.fadeIn(duration*0.5)
        let fadeOut = cc.fadeOut(duration*0.5)
        let fadeInOut = cc.sequence(fadeIn, fadeOut)
        let moveTo = cc.moveTo(duration, cc.p(50, 0))
        let moveBack = cc.moveTo(0.01, cc.p(0, 0))
        let spawn = cc.spawn(fadeInOut, moveTo)
        var delay = cc.delayTime(0.4)
        var seq = cc.sequence(spawn, moveBack, delay)
        var repeatForever = cc.repeatForever(seq)
        this.node.runAction(repeatForever)
    },
    
});
