import Config from '../constants/Config'
import RxJsMgr from '../model/RxJsMgr'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // use this for initialization
    onLoad: function () {
        this.inBackTime = 0
        this.isInBackGround = false
        this.subscribes = []
        let connectState$ = RxJsMgr.connectData.propChange$.connectState
        this.subscribes[this.subscribes.length] = connectState$.subscribe({
            next: type => {
                this.showAlert(type)
            }
        });

        cc.game.on(cc.game.EVENT_HIDE, function () {  
            this.inBackTime = (+new Date())
            this.isInBackGround = true
        }.bind(this));
        cc.game.on(cc.game.EVENT_SHOW , function () {  
            this.isInBackGround = false
            //先預設180秒自己斷線
            if (this.inBackTime!=0){
                if ((((+new Date()) - this.inBackTime) / 1000) > 180){
                    Casino.User.inBackGroundDisConnect = true
                    SignalR.disconnect()
                }
            }
            this.inBackTime = 0

            Casino.Tool.setKeepScreenOn(true)
        }.bind(this));
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },

    showAlert:function(type){
        let msgDataID = null
        if (Config.ConnectState.DISCONNECTED == type) {
            if (Casino.User.inBackGroundDisConnect) {
                msgDataID = Casino.Localize.InBackGroundLongTime
            } else {
                msgDataID = Casino.Localize.ConnectClose
            }
        } else if (Config.ConnectState.ERROR == type) {
            msgDataID = Casino.Localize.ConnectError
        }
        if (msgDataID == null) return 
        if (Casino.User.is5RunNobet) return // 已觸發五局未下注，故訊息顯示由大廳決定
        if (Casino.User.is5MinuteNobet) return // 已觸發五分鐘未下注，故訊息顯示由大廳決定
        if (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID) {
            let titleDataID = ""
            Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                Casino.User.logOut(false, false)
                Casino.Tool.goToLoginScene()
            }.bind(this))
        }
    },

});
