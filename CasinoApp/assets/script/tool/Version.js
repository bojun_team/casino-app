import Config from '../constants/Config'
import HotUpdate from '../../hotUpdate/script/module/HotUpdate'

cc.Class({
    extends: cc.Component,

    properties: {
        lab:cc.Label
    }, 
    onLoad: function () {
        if (cc.sys.isBrowser){
            this.lab.string = Config.assetVersion + ' ' + Config.gameOutput + ' '
            return
        }

        let addStr = ((Config.gamePlatform == Config.Platform.JHONG_FA) ? ("Jhong Fa") : (Config.gameCountry));

        this.lab.string = HotUpdate.propChange$.localVersion.getValue() + ' ' + Config.gameOutput + ' ' + addStr
    }, 
});
