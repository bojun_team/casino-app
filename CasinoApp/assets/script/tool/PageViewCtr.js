cc.Class({
    extends: cc.Component,

    properties: {
        pageView:cc.PageView,
        pageViewIndicator:cc.Node,
        pageViewArrow:cc.Node,
        west_pageOn1:cc.Node,
        west_pageOn2:cc.Node,
        west_pageArrow1:cc.Node,
        west_pageArrow2:cc.Node,
    },
    onLoad: function () {
        // 當右邊的頁面箭頭，換到第2頁
        this.west_pageArrow1.on("click", (function () {
            let pageNumber = 2
            let idx = pageNumber - 1
            let timeInSecond = 0.1
            this.pageView.scrollToPage(idx, timeInSecond)
        }), this);

        // 當左邊的頁面箭頭，換到第1頁
        this.west_pageArrow2.on("click", (function () {
            let pageNumber = 1
            let idx = pageNumber - 1
            let timeInSecond = 0.1
            this.pageView.scrollToPage(idx, timeInSecond)
        }), this);

         // 當注區換頁時，更新頁面
        this.updatePage()
        this.pageView.node.on("page-turning", (function () {
            this.updatePage()
        }), this);
    },
    // 更新頁面
    updatePage: function () {
        let pageNum = this.pageView.getCurrentPageIndex() + 1
        this.west_pageOn1.active = (pageNum == 1)
        this.west_pageOn2.active = (pageNum == 2)
        this.west_pageArrow1.active = (pageNum == 1)
        this.west_pageArrow2.active = (pageNum == 2)
    },
    setPageViewEnable: function (isEnable) {
        if(isEnable) {
            this.enablePageView()
        } else {
            this.disablePageView()
        }
    },
    // 關閉PageView功能並設定到第一頁
    disablePageView: function () {
        const page1PosX = -720 // 第一頁的x位置
        this.pageViewIndicator.active = false
        this.pageViewArrow.active = false
        this.pageView.enabled = false
        this.pageView.content.setPositionX(page1PosX)
    },
    enablePageView: function () {
        this.pageViewIndicator.active = true
        this.pageViewArrow.active = true
        this.pageView.enabled = true
    }
});
