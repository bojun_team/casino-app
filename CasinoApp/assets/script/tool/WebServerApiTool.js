
export default class WebServerApiTool {

//     // Casino 編碼解碼工具
//     // 編碼版本1.0
    static encodeCasino1(jsonString){
        let splitNumber
        let strBase64 = Buffer(jsonString).toString('base64')
        // 加密
        splitNumber = 0
        if (strBase64.length <= 4) {
            splitNumber = 1
        } else if (strBase64.length <= 20) {
            splitNumber = 2
        } else if (strBase64.length <= 60) {
            splitNumber = 5
        } else if (strBase64.length <= 100) {
            splitNumber = 8
        } else if (strBase64.length <= 150) {
            splitNumber = 15
        } else {
            splitNumber = 32
        }
        let strReturn =  strBase64.substring(splitNumber+1-1,splitNumber*2) + strBase64.substring(1-1,splitNumber) +strBase64.substring(splitNumber*2+1-1)
        return strReturn
    }

}
