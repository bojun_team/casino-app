
// JackpotAlert.js
import Localize from '../constants/Localize'
import Tool from '../tool/Tool'
import Config from '../constants/Config'
import Casino from '../model/Casino'

const ACCEPT_BUTTON_NAME = 'acceptButton'

var isShowBetHintAlert = false

cc.Class({
    extends: cc.Component,

    properties: {
        normalWinAlert: cc.Node,
        normalWinMessage: require("LocalizedLabel"),
        superBigWinAlert: cc.Node,
        superBigWinMessage: require("LocalizedLabel"),
        betHintAlert: cc.Node,
        jackpotBigWinAlert: cc.Node,
        jackpotBigWinMessage: require("LocalizedLabel"),
        jackpotBigWinCollectBtn: cc.Node,
    },

    onLoad: function () {
        
    },
    
    closeAlert: function() {
        this.node.removeFromParent(true)
        this.destroy()
    },

    showAlert: function(alert, callback) {
        let acceptButton = alert.getChildByName(ACCEPT_BUTTON_NAME)
        acceptButton.on('click', ()=> {
            callback()
            this.closeAlert()
        }, this)
        alert.active = true
    },

    showNormalWin: function(continueWin, winAmount, callback) {
        let winAmountStr = Tool.getThousandsOfBits(winAmount).replace('$', '')
        this.normalWinMessage.dataArguments = [continueWin, winAmountStr]
        this.normalWinMessage.dataID = this.normalWinMessage.dataID
        this.showAlert(this.normalWinAlert, callback)
    },

    showSuperBigWin: function(continueWin, winAmount, callback) {
        let winAmountStr = Tool.getThousandsOfBits(winAmount).replace('$', '')
        this.superBigWinMessage.dataArguments = [continueWin, winAmountStr]
        this.superBigWinMessage.dataID = this.superBigWinMessage.dataID
        this.showAlert(this.superBigWinAlert, callback)
    },
    
    showBetHintAlert: function(callback) {
        this.showAlert(this.betHintAlert, callback)
    },

    showJackpotBigWin: function(callback) {
        let nextBigWinRatio = Casino.User.jackpotWinInfo.nextBigWinRatio
        let expectedWinAmount = Tool.getThousandsOfBits(Casino.User.jackpotWinInfo.expectedWinAmount)
        let nextWinExpectedWinAmount = Tool.getThousandsOfBits(Casino.User.jackpotWinInfo.nextWinExpectedWinAmount)
        this.jackpotBigWinMessage.dataArguments = [expectedWinAmount, nextBigWinRatio, nextWinExpectedWinAmount]
        this.jackpotBigWinMessage.dataID = this.jackpotBigWinMessage.dataID
        this.jackpotBigWinCollectBtn.on('click', ()=> {
            callback(Config.JackpotBigWinResponse.NOT_CONTINUE_AND_GET_BONUS)
            this.closeAlert()
        }, this)
        this.jackpotBigWinAlert.getChildByName(ACCEPT_BUTTON_NAME).on('click', ()=> {
            callback(Config.JackpotBigWinResponse.CONTINUE)
            this.closeAlert()
        }, this)
        this.jackpotBigWinAlert.active = true
    }
});

var JackpotAlert = {
    getAlert(callback=function(alert){}) {
        if(this.prefab == null) {
            cc.loader.loadRes("prefab/common/JackpotAlert", (err, prefab) => {
                this.prefab = prefab
                this.getAlert(callback)
            })
        } else {
            let newNode = cc.instantiate(this.prefab)
            cc.director.getScene().addChild(newNode, 998)
            let alert = newNode.getComponent("JackpotAlert")
            callback(alert)
        }
    },

    showNormalWin(continueWin=0, winAmount=0, callback=function(){}) {
        this.getAlert((alert) => {
            alert.showNormalWin(continueWin, winAmount, callback)
        })
    },

    showSuperBigWin(continueWin=0, winAmount=0, callback=function(){}) {
        this.getAlert((alert) => {
            alert.showSuperBigWin(continueWin, winAmount, callback)
        })
    },
    
    showBetHintAlert(callback=function(){}) {
        if(isShowBetHintAlert == true) return
        if(Config.JACKPOT_ENABLE == false) return
        let showJpBetHintTimes = Tool.loadData(Config.saveData.showJpBetHintTimes, '0')
        let num = Number(showJpBetHintTimes)
        if(++num > Config.maxShowJpBetHintTimes) return
        Tool.saveData(Config.saveData.showJpBetHintTimes, String(num))
        isShowBetHintAlert = true
        
        this.getAlert((alert) => {
            alert.showBetHintAlert(callback)
        })
    },

    showJackpotBigWin: function(callback=function(acceptBigWinResponse){}) {
        this.getAlert((alert) => {
            alert.showJackpotBigWin(callback)
        })
    }
}

module.exports = JackpotAlert