import Casino from '../model/Casino'
import Config from '../constants/Config'
import RxJsMgr from '../model/RxJsMgr'

var MonitorWebInternet = {
    recvServerDataEvtId: -1,
    timerId: -1,

    listenRecvServerData() {
        this.recvServerDataEvtId = Casino.NativeConnect.listenRecvServerData((data)=> {
            clearTimeout(this.timerId)
            this.timerId = setTimeout(()=> {
                this.stopRecvServerData()
                let titleDataID = Casino.Localize.AlertTitleWarning
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.User.logOut(false, false)
                })
            }, Config.detectWebInternertTimeMS)
        })
    },

    stopRecvServerData() {
        clearTimeout(this.timerId)
        Casino.NativeConnect.stoplistenRecvServerData(this.recvServerDataEvtId)
    },

    subscribeConnectState() {
        RxJsMgr.connectData.propChange$.connectState.subscribe({
            next: connectState => {  
                if (connectState == Config.ConnectState.DISCONNECTED) {
                    this.stopRecvServerData()
                } else if (connectState == Config.ConnectState.CONNECTED) {   
                    this.listenRecvServerData()
                }
            },  
        })
    }

}

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onLoad: function () {
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node
        if (cc.sys.isBrowser) MonitorWebInternet.subscribeConnectState()
    },

});
