cc.Class({
    extends: cc.Component,

    properties: {
        ZOrder:0
    },

    // use this for initialization
    onLoad: function () {
        this.node.zIndex = this.ZOrder
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
