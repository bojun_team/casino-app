import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },

    // use this for initialization
    onLoad: function () {
        this.subscribes = []
        let comingMaintainCount$ = Casino.User.propChange$.comingMaintainCount
        this.subscribes[this.subscribes.length] = comingMaintainCount$.subscribe({
            next: comingMaintainCount => { 
                if (comingMaintainCount!=0) {
                    let message = Casino.User.comingMaintainMessage
                    let titleDataID = ""
                    let msgDataID = Casino.Localize.NoLocalize
                    Casino.Tool.showAlert(titleDataID, msgDataID, null, [message])                    
                }
            },  
        });
    },
});
