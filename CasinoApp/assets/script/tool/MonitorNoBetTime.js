import Config from '../constants/Config'
import RxJsMgr from '../model/RxJsMgr'
import SignalR from '../native/SignalR'
import Casino from '../model/Casino'

const TOAST_BUFF_TIME = 1000
const TOAST_SHOW_TIME = 3
const SCHEDULE_INTERVAL_TIME = 1

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onDestroy: function() {
        this.stopAllTimer()
    },

    onLoad: function () {
        this.isShowToastList = [false, false]
        this.toastTimeList = []
        this.toastTimeList.push(Config.MultipleTableKickUserToastTime1)
        this.toastTimeList.push(Config.MultipleTableKickUserToastTime2)
        this.startTimer()

        this.subscribes = []
        this.betSumList = []
        this.betZoneTypeList = []
        // 監聽resetNoBetTimeCount，有變化表示有下注行為
        let resetNoBetTimeCount$ =  Casino.Game.propChange$.resetNoBetTimeCount
        this.subscribes.push(resetNoBetTimeCount$.subscribe({
            next: resetNoBetTimeCount => {
                if(resetNoBetTimeCount == 0) return
                this.reStartTimer()
            },  
        }))
    },

    stopAllTimer: function() {
        this.unscheduleAllCallbacks()
        this.isShowToastList = [false, false]
    },

    startTimer: function() {
        this.stopAllTimer()
        this.startTime = (+new Date())
        this.schedule(this.kickUsetTimer, SCHEDULE_INTERVAL_TIME)
    },

    kickUsetTimer: function() {
        let runTime = (+new Date()) - this.startTime
        // 時間到踢人
        if(runTime >= Config.MultipleTableKickUserAlertTime) {
            this.stopAllTimer()
            this.kickUser()
            return
        }

        // 時間快到，前兩次先做提醒
        for(let i=0; i<this.toastTimeList.length; i++) {
            let toastTime = this.toastTimeList[i]
            if(this.isShowToastList[i] == false && runTime >= toastTime) {
                if(runTime >= toastTime + TOAST_BUFF_TIME || RxJsMgr.connectData.connectState == Config.ConnectState.DISCONNECTED) {
                    this.isShowToastList[i] = true
                    return
                }
                this.isShowToastList[i] = true
                Casino.Tool.showToast(Casino.Localize.RemindNoBetFiveMinute, TOAST_SHOW_TIME)
                return
            }
        }
    },

    reStartTimer: function() {
        this.stopAllTimer()
        this.startTimer()
    },

    kickUser: function() {
        if (RxJsMgr.connectData.connectState == Config.ConnectState.DISCONNECTED) {
            return
        }

        let succcessFun = function() {
            Casino.User.is5MinuteNobet = true
            Casino.Tool.goToLobbyScene()
        }
        let failFun = function() {
            SignalR.disconnect()
        } 
        Casino.Game.leaveGame(succcessFun, failFun)
    },
    
});
