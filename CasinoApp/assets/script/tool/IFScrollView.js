cc.Class({
    extends: cc.Component,

    properties: {
        content:cc.Node,
        enableX:true,
        enableY:true,
        enableSlideOver:false
    },

    // use this for initialization
    onLoad: function () {
        this.maxX = this.content.x
        this.minY = this.content.y
         
        this.node.on(cc.Node.EventType.TOUCH_START, function (event) {   
            this.lastPos = new cc.v2(event.getLocationX(), event.getLocationY())
            this.isCancel = false 
            // this.startLocation = this.lastPos
        }.bind(this)); 
        this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {  
            const moveGap = 30
            if (this.isCancel) return
            let touchX = event.getLocationX()
            let touchY = event.getLocationY() 
            if (this.lastPos == null) {
                this.lastPos = new cc.v2(touchX, touchY)
                return
            } 
            
            // if (Math.abs(this.startLocation.y - touchY) < moveGap) return
            if (this.enableX) this.content.x = this.content.x + (touchX - this.lastPos.x) 
            if (this.enableY) this.content.y = this.content.y + (touchY - this.lastPos.y)
            if (!this.enableSlideOver) this.processSlideOver()
            this.lastPos = new cc.v2(touchX, touchY) 
        }.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {   
            this.isCancel = true  
            this.lastPos = new cc.v2(0, 0)
            this.fixContent(event)
        }.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {  
            this.isCancel = true 
            this.lastPos = new cc.v2(0, 0)
            this.fixContent(event)
        }.bind(this));  
        if (this.isScrollToRightWhenOnload)
            this.scrollToRight()
    },
    processSlideOver:function(){ 
        this.fixContent()
    },

    fixContent:function(){
        let contentX = this.content.x
        let contentY = this.content.y  
        if (this.enableX){ 
            if (contentX > this.maxX) this.content.x = this.maxX
            const minX = this.maxX - this.content.width + this.node.width 
            if (contentX < minX) this.content.x = minX
        }
        if (this.enableY){ 
            if (contentY < this.minY) this.content.y = this.minY
            let maxY = this.minY + this.content.height - this.node.height
            if(this.content.height<=this.node.height) maxY = 0;
            if (contentY > maxY) this.content.y = maxY
        }
    },
    scrollToRight:function(){
        if (this.maxX == null) {
            this.isScrollToRightWhenOnload = true
            return
        }
        const rightPosX = this.maxX - this.content.width + this.node.width 
        this.content.x = rightPosX
    }
});
