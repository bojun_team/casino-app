import Tool from "../tool/Tool"
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import JackpotAlert from '../tool/JackpotAlert'
cc.Class({
    extends: cc.Component,

    properties: {
         
    },

    // use this for initialization
    onLoad: function () {
        if(Config.IS_SHOW_LOBBY_UPDATE_MESSAGE == false) {
            this.node.active = false
            this.onConfirmRelease()
            return
        }
        let lastVersion = Tool.loadData(Config.saveName.lastVersion, "null")  
        this.node.active = lastVersion != NativeConnect.gameVersion
    },
    
    onConfirmRelease:function(){
        Tool.saveData(Config.saveName.lastVersion, NativeConnect.gameVersion)
        this.node.active = false
    },
});
