import Tool from "../tool/Tool"
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
      userCredit:cc.Label,
    },

    // use this for initialization
    onLoad: function () { 
        let usercredit$ = Casino.User.propChange$.usercredit
        this.subscribes = []
        this.subscribes[this.subscribes.length] = usercredit$.subscribe({
            next: Credit => {  
                this.setUserCredit(Credit)   
            }
        });
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },
    setUserCredit: function(Credit){
        this.userCredit.string = Tool.getThousandsOfBits(Credit)
    },    
});
