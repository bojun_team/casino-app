import Rx from 'rxjs'
import RxJsMgr from '../model/RxJsMgr'
import Tool from '../tool/Tool'
import JackpotWinView from '../tool/JackpotWinView'

cc.Class({
    extends: cc.Component,

    properties: {
        prefabList:[cc.Prefab]
    },
    
    onLoad: function () { 
        Tool.setKeepScreenOn(true)
        
        this.orderMap = {
            mainNode:1,
            settingPage:2,
            releaseNote:3,
        }

        RxJsMgr.lobbyUIState.reset()

        this.scheduleOnce(()=>{
            this.loadNode()
        }, 0.1)

        JackpotWinView.getWinView() // 預讀
    },

    updateLoading: function(index) {
        let maxCount = this.prefabList.length
        RxJsMgr.lobbyUIState.isHideLoading = (index / maxCount) >= 1
        RxJsMgr.lobbyUIState.loadingBarProgress = index / maxCount
    },
      
    loadNode: function(index=0) {
        this.updateLoading(index)
        if(index >= this.prefabList.length) return

        let prefab = this.prefabList[index]
        let zOrder = this.orderMap[prefab.name]
        let newNode = cc.instantiate(prefab);
        this.node.addChild(newNode, zOrder);
        
        this.scheduleOnce(()=>{
            this.loadNode(++index)
        }, 0.1)
    }
});
