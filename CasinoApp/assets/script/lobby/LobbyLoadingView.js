import RxJsMgr from '../model/RxJsMgr'
import Casino from '../model/Casino';

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    onLoad: function () { 
        // 打開loading並設定文字
        Casino.Tool.openLoadingView()
        Casino.Tool.getLoadingView().loadingLabel.dataID = Casino.Localize.LoadingTable

        this.loadingInfo = {
            isLoadLobby : false,
            isLoadTable : false,
            isGetTableData : false,
        }
        this.subscribes = [] 
        //------訂閱 isHideLoading
        let isHideLoading$ = RxJsMgr.lobbyUIState.propChange$.isHideLoading
        this.subscribes[this.subscribes.length] = isHideLoading$.subscribe({
            next: isHide => {                
                if (isHide) {
                    const delayTime = 0.1 //晚一點關閉loaading
                    this.scheduleOnce(Casino.Tool.closeLoadingView, delayTime);
                }
            },  
        });       
        let loadingBarProgress$ = RxJsMgr.lobbyUIState.propChange$.loadingBarProgress
        this.subscribes[this.subscribes.length] = loadingBarProgress$.subscribe({
            next: loadingBarProgress => {     
                Casino.Tool.getLoadingView().progressBar.progress = loadingBarProgress
            },  
        });    
    }
});
