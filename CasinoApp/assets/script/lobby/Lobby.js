import Game from '../model/Game' 
import Config from '../constants/Config'
import RxJsMgr from '../model/RxJsMgr'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: { 
        accountLab:cc.Label,
        creditLab:cc.Label,
        scrollView:cc.ScrollView,
        baccaratScrollItem:cc.Prefab,
        multiTableEnterButton:cc.Button
    },

    // use this for initialization
    onLoad: function () { 
        if (Casino.User.is5RunNobet) {
            let titleDataID = ""
            let msgDataID = Casino.Localize.NoBetFiveRun
            Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                this.showInBackGroundLongTimeMsg() // 如果閒置過久 顯示閒置過久訊息
            }.bind(this))
            Casino.User.is5RunNobet = false
        }
        if (Casino.User.is5MinuteNobet) {
            let titleDataID = ""
            let msgDataID = Casino.Localize.NoBetFiveMinute
            Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                this.showInBackGroundLongTimeMsg() // 如果閒置過久 顯示閒置過久訊息
            }.bind(this))
            Casino.User.is5MinuteNobet = false
        }


        this.setScrollItem() 
        Casino.User.isOpenSetting = false //關閉設定頁

        // 如果不是連線狀態不會往下執行
        if (RxJsMgr.connectData.connectState != Config.ConnectState.CONNECTED) return
        
        // 設定玩家關注的注區ID
        Casino.User.setPushBettingPoolSpotId (function(){})

        // 更新桌檯網路參數
        Casino.Tables.updateBaccaratOnlineSetting();
    },
    onTouchMultiTableEnterButton: function () {
        if (this.isSeating) return
        this.isSeating = true

        window.isMultiTable = true

         // 更新桌檯網路參數
        Casino.Tables.updateBaccaratOnlineSetting();

        Casino.Game = new Game(Config.TableName.MultipleTable, "", Casino.spotInfo)
        RxJsMgr.gameUIState.reset()

        let tableNameInfos = [];
        for(let key in Casino.Tables._baccaratInfos) {
            let tableInfo = Casino.Tables._baccaratInfos[key]
            tableNameInfos.push({
                TableName: tableInfo.tableName,
                TableId: tableInfo.tableID
            });
        }
        
        Casino.Tool.getMusicControl().playBtnEffect()
        Casino.User.multipleTableSeat(tableNameInfos, function(cmd, resp, error) {    
            if (resp == Config.requireToServerError.timeOut) {
                let titleDataID = ""
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.User.logOut(false, false)
                    Casino.Tool.goToLoginScene(() => {
                        Casino.Game = null;
                    });
                })
                return
            }
            if (error != null){
                let errorCode = String(resp)
                let errorMessage = String(error)                        
                let titleDataID = Casino.Localize.AlertTitleError
                let msgDataID = Casino.Localize.UnknownError
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.NativeConnect.exit()
                }, [errorCode, errorMessage])
                return
            }
            
            if (resp.ErrorCode == Config.GameCenterErrorCode.OperationSuccess) {
                Casino.Game.parseMultipleSeatTableInfo(resp)
                Casino.Game.sortList();
                Casino.Tool.getLoadingView().progressBar.progress = 0
                Casino.Tool.openLoadingView()
                cc.director.loadScene("MultipleGame")
                return
            }
            let titleDataID = ""
            let errorCode = resp.ErrorCode
            let msgDataID = Casino.Localize.seatFailMsg(errorCode)
            Casino.Tool.showAlert(titleDataID, msgDataID, null, [JSON.stringify(errorCode)])
            this.isSeating = false
        }.bind(this))
    },

    showInBackGroundLongTimeMsg:function() {
        // 如果未觸發顯示過久，則不顯示訊息。
        if(Casino.User.inBackGroundDisConnect == false) return
        Casino.User.inBackGroundDisConnect = false
        let titleDataID = ""
        let msgDataID = Casino.Localize.InBackGroundLongTime
        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
            Casino.User.logOut(false, false)
            Casino.Tool.goToLoginScene()
        })
    },
    onTouchSetting: function() {

    },
    setScrollItem: function() {
        let isRecvInitTables$ = Casino.Tables.propChange$.isRecvInitTables
        isRecvInitTables$.subscribe({
            next: isRecvInitTables => {  
                if (isRecvInitTables == true)
                    this.creatorScrollItem()
            }, 
        });  
    },
    creatorScrollItem: function() {
        let index = 0;
        index = this.createRoulette(index);
        index = this.createBaccarat(index);
    }, 
    createRoulette: function(index) {
        let ids = Casino.Tables.getTableId(Config.TableName.Roulette);
        for (var i = 0; i < ids.length; i++) { 
            let newNode = cc.instantiate(this.baccaratScrollItem); 
            this.scrollView.content.addChild(newNode); 
            let item = newNode.getComponent("BaccaratScrollItem") 
            newNode.setPosition(new cc.Vec2(0, - newNode.height * index)) 
            this.scrollView.content.height = newNode.height * (index + 1) 
            item.subscribeTableData(Config.TableName.Roulette, ids[i]) 
            item.setSeatEvent(this.onSeat.bind(this)) 
            index++;
        }
        return index;
    },
    createBaccarat: function(index) {
        let ids = Casino.Tables.getTableId("Baccarat")
        // sort list
        let firstIds = [];
        let normalIds = [];
        let atferIds = [];
        for (var i = 0; i < ids.length; i++) {
            let info = Casino.Tables.getTableInfo(Config.TableName.Baccarat, ids[i]);
            if (info.isR18 == true && info.tableStatus == Config.tableStatus.Normal) {
                firstIds.push(ids[i]);
            } else if(info.tableStatus == Config.tableStatus.Normal){
                normalIds.push(ids[i]);
            } else {
                atferIds.push(ids[i]);
            }
        }
        ids = firstIds.concat(normalIds).concat(atferIds);
        
        let normalId = 0;
        let sexId = 0;
        for (var i = 0; i < ids.length; i++) { 
            let info = Casino.Tables.getTableInfo(Config.TableName.Baccarat, ids[i]);
            let newNode = cc.instantiate(this.baccaratScrollItem); 
            this.scrollView.content.addChild(newNode); 
            let item = newNode.getComponent("BaccaratScrollItem") 
            newNode.setPosition(new cc.Vec2(0, - newNode.height * index)) 
            this.scrollView.content.height = newNode.height * (index + 1) 
            if (info.isR18 == true) {
                item.imageName = "sexHost" + (++sexId).toString();
            } else {
                item.imageName = "normalHost" + (++normalId).toString();
            }
            item.subscribeTableData("Baccarat", ids[i]) 
            item.setSeatEvent(this.onSeat.bind(this)) 
            index++;
        }
        return index;
    },

    onSeat:function(tableType, tableID){
        // 重置五局未下注
        Casino.User.resetIdleRunNeverBet()
        if (this.isSeating) return
        this.isSeating = true
        window.isMultiTable = false

         // 更新桌檯網路參數
        Casino.Tables.updateBaccaratOnlineSetting();
        
        Casino.Game = new Game(
            String(tableType),
            String(tableID),
            Casino.spotInfo,
        )
        RxJsMgr.gameUIState.reset()
        
        Casino.Tool.getMusicControl().playBtnEffect()
        Casino.User.seat(String(tableType), String(tableID), function(cmd, resp, error) {    
            if (resp == Config.requireToServerError.timeOut){
                let titleDataID = Casino.Localize.AlertTitleWarning
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.User.logOut(false, false)
                    Casino.Tool.goToLoginScene()
                })
                return
            }
            if (error != null){
                let errorCode = String(resp)
                let errorMessage = String(error)                        
                let titleDataID = Casino.Localize.AlertTitleError
                let msgDataID = Casino.Localize.UnknownError
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.NativeConnect.exit()
                }, [errorCode, errorMessage])
                return
            }
            if (resp == "0") {
                Casino.Tool.getLoadingView().progressBar.progress = 0
                Casino.Tool.openLoadingView()
                if(tableType == Config.TableName.Baccarat) {
                    cc.director.loadScene("Game")
                    return
                } else if(tableType == Config.TableName.Roulette) {
                    cc.director.loadScene("RouletteGame")
                    return
                } else {
                    // cc.director.loadScene("DynamicOdds");
                    // return
                }
            }
            let titleDataID = ""
            let errorCode = resp
            let msgDataID = Casino.Localize.seatFailMsg(errorCode)
            Casino.Tool.showAlert(titleDataID, msgDataID, null, [JSON.stringify(errorCode)])
            this.isSeating = false
        }.bind(this))
    }
});
