

import BaccaratRoad from '../tool/baccaratRoad'
var { mapToDownThreeRoad, mapToDragonRoad, mapToBigRoad, predictDownThreeRoadTypes, parserRoadMap, putRoadMapContainer } = BaccaratRoad

import Config from '../constants/Config'
import Casino from '../model/Casino'

const MAX_BIG_ROAD_COLUMN = 11

let _girlIndex=0;

//填充截取法 加 24小時轉12小時
function pdNum(num, length, isHour=false) {
    num = (isHour && num > 12) ? (num-12): num;
    return (Array(length).join("0") + num).slice(-length);
}

cc.Class({
    extends: cc.Component,

    properties: {
        runLab:cc.Label,
        idLab:cc.Label,
        typeLab:require("LocalizedLabel"),
        modeLab:require("LocalizedLabel"),
        countDownLab:cc.Label,
        stateLab:require("LocalizedLabel"),
        roadMapNode:cc.Node,
        roadmapItemSize:45,
        roadmapItemScale:0.8,
        roadmapItemHightCount:6,
        roadmapItemWidthCount:12,
        mainNode:cc.Node,
        btnBgLight:cc.Button,
        spriteBgLight:cc.Sprite,
        baseBigRoadItem:cc.Node,
        baccaratGirl:cc.Sprite,
        lobbyAtlas: cc.SpriteAtlas,

        // For 18禁
        pinkColor:cc.Color,
        baccaratEnterCase: cc.Sprite,

        // For 開放時間
        roadSprite: cc.Node,
        closeBg: cc.Node,
        timeTitleLabel: cc.Label,
        timeLabel:cc.Label,
        stateOpenNode:cc.Node,
        stateCloseLab:cc.Node,

        imageName:"normalHost1"
    },
    // use this for initialization
    onLoad: function () {   
        this.roadMapNodeDefaultWidth = this.roadMapNode.width
        this.roadMapNodeDefaultX = this.roadMapNode.getPositionX()

        this.isMoved = false // 判斷是否是要進桌
        this.tableType = ""
        this.tableID = ""
        this.touchTabelLocationX = 0
        this.touchTabelLocationY = 0
        this.setBtnBgLight()

        
        // this.subscribes = []
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
        this.unschedule(this.upateEverySec);
    },
    setBtnBgLight: function(){ 
        this.btnBgLight.node.on(cc.Node.EventType.TOUCH_START, function (event) { 
            this.isMoved = false
            this.spriteBgLight.node.active = true
            this.touchTabelLocationX = event.getLocationX()
            this.touchTabelLocationY = event.getLocationY()
        }.bind(this));
        this.btnBgLight.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) { 
            if ((Math.abs(this.touchTabelLocationX - event.getLocationX()) > 20) || (Math.abs(this.touchTabelLocationY - event.getLocationY()) > 20)){
                this.isMoved = true
                this.spriteBgLight.node.active = false
            }

        }.bind(this));
        this.btnBgLight.node.on(cc.Node.EventType.TOUCH_END, function (event) { 
            if (false == this.isMoved ){ 
                if (this.gameState == Config.gameState.Maintenance) 
                {
                    let titleDataID = Casino.Localize.AlertTitleWarning
                    let msgDataID = Casino.Localize.TableMaintenance
                    Casino.Tool.showAlert(titleDataID, msgDataID, null)
                    this.spriteBgLight.node.active = false
                    return
                }
                if (this.seatEvt != null) this.seatEvt(this.tableType, this.tableID)
            }
            this.spriteBgLight.node.active = false
        }.bind(this));
        this.btnBgLight.node.on(cc.Node.EventType.TOUCH_CANCEL, function (event) { 
            this.spriteBgLight.node.active = false
        }.bind(this));
    },
    

    subscribeTableData: function(tableName, tableID) { 
        this.tableName = tableName
        this.tableID = tableID
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)

        // R18 文字顏色為#FF8EC0 
        if(table.isR18 == true) {
            this.idLab.node.color = this.pinkColor;
            this.modeLab.node.color = this.pinkColor;
            this.countDownLab.node.color = this.pinkColor;
            this.timeTitleLabel.node.color = this.pinkColor; 
            this.timeLabel.node.color = this.pinkColor; 
            this.stateLab.node.color = this.pinkColor;
            this.stateCloseLab.color = this.pinkColor;
            let pinkCase = "baccarat_enter_case_pink";
            this.baccaratEnterCase.spriteFrame = this.lobbyAtlas.getSpriteFrame(pinkCase);
        }

        this.subscribes = []
        this.tableType = tableName
        this.tableID = tableID
        this.idLab.string = "" + tableID
        this.typeLab.dataID = this.tableType
        //---------------- 設定路圖
        let roadmap$ = table.propChange$.roadmap
        this.subscribes[this.subscribes.length] = roadmap$.subscribe({
            next: cubeRoad => {
                if (cubeRoad.length == 0){
                    this.resetRoadMap()
                    return
                }
                var res = parserRoadMap(cubeRoad)
                var bigRoad = mapToBigRoad(res)
                this.setRoadMapData(bigRoad)
            }
        });
        //---------------- 設定局數 
        let run$ = table.propChange$.run
        this.subscribes[this.subscribes.length] = run$.subscribe({
            next: value => { 
                this.runLab.string = "" + value 
            }
        }); 
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: value => {  
                this.gameState = value
                this.updateStatus(value, table)
            }
        });
        let tableMode$ = table.propChange$.tableMode
        this.subscribes[this.subscribes.length] = tableMode$.subscribe({
            next: value => {
                this.setModelLab(value)
            }
        });
        //---------------- 設定現場開放時間及關閉時間
        let openingTimes$ = table.propChange$.openingTimes;
        this.subscribes[this.subscribes.length] = openingTimes$.subscribe({
            next: openingTimes => {
                this.updateOpeningTimes(openingTimes.startingAt, openingTimes.closingAt);
            }
        });
        this.schedule(this.upateEverySec, 1);//每一秒更新一次 
        //---------------- 設定維護頁面
        let tableStatus$ = table.propChange$.tableStatus;
        this.subscribes[this.subscribes.length] = tableStatus$.subscribe({
            next: status => {
                this.updateBaccaratTableStatus(status);
            }
        });
        //換圖片
        let spriteFrame = this.lobbyAtlas.getSpriteFrame(this.imageName);
        if(spriteFrame == null) {
            if(table.isR18 == true) { 
                spriteFrame = this.lobbyAtlas.getSpriteFrame("sexHost1");
            } else { 
                spriteFrame = this.lobbyAtlas.getSpriteFrame("normalHost1");
            }
        } 
        this.baccaratGirl.spriteFrame = spriteFrame; 
    }, 
    upateEverySec:function () {
        if(Casino.Tables == null) return
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)    
        if (this.gameState != Config.gameState.CountDown + "") return  
        if (table.countDownEndDate == null) return  
        let remainTime = table.countDownEndDate - (+new Date())  
        if(remainTime < 0) remainTime = 0
        else remainTime = Math.floor(remainTime / 1000)
        this.updateCountDown(remainTime) 
    },
    setRoadMapData: function(bigRoad) { 
        const xOffset = this.roadmapItemSize * 0.5 - 5 // x 位移 
        const yOffset = this.roadmapItemSize * 0.5 - 5 // x 位移 
        
        // 優化大廳路圖顯示 超過11行，只顯示最後11行
        if(bigRoad.length > MAX_BIG_ROAD_COLUMN) {
            let newBigRoad = []
            for(let i=bigRoad.length-MAX_BIG_ROAD_COLUMN;i<bigRoad.length;i++) {
                newBigRoad.push(bigRoad[i])
            }
            bigRoad = newBigRoad
        }

        var roadMapContainer = putRoadMapContainer(this.roadmapItemHightCount, bigRoad)
        if (this.roadMapItems == null) this.roadMapItems = []
        const xLength = this.roadmapItemWidthCount > roadMapContainer.length ? this.roadmapItemWidthCount + 1 : roadMapContainer.length + 1
        for(var x = 0; x < xLength; x++)
        {
            if (this.roadMapItems[x] == null) this.roadMapItems[x] = []
            for(var y = 1; y < this.roadmapItemHightCount + 1; y++)
            {  
                if (roadMapContainer.length > x && roadMapContainer[x].length > y){
                    const pos = new cc.Vec2(this.roadmapItemSize * (x) + xOffset, -this.roadmapItemSize * y + yOffset)
                    if (this.roadMapItems[x][y] == null){
                        this.roadMapItems[x][y] = this.createRoadMapItems(pos)  
                    }
                    this.roadMapItems[x][y].node.setPosition(pos)   
                    this.roadMapItems[x][y].setData(roadMapContainer[x][y])  
                } 
            }
        } 
        if (roadMapContainer.length + 1 > this.roadmapItemWidthCount)
        {
            this.roadMapNode.width = (roadMapContainer.length) * this.roadmapItemSize 
            const xShift = ((roadMapContainer.length - this.roadmapItemWidthCount) + 1) * this.roadmapItemSize
            this.roadMapNode.setPositionX(-xShift)  
        }
        else
        {
            this.roadMapNode.width = this.roadMapNodeDefaultWidth
            this.roadMapNode.setPositionX(this.roadMapNodeDefaultX)
        } 
         
    }, 
    createRoadMapItems: function(pos) { 
        var newNode = cc.instantiate(this.baseBigRoadItem)
        this.roadMapNode.addChild(newNode)
        var item = newNode.getComponent('BigRoadItem')
        newNode.setPosition(pos)
        newNode.active = true
        newNode.scale = this.roadmapItemScale
        return item
    },

    updateStatus: function(status, table) {
        if ("1" == status){
            this.stateLab.node.active = false
            this.countDownLab.node.active = true 
        } else {
            this.stateLab.node.active = true
            this.countDownLab.node.active = false
            this.stateLab.dataID =  Casino.Localize.gameStateDataID(status)
        }
    }, 
    updateCountDown: function(sec){
        this.countDownLab.string = String(sec)
    },
    setModelLab: function(mode){ 
        this.modeLab.dataID = mode
    },
    setSeatEvent:function(fun){
        this.seatEvt = fun
    },
    //重置路圖
    resetRoadMap:function(){  
        this.roadMapNode.removeAllChildren() 
        this.roadMapItems = null
    }, 
    /** 更新桌檯開放時間 */
    updateOpeningTimes: function(startingAt, closingAt) {
        if(startingAt == null || closingAt == null) return;
        if(startingAt.length > 0 && closingAt.length > 0) {
            let startTime = new Date(startingAt);
            let closeTime = new Date(closingAt);
            let startAM = (startTime.getHours() >= 12) ? ("PM") : ("AM");
            let closeAM = (closeTime.getHours() >= 12) ? ("PM") : ("AM");
            let startHour = pdNum(startTime.getHours(), 2, true);
            let closeHour = pdNum(closeTime.getHours(), 2, true);
            let startMinute = pdNum(startTime.getMinutes(), 2);
            let closeMinute = pdNum(closeTime.getMinutes(), 2);
            
            this.timeLabel.string = `${startAM} ${startHour}:${startMinute} - ${closeAM} ${closeHour}:${closeMinute}`;
        }
    },   
    /** 更新桌檯維護狀態 */
    updateBaccaratTableStatus(status) {
        if((status & Config.tableStatus.Closed) == Config.tableStatus.Closed) {
            this.roadSprite.active = this.roadMapNode.active = false;
            this.btnBgLight.node.active = false;
            this.closeBg.active = true;
            this.stateOpenNode.active = false;
            this.stateCloseLab.active = true;
        } else if((status & Config.tableStatus.Suspend) == Config.tableStatus.Suspend) {
            this.roadSprite.active = this.roadMapNode.active = false;
            this.btnBgLight.node.active = true;
            this.closeBg.active = false;
            this.stateOpenNode.active = true;
            this.stateCloseLab.active = false;
        } else {
            this.roadSprite.active = this.roadMapNode.active = true;
            this.btnBgLight.node.active = true;
            this.closeBg.active = false;
            this.stateOpenNode.active = true;
            this.stateCloseLab.active = false;
        }
    }
});
