import Tool from '../tool/Tool'
import Config from '../constants/Config'
cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onLoad: function () {
        this.node.on('click', ()=> {
            Tool.returnToPlatform()
        })
    },
    
});
