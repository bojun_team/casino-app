
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
       lbUserName:cc.Label,
    },
    
    onLoad: function () {
        let username$ = Casino.User.propChange$.username
        this.subscribes = []
        this.subscribes[this.subscribes.length] = username$.subscribe({
            next: x => {  
                if (x) this.setUserName()
            }
        });
    },

    setUserName:function(){
        this.lbUserName.string = Casino.User.username
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },
});
