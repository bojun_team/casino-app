
import Type from '../constants/type'
var { BANKER, PLAYER, TIE } = Type

cc.Class({
    extends: cc.Component,

    properties: {
        bule:cc.Sprite,
        red:cc.Sprite,
        numLab:cc.Label,
        green:cc.Sprite,
    }, 
    setData: function(data)
    { 
        this.green.node.active = false
        if (data == null) {
            this.bule.node.active = false
            this.red.node.active = false
            this.numLab.node.active = false
            return
        }
        this.bule.node.active = data.type == PLAYER
        this.red.node.active = data.type == BANKER
        this.numLab.node.active = data.value != 0
        if (data.value != 0){
            this.numLab.string = data.value
            if (data.type != PLAYER && data.type != BANKER && data.value !="") this.green.node.active = true
        }

        this.data = data
        this.isHaveData = true 
    }, 
    startBlink: function(data){ 
        this.setData(data)
        this.isHaveData = false 
        const blinkDuration = 10
        const blinkCount = 20
        var seq = cc.sequence(cc.blink(blinkDuration, blinkCount), cc.callFunc(this.stopBlink, this))
        this.node.runAction(seq) 
    },
    stopBlink: function(){ 
        this.node.stopAllActions()
        if (this.isHaveData)
        { 
            this.setData(this.data)
        }
        else
        { 
            this.bule.node.active = false
            this.red.node.active = false
        }  
    },
});
