
import DebugFunction from "../debugPanel/DebugFunction"
var ResultView = require("ResultView");

cc.Class({
    extends: cc.Component,

    properties: {
       resultView: ResultView
    },

    // use this for initialization
    onLoad: function () {

    },

    onClickResetBtn: function() {
        this.resultView.resetBlinkNode()
    },

    onClickPlayerWinBtn: function() {
        this.onClickResetBtn()
        this.resultView.blinkPlayer()
    },

    onClickBankerWinBtn: function() {
        this.onClickResetBtn()
        this.resultView.blinkBanker()
    },

    onClickTieBtn: function() {
        this.onClickResetBtn()
        this.resultView.blinkTie()
    }

});
