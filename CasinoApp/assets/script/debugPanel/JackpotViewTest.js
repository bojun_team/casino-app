
// JackpotViewTest.js
import JackpotAlert from '../tool/JackpotAlert'
import JackpotWinView from '../tool/JackpotWinView'
import Casino from '../model/Casino'
import Config from '../constants/Config'

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onLoad: function () {
        this.node.zIndex = 999
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node
        let btns = this.node.getChildByName("btns")
        this.node.getChildByName("btn0").on("click", ()=>{
            btns.active = !btns.active
        }, this)
        btns.getChildByName("btn1").on("click", ()=>{
            this.continueWin = (++this.continueWin > 20) ? 0 : this.continueWin
            let kind = (this.continueWin>14) ? (Config.JackpotKind.JACKPOT_BIG_WIN_NOT_ARRIVE) : (Config.JackpotKind.NON_JACKPOT)
            Casino.User.updateJackpotInfo(this.continueWin, kind, 0, 0, 0)
        }, this)
        btns.getChildByName("btn3").on("click", ()=>{
            Casino.User.updateJackpotInfo(10, Config.JackpotKind.JACKPOT_BONUS, 6000, 100, 0)
        }, this)
        btns.getChildByName("btn4").on("click", ()=>{
            Casino.User.updateJackpotInfo(10, Config.JackpotKind.NON_JACKPOT, 0, 100, 0)
        }, this)
        btns.getChildByName("btn5").on("click", ()=>{
            Casino.User.updateJackpotInfo(15, Config.JackpotKind.JACKPOT_BIG_WIN_NOT_ARRIVE, 0, 1000, 0)            
        }, this)
        btns.getChildByName("btn6").on("click", ()=>{
            Casino.User.updateJackpotInfo(15, Config.JackpotKind.JACKPOT_BIG_WIN, 0, 1000300, 1.5)
        }, this)
        btns.getChildByName("btn7").on("click", ()=>{
            Casino.User.updateJackpotInfo(20, Config.JackpotKind.FINAL_JACKPOT_BIG_WIN, 9487, 1000300, 1.5)
        }, this)
        
    }
    
});
