import DebugFunction from "../debugPanel/DebugFunction"
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        roadmapInput:cc.EditBox,
        printLab:cc.Label,
        newInput:cc.EditBox,
        newInput01:cc.EditBox,
        newInput02:cc.EditBox,
        newInput03:cc.EditBox,
    },

    // use this for initialization
    onLoad: function () {
        this.node.zIndex = 99999
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node
    },
    close:function(){
        this.node.active = !this.node.active
    },
    Roadmapupdate:function(){
        DebugFunction.Roadmapupdate(this.roadmapInput.string, this.newInput.string, this.newInput01.string)
        // DebugFunction.sendinitialGambleTable()
    },
    Statussettlement:function(){
        DebugFunction.Statussettlement(this.newInput.string, this.newInput01.string)
    },
    UpdateStatus:function(){
        DebugFunction.UpdateStatus(this.newInput.string, this.newInput01.string)
    },
    Stateopenpoker:function(){
        DebugFunction.Stateopenpoker(this.newInput.string, this.newInput01.string)
    },
    Modifythestate:function(){
        DebugFunction.Modifythestate(this.newInput02.string, this.newInput03.string)
    },
     Loginpacket:function(){
        DebugFunction.Loginpacket()
    },
     Openanewbureau:function(){
        DebugFunction.Openanewbureau(this.newInput.string)
    },

    WinBanker:function(){
        DebugFunction.Roadmapupdate("100", this.newInput.string, this.newInput01.string)
    },

    WinTie:function(){
        DebugFunction.Roadmapupdate("300", this.newInput.string, this.newInput01.string)
    },

    WinPlayer:function(){
        DebugFunction.Roadmapupdate("200", this.newInput.string, this.newInput01.string)
    },

    NoBet5: function() {
        console.log('jun button click')
        let data = {
            H: 'CasinoHub',
            M: 'idleRunNeverBet',
            A:[5]
        }
        let jsonStr = JSON.stringify(data)
        window.NativeConnect.onRecvServerData(jsonStr)
    }
});
