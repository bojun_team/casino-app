import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onCountDown: function(events, params) {
        this.tableName = this.node.parent.getComponent("multipleTableMaintain").tableName
        this.tableID = this.node.parent.getComponent("multipleTableMaintain").tableID
        
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        table.status = Config.gameState.CountDown
    },

    onMaintenance: function(events, params) {
        this.tableName = this.node.parent.getComponent("multipleTableMaintain").tableName
        this.tableID = this.node.parent.getComponent("multipleTableMaintain").tableID

        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        table.status = Config.gameState.Maintenance
    },
});
