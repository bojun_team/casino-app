import localizeMgr from '../tool/localize/LocalizeMgr'
import Localize from '../constants/Localize'
import Config from '../constants/Config'
import Casino from '../model/Casino';

cc.Class({
    extends: cc.Component,

    properties: {

    },
    
    onLoad: function () {
        this.node.zIndex = 999
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node

        

        this.node.getChildByName("ZH").on("click", (function () {
            localizeMgr.changeLanguage(Config.Language.ZH)
        }), this);

        this.node.getChildByName("CN").on("click", (function () {
            localizeMgr.changeLanguage(Config.Language.CN)
        }), this);

        
    },

});
