import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        tableName:{
            visable:false,
            default:""
        },
        tableID:{
            visable:false,
            default:""
        },
    },
    onLoad: function () {
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.tableName = Casino.Game.tableName
            this.tableID = Casino.Game.tableID
        } else {
            this.tableName = this.node.parent.getComponent("BetZoneCtr").tableName
            this.tableID = this.node.parent.getComponent("BetZoneCtr").tableID
        }
    },
    onWin: function(events, params) {
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        table.status = Config.gameState.Computing
        let game = Casino.Game.getGame(this.tableName, this.tableID)
        game.result.totalAmount = 1500
        game.result.hitSpots = [String(params)]
    },
    stopBlink: function() {
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        table.status = Config.gameState.CountDown
        this.cleanAndSaveChipAmount()
    },
    cleanAndSaveChipAmount: function() {
        let game = Casino.Game.getGame(this.tableName, this.tableID)
        game.spotMgr.cleanAndSaveChipAmount ()
    },
    onLess30RunButtonClick: function() {
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        table.run = 15
    },
    onMoreThan30RunButtonClick: function() {
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        table.run = 32
    },
    onStandardButtonClick: function() {
        Casino.Game.getGame(this.tableName, this.tableID).betZoneType = Config.betZoneType.Standard
    },
    onNoWaterButtonClick: function() {
        Casino.Game.getGame(this.tableName, this.tableID).betZoneType = Config.betZoneType.NoWater
    },
});
