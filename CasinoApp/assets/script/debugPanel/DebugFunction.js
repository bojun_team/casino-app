import NativeConnect from '../native/NativeConnect'
import Config from '../constants/Config'
var DebugFunction = {
   Roadmapupdate :function(Roadmapunumber,Gametable,Gameboard){ //路途更新
              var str = "{\"H\":\"CasinoHub\",\"M\":\"roadMapMessageReceived\",\"A\":"+
                       "["+
                           "{"+
                               "\"TableName\":\"Baccarat\","+ //遊戲名稱
                               "\"TableID\":\"" + Gametable + "\","+ //桌檯
                               "\"Run\":\"" + Gameboard + "\","+  //局數
                               "\"Status\":0,"+ //遊戲狀態
                               "\"Round\":\"1866380\","+ //輪數
                               "\"Roadmap\":\"" + Roadmapunumber + "\""+
                           "}"+
                       "]"+
                   "}"
       window.NativeConnect.onRecvServerData(str)
  },

 Statussettlement :function(Gametable,Gameboard){ //結算中
           var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":"+
           "["+
           "{"+
           "\"TableName\":\"Baccarat\","+ //遊戲名稱
           "\"TableID\":\""+ Gametable +"\","+ //桌檯
           "\"Run\":\"" + Gameboard + "\","+  //局數
           "\"Stats\":{\"BankerOdd\":24,"+
           "\"Banker\":22,"+
           "\"Poker5\":\"S7\","+
           "\"PlayerBankerJQK\":0,"+
           "\"PlayerA\":0,"+
           "\"Small\":11,"+
           "\"BankerJQK\":0,"+
           "\"Poker2\":\"HQ\","+
           "\"Tie\":3,"+
           "\"BankerA\":0,"+
           "\"Player\":17,"+
           "\"PlayerBankerA\":0,"+
           "\"Poker4\":\"D2\","+
           "\"PlayerOdd\":24,"+
           "\"WinFlag\":1,"+
           "\"PlayerPair\":3,"+
           "\"PlayerEven\":18,"+
           "\"Poker6\":\"D2\","+
           "\"BankerPair\":7,"+
           "\"Big\":31,"+
           "\"Poker1\":\"CQ\","+
           "\"BankerEven\":18,"+
           "\"BankerJQKA\":0,"+
           "\"PlayerJQK\":0,"+
           "\"PlayerJQKA\":0,"+
           "\"PlayerBankerJQKA\":0,"+
           "\"Super6\":0,"+
           "\"Poker3\":\"S4\"},"+
           "\"Round\":\"1866258\","+ //輪數
           "\"Status\":3"+ //遊戲狀態
           "}"+
           "]"+
           "}"

        window.NativeConnect.onRecvServerData(str)
    },


 UpdateStatus :function(Gametable,Gameboard){ //更新狀態
           var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":"+
           "["+
           "{"+
           "\"TableName\":\"Baccarat\","+ //遊戲名稱
           "\"TableID\":\""+ Gametable +"\","+ //桌檯
           "\"Run\":\"" + Gameboard + "\","+  //局數
           "\"Status\":6,"+ //遊戲狀態
           "\"TotalTime\":30,"+
           "\"Round\":\"1866258\""+ //輪數
           "}"+ 
           "]"+
           "}"

        window.NativeConnect.onRecvServerData(str)
    },

 Stateopenpoker :function(Gametable,Gameboard){ //開牌中
           var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":"+
           "["+
           "{"+
           "\"Result\":\"D7\","+
           "\"Run\":\"" + Gameboard + "\","+ //局數
           "\"Status\":2,"+ //遊戲狀態
           "\"Round\":\"1866259\","+ //輪數
           "\"TableName\":\"Baccarat\","+
           "\"Latency\":0,"+
           "\"Sequence\":1,"+
           "\"TableID\":\""+ Gametable +"\""+//桌檯
           "}"+ 
           "]"+
           "}"

        window.NativeConnect.onRecvServerData(str)
    },    

 Openanewbureau :function(Gametable){ //開啟新局
           var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":"+
           "["+
           "{"+
           "\"TableName\":\"Baccarat\","+ //遊戲名稱
           "\"Status\":1,"+ //遊戲狀態
           "\"TableID\":\""+ Gametable +"\""+//桌檯
           "}"+ 
           "]"+
           "}"

        window.NativeConnect.onRecvServerData(str)
    },       
    setDynamicOddsState :function(Gametable, state){ //開啟新局
        const stateStr = "" + state
        var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":"+
           "["+
           "{"+
           "\"TableName\":\"Baccarat\","+ //遊戲名稱
           "\"Status\":"+ stateStr + ","+ //遊戲狀態
           "\"TableID\":\""+ Gametable +"\""+//桌檯
           "}"+ 
           "]"+
           "}"
        cc.log("bean", str)
        window.NativeConnect.onRecvServerData(str)
    },
 Modifythestate :function(Gametable,Gamestate){ //修改
           var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":"+
           "["+
           "{"+
           "\"TableName\":\"Baccarat\","+ //遊戲名稱
           "\"Status\":"+ Gamestate +","+ //遊戲狀態
           "\"TableID\":\""+ Gametable +"\""+//桌檯
           "}"+ 
           "]"+
           "}"

        window.NativeConnect.onRecvServerData(str)
    },       

     Loginpacket :function(){ //登入發送
           var str = "{\"H\":\"CasinoHub\",\"M\":\"initialCasinoTable\",\"A\":"+
           "["+
           "["+
           "{"+
           "\"Run\":\"39\","+ //局數
           "\"MiCardTotalTime\":0,"+ 
           "\"TotalTime\":30,"+
           "\"TableName\":\"Baccarat\","+ //遊戲名稱
           "\"TableMode\":\"BaccaratSpeed\","+ 
           "\"Round\":\"1866376\","+ //輪數
           "\"GameStatus\":3,"+
           "\"CountDownSec\":0,"+
           "\"TableID\":\"A\","+ //桌檯
           "\"Roadmap\":\"180,190,150,150,291,190,170,240,190,180,160,370,260,370,270,180,250,180,150,250,280,130,190,390,290,170,170,260,240,182,280,290,190,150,380,290,280,180,160\"},"+
           "{"+
           "\"Run\":\"3\","+
           "\"MiCardTotalTime\":0,"+
           "\"TotalTime\":30,"+
           "\"TableName\":\"Baccarat\","+
           "\"TableMode\":\"BaccaratSpeed\","+
           "\"Round\":\"1866383\","+
           "\"GameStatus\":4,"+
           "\"CountDownSec\":0,\"TableID\":\"B\",\"Roadmap\":\"\"},{\"Run\":\"42\",\"MiCardTotalTime\":0,\"TotalTime\":20,\"TableName\":\"Baccarat\",\"TableMode\":\"BaccaratStandard\",\"Round\":\"1866378\",\"GameStatus\":1,\"CountDownSec\":6,\"TableID\":\"C\",\"Roadmap\":\"190,191,252,290,250,380,170,270,180,290,130,190,180,260,290,160,190,290,291,160,350,190,370,250,240,270,250,290,180,260,232,190,270,170,290,170,280,290,190,280,260\"},{\"Run\":\"24\",\"MiCardTotalTime\":20000,\"TotalTime\":30,\"TableName\":\"Baccarat\",\"TableMode\":\"BaccaratStandard\",\"Round\":\"1866377\",\"GameStatus\":1,\"CountDownSec\":20,\"TableID\":\"D\",\"Roadmap\":\"260,282,290,261,190,370,290,190,260,190,180,240,270,160,342,150,170,192,280,240,290,260,360\"},{\"Run\":\"19\",\"MiCardTotalTime\":0,\"TotalTime\":30,\"TableName\":\"Baccarat\",\"TableMode\":\"BaccaratMiCard\",\"Round\":\"1866380\",\"GameStatus\":2,\"CountDownSec\":0,\"TableID\":\"E\",\"Roadmap\":\"280,180,270,250,150,260,270,190,290,190,281,150,270,302,361,160,191,150\"},{\"Run\":\"32\",\"MiCardTotalTime\":0,\"TotalTime\":30,\"TableName\":\"Baccarat\",\"TableMode\":\"BaccaratMiCard\",\"Round\":\"1866379\",\"GameStatus\":2,\"CountDownSec\":0,\"TableID\":\"F\",\"Roadmap\":\"270,370,290,180,120,270,150,260,180,171,170,243,292,141,190,280,190,180,190,180,292,151,262,210,261,272,180,110,260,190,240\"}]]}"

        window.NativeConnect.onRecvServerData(str)
    },    

    seat:function(type, tableID, fun){
        this.sendGetUserLimitStake()
        this.sendinitialGambleTable(type, tableID)
        this.sendBettingList()
        fun("seat", "0")
    },
    sendBettingList:function(){
        window.NativeConnect.onRecvServerData("{\"H\":\"CasinoHub\",\"M\":\"getSelfBettingList\",\"A\":[null]}")
    },
    sendGetUserLimitStake:function(){
        const str = "{\"H\":\"CasinoHub\",\"M\":\"getUserLimitStake\",\"A\":[[{\"LimitId\":1,\"Selected\":0,\"Chip\":[\"10\",\"50\",\"1000\"],\"MinLimit\":50,\"GameName\":\"Baccarat\",\"MaxLimit\":1000},{\"LimitId\":2,\"Selected\":0,\"Chip\":[\"500\",\"1000\",\"2000\"],\"MinLimit\":100,\"GameName\":\"Baccarat\",\"MaxLimit\":2000},{\"LimitId\":3,\"Selected\":0,\"Chip\":[\"1000\",\"2000\",\"5000\"],\"MinLimit\":200,\"GameName\":\"Baccarat\",\"MaxLimit\":5000},{\"LimitId\":9,\"Selected\":0,\"Chip\":[\"2000\",\"5000\",\"10000\"],\"MinLimit\":500,\"GameName\":\"Baccarat\",\"MaxLimit\":10000},{\"LimitId\":6,\"Selected\":1,\"Chip\":[\"1\",\"2000\",\"5000\"],\"MinLimit\":10,\"GameName\":\"Baccarat\",\"MaxLimit\":100000}]]}"
        window.NativeConnect.onRecvServerData(str)
        
    },
    sendinitialGambleTable:function(tableName, tableId){
         var str = "{\"H\":\"CasinoHub\",\"M\":\"initialGambleTable\",\"A\":[{\"ErrorCode\":0,\"TableTimerInfo\":{\"TotalTime\":20,\"MiCardTotalTime\":0,\"CountDownSec\":0},\"BetingnStatisticsList\":[{\"SpotId\":11001,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11002,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11003,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11004,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11005,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11006,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11007,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11008,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11009,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11010,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11011,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11012,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11013,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11014,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11015,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11016,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11017,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11018,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11019,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11020,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11021,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11022,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11023,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11024,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11025,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},{\"SpotId\":11026,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0}],\"BaccaratGameLimitStake\":{\"TableName\":\"Baccarat\",\"Limits\":[{\"LimitMax\":100000,\"LimitMin\":50,\"ApplySpotIds\":[11001,11002,11003,11004,11005,11006,11007,11008,11009,11010,11022,11021],\"Odds\":1,\"Id\":0},{\"LimitMax\":100000,\"LimitMin\":50,\"ApplySpotIds\":[11011],\"Odds\":3,\"Id\":0},{\"LimitMax\":100000,\"LimitMin\":50,\"ApplySpotIds\":[11012,11013],\"Odds\":4,\"Id\":0},{\"LimitMax\":50000,\"LimitMin\":50,\"ApplySpotIds\":[11014,11023],\"Odds\":8,\"Id\":0},{\"LimitMax\":25000,\"LimitMin\":50,\"ApplySpotIds\":[11015,11016,11025,11024],\"Odds\":11,\"Id\":0},{\"LimitMax\":25000,\"LimitMin\":50,\"ApplySpotIds\":[11026],\"Odds\":12,\"Id\":0},{\"LimitMax\":25000,\"LimitMin\":50,\"ApplySpotIds\":[11017,11018],\"Odds\":14,\"Id\":0},{\"LimitMax\":10000,\"LimitMin\":50,\"ApplySpotIds\":[11019],\"Odds\":25,\"Id\":0},{\"LimitMax\":10000,\"LimitMin\":50,\"ApplySpotIds\":[11020],\"Odds\":200,\"Id\":0}]},\"VideoInfo\":{\"ChinaVideoUrl\":{\"MobileVideoUrl\":null,\"WebVideoUrl\":null},\"UniversalVideoUrl\":{\"MobileVideoUrl\":\"stream.wm77.asia\/live2\/stream3\",\"WebVideoUrl\":\"stream.wm77.asia\/live2\/stream1\"}},\"IsAvailable\":true,\"BaccaratGameStats\":{\"BankerOdd\":31,\"Banker\":21,\"Poker5\":\"\",\"PlayerBankerJQK\":0,\"PlayerA\":0,\"Small\":23,\"BankerJQK\":0,\"Poker2\":\"\",\"Tie\":6,\"BankerA\":0,\"Player\":24,\"PlayerBankerA\":0,\"Poker4\":\"\",\"PlayerOdd\":20,\"WinFlag\":3,\"PlayerPair\":3,\"PlayerEven\":31,\"Poker6\":\"\",\"BankerPair\":2,\"Big\":28,\"Poker1\":\"\",\"BankerTotal\":0,\"BankerEven\":20,\"PlayerTotal\":0,\"PlayerJQK\":0,\"BankerJQKA\":0,\"PlayerJQKA\":0,\"Poker3\":\"\",\"PlayerBankerJQKA\":0,\"Super6\":0}}]}"

        let data = JSON.parse(str)
        data.A[0].TableName = tableName
        data.A[0].TableId = tableId
        str = JSON.stringify(data)

        window.NativeConnect.onRecvServerData(str)
    } ,
    sendUpdateBettingPool:function(){
        var str = "{\"H\":\"CasinoHub\",\"M\":\"updateBettingPool\",\"A\":[["+
        "{\"SpotId\":11001,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":100,\"SpotTotal\":100},"+
        "{\"SpotId\":11002,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":200,\"SpotTotal\":3210},"+
        "{\"SpotId\":11003,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":300,\"SpotTotal\":4320},"+
        "{\"SpotId\":11004,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11005,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11006,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11007,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11008,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11009,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11010,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11011,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11012,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11013,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11014,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11015,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11016,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11017,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11018,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11019,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11020,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11021,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11022,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11023,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11024,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11025,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0},"+
        "{\"SpotId\":11026,\"PlayerBetingDetailList\":[],\"PlayerBetingList\":[],\"SpotPlayerCount\":0,\"SpotTotal\":0}]]}"
        window.NativeConnect.onRecvServerData(str)
    },
    sendUpdateComputingResult:function(winAmount){
        var str = "{\"H\":\"CasinoHub\",\"M\":\"updateComputingResult\",\"A\":[{\"TableName\":\"Baccarat\",\"HitSpots\":[11014,11004,11007,11010],\"TotalAmount\":"+winAmount+",\"UiTotalAmount\":950,\"PlayerHitSummary\":[{\"HitSpot\":11001,\"LoseWinAmount\":950,\"WinCapital\":1000},{\"HitSpot\":11002,\"LoseWinAmount\":-1000,\"WinCapital\":1000},{\"HitSpot\":11014,\"LoseWinAmount\":-1000,\"WinCapital\":1000},{\"HitSpot\":11015,\"LoseWinAmount\":-1000,\"WinCapital\":1000},{\"HitSpot\":11016,\"LoseWinAmount\":-1000,\"WinCapital\":1000}],\"TableId\":\"A\"}]}"
        window.NativeConnect.onRecvServerData(str)
    },
    sendGameStatusUpdateMessageReceivedStatu2:function(Sequence, pokerName){
        var str = "{\"H\":\"CasinoHub\",\"M\":"+
        "\"gameStatusUpdateMessageReceived\",\"A\":[{"+
        "\"Result\":\""+pokerName+"\",\"Run\":\"71\",\"Status\":2,"+
        "\"Round\":\"1867391\",\"TableName\":\"Baccarat\","+
        "\"Latency\":0,\"Sequence\":"+Sequence+",\"TableID\":\"A\"}]}" 
        window.NativeConnect.onRecvServerData(str) 
    },
    sendGameStatusUpdateMessageReceivedStatu3:function(Sequence, pokerName){
        var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":[{\"TableName\":\"Baccarat\",\"TableID\":\"A\",\"Run\":\"19\",\"Stats\":{\"BankerOdd\":7,\"Banker\":10,\"Poker5\":\"\",\"PlayerBankerJQK\":0,\"PlayerA\":0,\"Small\":7,\"BankerJQK\":0,\"Poker2\":\"C1\",\"Tie\":0,\"BankerA\":0,\"Player\":9,\"PlayerBankerA\":0,\"Poker4\":\"H7\",\"PlayerOdd\":9,\"WinFlag\":1,\"PlayerPair\":1,\"PlayerEven\":10,\"Poker6\":\"\",\"BankerPair\":0,\"Big\":12,\"Poker1\":\"S0\",\"BankerEven\":12,\"BankerJQKA\":0,\"PlayerJQK\":0,\"PlayerJQKA\":0,\"PlayerBankerJQKA\":0,\"Super6\":0,\"Poker3\":\"H2\"},\"Round\":\"1867404\",\"Status\":3}]}"
        window.NativeConnect.onRecvServerData(str) 
    },
    sendGameStatusUpdateMessageReceivedStatu1:function(Sequence, pokerName){
        var str = "{\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":[{\"TableName\":\"Baccarat\",\"TableID\":\"A\",\"Run\":\"42\",\"Status\":1,\"Round\":\"1867400\"}]}"
        window.NativeConnect.onRecvServerData(str) 
    },
    sendGameStatusUpdateMessageReceivedStatuAny:function(state){
        var str = " {\"H\":\"CasinoHub\",\"M\":\"gameStatusUpdateMessageReceived\",\"A\":[{\"TableName\":\"Baccarat\",\"TableID\":\"A\",\"Run\":\"73\",\"Status\":"+state+",\"Round\":\"1869190\"}]}"
        window.NativeConnect.onRecvServerData(str) 
    },
    sendInitTables:function()
    {
        var str = "{" +
            "\"H\":\"CasinoHub\"," +
            "\"M\":\"initialCasinoTable\"," +
            "\"A\":[" +
                "[" +
                    "{\"Run\":\"56\",\"MiCardTotalTime\":0,\"TotalTime\":20,\"TableName\":\"Baccarat\",\"VideoProvider\":4,\"isR18\":false,\"liveAuth\":false,\"TableMode\":\"BaccaratSpeed\",\"Round\":\"1868621\",\"GameStatus\":1,\"CountDownSec\":4,\"TableID\":\"A\",\"Roadmap\":\"300,310,310,280,280,280,280,280,280,280,310,150,310,250,250\"}," +
                    "{\"Run\":\"65\",\"MiCardTotalTime\":0,\"TotalTime\":20,\"TableName\":\"Baccarat\",\"VideoProvider\":4,\"isR18\":false,\"liveAuth\":false,\"TableMode\":\"BaccaratSpeed\",\"Round\":\"1868620\",\"GameStatus\":5,\"CountDownSec\":0,\"TableID\":\"B\",\"Roadmap\":\"310\"}," +
                    "{\"Run\":\"68\",\"MiCardTotalTime\":0,\"TotalTime\":30,\"TableName\":\"Roulette\",\"VideoProvider\":4,\"isR18\":false,\"liveAuth\":false,\"TableMode\":\"BaccaratStandard\",\"Round\":\"1868616\",\"GameStatus\":4,\"CountDownSec\":0,\"TableID\":\"C\",\"Roadmap\":\"\"}," +
                    "{\"Run\":\"63\",\"MiCardTotalTime\":0,\"TotalTime\":30,\"TableName\":\"Baccarat\",\"VideoProvider\":4,\"isR18\":false,\"liveAuth\":false,\"TableMode\":\"BaccaratStandard\",\"Round\":\"1868619\",\"GameStatus\":1,\"CountDownSec\":8,\"TableID\":\"D\",\"Roadmap\":\"\"}," +
                    "{\"Run\":\"63\",\"MiCardTotalTime\":0,\"TotalTime\":30,\"TableName\":\"Baccarat\",\"VideoProvider\":4,\"isR18\":true,\"liveAuth\":true,\"TableMode\":\"BaccaratStandard\",\"Round\":\"1868619\",\"GameStatus\":1,\"CountDownSec\":30,\"TableID\":\"E\",\"Roadmap\":\"\"}" +
                    
                "]" +
            "]" +
        "}"
        window.NativeConnect.onRecvServerData(str) 
    },
    sendGetUserCredit:function()
    {
        var str = "{\"H\":\"CasinoHub\",\"M\":\"getUserCredit\",\"A\":[{\"CashType\":2,\"UserPassId\":\"a5b4c38c-0939-472b-af34-aee4d38ec7d3\",\"UserCredit\":{\"Result\":1,\"Casino\":8990325,\"Setting\":9000000,\"Date\":\"2017-03-24T00:00:00\",\"Used\":9675,\"WinCredit\":-867500,\"CreditType\":0}}]}"
        window.NativeConnect.onRecvServerData(str) 
    },
    // sendGetUserCredit:function()
    // {
    //     var str = "{\"H\":\"CasinoHub\",\"M\":\"getUserCredit\",\"A\":[{\"Setting\":19330,\"Casino\":19330,\"Date\":\"2017-02-23T00:00:00\",\"Result\":1,\"CreditType\":0,\"Used\":0}]}"
    //     window.NativeConnect.onRecvServerData(str) 
    // }
    
} 

module.exports = DebugFunction