
import SoundControl from '../tool/SoundControl'
import Config from '../constants/Config'

cc.Class({
    extends: cc.Component,

    properties: {
        
    },
    
    onLoad: function () {
        this.node.zIndex = 9999
        cc.game.addPersistRootNode(this.node) // 跳場景不會關閉node

        this.node.getChildByName("init").on("click", (function () {
            SoundControl.init()
        }), this)

        let isMute = false
        this.node.getChildByName("music").on("click", (function () {
            isMute = !isMute
            SoundControl.setBgMusicMute(isMute)
        }), this)

        this.node.getChildByName("playBtnEffect").on("click", (function () {
            SoundControl.playBtnEffect()
        }), this)

        let list1 = [
            Config.gameState.NewRoundRun,
            Config.gameState.EndBetTime,
            Config.gameState.HalfTime
        ]
        let list1Idx = -1
        this.node.getChildByName("gameStatus").on("click", (function () {
            if(++list1Idx > 2) list1Idx = 0
            SoundControl.gameStatus(list1[list1Idx])
        }), this)

        let countDownIdx = 6
        this.node.getChildByName("countDown").on("click", (function () {
            if(--countDownIdx < 0) countDownIdx = 6
            SoundControl.countDown(countDownIdx)
        }), this)

        let list2 = [
            'banker',
            'player',
            'tie'
        ]
        let list2dx = -1
        let point = -1
        this.node.getChildByName("whichWin").on("click", (function () {
            if(++list2dx > 2) list2dx = 0
            if(++point > 9) point = 0
            SoundControl.baccaratWhichWin(list2[list2dx], point)
        }), this)

        this.node.getChildByName("betEffect").on("click", (function () {
            SoundControl.betEffect()
        }), this)

        this.node.getChildByName("winMoney").on("click", (function () {
            SoundControl.winMoney()
        }), this)
        
    }
    
});
