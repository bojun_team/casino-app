var Localize = {
    // gameApp
    Baccarat: 'Baccarat',
    BaccaratMiCard: 'BaccaratMiCard',
    BaccaratSpeed: 'BaccaratSpeed',
    BaccaratStandard: 'BaccaratStandard',
    Table: 'Table',
    Round: 'Round',
    Run: 'Run',
    Limit: 'Limit',
    TotalBets: 'TotalBets',
    JackpotRule: 'JackpotRule',
    JackpotContinueWin: 'JackpotContinueWin',
    JackpotYouContinueWin: 'JackpotYouContinueWin',
    Banker: 'Banker',
    Player: 'Player',
    Tie: 'Tie',
    BankerPair: 'BankerPair',
    PlayerPair: 'PlayerPair',
    Big: 'Big',
    Small: 'Small',
    Super6: 'Super6',
    BankerWin: 'BankerWin',
    PlayerWin: 'PlayerWin',
    TieWin: 'TieWin',
    CountDown: 'CountDown',
    Dealing: 'Dealing',
    Computing: 'Computing',
    HalfTime: 'HalfTime',
    Maintenance: 'Maintenance',
    NewRoundRun: 'NewRoundRun',
    ComputingFinish: 'ComputingFinish',
    EndBetTime: 'EndBetTime',
    MiCard: 'MiCard',
    EndMiCardTime: 'EndMiCardTime',
    CancelRun: 'CancelRun',
    TouchOpenVideo: 'TouchOpenVideo',
    SelectChip: 'SelectChip',
    TableMaintain: 'TableMaintain',
    WebLoading: 'WebLoading',

    // setting page
    Setting: 'Setting',
    Video: 'Video',
    Music: 'Music',
    Effect: 'Effect',
    HistoryBetList: 'HistoryBetList',
    PlayRule: 'PlayRule',
    CustomerService: 'CustomerService',

    // loading page
    LoadingTable: 'LoadingTable',
    LoadingLogin: 'LoadingLogin',
    LoadingLogout: 'LoadingLogout',
    LoadingGetChip: 'LoadingGetChip',
    LoadingGetSpot: 'LoadingGetSpot',
    LoadingGetServer: 'LoadingGetServer',
    LoadingGetVersion: 'LoadingGetVersion',
    LoadingGetImage: 'LoadingGetImage',
    LoadingGetRoadMap: 'LoadingGetRoadMap',
    LoadingAssetsUpdate: 'LoadingAssetsUpdate',

    // messageApp
    NoLocalize: 'NoLocalize',
    JPRemindThreeTimeWithGame: 'JPRemindThreeTimeWithGame',
    CancelAndReturnMoney: 'CancelAndReturnMoney',
    CreditRecoverAPI: 'CreditRecoverAPI',
    JackpotNormalWinMessage: 'JackpotNormalWinMessage',
    JackpotSuperBigWinMessage: 'JackpotSuperBigWinMessage',
    JackpotBigWinMessage: 'JackpotBigWinMessage',
    UpdateInfoTitle: 'UpdateInfoTitle',
    UpdateInfoMessage: 'UpdateInfoMessage',

    // BetFailMsg
    BetFailMsgDefault: 'BetFailMsgDefault',
    OperationFail: 'OperationFail',
    PhysicalError: 'PhysicalError',
    TableNotFound: 'TableNotFound',
    NotLogin: 'NotLogin',
    PlayerNotSeatIn: 'PlayerNotSeatIn',
    NotSufficientCredit: 'NotSufficientCredit',
    UserNotFound: 'UserNotFound',
    UserBetLocked: 'UserBetLocked',
    TableLimitBetFaild: 'TableLimitBetFaild',
    LimitStakeBetFaild: 'LimitStakeBetFaild',
    CreditServerUpdateFaild: 'CreditServerUpdateFaild',
    WeekWinBetFaild: 'WeekWinBetFaild',
    WeekLoseBetFaid: 'WeekLoseBetFaid',
    LimitStakeMaxBetFail: 'LimitStakeMaxBetFail',
    LimitStakeMinBetFail: 'LimitStakeMinBetFail',
    TableLimitCantNotCalac: ' TableLimitCantNotCalac',
    WeekAndLoseNotCalac: 'WeekAndLoseNotCalac',
    PlayerNoPremission: 'PlayerNoPremission',
    BettingOfPlayerNotFound: 'BettingOfPlayerNotFound',
    RebetLowerBetMiniLimit: 'RebetLowerBetMiniLimit',
    BetIsMax: 'BetIsMax',
    BetOverCredit: 'BetOverCredit',
    BetOverLimit: 'BetOverLimit',

    // SeatFailMsg
    SeatFailMsgDefault: 'SeatFailMsgDefault',
    SeatFailWeekWinBetFaild: "SeatFailWeekWinBetFaild",
    SeatFailWeekLoseBetFaid: "SeatFailWeekLoseBetFaid",
    SeatFailPlayerNoPremission: "SeatFailPlayerNoPremission",
    SeatFailDuplicateIPInTable: "SeatFailDuplicateIPInTable",
    SeatFailHaveBettedInSingleTable: "SeatFailHaveBettedInSingleTable",
    SeatFailHaveBettedInMultipleTable: "SeatFailHaveBettedInMultipleTable",
    SeatFailToBeMaintain: "SeatFailToBeMaintain",
    SeatFailTimeOut: "SeatFailTimeOut",
    SeatFailSignalRError: "SeatFailSignalRError",

    // WebApiFailMsg
    WebApiFailMsgDefault: 'WebApiFailMsgDefault',
    WebApiFailCantParserRequest: "WebApiFailCantParserRequest",
    WebApiFailParmError: "WebApiFailParmError",
    WebApiFailLoginFail: "WebApiFailLoginFail",

    // Alert
    AlertTitleWarning: 'AlertTitleWarning',
    AlertTitleError: 'AlertTitleError',
    AlertTitleNewVersion: 'AlertTitleNewVersion',

    LoadingGameDataTimeOut: 'LoadingGameDataTimeOut',
    LoadingGameFail: 'LoadingGameFail',
    LoadingGameUITimeOut: 'LoadingGameUITimeOut',
    GameTimeOutFail: 'GameTimeOutFail',
    TableMaintenance: 'TableMaintenance',
    NoBetFiveRun: 'NoBetFiveRun',
    NoBetFiveMinute: 'NoBetFiveMinute',
    InBackGroundLongTime: 'InBackGroundLongTime',
    RemindNoBetFiveRun: 'RemindNoBetFiveRun',
    RemindNoBetFiveMinute: 'RemindNoBetFiveMinute',
    KickUserNoAuthority: 'KickUserNoAuthority',
    KickUserTypeRelogin: 'KickUserTypeRelogin',
    KickUser: 'KickUser',
    ConnectClose: 'ConnectClose',
    ConnectError: 'ConnectError',
    NoSpotInfo: 'NoSpotInfo',
    GetSpotInfoTimeOut: 'GetSpotInfoTimeOut',
    NoChipInfo: 'NoChipInfo',
    GetChipInfoTimeOut: 'GetChipInfoTimeOut',
    NoGetVersion: 'NoGetVersion',
    GetVersionTimeOut: 'GetVersionTimeOut',
    NoServerState: 'NoServerState',
    GetServerStateTimeOut: 'GetServerStateTimeOut',
    LoginWebApiFail: 'LoginWebApiFail',
    GetLoginInfoTimeOut: 'GetLoginInfoTimeOut',
    VersionIsOld: 'VersionIsOld',
    VersionIsOldHotUpdate: 'VersionIsOldHotUpdate',
    LoginFail: 'LoginFail',
    GameServerMaintain: 'GameServerMaintain',
    CooperationLogout: 'CooperationLogout',
    CooperationFirstLogin: 'CooperationFirstLogin',
    CooperationTokenError: 'CooperationTokenError',
    CoopertaionAppEnvError: ' CoopertaionAppEnvError',
    UnknownError: 'UnknownError',
    CoopertaionTokenFail: 'CoopertaionTokenFail',

    webApiErrorMsg(errorCode=0) {
        switch(parseInt(errorCode)) {
            case -2 : return this.WebApiFailCantParserRequest
            case -1 : return this.WebApiFailParmError
            case 0  : return this.WebApiFailLoginFail
            default : return this.WebApiFailMsgDefault
        }
    },

    seatFailMsg(errorCode=0) {
        switch(parseInt(errorCode)) {
            case 110    : return this.SeatFailWeekWinBetFaild
            case 111    : return this.SeatFailWeekLoseBetFaid
            case 116    : return this.SeatFailPlayerNoPremission
            case 122    : return this.SeatFailDuplicateIPInTable
            case 123    : return this.SeatFailHaveBettedInSingleTable
            case 124    : return this.SeatFailHaveBettedInMultipleTable
            case 125    : return this.SeatFailToBeMaintain
            case -10001 : return this.SeatFailTimeOut
            case -10002 : return this.SeatFailSignalRError
            default     : return this.SeatFailMsgDefault
        }
    },

    betFailMsg(errorCode=0) {
        switch(parseInt(errorCode)) { 
            case 1	    : return this.OperationFail
            case 100	: return this.PhysicalError
            case 101	: return this.TableNotFound
            case 102	: return this.NotLogin
            case 103	: return this.PlayerNotSeatIn
            case 104	: return this.NotSufficientCredit
            case 105	: return this.UserNotFound
            case 106	: return this.UserBetLocked
            case 107	: return this.TableLimitBetFaild
            case 108	: return this.LimitStakeBetFaild
            case 109	: return this.CreditServerUpdateFaild
            case 110	: return this.WeekWinBetFaild
            case 111	: return this.WeekLoseBetFaid
            case 112	: return this.LimitStakeMaxBetFail
            case 113	: return this.LimitStakeMinBetFail
            case 114	: return this.TableLimitCantNotCalac
            case 115	: return this.WeekAndLoseNotCalac
            case 116	: return this.PlayerNoPremission
            case 117	: return this.BettingOfPlayerNotFound
            case -20001	: return this.RebetLowerBetMiniLimit
            case -20002	: return this.BetIsMax
            case 30001	: return this.BetOverCredit
            case 30002	: return this.BetOverLimit
            default     : return this.BetFailMsgDefault
        }
    },
    
    gameStateDataID(gameState=0) {
        switch(parseInt(gameState)) {
            case 1: return this.CountDown
            case 2: return this.Dealing
            case 3: return this.Computing
            case 4: return this.HalfTime
            case 5: return this.Maintenance
            case 6: return this.NewRoundRun
            case 7: return this.ComputingFinish
            case 8: return this.EndBetTime
            case 9: return this.MiCard
            case 10: return this.EndMiCardTime
            case 11: return this.CancelRun
            case 12: return this.CountDown
            case 13: return this.Dealing
            case 14: return this.CountDown
            case 15: return this.Dealing
            default: return ''; 
        }
    },

    tableNameDataID(tableName) {
        switch(String(tableName)) {
            case 'Baccarat': return this.Baccarat
            default: return ''; 
        }
    },
}

module.exports = Localize