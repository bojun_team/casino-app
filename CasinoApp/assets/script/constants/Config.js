

/* ============================================================================================= */
/* 遊戲的版本 */
const GAME_VERSION  = '2.2.44' 
const ASSET_VERSION = '2.2.44.38'

/* 遊戲的發佈環境 */
// const GAME_OUTPUT = 'test' 
// const GAME_OUTPUT = 'stage'         // 2.2.44.38.2
const GAME_OUTPUT = 'release'    // 2.2.44.38


/* 是否啟用Jackpot */
const JACKPOT_ENABLE = true

/* 是否顯示大廳更新公告 */
const IS_SHOW_LOBBY_UPDATE_MESSAGE = false

/* JP下注門檻提示 出現次數 */
const MAX_SHOW_JP_BET_HINT_TIMES = 3

/* ============================================================================================= */
/* 法老 皇璽 版本檢查 API */
const VERSION_CHECK_NORMAL_TEST = "https://pha168-apps-stage.sytepoker.com/apps/tw-casino-test/version";
const VERSION_CHECK_NORMAL_STAGE = "https://pha168-apps-stage.sytepoker.com/apps/tw-casino-stage/version";
const VERSION_CHECK_NORMAL_ONLINE = "https://pha168-apps.sytepoker.com/apps/tw-casino/version";

/* 眾發 版本檢查 API */
const VERSION_CHECK_JF_TEST = "https://pha168-apps-stage.sytepoker.com/apps/tw-test-casino-jhong-fa/version";
const VERSION_CHECK_JF_STAGE = "https://pha168-apps-stage.sytepoker.com/apps/tw-stage-casino-jhong-fa/version";
const VERSION_CHECK_JF_ONLINE = "https://pha168-apps.sytepoker.com/apps/tw-casino-jhong-fa/version";

/* ============================================================================================= */
/* 台灣版 遊戲伺服器IP */
const HOST_TW_TEST = 'http://192.168.119.72:8881'
const HOST_TW_STAGE = 'http://stage-game-center.tw.livecasino168.com:8881'
const HOST_TW_ONLINE = 'http://54.65.37.189:8881'

/* 台灣版 平台IP */
const PLATFORM_IP_TW_TEST = 'http://casino-api-local.casino.com'
const PLATFORM_IP_TW_STAGE = 'http://casino-api-stage.greatvirtue.com.tw'
const PLATFORM_IP_TW_ONLINE = 'http://api-tw.livecasino168.com'

/* 大陸版 遊戲伺服器IP */
const HOST_CN_TEST = 'http://192.168.119.72:8881' // 同台灣版
const HOST_CN_STAGE = 'http://slot.zhcbank168.com:8881'
const HOST_CN_ONLINE = 'http://studio.watvip.cn:8881'

/* 大陸版 平台IP */
const PLATFORM_IP_CN_TEST = 'http://casino-api-local.casino.com' // 同台灣版
const PLATFORM_IP_CN_STAGE = 'http://slot.zhcbank168.com:9487'
const PLATFORM_IP_CN_ONLINE = 'http://studio.watvip.cn:9487'


/* ============================================================================================= */
/* 遊戲伺服器 api */
const HOST_PATH_SIGNAL_R = '/signalr'
const HOST_PATH_CASINO_GET_SPOT = '/api/CasinoGetSpot'
const HOST_PATH_CASINO_GET_CHIP_SET = '/api/CasinoGetChipSet'

/* 台灣 客服網址 */
const CUSTOMER_TW_CREDIT = 'https://chat1.murmur365.com?companyID=1&iconID=6&website='
const CUSTOMER_TW_CASH = 'https://chat1.murmur365.com?companyID=1&iconID=1&website='

const CUSTOMER_TW_CREDIT_TEST = 'https://chatmem.murmurtest.com?companyID=1&iconID=6&website='
const CUSTOMER_TW_CASH_TEST = 'https://chatmem.murmurtest.com?companyID=1&iconID=1&website='

/* 大陸 客服網址 */
const CUSTOMER_CN_CREDIT = 'https://chat1.murmur365.com?companyID=3&iconID=3&website='
const CUSTOMER_CN_CASH = 'https://chat1.murmur365.com?companyID=3&iconID=3&website='

/* 串接版 平台 api */
const API_PATH_REPORT = '/player/report?token='
const API_PATH_RULE = '/game/rule?cashType='
const API_PATH_JACKPOT_RULE = '/game/jackpot?appBack=pharao-casino-mobile-cn://'
const API_PATH_VERSION_CHECK = '/app-api/version-check?'
const API_PATH_MAINTAIN_CHECK = '/app-api/maintain-check'
const API_PATH_LOGIN = '/app-api/login-game?'
const API_PATH_DIRECT_LOGIN = '/app-api/direct-login-game?dToken='
const API_PATH_FAKE_LOGIN = '/app-api/visitor'
const API_PATH_LOG_RECORD = '/app-api/log-record'

/* ============================================================================================= */
var Config = {
    DeepLinkHost: {
       LAUNCH_GAME: "launch-game"
    },

    webApiLanguage: function(lang) {
        switch(String(lang)) {
            case "zh_TW"    : return Config.Language.ZH
            case "zh_CN"    : return Config.Language.CN
            default     : return Config.Language.ZH
        }
    },

    Gametype: {
        CASINO: 1
    },

    Sourcetype: {
        ANDROID: 1,
        IOS: 2 
    },

    get currentSourcetype() {
        switch(cc.sys.os) {
            case cc.sys.OS_ANDROID: return this.Sourcetype.ANDROID
            default: return this.Sourcetype.IOS
        }
    },

    Language: {
        ZH:"zh", // 繁體中文
        CN:"cn", // 簡體中文
    },
    gameState: {
        CountDown : 1, //倒數
        Dealing : 2, //開牌
        Computing : 3, //結算
        HalfTime : 4, //中場
        Maintenance : 5, // 維修
        NewRoundRun : 6, // 新輪新局
        ComputingFinish : 7, //結算完畢
        EndBetTime : 8, //下注時間結束
        MiCard : 9, // 眯牌
        EndMiCardTime : 10, // 眯牌時間結束
        CancelRun : 11, // 取消此局
        OpenBankerCardBet:12,  //動態賠率 開莊
        OpenBankerCardBetEnd:13, //動態賠率 停止下注
        OpenPlayerCardBet:14, //動態賠率 開閒
        OpenPlayerCardBetEnd:15, //動態賠率 停止下注
    },
    tableStatus: {  // Bitwise
        Normal: 0,  // 一般
        Suspend: 1, // 維護
        Closed: 2,  // 關閉
    },
    betZoneType: {
        Standard: 1, //標準
        West: 2, //西洋
        NoWater: 3, //免水
    },
    BaccaratGameMode:{
        BaccaratSpeed:"BaccaratSpeed",
        BaccaratStandard:"BaccaratStandard",
        BaccaratMiCard:"BaccaratMiCard"
    },
    TableName : {
        Baccarat:"Baccarat",	// 百家樂
        Roulette:"Roulette",	// 輪盤
        DragonTiger:"DragonTiger",	// 	龍虎
        MultipleTable:"MultipleTable",	// 多桌
    },
    kickState:{
        default:0,
        kick:1, // 沒原因踢人
        noAuthority:2, //停用踢人
        relogin:3, // 重複登入踢人
    },
    miCardState: {
        // 應已收到需要咪的牌，並且確認是否需要補牌
        StartAndCheck : 1,      // 準備咪牌 : "必須"確認玩家是否有咪牌資格
        BaseCard : 2,           // 開始咪牌 
        OpenBaseCard : 3,       // 開牌    ： 第一階段咪牌結束 － 開牌
        PlayerAddCard : 4,      // 補牌    ： 閒補牌
        OpenPlayerAddCard : 5,  // 開牌    ： 閒補牌開牌
        BankerAddCard : 6,      // 補牌    ： 莊補牌
        OpenBankerAddCard : 7,  // 開牌    ： 莊補牌開牌
        EndAndReset: 0,         // 咪牌結束 : 關閉咪牌頁面 並且重置所有牌位置
    },
    videoErrorState: {
        VideoError : 1,
        VideoDownloadRate : 2,
    },
         
    platFormId: 2,
    apiBreakTime: 10000,
    deepLinkBreakTime: 2000,
    iosVideoDelayTime: 0,
    gameBreakTime: 5000,
    MultipleTableKickUserToastTime1: 180000,
    MultipleTableKickUserToastTime2: 240000,
    MultipleTableKickUserAlertTime: 300000,
    TOAST_SHOW_TIME_DEFAULT: 1.5, // 預設1.5秒

/* ============================================================================================= */
    Country: {
        CN: "cn",
        TW: "tw"
    },

    Output: {
        TEST: 'test',
        STAGE: 'stage',
        RELEASE: 'release'
    },

    /** 發佈平台 */
    Platform: {
        /** 一般(法老, 皇璽) */
        NORMAL: "NORMAL",
        /** 眾發 */
        JHONG_FA: "JHONG_FA"    
    },
    
    gameCountry: 'tw',
    /** 發佈平台 */
    gamePlatform: 'NORMAL',
    gameOutput: GAME_OUTPUT,    
    gameVersion: GAME_VERSION,
    assetVersion: ASSET_VERSION,

    getHostTW: function() {
        switch(this.gameOutput) {
            case this.Output.TEST: return HOST_TW_TEST
            case this.Output.STAGE: return HOST_TW_STAGE
            case this.Output.RELEASE: return HOST_TW_ONLINE
        }
    },

    getHostCN: function() {
        switch(this.gameOutput) {
            case this.Output.TEST: return HOST_CN_TEST
            case this.Output.STAGE: return HOST_CN_STAGE
            case this.Output.RELEASE: return HOST_CN_ONLINE
        }
    },

    getHost: function() {
        switch(this.gameCountry) {
            case this.Country.TW: return this.getHostTW()
            case this.Country.CN: return this.getHostCN()
        }
    },

    getSignalrURL: function() {
        return this.getHost() + HOST_PATH_SIGNAL_R
    },

    getSpotURL: function() {
        return this.getHost() + HOST_PATH_CASINO_GET_SPOT
    }, 
    
    getChipSetURL: function() {
        return this.getHost() + HOST_PATH_CASINO_GET_CHIP_SET
    },

    getTWPlatformIP: function() {
        switch(this.gameOutput) {
            case this.Output.TEST: return PLATFORM_IP_TW_TEST
            case this.Output.STAGE: return PLATFORM_IP_TW_STAGE
            case this.Output.RELEASE: return PLATFORM_IP_TW_ONLINE
        }
    },

    getCNPlatformIP: function() {
        switch(this.gameOutput) {
            case this.Output.TEST: return PLATFORM_IP_CN_TEST
            case this.Output.STAGE: return PLATFORM_IP_CN_STAGE
            case this.Output.RELEASE: return PLATFORM_IP_CN_ONLINE
        }
    },

    getPlatformIP: function() {
        switch(this.gameCountry) {
            case this.Country.TW: return this.getTWPlatformIP()
            case this.Country.CN: return this.getCNPlatformIP()
        }
    },
    
    getReportURL(dToken='') {
        return this.getPlatformIP() + API_PATH_REPORT + dToken
    },

    getJackpotRuleURL: function() {
        return this.getPlatformIP() + API_PATH_JACKPOT_RULE
    },

    getRuleURL(cashType='') {
        return this.getPlatformIP() + API_PATH_RULE + cashType
    },

    getLogRecordURL() {
        return this.getPlatformIP() + API_PATH_LOG_RECORD
    },

    getCustomerCreditURL: function() {
        switch(this.gameOutput) {
            case this.Output.TEST:
            case this.Output.STAGE: return CUSTOMER_TW_CREDIT_TEST;
            case this.Output.RELEASE: return CUSTOMER_TW_CREDIT;
        }
    },

    getCustomerCashURL: function() {
        switch(this.gameOutput) {
            case this.Output.TEST:
            case this.Output.STAGE: return CUSTOMER_TW_CASH_TEST;
            case this.Output.RELEASE: return CUSTOMER_TW_CASH;
        }
    },
    
    getCustomerURL: function(cashType) {
        switch(cashType) {
            case this.CashType.CREDIT: return this.getCustomerCreditURL() + this.getPlatformIP()
            case this.CashType.CASH: return this.getCustomerCashURL() + this.getPlatformIP()
        }
    },

    getVersionCheckNormalURL: function() {
        switch(this.gameOutput) {
            case this.Output.TEST: return VERSION_CHECK_NORMAL_TEST
            case this.Output.STAGE: return VERSION_CHECK_NORMAL_STAGE
            case this.Output.RELEASE: return VERSION_CHECK_NORMAL_ONLINE
        }
    },

    getVersionCheckJhongFaURL: function() {
        switch(this.gameOutput) {
            case this.Output.TEST: return VERSION_CHECK_JF_TEST
            case this.Output.STAGE: return VERSION_CHECK_JF_STAGE
            case this.Output.RELEASE: return VERSION_CHECK_JF_ONLINE
        }
    },

    getVersionCheckURL: function() {
        switch(this.gamePlatform) {
            case this.Platform.NORMAL: return this.getVersionCheckNormalURL();
            case this.Platform.JHONG_FA: return this.getVersionCheckJhongFaURL();
        }
        return this.getPlatformIP() + API_PATH_VERSION_CHECK
    },

    getServerStateURL: function() {
        return this.getPlatformIP() + API_PATH_MAINTAIN_CHECK
    },

    getLoginGameURL: function() {
        return this.getPlatformIP() + API_PATH_LOGIN
    },
    
    getDirectLoginGameURL: function() {
        return this.getPlatformIP() + API_PATH_DIRECT_LOGIN
    },

    getFakeLoginGameURL: function() {
        return this.getPlatformIP() + API_PATH_FAKE_LOGIN
    },

 /* ============================================================================================= */   
    requireToServerError:{
        timeOut:-10001,
        signalR:-10002,
    },
    betErrorId:{
        overBetMaxLimit:112,
        lowerBetMiniLimit:113, 
        rebetLowerBetMiniLimit:-20001,
        betIsMax:-20002,
        betOverCredit:30001,//重複下注超出玩家金額
        betOverLimit:30002,//超過補滿下注
    },
    HttpRequestStatus: {
        SUCCESS: 200
    },
    HttpRequestReadyState: {
        TIME_OUT: -1, // 連線逾時
        UNSENT: 0, // 客戶端已被建立，但 open() 方法尚未被呼叫。
        OPENED: 1, // open() 方法已被呼叫。
        HEADERS_RECEIVED: 2, // send() 方法已被呼叫，而且可取得 header 與狀態。
        LOADING: 3, // 回應資料下載中，此時 responseText 會擁有部分資料。
        DONE: 4 // 完成下載操作。
    },
    CheckGameVersionReturnCode: {
        CANT_PARSE_REQUEST: -2,
        PARAMETER_ERROE: -1,
        NEW_VERSION: 1,
        NO_NEW_VERSION: 2
    },
    LoginWebReturnCode: {
        CANT_PARSE_REQUEST: -2,
        PARAMETER_ERROE: -1,
        FAIL: 0,
        SUCCESS:1
    }, 

    saveName:{
        userName:"username",
        password:"password",
        isAutoLogin:"isAutoLogin",
        lastVersion:"lastVersion",
        origin:"origin",
        userToken:"userToken",
        videoEnable:"videoEnable",  // 視訊開關
        dToken:"dToken" // 用來當作串接版永久登入用的參數
    },
    saveAudio:{
        music:"playBgMusic",
        effect:"playEffect",
    },
    saveData:{
        isShowFirstBetingTypeHint:"isShowFirstBetingTypeHint", // 有沒有秀過 百家樂玩法(標準, 免水)教學
        isShowJackpotNewPlay: 'isShowJackpotNewPlay' // 有沒有秀過 Jackpot彩金登場 訊息
        , showJpBetHintTimes: 'showJpBetHintTimes' // 秀過 JP下注門檻提示 的次數
        , platformURL: 'platformURL' // 串接版返回平台的網址
        , isLoginGame: 'isLoginGame' // 登入過串接版了
        , appLanguage: 'appLanguage' // app 語言 
    }, 
    
    Spot: {
        Default: 'Default',
        Banker: 'Banker',
        Player: 'Player',
        Tie: 'Tie'
    },
    
    spotID:{
        Banker:"11001",
        Player:"11002",
        Big:"11003",
        Small:"11004",
        PlayerJQK:"11005",
        BankerJQK:"11006",
        PlayerOdd:"11007",
        PlayerEven:"11008",
        BankerOdd:"11009",
        BankerEven:"11010",
        PlayerBankerJQK:"11011",
        PlayerA:"11012",
        BankerA:"11013",
        Tie:"11014",
        PlayerPair:"11015",
        BankerPair:"11016",
        PlayerJQKA:"11017",
        BankerJQKA:"11018",
        PlayerBankerA:"11019",
        PlayerBankerJQKA:"11020",
        BankerNoWater:"11021",
        PlayerNoWater:"11022",
        TieNoWater:"11023",
        BankerPairNoWater:"11024",
        PlayerPairNoWater:"11025",
        Super6:"11026", 
    },

    // origin =======
    CompanyId:{
        Pharaoh:1,
        SignatureClub:100,
        Sands:5,
        UbenClub:101
    },

    customerId:{
        iwin168:1,
        iwinbet:2,
        iwin999:3,
    },

    companyIdToName: function(id) {
        let customerId = 0
        switch (Number(id)) {
            case Config.CompanyId.SignatureClub: 
                customerId = Config.customerId.iwinbet
                break;
            case Config.CompanyId.UbenClub:  
                customerId = Config.customerId.iwin999
                break;
            default: 
                customerId = Config.customerId.iwin168
            break;
        } 
        for(var key in Config.customerId){
            if (customerId == Number(Config.customerId[key])) return key
        } 
    },
    
    customerServiceFB:{
        iwin168:"1667047676851255",
        iwinbet:"583118081838441",
        iwin999:"382365508771795",
    },
    customerServiceQQ:{
        iwin168:"2080595885",
        iwinbet:"1608584476",
        iwin999:"2914291857",
    },
    customerServiceLine:{
        iwin168:"http://line.naver.jp/ti/p/MHnVgS6_UU",
        iwinbet:"http://line.naver.jp/ti/p/j-irCViiWz",
        iwin999:"http://line.me/ti/p/aqSNGLkRao",
    },
    ConnectState:{
        CONNECTING: 0,
        CONNECTED: 1,
        RECONNECTING: 2,
        DISCONNECTED: 3,
        ERROR:-1
    },

    get chips() { 
        switch(this.gameCountry) { 
            case this.Country.CN: 
                return [10,50,100,200,500,1000,2000,5000,10000,50000,100000] 
            case this.Country.TW: 
                return [50,100,200,500,1000,2000,5000,10000,50000,100000] 
        } 
    }, 

    get gameUseCoinArray() { 
        switch(this.gameCountry) { 
            case this.Country.CN: 
                return [50000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 10] 
            case this.Country.TW: 
                return [100000, 50000, 10000, 5000, 2000, 1000, 500, 200, 100, 50] 
        } 
    }, 

    GameCenterErrorCode: {
        OperationSuccess: 0,
        NotSufficientCredit: 104,
        ServerIsSuspend: 121
    },

    // 視頻提供商
    VideoProvider : {
        Demo: 1,            //測試機
        HotleAndCasion: 2,  //柬埔寨
        WaterCube: 3,        //水立方
        Vivo: 4,        //1xbet
        VivoBigTable: 5       //1xbet b 桌
    },

    // 場景名稱
    SceneName: {
        Login: "Login",
        Lobby: "Lobby",
        Game: "Game",
        MultipleGame: "MultipleGame"
    },
    
    SIGNAL_R_PROXY_LIST: [
        // Member Information Listeners
        "getUserCredit",                    // User
        "updateUserCredit",                 // User
        "restoreCredit",                    // User
        "kickUser",                         // User
        "getPersonalGameSetting",           // 未使用
        "getUserLimit",                     // 未使用
        "getUserLimitWeek",                 // 未使用
        "getUserLimitStake",                // Game
        "getSelfBettingList",               // BaccaratGame
        "idleRunNeverBet",                  // BaccaratGame
        "comingMaintain",                   // User
        'updateJackpotInfo',                // User
    
        // Game Information Listeners
        "initialCasinoTable",               // Tables
        "updateCasinoTable",                // Tables
        "roadMapMessageReceived",           // Tables
        "gameStatusUpdateMessageReceived",  // BaccaratGame, Tables
        "gameMiCardStartMessageReceived",   // Tables
        "gameMiCardDrawingMessageReceived", // Tables
        "updateOpeningTimes", // Tables
        "updateGameStatus", // Tables
        
        // After excute seat action
        "initialGambleTable",               // BaccaratGame, Tables
        "updateBettingPool",                // BaccaratGame
        "updateComputingResult",            // BaccaratGame
        "getBaccaratGameMode",              // BaccaratGame 
    
        //JP
        'updateJackpotPoolAmount', // User
        'updateJackpotInfo', // User
        
        // Promotion Information Listeners
        "updateAllMarquee"                  // 未使用
    ],

    SignalRCommand: {
        LOGIN: 'login',
        SET_PUSH_BETTING_POOL_SPOT_ID: 'setPushBettingPoolSpotId',
        SEAT: 'seat',
        LEAVE: 'leave',
        MULTIPLE_TABLE_SEAT: 'multipleTableSeat',
        MULTIPLE_TABLE_LEAVE: 'multipleTableLeave',
        UPDATE_USER_LIMIT_STAKE: 'updateUserLimitStake',
        UPDATE_MULTIPLE_TABLE_USER_LIMIT_STAKE:  'updateMultipleTableUserLimitStake',
        UPDATE_PERSONAL_CHIPS_SETTING: 'updatePersonalChipSetting',
        UPDATE_BACCARAT_GAME_MODE: 'updateBaccaratGameMode',
        BET: 'bet',
        MULTIP_BET: 'multipBet',
        REVOKE_ALL_BET: 'revokeAllBet',
        ACCEPT_JACKPOT_BIG_WIN: 'AcceptJackpotBigWin',
        LIVE_AUTH: 'LiveAuth',
        GET_OPENING_TIMES: "getOpeningTimes",
        GET_BACCARAT_TABLE_STATUS: "getBaccaratTableStatus",
    },

    JACKPOT_MAX_WIN: 20,
    JackpotKind: {
        NON_JACKPOT: 0,
        JACKPOT_BONUS: 1,
        JACKPOT_BIG_WIN: 2,
        JACKPOT_BIG_WIN_NOT_ARRIVE: 3,
        FINAL_JACKPOT_BIG_WIN: 4
    },
    JackpotBigWinResponse: {
        CONTINUE: 0, //繼續比倍
        NOT_CONTINUE_AND_GET_BONUS: 1 //不繼續比倍取回BigWin獎金
    },
    JACKPOT_ENABLE: JACKPOT_ENABLE,
    JACKPOT_ENABLE_BUILD: JACKPOT_ENABLE,
    Audio: {
        BG_MUSIC: 'resources/music/bgMusic.mp3',
        BUTTON_CLICK: 'resources/music/buttonClick.wav',
        START_BET: 'resources/music/startbet.wav',
        END_BET: 'resources/music/endbet.wav',
        HALF_TIME: 'resources/music/halfTime.wav',
        LAST_5: 'resources/music/last5.wav',
        DINDIN: 'resources/music/dindin.wav',
        PLAYER_WIN: 'resources/music/playerWin.wav',
        BANKER_WIN: 'resources/music/bankerWin.wav',
        TIE: 'resources/music/tie.wav',
        CLICK_CHIP: 'resources/music/clickChip.wav',
        WIN_MONEY: 'resources/music/winMoney.wav'
    },

    gameStatusEffect: function(gameStatus) {
        switch (gameStatus) {
            case this.gameState.NewRoundRun: return this.Audio.START_BET
            case this.gameState.EndBetTime: return this.Audio.END_BET
            case this.gameState.HalfTime: return this.Audio.HALF_TIME
            default: return ''
        } 
    },

    LAST_5_SEC: 5,

    countDownEffect: function(sec) {
        switch (sec) {
            case this.LAST_5_SEC: return this.Audio.LAST_5
            default: return this.Audio.DINDIN
        }
    },

    baccaratWhichWinEffect: function(which) {
        switch (which) {
            case this.Spot.Player: return this.Audio.PLAYER_WIN
            case this.Spot.Banker: return this.Audio.BANKER_WIN
            case this.Spot.Tie: return this.Audio.TIE
            default: return ''
        }
    },

    pointEffect: function(pokerPoint) {
        return 'resources/music/Point' + String(pokerPoint) + '.wav'
    },

    Prefab: {
        MUSIC_NODE: 'prefab/common/MusicNode'
    },
    
    IS_SHOW_LOBBY_UPDATE_MESSAGE: IS_SHOW_LOBBY_UPDATE_MESSAGE,

    // CashType 1:信用 2:現金
    CashType: {
        CREDIT: 1,  // 信用
        CASH: 2,    // 現金
    },

    AppEnv: {
        TW_LOCAL: 'tw_local',
        CN_LOCAL: 'cn_local',
        TW_STAGE: 'tw_stage',
        CN_STAGE: 'cn_stage',
        TW_ONLINE: 'tw_online',
        CN_ONLINE: 'cn_online',
    },
    
    isRightAppEnv: function(appEnv) {
        let isRight = false
        switch(appEnv) {
            case this.AppEnv.TW_LOCAL:
                if(Config.gameCountry == Config.Country.TW && Config.gameOutput == Config.Output.TEST) isRight = true
                break
            case this.AppEnv.CN_LOCAL:
                if(Config.gameCountry == Config.Country.CN && Config.gameOutput == Config.Output.TEST) isRight = true
                break
            case this.AppEnv.TW_STAGE:
                if(Config.gameCountry == Config.Country.TW && Config.gameOutput == Config.Output.STAGE) isRight = true
                break
            case this.AppEnv.CN_STAGE:
                if(Config.gameCountry == Config.Country.CN && Config.gameOutput == Config.Output.STAGE) isRight = true
                break
            case this.AppEnv.TW_ONLINE:
                if(Config.gameCountry == Config.Country.TW && Config.gameOutput == Config.Output.RELEASE) isRight = true
                break
            case this.AppEnv.CN_ONLINE:
                if(Config.gameCountry == Config.Country.CN && Config.gameOutput == Config.Output.RELEASE) isRight = true
                break
        }
        return isRight
    },

    // 登入錯誤訊息代碼
    LoginReturnCode: {
        POST_IS_NOT_ALLOWED: 2,
        QUERY_FAILED: -2,
        D_TOKEN_ERROR: -3,
        TOKEN_ERROR: -4
    },
    GlobalZ_Index:{
        ALERT:998,
        WEB_VIEW:1000,
        LOADING_VIEW:318,
        DAILY_UPDATE:999,
        MONEY_TOOL: 1001,
        NUMBER_KEYBOARD: 1002
    }

    , maxShowJpBetHintTimes: MAX_SHOW_JP_BET_HINT_TIMES

    , detectWebInternertTimeMS: 5000

    , MaintainCheck: {
        SERVER_UP: 0,
        SERVER_DOWN: -4
    }
}

module.exports = Config