const directions = {
  0: 'center',
  1: 'upleft',
  2: 'up',
  3: 'upright',
  4: 'right',
  5: 'downright',
  6: 'down',
  7: 'downleft',
  8: 'left'
}

export default {
  directions: directions,
  BANKER: 1,
  PLAYER: 2,
  TIE: 3
}
