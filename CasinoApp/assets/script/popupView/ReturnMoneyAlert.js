import Config from '../constants/Config'

// 訊號不穩-退錢提示視窗 (2018.03.24)
// 企劃書：https://goo.gl/pXJUw6

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // use this for initialization
    onLoad: function () {
        let acceptBtn = this.node.getChildByName('acceptBtn')
        acceptBtn.on('click', ()=> {
            this.node.removeFromParent(true)
            this.node.destroy()
        })
    },
    
});


var _prefab = null
var getPrefab = (cb = (prefab) => {}) => {
    if(_prefab != null) cb(_prefab)
    cc.loader.loadRes('prefab/common/ReturnMoneyAlert', (err, res) => cb(res))
}

var ReturnMoneyAlert = {

    show() {
        getPrefab((prefab) => {
            let newNode = cc.instantiate(prefab)
            cc.director.getScene().addChild(newNode, Config.GlobalZ_Index.ALERT)
        })
    }
}

module.exports = ReturnMoneyAlert
