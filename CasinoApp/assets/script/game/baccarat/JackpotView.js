
// JackpotView.js
import Config from '../../constants/Config'
import Localize from '../../constants/Localize'
import Tool from '../../tool/Tool'
import Casino from '../../model/Casino'
import JackpotAlert from '../../tool/JackpotAlert'
import JackpotWinView from '../../tool/JackpotWinView'

const BarColor = {
    YELLOW: '#E5B200',
    GREEN: '#0CAF2E',
    RED: '#F11616'
}
const YELLOW_WIN_COUNT_MAX = 10  //  0 ~ 10
const GREEN_WIN_COUNT_MIN = 11  // 11 ~ 14
const GREEN_WIN_COUNT_MAX = 14
const RED_WIN_COUNT = 15    // 15 ~ 20
const IS_CLOCKWISE = true // 順時針方向為true
const BAR_MOVE_IN_POS_X = 340 // bar展開時的x位置
const BAR_MOVE_OUT_POS_X = 0   // bar收合時的x位置
const SUB_BAR_MOVE_IN_POS_X = 460 // sub bar展開時的x位置
const SUB_BAR_MOVE_OUT_POS_X = 0   // sub bar收合時的x位置
const BAR_POS_Y = 0            // bar的y位置
const BAR_MOVE_TIME_SEC = 0.1  // bar的展開或收合的動作時間
const BAR_DIRECTION = (IS_CLOCKWISE == true) ? -1 : 1
const MONEY_SYMBOL = '$ '
const BAR_MOVE_IN_TIME_WHEN_SEAT_TABLE_SEC = 5

cc.Class({
    extends: cc.Component,

    properties: {
        jpBars: cc.Node,

        jpStateBar: cc.Node,
        jpFillY: cc.Sprite,
        jpFillG: cc.Sprite,
        jpFillR: cc.Sprite,
        winCountLabel: cc.Label,
        winMoneyLabel: cc.Label,

        jpStateSubBar: cc.Node,

        miniJpStateBar: cc.Node,
        miniJpFill: cc.Sprite,
        miniWinCountLabel: cc.Label,
        miniWinMoneyLabel: cc.Label,

        jpStateBarButton: cc.Node,
        acceptJackpotBigWinButton:require('IFBlinkButton'),
        miniAcceptJackpotBigWinButton:require('IFBlinkButton'),

        jpBarsOffset: cc.Node,
    },

    onDestroy: function() {
        this.jpStateBar.targetOff(this)
        this.subscribes.forEach((subscribe) => {
            subscribe.unsubscribe()
        })
    },

    onLoad: function () {
        this.node.active = Config.JACKPOT_ENABLE
        
        // 多台位置位移
        if(Casino.Game.tableName == Config.TableName.MultipleTable) {
            this.jpBarsOffset.setPositionY(95)
        }

        // 有入桌動作，JP要展開五秒再收回
        this.isMoveIn = true
        this.jpStateBar.x = BAR_MOVE_IN_POS_X
        this.jpStateSubBar.x = SUB_BAR_MOVE_IN_POS_X
        this.isTouchBar = false // 當用戶點擊關閉時，則為true，不會再收回動作
        this.scheduleOnce(()=> {
            if(this.isTouchBar) return
            this.moveBar()
        }, BAR_MOVE_IN_TIME_WHEN_SEAT_TABLE_SEC)

        this.isMoving = false
        this.jpStateBarButton.on(cc.Node.EventType.TOUCH_START, this.moveBar, this)
        this.acceptJackpotBigWinButton.addClickEvent(this.onAcceptJackpotBigWinButtonClick)
        this.miniAcceptJackpotBigWinButton.addClickEvent(this.onAcceptJackpotBigWinButtonClick)

        this.subscribes = []
        this.betSumList = []
        for(let index in Casino.Game.games) {
            let game = Casino.Game.games[index]
            // 訂閱 betSum 只有在沒有下注的時候可以按領取彩金按鈕
            let betSum$ = game.spotMgr.propChange$.betSum
            this.subscribes[this.subscribes.length] = betSum$.subscribe({
                next: betSum => {     
                    this.betSumList[index] = betSum
                    this.updateAcceptJackpotBigWinButton(Casino.User.jackpotWinInfo.kind)
                },  
            })
        }

        this.subscribes.push(Casino.User.propChange$.jpPoolAmount.subscribe({
            next: (jpPoolAmount) => {
                this.updateJpPoolAmount(jpPoolAmount)
            }
        }))

        this.subscribes.push(Casino.User.propChange$.jackpotWinInfo.subscribe({
            next: (jackpotWinInfo) => {
                this.updateContinueWin(jackpotWinInfo.continueWin)
                this.updateAcceptJackpotBigWinButton(jackpotWinInfo.kind)
                this.moveJpStateBarIn(jackpotWinInfo.kind)
            }
        }))
        
        // 監聽 User 的 videoEnabled 來選擇Jackpot顯示方式
        if(Casino.Game.tableName != Config.TableName.MultipleTable) return
        this.subscribes.push(Casino.User.propChange$.videoEnabled.subscribe({
            next: videoEnabled => {
                this.jpBars.opacity = ((videoEnabled == true) ? 255 : 0)
                this.miniJpStateBar.opacity = ((videoEnabled == false) ? 255 : 0)
            }
        }))

        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => { 
                this.miniAcceptJackpotBigWinButton.node.active = !videoEnabled
            }
        })
    },

    isBet: function() {
        for(let index in this.betSumList) {
            if(this.betSumList[index] > 0) {
                return true
            }
        }
        return false
    },

    updateAcceptJackpotBigWinButton: function(kind) {
        if(kind != Config.JackpotKind.JACKPOT_BIG_WIN) {
            this.acceptJackpotBigWinButton.interactable = false
            this.miniAcceptJackpotBigWinButton.interactable = false
            return
        }
        this.acceptJackpotBigWinButton.interactable = (this.isBet() == false)
        this.miniAcceptJackpotBigWinButton.interactable = (this.isBet() == false)
    },

    onAcceptJackpotBigWinButtonClick: function() {
        JackpotAlert.showJackpotBigWin((acceptBigWinResponse) => {
            Casino.User.acceptJackpotBigWin(acceptBigWinResponse, (cmd, resp, error)=>{
                if(resp == null) return
                if(acceptBigWinResponse != Config.JackpotBigWinResponse.NOT_CONTINUE_AND_GET_BONUS) return
                if(resp.ErrorCode == Config.GameCenterErrorCode.OperationSuccess) {
                    Casino.User.updateJackpotInfo(0, Config.JackpotKind.NON_JACKPOT, 0, 0, 0)
                    JackpotWinView.playAnimation(Math.floor(resp.WinAmount))
                }
            })
        })
    },

    moveJpStateBarIn: function(kind) {
        if(kind != Config.JackpotKind.JACKPOT_BIG_WIN) return
        if(this.isMoveIn == true) return
        this.moveBar()
    },

    moveBar: function() {
        this.isTouchBar = true

        if(this.isMoving) return
        this.isMoving = true

        let moveToSubX = (this.isMoveIn == false) ? SUB_BAR_MOVE_IN_POS_X : SUB_BAR_MOVE_OUT_POS_X
        let subAction = cc.moveTo(BAR_MOVE_TIME_SEC, cc.v2(moveToSubX, BAR_POS_Y))
        this.jpStateSubBar.runAction(subAction)
        
        let moveToX = (this.isMoveIn == false) ? BAR_MOVE_IN_POS_X : BAR_MOVE_OUT_POS_X
        let finished = cc.callFunc(() => {
            this.isMoveIn = !this.isMoveIn
            this.isMoving = false
        }, this);
        let action = cc.sequence(cc.moveTo(BAR_MOVE_TIME_SEC, cc.v2(moveToX, BAR_POS_Y)), finished)
        this.jpStateBar.runAction(action)
    },
    
    updateContinueWin: function(jackpotWinCount) {
        this.jpFillY.fillRange = BAR_DIRECTION*jackpotWinCount/Config.JACKPOT_MAX_WIN
        this.jpFillG.fillRange = BAR_DIRECTION*jackpotWinCount/Config.JACKPOT_MAX_WIN
        this.jpFillR.fillRange = BAR_DIRECTION*jackpotWinCount/Config.JACKPOT_MAX_WIN
        this.miniJpFill.fillRange = jackpotWinCount/Config.JACKPOT_MAX_WIN
        this.jpFillR.node.active = (jackpotWinCount >= RED_WIN_COUNT)
        this.jpFillG.node.active = (jackpotWinCount >= GREEN_WIN_COUNT_MIN && jackpotWinCount <= GREEN_WIN_COUNT_MAX)
        this.jpFillY.node.active = (jackpotWinCount <= YELLOW_WIN_COUNT_MAX)

        if(jackpotWinCount >= RED_WIN_COUNT) {
            this.winCountLabel.node.color = cc.hexToColor(BarColor.RED)
            this.miniWinCountLabel.node.color = cc.hexToColor(BarColor.RED)
            this.miniJpFill.node.color = cc.hexToColor(BarColor.RED)
        } else if(jackpotWinCount >= GREEN_WIN_COUNT_MIN) {
            this.winCountLabel.node.color = cc.hexToColor(BarColor.GREEN)
            this.miniWinCountLabel.node.color = cc.hexToColor(BarColor.GREEN)
            this.miniJpFill.node.color = cc.hexToColor(BarColor.GREEN)
        } else {
            this.winCountLabel.node.color = cc.hexToColor(BarColor.YELLOW)
            this.miniWinCountLabel.node.color = cc.hexToColor(BarColor.YELLOW)
            this.miniJpFill.node.color = cc.hexToColor(BarColor.YELLOW)
        }

        if(this.winCountLabel.string == String(jackpotWinCount)) return
        this.winCountLabel.string = String(jackpotWinCount)
        this.miniWinCountLabel.string = String(jackpotWinCount)
        if(jackpotWinCount == 0) return
        let actions = cc.sequence(
            cc.scaleTo(0.1, 1.2), 
            cc.delayTime(0.8), cc.scaleTo(0.1, 1)
        )
        this.winCountLabel.node.stopAllActions()
        this.winCountLabel.node.setScale(1)
        this.winCountLabel.node.runAction(actions)
    },

    updateJpPoolAmount: function(jpPoolAmount) {
        this.winMoneyLabel.string = Tool.getThousandsOfBits(jpPoolAmount)
        this.miniWinMoneyLabel.string = Tool.getThousandsOfBits(jpPoolAmount)
    }
});
