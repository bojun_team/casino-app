var Fallow = require("Fallow")
const DIRECTION_CENTER = 0
const DIRECTION_UP_LEFT = 1
const DIRECTION_UP = 2
const DIRECTION_UP_RIGHT = 3
const DIRECTION_RIGHT = 4
const DIRECTION_DOWN_RIGHT = 5
const DIRECTION_DOWN = 6
const DIRECTION_DOWN_LEFT = 7
const DIRECTION_LEFT = 8

const TOUCHCARD_NO_TOUCH = 0
const TOUCHCARD_FIRSR_TOUCH = 1
const TOUCHCARD_FIRSR_END = 2
const TOUCHCARD_SEC_TOUCH = 3
const TOUCHCARD_MOVE = 4

cc.Class({
    extends: cc.Component,

    properties: {
        backCard:cc.Sprite,
        frontCard:cc.Sprite,
        frontNode:cc.Node,
        parnetNode:cc.Node, 
        squareMaskNode:cc.Node,
        fingers:[Fallow],
        isOpen:false,
        isTouchOpen:0,
        touch:cc.Node
    },
    setActiveFingers:function(isActive){
        for(let key in this.fingers) {
            this.fingers[key].node.active = isActive
            this.fingers[key].target.active = isActive
        }
            
    },
    getFingers:function(direction){
        const left_up = 0
        const left_down = 1
        const right_up = 2
        const right_down = 3
        const up = 4
        const down = 5
        const left = 6
        const right = 7
        if (this.fingers[left_up].node.scaleX < 0)
            this.fingers[left_up].node.scaleX = -this.fingers[left_up].node.scaleX
        if (this.fingers[left_down].node.scaleX < 0)
            this.fingers[left_down].node.scaleX = -this.fingers[left_down].node.scaleX
        if (this.fingers[right_up].node.scaleX < 0)
            this.fingers[right_up].node.scaleX = -this.fingers[right_up].node.scaleX
        if (this.fingers[right_down].node.scaleX < 0)
            this.fingers[right_down].node.scaleX = -this.fingers[right_down].node.scaleX

        switch (direction) {
            case DIRECTION_UP_LEFT:
                return [this.fingers[left_down]]
            case DIRECTION_UP_RIGHT:
                return [this.fingers[right_down]]
            case DIRECTION_DOWN_LEFT:
                return [this.fingers[left_up]]
            case DIRECTION_DOWN_RIGHT:
                return [this.fingers[right_up]]
            case DIRECTION_UP:
                this.fingers[right_down].node.scaleX = -this.fingers[right_down].node.scaleX
                return [this.fingers[left_down], this.fingers[right_down]]
            case DIRECTION_DOWN:
                this.fingers[right_up].node.scaleX = -this.fingers[right_up].node.scaleX
                return [this.fingers[left_up], this.fingers[right_up]]
            case DIRECTION_LEFT:
                this.fingers[left_up].node.scaleX = -this.fingers[left_up].node.scaleX
                this.fingers[left_down].node.scaleX = -this.fingers[left_down].node.scaleX
                return [this.fingers[left_up], this.fingers[left_down]]
            case DIRECTION_RIGHT:
                return [this.fingers[right_up], this.fingers[right_down]] 
        }
    },
    onLoad: function () {
        this.height = this.backCard.node.height;
        this.width = this.backCard.node.width; 
        this.angleMove = this.height/2 + this.width/2
        this.isTouchOpen = TOUCHCARD_NO_TOUCH
        this.direction = DIRECTION_CENTER
        const conerGap = 15
        this.setActiveFingers(false)

        this.touchStartPos = null
        this.touch.on(cc.Node.EventType.TOUCH_START, function (event) {  
            if (this.isOpen) return
            // this.touchStartPos = event.getLocation()
            if( this.isTouchOpen == TOUCHCARD_NO_TOUCH) {
                this.isTouchOpen = TOUCHCARD_FIRSR_TOUCH
                this.touchStartPos = event.getLocation()
            } else if( this.isTouchOpen == TOUCHCARD_FIRSR_END) {
                this.isTouchOpen = TOUCHCARD_SEC_TOUCH
            }
            this.reBack()
            this.lastPos = this.node.convertToNodeSpace(new cc.v2(event.getLocationX(), event.getLocationY()))
            this.initFrontCard()
            
        }.bind(this));
        
        this.touch.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            if (this.isOpen) return
            if (Math.abs(this.touchStartPos.x - event.getLocationX()) < 2 || Math.abs(this.touchStartPos.y - event.getLocationY()) < 2) return
            this.isTouchOpen = TOUCHCARD_MOVE

            var location = this.node.convertToNodeSpace(new cc.v2(event.getLocationX(), event.getLocationY()))  
            var lastX = this.lastPos.x - location.x
            var lastY = this.lastPos.y - location.y  

            switch (this.direction){
                case DIRECTION_LEFT :
                case DIRECTION_UP_LEFT :
                case DIRECTION_DOWN_LEFT :
                    if (this.flipFactorCheck(lastX)) { 
                        this.flipPoker()
                        this.setActiveFingers(false)
                        return
                    } 
                    if (lastX > -conerGap) {
                        lastX = -conerGap
                    }
                    this.dragPoker(-lastX)
                    break

                case DIRECTION_RIGHT :
                case DIRECTION_UP_RIGHT :
                case DIRECTION_DOWN_RIGHT :
                    if (this.flipFactorCheck(lastX)) { 
                        this.flipPoker()
                        this.setActiveFingers(false)
                        return
                    } 
                    if (lastX < conerGap) {
                        lastX = conerGap
                    }
                    this.dragPoker(lastX)
                    break
                
                case DIRECTION_UP :
                    if (this.flipFactorCheck(lastY)) { 
                        this.flipPoker()
                        this.setActiveFingers(false)
                        return
                    } 
                    if (lastY < conerGap) {
                        lastY = conerGap
                    }
                    this.dragPoker(lastY)
                    break

                case DIRECTION_DOWN :
                    if (this.flipFactorCheck(lastY)) { 
                        this.flipPoker()
                        this.setActiveFingers(false)
                        
                        return
                    }
                    if (lastY > -conerGap) {
                        lastY = -conerGap
                    }
                    this.dragPoker(lastY)
                    break
            }
        }.bind(this));

        this.touch.on(cc.Node.EventType.TOUCH_END, function (event) {  
            if (this.isOpen) return

            if (this.isTouchOpen == TOUCHCARD_MOVE ) {
                var move = cc.moveTo(0.2, 0, 0)
                switch (this.direction){
                    case DIRECTION_UP_LEFT :
                    case DIRECTION_DOWN_LEFT :
                    case DIRECTION_UP_RIGHT :
                    case DIRECTION_DOWN_RIGHT :
                        this.parnetNode.runAction(move)
                        break

                    case DIRECTION_UP :
                    case DIRECTION_DOWN :
                    case DIRECTION_LEFT :
                    case DIRECTION_RIGHT :
                        this.squareMaskNode.runAction(move)
                    break
                }
                let closeFingerFun = cc.callFunc(function(){this.setActiveFingers(false)}, this)
                let seq = cc.sequence(cc.moveTo(0.2, 0, 0), closeFingerFun)
                this.frontCard.node.runAction(seq)
                this.backCard.node.runAction(cc.moveTo(0.2, 0, 0))
                this.isTouchOpen = TOUCHCARD_NO_TOUCH
            } else {
                if (this.isTouchOpen == TOUCHCARD_FIRSR_TOUCH) {
                    this.isTouchOpen = TOUCHCARD_FIRSR_END
                    this.backCard.node.setPosition(cc.v2(0, 0))
                    this.frontCard.node.setPosition(cc.v2(0, 0))
                    this.parnetNode.setPosition(cc.v2(0, 0))
                    this.squareMaskNode.setPosition(cc.v2(0, 0))
                    this.setActiveFingers(false)
                } else if (this.isTouchOpen == TOUCHCARD_SEC_TOUCH) {
                    this.flipPoker()
                    this.isTouchOpen = TOUCHCARD_NO_TOUCH
                    this.setActiveFingers(false)
                }
                
            }
        }.bind(this)); 
        
        this.touch.on(cc.Node.EventType.TOUCH_CANCEL, function (event) { 
            this.setActiveFingers(false)
            if (this.isOpen) return
            this.isTouchOpen = TOUCHCARD_NO_TOUCH
            this.lastPos = new cc.v2(0, 0)
            this.backCard.node.setPosition(this.lastPos)
            this.frontCard.node.setPosition(this.lastPos)
            this.parnetNode.setPosition(this.lastPos)
            this.squareMaskNode.setPosition(this.lastPos)
            
        }.bind(this));
    },
    onDestroy: function(){
         if (this.atlas != null)  cc.loader.releaseRes("image/poker");
    },
    setFrontCard: function(imgName,fun) {
        if (this.atlas == null) { 
            cc.loader.loadRes("image/poker", cc.SpriteAtlas, function (err, atlas) {
                this.atlas = atlas
                this.setFrontCard(imgName,fun)
            }.bind(this));
        } else {  
            this.frontCard.spriteFrame = this.atlas.getSpriteFrame(imgName.toLowerCase())
            this.funMiCardResultEvent = fun
        } 
    },
    miCardOver:function() {
        if (this.funMiCardResultEvent != null)
            this.funMiCardResultEvent()
    },
    getDirection:function(){
        const detectDown = this.height / 4
        const detectTop = this.height * 3 / 4
        const detectLeftRight = this.width / 2  
        if (this.lastPos.y < detectDown) return DIRECTION_DOWN
        else if (this.lastPos.y > detectTop) return DIRECTION_UP
        else{
            if (this.lastPos.x > detectLeftRight) return DIRECTION_RIGHT
            else return DIRECTION_LEFT
        } 
    }, 
    getFrontCardParameter:function(direction){
        const conerGap = 15 
        let pos = cc.v2(0, 0)
        let rotation = 0
        let gap = conerGap
        switch (direction) {
            case DIRECTION_LEFT:
                pos = cc.v2((0 - this.width) , 0) 
                break;
            case DIRECTION_RIGHT:
                pos = cc.v2(this.width , 0)  
                break;
            case DIRECTION_UP:
                pos = cc.v2(0 , this.height) 
                rotation = 180
                gap = conerGap
                break;
            case DIRECTION_DOWN:
                pos = cc.v2(0 , -this.height) 
                rotation = 180
                gap = -conerGap
                break; 
            default:
                return null
        }
        return {"pos":pos, "rotation":rotation, "gap":gap }
    }, 

    initFrontCard:function(){
        const touchGap = 10
        this.direction = this.getDirection() 
        let frontCardParameter = this.getFrontCardParameter(this.direction) 
        if (frontCardParameter == null) return
        this.frontNode.setPosition(frontCardParameter.pos)
        this.frontCard.node.rotation = frontCardParameter.rotation  
        
        this.dragPoker(frontCardParameter.gap)
        this.frontCard.node.active = true
        let fingers = this.getFingers(this.direction)
        this.setActiveFingers(false)
        for(let key in fingers){
            fingers[key].node.active = true
            fingers[key].target.active = true
        }
    },
    resetPocker: function(){
        this.stopAllPokerActions() 
        this.isOpen = false
        this.isTouchOpen = TOUCHCARD_NO_TOUCH
        this.backCard.node.active = true
        this.backCard.node.setPosition(0,0)
        this.frontCard.node.active = false
        this.squareMaskNode.setPosition(0,0)
        this.setActiveFingers(false)
    },
    reBack: function(){
        this.stopAllPokerActions() 

        this.backCard.node.setPosition(0,0)
        this.frontCard.node.setPosition(0,0)
        this.parnetNode.setPosition(0,0)
        this.squareMaskNode.setPosition(0,0)
    },
    dragPoker:function(d){
        switch (this.direction){
            case DIRECTION_UP_LEFT :
                this.backCard.node.setPosition(new cc.v2(-d, d * (this.height / this.width)))
                this.frontCard.node.setPosition(new cc.v2(d, -d * (this.height / this.width)))
                this.parnetNode.setPosition(new cc.v2(d, -d * (this.height / this.width)))
                break

            case DIRECTION_DOWN_LEFT :
                this.backCard.node.setPosition(new cc.v2(-d, -d * (this.height / this.width)))
                this.frontCard.node.setPosition(new cc.v2(d, d * (this.height / this.width)))
                this.parnetNode.setPosition(new cc.v2(d, d * (this.height / this.width)))
                break

            case DIRECTION_UP_RIGHT :
                this.backCard.node.setPosition(new cc.v2(d, d * (this.height / this.width)))
                this.frontCard.node.setPosition(new cc.v2(-d, -d * (this.height / this.width)))
                this.parnetNode.setPosition(new cc.v2(-d, -d * (this.height / this.width)))
                break

            case DIRECTION_DOWN_RIGHT :
                this.backCard.node.setPosition(new cc.v2(d, -d * (this.height / this.width)))
                this.frontCard.node.setPosition(new cc.v2(-d, d * (this.height / this.width)))
                this.parnetNode.setPosition(new cc.v2(-d, d * (this.height / this.width)))
                break

            case DIRECTION_RIGHT :
                this.backCard.node.setPosition(new cc.v2(d, 0))
                this.squareMaskNode.setPosition(new cc.v2(-d, 0))
                this.frontCard.node.setPosition(new cc.v2(-d,0))
                break

            case DIRECTION_LEFT :
                this.backCard.node.setPosition(new cc.v2(-d, 0))
                this.squareMaskNode.setPosition(new cc.v2(d, 0))
                this.frontCard.node.setPosition(new cc.v2(d, 0))
                break
            
            case DIRECTION_UP :
                this.backCard.node.setPosition(new cc.v2(0, d ))
                this.squareMaskNode.setPosition(new cc.v2(0, -d ))
                this.frontCard.node.setPosition(new cc.v2(0, -d ))
                break

            case DIRECTION_DOWN :
                this.backCard.node.setPosition(new cc.v2(0, d ))
                this.squareMaskNode.setPosition(new cc.v2(0, -d ))
                this.frontCard.node.setPosition(new cc.v2(0, -d ))
                break
        }
    },
    flipFactorCheck:function(d){
        switch (this.direction){ 
            case DIRECTION_LEFT :
                if (-d > (this.width/2)) return true
                break 
            case DIRECTION_RIGHT :
                if (d > (this.width/2)) return true
                break
                
            case DIRECTION_UP :
                if (d > (this.height/2)) return true
                break

            case DIRECTION_DOWN :
                if (-d > (this.height/2)) return true
                break
        }
        return false
    },
    flipPoker:function(){ 
        this.isOpen = true
        this.frontCard.node.rotation = 0
        var move = cc.moveTo(0.2, 0, 0)
        switch (this.direction){
            case DIRECTION_UP_LEFT :
                var d = (this.width / 2)
                this.frontNode.setPosition(-this.width , this.height)
                this.frontCard.node.setPosition(new cc.v2(d, -d * (this.height / this.width)))
                this.frontCard.node.runAction(cc.moveTo(0.2, this.width, -this.height))
                this.parnetNode.setPosition(new cc.v2(d, -d * (this.height / this.width)))
                this.parnetNode.runAction(move)
                break
            case DIRECTION_DOWN_LEFT :
                var d = (this.width / 2)
                this.frontNode.setPosition(-this.width , -this.height)
                this.frontCard.node.setPosition(new cc.v2(d, d * (this.height / this.width)))
                this.frontCard.node.runAction(cc.moveTo(0.2, this.width, this.height))
                this.parnetNode.setPosition(new cc.v2(d, d * (this.height / this.width)))
                this.parnetNode.runAction(move)
                break
            case DIRECTION_UP_RIGHT :
                var d = (this.width / 2)
                this.frontNode.setPosition(this.width , this.height)
                this.frontCard.node.setPosition(new cc.v2(-d, -d * (this.height / this.width)))
                this.frontCard.node.runAction(cc.moveTo(0.2, -this.width, -this.height))
                this.parnetNode.setPosition(new cc.v2(-d, -d * (this.height / this.width)))
                this.parnetNode.runAction(move)
                break
            case DIRECTION_DOWN_RIGHT :
                var d = (this.width / 2)
                this.frontNode.setPosition(this.width , -this.height)
                this.frontCard.node.setPosition(new cc.v2(-d, d * (this.height / this.width)))
                this.frontCard.node.runAction(cc.moveTo(0.2, -this.width, this.height))
                this.parnetNode.setPosition(new cc.v2(-d, d * (this.height / this.width)))
                this.parnetNode.runAction(move)
                break
            case DIRECTION_RIGHT :
                var d = (this.width / 2)
                this.frontNode.setPosition(this.width , 0)
                this.frontCard.node.setPosition(new cc.v2(-d, 0))
                this.frontCard.node.runAction(cc.moveTo(0.2, -this.width, 0))
                this.squareMaskNode.setPosition(new cc.v2(-d, 0))
                this.squareMaskNode.runAction(move)
                break
            case DIRECTION_LEFT :
                var d = (this.width / 2)
                this.frontNode.setPosition(-this.width , 0)
                this.frontCard.node.setPosition(new cc.v2(d, 0))
                this.frontCard.node.runAction(cc.moveTo(0.2, this.width, 0))
                this.squareMaskNode.setPosition(new cc.v2(d, 0))
                this.squareMaskNode.runAction(move)
                break
            case DIRECTION_UP :
                var d = (this.height / 2)
                this.frontNode.setPosition(0 , this.height)
                this.frontCard.node.setPosition(new cc.v2(0, -d))
                this.frontCard.node.runAction(cc.moveTo(0.2, 0, -this.height))
                this.squareMaskNode.setPosition(new cc.v2(0, -d))
                this.squareMaskNode.runAction(move)
                break
            case DIRECTION_DOWN :
                var d = (this.height / 2)
                this.frontNode.setPosition(0 , -this.height)
                this.frontCard.node.setPosition(new cc.v2(0, d))
                this.frontCard.node.runAction(cc.moveTo(0.2, 0, this.height))
                this.squareMaskNode.setPosition(new cc.v2(0, d))
                this.squareMaskNode.runAction(move)
                break            
        }
        this.backCard.node.active = false
        setTimeout(() => {
            this.miCardOver()
            this.setActiveFingers(false)  
        }, 400)
    },
    stopAllPokerActions:function(){
        this.parnetNode.stopAllActions()
        this.squareMaskNode.stopAllActions()
        this.frontCard.node.stopAllActions()
        this.backCard.node.stopAllActions()
    },
    timeUpOpenPoker:function(){
        if (!this.isOpen) {
            this.stopAllPokerActions() 
            this.isOpen = true
            this.parnetNode.setPosition(0, 0)
            this.frontNode.setPosition(0, 0)
            this.squareMaskNode.setPosition(0, 0)
            this.frontCard.node.rotation = 0
            this.frontCard.node.setPosition( 0, 0)
            this.frontCard.node.active = true
            this.backCard.node.active = false
            this.setActiveFingers(false)
        }
    },

});