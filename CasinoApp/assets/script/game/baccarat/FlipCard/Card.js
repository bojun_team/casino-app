var FlipCard = require("FlipCard")
cc.Class({
    extends: cc.Component,
    properties: {
        selectBtn:cc.Button,
        poker:FlipCard,
        forcePos:cc.Vec2,
        forceScale:3,
    },
    onLoad: function () { 
        this.selectBtn.node.on('click', this.onSelect.bind(this), this);  
    },
    setCardConvert:function(cardForceWorldPos){
        this.forcePos = this.node.convertToNodeSpaceAR(cardForceWorldPos)
    },
    setCardIndex:function(index){
        this.cardIndex = index
    },
    setForceRotate:function(rotate){
        this.forceRotate = rotate
    },
    
    getIsHaveData:function(){
        return this.isHaveData
    },
    resetCard:function(){ 
        if (this.isForce) this.setForce(false, false)
        this.isHaveData = false
        this.isForce = false
        this.isLock = false
        this.poker.resetPocker() 
    },
    setCard:function(cardName){ 
        this.isHaveData = true
        this.poker.setFrontCard(cardName, this.onFinishMiCard.bind(this))
    },
    setLock:function(isLock){
        this.isLock = isLock
    },
    getLock:function(isLock){
        return this.isLock
    },
    openCard:function(){
        this.poker.timeUpOpenPoker()
        this.isLock = true
    },
    listenSelect:function(selectFun){
        this.selectEvent = selectFun  
    },
    onSelect:function(){    
        if (this.isLock) return
        if(!this.isHaveData) return
        if (this.selectEvent != null) this.selectEvent(this.cardIndex)
        this.setForce(true)
    },
    listenMiCardFinish:function(miCardFinishEvent){
        this.miCardFinishEvent = miCardFinishEvent  
    },
    onFinishMiCard:function(){
        this.isLock = true  
        if (this.miCardFinishEvent != null) this.miCardFinishEvent(this.cardIndex)
    },
    setCanSelect:function(isCanSelect){
        this.isCanSelect = isCanSelect
    },
    setForce:function(isForce, isAnimate){ 
        if (isAnimate == null) isAnimate = true
        this.isForce = isForce
        const duration = 0.5
        this.poker.node.stopAllActions()
        let pos = new cc.Vec2(0, 0)
        let scale = 1 
        let rotate = 0
        if (isForce){ 
            pos = this.forcePos
            scale = this.forceScale
            rotate = this.forceRotate
        }
        if (isAnimate){
            let move = cc.moveTo(duration, pos)
            let scaleAct = cc.scaleTo(duration, scale, scale)
            let rotateAct = cc.rotateTo(duration, rotate)
            var animation = cc.spawn(move, scaleAct, rotateAct)
            this.poker.node.runAction(animation)
        }
        else{
            this.poker.node.setScale(cc.v2(scale, scale))
            this.poker.node.setPosition(pos)
            this.poker.node.rotation = rotate
        }
        this.selectBtn.interactable = !this.isForce
        this.selectBtn.node.active = !this.isForce
    }
});
