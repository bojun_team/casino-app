
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        button:cc.Button,
        choseSprite:cc.Sprite,
        buttonSprite:cc.Sprite,
        chipsAtlas:cc.SpriteAtlas,
    },

    // use this for initialization
    onLoad: function () {
        this.button.node.on("click", 
            function(){
                Casino.Game.useChipIndex = this.index
            }.bind(this)
            , this)

            //------訂閱
        this.subscribes = []
        //------訂閱 currentLimitStakeIndex
        let useChipIndex$ = Casino.Game.chipMgr.propChange$.useChipIndex
        this.subscribes[this.subscribes.length] = useChipIndex$.subscribe({
            next: index => {     
                this.setChangeSprite()  
            },  
        });   


    },

    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    },

    setSprite:function(value){
        var buttonSpriteF = "chip_" + String(value)
        var choseSpriteF = "stand_chip_" + String(value)
        this.choseSprite.spriteFrame = this.chipsAtlas.getSpriteFrame(choseSpriteF);
        this.buttonSprite.spriteFrame = this.chipsAtlas.getSpriteFrame(buttonSpriteF);
    },

    setChose:function(isChose){
        
        if (isChose){
            this.choseSprite.node.active = true
            this.buttonSprite.node.active = false
        }else{
            this.choseSprite.node.active = false
            this.buttonSprite.node.active = true
        }
    },

    setIndex:function(index){
        this.index = index
    },

    setChangeSprite:function(){
        this.setChose(false)
        if (Casino.Game.getUseChipIndex() == this.index) this.setChose(true)
    },

    setDisButton:function(type){
        this.button.interactable = type
    },


});
