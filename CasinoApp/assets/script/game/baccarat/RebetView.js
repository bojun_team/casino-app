/*
    功能描述：送出下注事件
    實作內容：
        1 把下注事件送出去
        2 訂閱 tableInfo 的 State 判斷是否要給玩家按。
        3 訂閱 spotMgr 的 canRebet 判斷是否要給玩家按。
*/
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        rebetBtn:{
            default:null,
            displayName:"[單台]rebetBtn",
            type:cc.Button,
        },
        reBetIFBtn:{
            default:null,
            displayName:"[多台]reBet_btn",
            type:require("IFButton"),
        },
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },
    onLoad: function () {
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
        } else {
            // 多台時rebetBtn相等於reBetIFBtn
            this.rebetBtn = this.reBetIFBtn
        }
    },
    setTableData: function(tableName, tableID) {
        this.tableName = tableName
        this.tableID = tableID

        this.subscribes = []
        // 訂閱 tableInfo 的 State 判斷是否要給玩家按。
        let tableInfo = Casino.Tables.getTableInfo(tableName, tableID)
        let gameStatus$ = tableInfo.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                let spotMgr = Casino.Game.getSpotMgr(this.tableName, this.tableID)  
                let canRebet = spotMgr.canRebet
                this.rebetBtn.interactable = (gameStatus == Config.gameState.CountDown && canRebet)
            },  
        });
        // 訂閱 spotMgr 的 canRebet 判斷是否要給玩家按。
        let spotMgr = Casino.Game.getSpotMgr(tableName, tableID)
        let canRebet$ = spotMgr.propChange$.canRebet
        this.subscribes[this.subscribes.length] = canRebet$.subscribe({
            next: canRebet => {
                let tableInfo = Casino.Tables.getTableInfo(this.tableName, this.tableID)
                let gameStatus = tableInfo.status
                this.rebetBtn.interactable = (canRebet && gameStatus == Config.gameState.CountDown)
            },  
        });
        // 設定IFButton點擊事件
        if(this.reBetIFBtn == null) return
        this.reBetIFBtn.addClickEvent(function(){
            this.onTouchRebet()
        }.bind(this))
    }, 
    onTouchRebet: function(){
        // 把下注事件送出去, 然後先不給玩家按, 等伺服器回覆之後, 根據tableInfo 的 State 判斷是否要給玩家按
        this.rebetBtn.interactable = false
        Casino.Game.rebet(this.tableName, this.tableID, function(resp){
            let tableInfo = Casino.Tables.getTableInfo(this.tableName, this.tableID)
            let gameStatus = tableInfo.status
            if (gameStatus == Config.gameState.CountDown) this.rebetBtn.interactable = true
            if (resp == Config.requireToServerError.timeOut){
                let titleDataID = Casino.Localize.AlertTitleWarning
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, null)
                return
            }
            if (resp == 0) return
            let errorCode = resp
            let dataID = Casino.Localize.betFailMsg(errorCode)
            Casino.Tool.showToast(dataID, Config.TOAST_SHOW_TIME_DEFAULT, [errorCode])
        }.bind(this))
    },
});
