/*
    切換注區
    功能描述：
        1 切換西洋、免水、標準注區
*/
import Config from '../../../constants/Config'
import Game from '../../../model/Game'
import Casino from '../../../model/Casino'

cc.Class({
    extends: cc.Component, 
    properties: {
        standard:cc.Node,
        noWater:cc.Node,
        west:cc.Node,
        standardIFBtn:{
            default:null,
            displayName:"[多台]standard_btn",
            type:require("MultipleBetTypeView"),
        },
        noWaterIFBtn:{
            default:null,
            displayName:"[多台]noWater_btn",
            type:require("MultipleBetTypeView"),
        },
        cancelBetIFBtn:{
            default:null,
            displayName:"[多台]cancelBet_btn",
            type:require("CancelBetView"),
        },
        reBetIFBtn:{
            default:null,
            displayName:"[多台]reBet_btn",
            type:require("RebetView"),
        },
    },
    onDestroy: function(){
        this.subscribes.forEach((subscribe) => {
            subscribe.unsubscribe()
        })
    }, 
    onLoad: function () {
        // 只有非多台才會跑 onLoad 
        if(Casino.Game.tableName == Config.TableName.MultipleTable) return
        this.setTableData(Casino.Game.tableName, Casino.Game.tableID)        
    },
    setTableData: function(tableName, tableID) {
        this.subscribes = [] 
        
        // 訂閱--- 注區玩法類型，並做切換。 
        let betZoneType$ = Casino.Game.getGame(tableName, tableID).propChange$.betZoneType
        this.subscribes[this.subscribes.length] = betZoneType$.subscribe({ 
            next: betZoneType => { 
                this.setBetZone(betZoneType)
            } 
        }); 
    
        // 訂閱 --- gameStatus 單桌百家樂中場時關閉注區, 新輪新局時開啟注區
        this.subscribeGameStatusToControlBetZone()
         
        // 設定多台 tableName, tableID
        this.setMultipleTableData(tableName, tableID)
    },
    subscribeGameStatusToControlBetZone: function() {
        let game = Casino.Game
        if(game.tableName != Config.TableName.Baccarat) return
        if(!Game.isWaterCube) return
 
        let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
        this.subscribes.push(table.propChange$.gameStatus.subscribe({
            next: gameStatus => {
                this.updateBetZoneByGameStatus(gameStatus)
            } 
        }))
    }, 
    updateBetZoneByGameStatus: function(gameStatus) {
        let game = Casino.Game.singleGame
        switch(Number(gameStatus)) {
            case Config.gameState.HalfTime:
                this.closeBetZone()
                break
            case Config.gameState.NewRoundRun:
                this.setBetZone(game.betZoneType)
                break
        }
    }, 
    closeBetZone: function() {
        this.standard.active = false
        this.noWater.active = false
        this.west.active = false
    },   
    setBetZone: function(betZoneType) {
        switch(Number(betZoneType)) {
            case Config.betZoneType.Standard:
                this.standard.active = true
                this.noWater.active = false
                this.west.active = false
                break
            case Config.betZoneType.NoWater:
                this.standard.active = false
                this.noWater.active = true
                this.west.active = false
                break
            case Config.betZoneType.West:
                this.standard.active = false
                this.noWater.active = false
                this.west.active = true
        }
    },    
    setMultipleTableData: function(tableName, tableID) {
        if(Casino.Game.tableName != Config.TableName.MultipleTable) return 
        
        this.standard.getComponent("StandardBetZone").setTableData(tableName, tableID) 
        this.noWater.getComponent("NoWaterBetZone").setTableData(tableName, tableID) 
        this.standardIFBtn.setTableData(tableName, tableID) 
        this.noWaterIFBtn.setTableData(tableName, tableID) 
        this.cancelBetIFBtn.setTableData(tableName, tableID) 
        this.reBetIFBtn.setTableData(tableName, tableID) 
    }
});
