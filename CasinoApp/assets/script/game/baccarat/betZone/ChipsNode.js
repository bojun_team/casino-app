/*
    顯示目前的籌碼數
    功能列表：
        1 數字
        2 目前堆的籌碼

    todo 太多魔數數字，請修正
*/
import Tool from "../../../tool/Tool"
import Config from "../../../constants/Config"

cc.Class({
    extends: cc.Component,

    properties: {
        mainNode:cc.Node,
        chipsNode:cc.Node,
        chipsLabBG:cc.Sprite,
        labChips:cc.Label,
    },

    // use this for initialization
    onLoad: function () {
    },

    setCountChips: function(data){
        if (0 == data) {
            this.mainNode.active = false
            return
        }
        this.mainNode.active = true
        this.gameType = "chip_"
        this.gameUseCoinArray = Config.gameUseCoinArray //使用的籌碼定義 //使用的籌碼定義
        this.labChips.string = Tool.conversionNumber(data)
        var moveMainNodeByTwoStack = -35 //2疊籌碼位移中心點 
        var moveMainNodeByThreeStack = -70 //3疊籌碼位移中心點 
        var coinGapY = 10 //籌碼上下層的y軸間距
        var coinGapX = 70 //籌碼疊與疊之間的x軸間距
        var delaySec = 0.05 //疊籌碼動畫延遲時間
        var nodeScale = 0.37 //整個籌碼node
        const coninStackAmount = 4 //一疊籌碼最多排放的數量
        const rowAmount = 3 //最多幾疊籌碼
        // 每次更新資料先移除所有籌碼
        this.chipsNode.removeAllChildren()
        var total = 0
        for(let i = 0; i < this.gameUseCoinArray.length; i++){
            //記錄這次要新增幾個籌碼
            var number = 0
            number = Math.floor(data / this.gameUseCoinArray[i])
            for(let b = 0; b < number; b ++ ){
                if (total + 1 > coninStackAmount * rowAmount ) break // 超過12個籌碼就不再疊加
                total = total + 1
                //新增籌碼
                var node = new cc.Node("sprite" + String(total))
                node.opacity = 0 //先隱藏
                var sprite = node.addComponent(cc.Sprite)
                //看要放的圖片名稱為
                Tool.setSpritFrame(sprite, "image/chips_and_limit", this.gameType + String(this.gameUseCoinArray[i]))
                node.scale = nodeScale
                this.chipsNode.addChild(node)
                //籌碼位置
                var indexY = 0
                if (0 == total % coninStackAmount ){
                    indexY = coninStackAmount
                }else{
                    indexY = total % coninStackAmount
                }
                if (coninStackAmount + 1 > total){ //第二疊籌碼 第5顆籌碼
                    node.x = 0
                    this.chipsNode.x = 0
                }else if (coninStackAmount * 2 + 1  > total){ //第三疊籌碼 第9顆籌碼
                    node.x = coinGapX
                    this.chipsNode.x = moveMainNodeByTwoStack
                }else{
                    node.x = coinGapX * 2
                    this.chipsNode.x = moveMainNodeByThreeStack
                }
                node.y = (indexY-1) * coinGapY
                var showChipsNode = function(thisNode){
                    thisNode.opacity = 255
                }
                var fun = cc.callFunc(showChipsNode, this)
                var delayTime = cc.delayTime(delaySec * total)
                var seq = cc.sequence(delayTime,fun)
                node.runAction(seq)
            }
            data = data -  (number * this.gameUseCoinArray[i])
        }
        return total
    } 
});
