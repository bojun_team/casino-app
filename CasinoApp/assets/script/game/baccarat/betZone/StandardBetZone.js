/*
    標準注區控制

    繼承：
        BetZoneView

    功能列表：
        1 顯示前每個注區下了多少籌碼
        2 取得結果，做注區的閃爍
        3 設定注區的狀態(可下注、不可下注，停止下注)
*/ 
import Config from '../../../constants/Config'
import Casino from '../../../model/Casino'

cc.Class({
    extends: require("BetZoneView"),

    properties: {
        
        pageViewCtr: {
            default:null,
            type:require("PageViewCtr"),
            tooltip:"百家樂單桌使用"
        },
        percentLayout:{
          default:null,
          type:require("IFPercentLayout"),
          tooltip:"多台使用"
        },
    },

    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },
    
    onLoad: function () {
        const playerID = Config.spotID.Player
        const bankerID = Config.spotID.Banker
        const playerPairID = Config.spotID.PlayerPair
        const bankerPairID = Config.spotID.BankerPair
        const tieID = Config.spotID.Tie
        const smallID = Config.spotID.Small
        const playerOddID = Config.spotID.PlayerOdd
        const bankerOddID = Config.spotID.BankerOdd
        const playerEvenID = Config.spotID.PlayerEven
        const bigID = Config.spotID.Big
        const bankerEvenID = Config.spotID.BankerEven
        
        //-----------設定注區ID 及 下注事件
        this.betZonesDict = {}
        for (var item in this.betZones){  
            this.betZones[item].setBetEvent(this.onBet.bind(this))
            // Page 1
            if (this.betZones[item].betZoneName == "player"){
                this.betZones[item].setId(playerID) 
            }
            else if (this.betZones[item].betZoneName == "banker"){
                this.betZones[item].setId(bankerID) 
            }
            else if (this.betZones[item].betZoneName == "bankerPair"){
                this.betZones[item].setId(bankerPairID)
            }
            else if (this.betZones[item].betZoneName == "playerPair"){
                this.betZones[item].setId(playerPairID)
            }
            else if (this.betZones[item].betZoneName == "tie"){ 
                this.betZones[item].setId(tieID) 
            }
            // Page 2
            else if (this.betZones[item].betZoneName == "small"){
                this.betZones[item].setId(smallID) 
            }
            else if (this.betZones[item].betZoneName == "playerOdd"){
                this.betZones[item].setId(playerOddID) 
            }
            else if (this.betZones[item].betZoneName == "bankerOdd"){
                this.betZones[item].setId(bankerOddID) 
            }
            else if (this.betZones[item].betZoneName == "playerEven"){
                this.betZones[item].setId(playerEvenID) 
            }
            else if (this.betZones[item].betZoneName == "big"){
                this.betZones[item].setId(bigID) 
            }
            else if (this.betZones[item].betZoneName == "bankerEven"){
                this.betZones[item].setId(bankerEvenID) 
            }
            this.betZonesDict[this.betZones[item].getId()] = this.betZones[item]
        } 

        this._super()
    },
    subscribe: function () {
        this.subscribes = []
        
    },
    setTableData: function(tableName, tableID) {
        this._super(tableName, tableID)

        let table = Casino.Tables.getTableInfo(tableName, tableID)
        // 訂閱局號，用來判斷是否超過30局
        let runNumber$ = table.propChange$.run
        this.subscribes[this.subscribes.length] = runNumber$.subscribe({
            next: runNumber => {     
                // 如果未超過30局時，開啟大小單雙注區
                let isEnable = runNumber <= 30
                this.setBigAndSmallBetZoneEnable(isEnable)
            },  
        });
    },
    setBigAndSmallBetZoneEnable: function(isEnable) {
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            if(this.pageViewCtr) {
                this.pageViewCtr.setPageViewEnable(isEnable); // 單台
            }
        } else {
            // 多台
            if(isEnable) {
                this.percentLayout.itemPercentList = [15.27, 15.27, 15.27, 13.55, 13.55, 13.55, 13.55]
            } else {
                this.percentLayout.itemPercentList = [21.03, 21.03, 21.03, 18.455, 0, 0, 18.455]
            }
            this.percentLayout.sortChildren()
        }
    },
});
