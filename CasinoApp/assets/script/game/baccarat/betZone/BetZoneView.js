/*
    基本注區 主要用於被繼承 
    功能列表：
    1 顯示前每個注區下了多少籌碼
    2 取得結果，做注區的閃爍
    3 設定注區的狀態(可下注、不可下注，停止下注)
*/ 
import Config from '../../../constants/Config'
import Casino from '../../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        betZones:[require("BetZoneItem")]
    },

    // use this for initialization
    onLoad: function () {
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
        }
    }, 
    setTableData: function(tableName, tableID) {
        this.tableName = tableName
        this.tableID = tableID

        this.gameState = Config.gameState.Computing
        if(this.subscribes == null) {
            this.subscribes = []
        }
        let game = Casino.Game.getGame(this.tableName, this.tableID)
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        //------訂閱 目前每個注區下了多少籌碼
        for(let key in this.betZonesDict){
            let spotID = String(key)
            let betingPool = game.spotMgr.getSpot(spotID)
            if(betingPool == null) continue
            let chipAmount$ = betingPool.propChange$.chipAmount
            this.subscribes[this.subscribes.length] = chipAmount$.subscribe({
                next: chipAmount => { //chipAmount 目前下了多少錢
                    this.betZonesDict[spotID].setChips(chipAmount) 
                },  
            });  
        }
        
        //------訂閱 開牌結果，取得結果，做注區的閃爍
        let result = game.result
        let hitSpots$ = game.result.propChange$.hitSpots
        this.subscribes[this.subscribes.length] = hitSpots$.subscribe({
            next: hitSpots => {
                try {
                    for (let key in hitSpots) {
                        if (this.betZonesDict[hitSpots[key]] == null) continue
                        this.betZonesDict[hitSpots[key]].startBlink()
                    }    
                } catch (error) {
                    let infoList = []
                    for(let table of Casino.Tables._baccaratInfos) {
                        let info = {
                            tableID:table.tableID,
                            round:table.round,
                            run:table.run
                        }
                        infoList.push(info);
                    }
                    let message = {
                        error: error,
                        hitSpots: hitSpots,
                        username: Casino.User.username
                    }
                    Casino.AppWebAPI.logRecord(JSON.stringify(infoList), JSON.stringify(message));
                } 
            },  
        }); 
        //------訂閱 遊戲狀態 設定注區的狀態(可下注、不可下注，停止下注)
        this.setBetZoneState(table.status)
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {  //遊戲狀態
                this.setBetZoneState(gameStatus)
            },  
        });
    },
    
    onDestroy: function(){
        for (let key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    }, 
    // 送出下注
    // betZoneID:注區ID
    onBet:function(betZoneID){
        if (this.gameState != Config.gameState.CountDown) return
        const credit = Casino.User.usercredit
        // 取得目前選擇的籌碼
        const choseChip = Casino.Game.choseChip
        // 錢不夠的處理
        if (credit < choseChip) {
            let errorCode = Config.GameCenterErrorCode.NotSufficientCredit
            let dataID = Casino.Localize.betFailMsg(errorCode)
            Casino.Tool.showToast(dataID, Config.TOAST_SHOW_TIME_DEFAULT, [errorCode])
            return
        }
        //關閉所有注區，等待Server回應。 
        for (let key in this.betZones){  
            this.betZones[key].setLockBtn(false)
        }
        Casino.Game.bet(this.tableName, this.tableID, betZoneID, choseChip, function(resp, error){
            if (resp == Config.requireToServerError.timeOut){
                let titleDataID = Casino.Localize.AlertTitleWarning
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, null)
                return
            }
            //Server正常回應，開啟所有注區。 
            if (this.gameState == Config.gameState.CountDown){
                for (let key in this.betZones){ 
                    this.betZones[key].setLockBtn(true)
                }
            }
            if (resp != 0){
                let errorCode = resp
                let dataID = Casino.Localize.betFailMsg(errorCode)
                Casino.Tool.showToast(dataID, Config.TOAST_SHOW_TIME_DEFAULT, [errorCode])
            }
        }.bind(this))
    },
    //設定注區狀態
    setBetZoneState:function(gameStatus){
        this.gameState = gameStatus 
        if (gameStatus == "" + Config.gameState.CountDown || 
            gameStatus == "" + Config.gameState.HalfTime ||
            gameStatus == "" + Config.gameState.Maintenance ||
            gameStatus == "" + Config.gameState.NewRoundRun ||
            gameStatus == "" + Config.gameState.ComputingFinish ||
            gameStatus == "" + Config.gameState.EndBetTime )
        {
            //停止閃爍
            for (let key in this.betZones){  
                this.betZones[key].stopBlink()
            }
        }
        if (gameStatus != "" + Config.gameState.CountDown){
            //鎖定注區
            for (let key in this.betZones){  
                this.betZones[key].setLockBtn(false)
            }
        } else {
            //開啟注區
            for (let key in this.betZones){  
                this.betZones[key].setLockBtn(true)
            }
        }
    }
});
