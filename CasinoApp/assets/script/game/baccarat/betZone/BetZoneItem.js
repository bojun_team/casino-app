/*
    功能描述：控制注區元件該顯示什麼內容
    實作內容：
        1 注區閃爍
        2 注區內籌碼
        3 鎖定注區
        4 存注區ID
        5 送出點擊注區事件
*/
var ChipsNode = require("ChipsNode");
cc.Class({
    extends: cc.Component,

    properties: {
        betZoneName:" ",
        winSprite:cc.Sprite,
        btn:{
            default:null,
            type:cc.Button,
            tooltip:"百家樂單桌用"
        },
        ifBtn:{
            default:null,
            type:require("IFButton"),
            tooltip:"多台使用，為了IFScrollView"
        },
        chipsNode: ChipsNode,
        spot_name_label:cc.Label,
        spot_number_label:cc.Label,
        normalColor:cc.Color,
        winColor:cc.Color,
        unableColor:cc.Color,
    },
    // use this for initialization
    onLoad: function () {
        if(this.ifBtn == null) return
        this.ifBtn.addClickEvent(this.onBet.bind(this))
    },
    setChips: function(credit){
        this.chipsNode.setCountChips(credit)
    },
    startBlink: function(){
        this.winSprite.node.active = true
        const blinkDuration = 1
        var blinkForever = cc.repeatForever(cc.blink(blinkDuration, 1))
        this.winSprite.node.runAction(blinkForever)
        if(this.spot_name_label != null) {
            this.startLabelBlink()
        }
    },
    startLabelBlink: function() {
        let setUnableColor = cc.callFunc(function(){
            this.spot_name_label.node.color = this.unableColor
            this.spot_number_label.node.color = this.unableColor
        }, this)
        let setWinColor = cc.callFunc(function(){
            this.spot_name_label.node.color = this.winColor
            this.spot_number_label.node.color = this.winColor
        }, this)
        let delay = cc.delayTime(0.5)
        let seq = cc.sequence(setUnableColor, delay, setWinColor, delay)
        let forever = cc.repeatForever(seq)
        this.node.runAction(forever)
    },
    stopBlink: function(){
        this.winSprite.node.active = false 
        this.winSprite.node.stopAllActions()
        this.node.stopAllActions()
    },
    onBet:function(){
        this.betEvent(this.id)
    },
    setLockBtn:function(isLock){
        let btn = (this.btn != null) ? (this.btn) : (this.ifBtn)
        // 設定button
        if(btn != null) {
            btn.interactable = isLock
        }
        // 設定文字
        if(this.spot_name_label != null) {
            let color = (isLock) ? (this.normalColor) : (this.unableColor)
            this.spot_name_label.node.color = color
            this.spot_number_label.node.color = color
        }
    },
    setBetEvent:function(fun){
        this.betEvent = fun
    },
    setId:function(id){
        this.id = id
    },
    getId:function(){
        return this.id
    },

});
