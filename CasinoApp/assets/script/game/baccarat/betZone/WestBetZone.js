/*
    西洋注區控制

    繼承：
        BetZoneView

    功能列表：
        1 顯示前每個注區下了多少籌碼
        2 取得結果，做注區的閃爍
        3 設定注區的狀態(可下注、不可下注，停止下注)
*/ 
import Config from '../../../constants/Config'

var BetZoneView = require("BetZoneView"); 

cc.Class({
    extends: BetZoneView,

    properties: {
        pageView:cc.PageView,
        west_pageOn1:cc.Node,
        west_pageOn2:cc.Node,
        west_pageArrow1:cc.Node,
        west_pageArrow2:cc.Node,
    },
    onLoad: function () {
        const playerID = Config.spotID.Player
        const tieID = Config.spotID.Tie
        const bankerID = Config.spotID.Banker

        const playerAID = Config.spotID.PlayerA
        const playerBankerAID = Config.spotID.PlayerBankerA
        const bankerAID = Config.spotID.BankerA

        const playerJQKID = Config.spotID.PlayerJQK
        const playerBankerJQKID = Config.spotID.PlayerBankerJQK
        const bankerJQKID = Config.spotID.BankerJQK

        const playerJQKAID = Config.spotID.PlayerJQKA
        const playerBankerJQKAID = Config.spotID.PlayerBankerJQKA
        const bankerJQKAID = Config.spotID.BankerJQKA

        // 當右邊的頁面箭頭，換到第2頁
        this.west_pageArrow1.on("click", (function () {
            let pageNumber = 2
            let idx = pageNumber - 1
            let timeInSecond = 0.1
            this.pageView.scrollToPage(idx, timeInSecond)
        }), this);

        // 當左邊的頁面箭頭，換到第1頁
        this.west_pageArrow2.on("click", (function () {
            let pageNumber = 1
            let idx = pageNumber - 1
            let timeInSecond = 0.1
            this.pageView.scrollToPage(idx, timeInSecond)
        }), this);

        // 當注區換頁時，更新頁面
        this.updatePage()
        this.pageView.node.on("page-turning", (function () {
            this.updatePage()
        }), this);

        //-----------設定注區ID 及 下注事件
        this.betZonesDict = {}
        for (var item in this.betZones){  
            this.betZones[item].setBetEvent(this.onBet.bind(this))
            // Page 1
            if (this.betZones[item].betZoneName == "player"){
                this.betZones[item].setId(playerID) 
            }
            else if (this.betZones[item].betZoneName == "tie"){ 
                this.betZones[item].setId(tieID) 
            }
            else if (this.betZones[item].betZoneName == "banker"){
                this.betZones[item].setId(bankerID) 
            }
            // Page 2
            else if (this.betZones[item].betZoneName == "playerA"){
                this.betZones[item].setId(playerAID) 
            }
            else if (this.betZones[item].betZoneName == "playerBankerA"){
                this.betZones[item].setId(playerBankerAID) 
            }
            else if (this.betZones[item].betZoneName == "bankerA"){
                this.betZones[item].setId(bankerAID) 
            }
            
            else if (this.betZones[item].betZoneName == "playerJQK"){
                this.betZones[item].setId(playerJQKID) 
            }
            else if (this.betZones[item].betZoneName == "playerBankerJQK"){
                this.betZones[item].setId(playerBankerJQKID) 
            }
            else if (this.betZones[item].betZoneName == "bankerJQK"){
                this.betZones[item].setId(bankerJQKID) 
            }
            
            else if (this.betZones[item].betZoneName == "playerJQKA"){
                this.betZones[item].setId(playerJQKAID) 
            }
            else if (this.betZones[item].betZoneName == "playerBankerJQKA"){
                this.betZones[item].setId(playerBankerJQKAID) 
            }
            else if (this.betZones[item].betZoneName == "bankerJQKA"){
                this.betZones[item].setId(bankerJQKAID) 
            }
            
            this.betZonesDict[this.betZones[item].getId()] = this.betZones[item]
        } 

        this._super()
    },
    // 更新頁面
    updatePage: function () {
        let pageNum = this.pageView.getCurrentPageIndex() + 1
        this.west_pageOn1.active = (pageNum == 1)
        this.west_pageOn2.active = (pageNum == 2)
        this.west_pageArrow1.active = (pageNum == 1)
        this.west_pageArrow2.active = (pageNum == 2)
    },
});
