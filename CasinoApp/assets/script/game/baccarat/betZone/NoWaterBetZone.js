/*
    免水注區控制

    繼承：
        BetZoneView

    功能列表：
        1 顯示前每個注區下了多少籌碼
        2 取得結果，做注區的閃爍
        3 設定注區的狀態(可下注、不可下注，停止下注)
*/ 
import Config from '../../../constants/Config'

var BetZoneView = require("BetZoneView"); 

cc.Class({
    extends: BetZoneView,

    properties: {
        
    },

    onLoad: function () {
        //取得各注區ID
        const playerPairNoWaterID = Config.spotID.PlayerPairNoWater
        const tieNoWaterID = Config.spotID.TieNoWater
        const bankerPairNoWaterID = Config.spotID.BankerPairNoWater
        const playerNoWaterID = Config.spotID.PlayerNoWater
        const super6ID = Config.spotID.Super6
        const bankerNoWaterID = Config.spotID.BankerNoWater
        
        //-----------設定注區ID 及 下注事件
        this.betZonesDict = {}
        for (var item in this.betZones){  
            //設定各注區點擊事件
            this.betZones[item].setBetEvent(this.onBet.bind(this))
            
            if (this.betZones[item].betZoneName == "playerPairNoWater"){ 
                this.betZones[item].setId(playerPairNoWaterID) 
            }
            else if (this.betZones[item].betZoneName == "tieNoWater"){ 
                this.betZones[item].setId(tieNoWaterID) 
            }
            else if (this.betZones[item].betZoneName == "bankerPairNoWater"){ 
                this.betZones[item].setId(bankerPairNoWaterID) 
            }
            else if (this.betZones[item].betZoneName == "playerNoWater"){ 
                this.betZones[item].setId(playerNoWaterID) 
            }
            else if (this.betZones[item].betZoneName == "super6"){ 
                this.betZones[item].setId(super6ID) 
            }
            else if (this.betZones[item].betZoneName == "bankerNoWater"){ 
                this.betZones[item].setId(bankerNoWaterID) 
            } 
            this.betZonesDict[this.betZones[item].getId()] = this.betZones[item]
        } 
        
        this._super()
    },
    
});
