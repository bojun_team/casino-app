import Rx from 'rxjs'
import Ramda from 'ramda'
var { contains } = Ramda
import Common from '../../tool/common'
var { capitalize } = Common
import Type from '../../constants/type'
var { BANKER, PLAYER, directions } = Type

const DIRECTION_CENTER = 0
const DIRECTION_UP_LEFT = 1
const DIRECTION_UP = 2
const DIRECTION_UP_RIGHT = 3
const DIRECTION_RIGHT = 4
const DIRECTION_DOWN_RIGHT = 5
const DIRECTION_DOWN = 6
const DIRECTION_DOWN_LEFT = 7
const DIRECTION_LEFT = 8

cc.Class({
  extends: cc.Component,

  properties: {
    type: {
      get: function () {
        return this._type || BANKER
      },
      set: function (type) {
        const typeString = (type == BANKER) ? 'banker' : 'player'

        this.backPoker.getComponent('cc.Sprite').spriteFrame = this[`${typeString}Back`]

        this._type = type
      }
    },

    value: {
      get: function () {
        return this._value || 'none-0'
      },
      set: function (value) {
        if (value == this.value) return

        const resource = `images/pokers/${value}`

        cc.loader.loadRes(resource, cc.SpriteFrame, (err, spriteFrame) => {

          this.frontPoker.getComponent('cc.Sprite').spriteFrame = spriteFrame
        })

        this._type = value
      }
    },

    dragPadding: 15,
    isDragging: false,

    isOpened: {
      get: function () {
        return this._isOpened || false
      },
      set: function (isOpened = true) {
        if (isOpened) {
          this.resetPoker()
          this.frontMask.setPosition(0, 0)
        } else {
          this.frontMask.setPosition(this.node.width, 0)
        }

        this.isInteractable = !isOpened
        this._isOpened = isOpened
      }
    },

    direction: {
      get: function () {
        return this._direction || 0
      },
      set: function (direction) {
        if (direction == DIRECTION_CENTER) {
          this.resetPoker()
        }

        this._direction = direction
      },
      visible: false
    },

    isInteractable: {
      get: function () {
        return this._isInteractable || false
      },
      set: function (interactable = true) {
        this._isInteractable = interactable
      }
    },

    dragDistance: {
      default: 0,
      visible: false
    },

    backMask: {
      default: null,
      type: cc.Node
    },

    backSideShadow: {
      default: null,
      type: cc.Node
    },

    backCornerShadow: {
      default: null,
      type: cc.Node
    },

    bankerBack: {
      default: null,
      type: cc.SpriteFrame
    },

    playerBack: {
      default: null,
      type: cc.SpriteFrame
    },

    backPoker: {
      default: null,
      type: cc.Node
    },

    frontMask: {
      default: null,
      type: cc.Node
    },

    frontPoker: {
      default: null,
      type: cc.Node
    },

    finger1Node: {
      default: null,
      type: cc.Node
    },

    finger2Node: {
      default: null,
      type: cc.Node
    },

    subscriptions: {
      default: [],
      visible: false
    }
  },

  onLoad: function () {
    const width = this.node.width
    const height = this.node.height

    this.halfwidth = width / 2
    this.halfheight = height / 2

    const mouseOver$ = Rx.Observable.fromEvent(this.node, 'mousemove')
    const mouseLeave$ = Rx.Observable.fromEvent(this.node, 'mouseleave')
    const mouseDown$ = Rx.Observable.fromEvent(this.node, 'mousedown')
    const mouseUp$ = Rx.Observable.fromEvent(this.node, 'mouseup')
    const doubleClick$ = Rx.Observable.fromEvent(this.node, 'mousedown')

    const mouseOverScription = mouseOver$.subscribe((e) => {
      if (!this.isInteractable) return

      const globalX = e.getLocationX()
      const globalY = e.getLocationY()
      const direction = (this.isDragging) ? this.direction : this.getDirection(globalX, globalY)

      const isUp = direction == DIRECTION_UP || direction == DIRECTION_UP_LEFT || direction == DIRECTION_UP_RIGHT
      const isDown = direction == DIRECTION_DOWN || direction == DIRECTION_DOWN_LEFT || direction == DIRECTION_DOWN_RIGHT
      const isLeft = direction == DIRECTION_LEFT || direction == DIRECTION_UP_LEFT || direction == DIRECTION_DOWN_LEFT
      const isRight = direction == DIRECTION_RIGHT || direction == DIRECTION_UP_RIGHT || direction == DIRECTION_DOWN_RIGHT

      this.showFinger(this.isDragging, direction, globalX, globalY)

      if (!this.isDragging && this.direction == direction) return

      if (this.isDragging) {
        const { x, y } = e.getDelta()

        this.dragDistance += (isLeft) ? x :
                             (isRight) ? -x :
                             (isDown) ? y :
                             (isUp) ? -y :
                             0
      }

      if (contains(direction, [DIRECTION_UP, DIRECTION_RIGHT, DIRECTION_DOWN, DIRECTION_LEFT])) {
        const padding = (this.dragDistance + this.dragPadding) / (this.isDragging ? 2 : 1)

        this.squintFromSide(direction, padding)
      } else {
        const paddingX = this.halfwidth + this.getLocationX(globalX) * (isLeft ? 1 : -1)
        const paddingY = this.halfheight + this.getLocationY(globalY)  * (isUp ? -1 : 1)
        const offsetX = this.isDragging ? paddingX : this.dragPadding * 2
        const offsetY = this.isDragging ? paddingY : this.dragPadding * 2

        this.squintFromCorner(direction, offsetX, offsetY)
      }

      this.direction = direction
    })

    const mouseLeaveScription = mouseLeave$.subscribe((e) => {
      if (!this.isInteractable) return

      const direction = this.direction
      const leaveDirection = this.getDirection(e.getLocationX(), e.getLocationY())

      this.direction = DIRECTION_CENTER

      if (this.isDragging && (
        (direction == DIRECTION_LEFT && leaveDirection == DIRECTION_RIGHT) ||
        (direction == DIRECTION_RIGHT && leaveDirection == DIRECTION_LEFT) ||
        (direction == DIRECTION_UP && leaveDirection == DIRECTION_DOWN) ||
        (direction == DIRECTION_DOWN && leaveDirection == DIRECTION_UP) ||
        (direction == DIRECTION_UP_LEFT && leaveDirection == DIRECTION_DOWN) ||
        (direction == DIRECTION_UP_LEFT && leaveDirection == DIRECTION_RIGHT) ||
        (direction == DIRECTION_UP_RIGHT && leaveDirection == DIRECTION_DOWN) ||
        (direction == DIRECTION_UP_RIGHT && leaveDirection == DIRECTION_LEFT) ||
        (direction == DIRECTION_DOWN_LEFT && leaveDirection == DIRECTION_UP) ||
        (direction == DIRECTION_DOWN_LEFT && leaveDirection == DIRECTION_RIGHT) ||
        (direction == DIRECTION_DOWN_RIGHT && leaveDirection == DIRECTION_UP) ||
        (direction == DIRECTION_DOWN_RIGHT && leaveDirection == DIRECTION_LEFT)
      )) {
        this.isOpened = true
      }

      this.dragDistance = 0
      this.isDragging = false

      this.hideFinger()
    })

    const mouseDownScription = mouseDown$.subscribe((e) => {
      if (!this.isInteractable) return

      const initDragPadding = this.dragPadding - 5
      this.isDragging = true

      if (contains(this.direction, [DIRECTION_UP, DIRECTION_RIGHT, DIRECTION_DOWN, DIRECTION_LEFT])) {
        this.squintFromSide(this.direction, initDragPadding)
      } else {
        this.squintFromCorner(this.direction, initDragPadding, initDragPadding)
      }
    })

    const mouseUpScription = mouseUp$.subscribe((e) => {
      if (!this.isInteractable) return

      this.direction = DIRECTION_CENTER
      this.isDragging = false
      this.dragDistance = 0
    })

    const doubleClickSubscription = doubleClick$.bufferWhen(() => doubleClick$.debounceTime(250))
                                                .map(list => list.length)
                                                .filter(x => x >= 2)
                                                .subscribe(() => {
                                                  if (!this.isInteractable) return

                                                  this.isOpened = true
                                                })

    this.value = this.value

    this.subscriptions.push(mouseOverScription, mouseLeaveScription, mouseDownScription, mouseUpScription, doubleClickSubscription)
  },

  onDestroy: function () {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe()
    })
  },

  getLocationX: function (x = 0) {
    return x - this.node.convertToWorldSpaceAR(this.node.x).x
  },

  getLocationY: function (y = 0) {
    return y - this.node.convertToWorldSpaceAR(0, this.node.y).y
  },

  getDirection: function (x, y) {
    const locationX = this.getLocationX(x)
    const locationY = this.getLocationY(y)
    const { dragPadding, halfwidth, halfheight } = this

    const padding = dragPadding + 10

    if (locationX > halfwidth - padding) {
      if (locationY > halfheight - padding) {
        return DIRECTION_UP_RIGHT
      } else if (locationY < - (halfheight - padding)) {
        return DIRECTION_DOWN_RIGHT
      } else {
        return DIRECTION_RIGHT
      }
    } else if (locationX < - (halfwidth - padding)) {
      if (locationY > halfheight - padding) {
        return DIRECTION_UP_LEFT
      } else if (locationY < - (halfheight - padding)) {
        return DIRECTION_DOWN_LEFT
      } else {
        return DIRECTION_LEFT
      }
    } else if (locationY > halfheight - padding) {
      return DIRECTION_UP
    } else if (locationY < - (halfheight - padding)) {
      return DIRECTION_DOWN
    } else {
      return DIRECTION_CENTER
    }
  },

  resetPoker: function () {
    this.getComponent('cc.Mask').enabled = true
    // Reset back poker
    this.backMask.setAnchorPoint(0.5, 0.5)
    this.backMask.setPosition(0, 0)
    this.backPoker.setPosition(0, 0)
    this.backMask.setContentSize(this.node.width, this.node.height)
    this.backMask.getComponent('cc.Mask').type = 0

    // Reset back shadow
    this.backSideShadow.active = false
    this.backCornerShadow.active = false

    // Reset front poker
    this.frontMask.setAnchorPoint(0.5, 0.5)
    this.frontMask.setPosition(this.node.width, 0)
    this.frontPoker.setPosition(0, 0)
    this.frontMask.setContentSize(this.node.width, this.node.height)
    this.frontMask.setRotation(0)
    this.frontMask.getComponent('cc.Mask').type = 0
  },

  squintFromSide: function (direction = DIRECTION_CENTER, padding = this.dragPadding) {
    if (direction == DIRECTION_CENTER) return

    this.resetPoker()

    const isUp = direction == DIRECTION_UP
    const isDown = direction == DIRECTION_DOWN
    const isLeft = direction == DIRECTION_LEFT
    const isRight = direction == DIRECTION_RIGHT
    const isUpOrDown = (isUp || isDown)
    const isLeftOrRight = (isLeft || isRight)

    const anchorX = isRight ? 0 :
                    isLeft ? 1 :
                    0.5
    const anchorY = isUp ? 0 :
                    isDown ? 1 :
                    0.5

    const targetSide = isLeftOrRight ? 'width' : 'height'
    const targetCoordinate = isLeftOrRight ? 'x' : 'y'

    // Initiate front node position
    this.frontMask.x = isRight ? this.node.width :
                       isLeft ? -this.node.width :
                       0
    this.frontMask.y = isUp ? this.node.height :
                       isDown ? -this.node.height :
                       0

    // Adjust back anchor and position
    this.backMask.setAnchorPoint(anchorX, anchorY)
    this.backMask[targetCoordinate] += this[`half${targetSide}`] * ((isUp || isRight) ? -1: 1)
    this.backPoker[targetCoordinate] += this[`half${targetSide}`] * ((isUp || isRight) ? 1: -1)

    // Adjust front anchor and position
    this.frontMask.setAnchorPoint(anchorX, (isUpOrDown ? switchOneZero(anchorY) : anchorY))
    this.frontMask[targetCoordinate] += this[`half${targetSide}`] * ((isUp || isRight) ? -1: 1)
    this.frontPoker[targetCoordinate] += this[`half${targetSide}`] * ((isUp || isLeft) ? -1: 1)

    // Rotate if node at up or down direction
    if (isUpOrDown) {
      this.frontMask.setRotation(180)
    }

    // Caculate front display region
    this.backMask[targetSide] -= padding
    this.frontMask[targetSide] = padding

    // Move front to propert position
    this.frontMask[targetCoordinate] += padding * 2 * ((isUp || isRight) ? -1: 1)

    this.setBackSideShadow(direction)
  },

  squintFromCorner: function (direction, x, y) {
    if (direction == DIRECTION_CENTER) return

    this.resetPoker()

    // Disable poker mask component to prevent cover reversible shape
    this.getComponent('cc.Mask').enabled = false

    const isLeft = direction == DIRECTION_UP_LEFT || direction == DIRECTION_DOWN_LEFT
    const isUp = direction == DIRECTION_UP_LEFT || direction == DIRECTION_UP_RIGHT

    const locationX = (this.halfwidth - x) * (isLeft ? -1 : 1)
    const locationY = (this.halfheight - y) * (isUp ? 1 : -1)

    const offsetX = (this.halfwidth + locationX * (isLeft ? 1 : -1)) / 2 + this.halfwidth
    const offsetY = (this.halfheight + locationY * (isUp ? -1 : 1)) / 2 + this.halfheight

    // `m` is slob and `b` is offset of slob function, this two factors determine the points of intersection of x and y
    const m = (locationX + this.halfwidth * (isLeft ? 1 : -1)) /
              (this.halfheight * (isUp ? 1 : -1) - locationY)
    const b = (this.halfheight * (isUp ? 1 : -1)) - m * (this.halfwidth * (isLeft ? -1 : 1))

    // `crossX` is point of intersection of x axis, aka (crossX, 0)
    const crossX = -b / m
    // `crossY` is point of intersection of y axis, aka (0, crossY)
    const crossY = b

    // Change mask type of back and front to ellipse
    this.backMask.getComponent('cc.Mask').type = 1
    this.frontMask.getComponent('cc.Mask').type = 1

    // Modify size of back and front to 4 times, that prevent mask to cover back and front sprite
    this.backMask.setContentSize(crossX * 4, crossY * 4)
    this.frontMask.setContentSize(crossX * 4, crossY * 4)

    this.backPoker.setPosition(
      offsetX * (isLeft ? -1 : 1),
      offsetY * (isUp ? 1 : -1)
    )
    this.backMask.setPosition(
      offsetX * (isLeft ? 1 : -1),
      offsetY * (isUp ? -1 : 1)
    )

    this.frontPoker.setPosition(
      this.node.width * 2 * (isLeft ? -1 : 1) + offsetX * (isLeft ? 1 : -1),
      this.node.height * 2 * (isUp ? -1 : 1) + offsetY * (isUp ? 1 : -1)
    )
    this.frontMask.setPosition(
      offsetX * (isLeft ? -1 : 1),
      offsetY * (isUp ? -1 : 1)
    )

    // According to mouse distance from corner to caculate rotation degree that match reverse point
    const distance = Math.sqrt(
      Math.pow((this.halfwidth * (isLeft ? -1 : 1) - locationX), 2) +
      Math.pow((this.halfheight * (isUp ? 1 : -1) - locationY), 2)
    )
    const degree = 2 * (Math.acos((this.halfwidth * (isLeft ? -1 : 1) - locationX) / distance) * 180 / Math.PI)

    this.frontMask.setRotation(degree * (isUp ? -1 : 1))

    // Move front poker mask to position of mouse and make poker reversible
    const frontPokerWorldPosition = this.frontPoker.convertToWorldSpaceAR(
      cc.v2(
        this.halfwidth * (isLeft ? 1 : -1),
        this.halfheight * (isUp ? 1 : -1)
      )
    )
    const mouseWorldPosition = this.node.convertToWorldSpaceAR(
      cc.v2(
        locationX,
        locationY
      )
    )

    this.frontMask.x -= frontPokerWorldPosition.x - mouseWorldPosition.x
    this.frontMask.y -= frontPokerWorldPosition.y - mouseWorldPosition.y

    const shadowDegree = Math.atan(crossY / crossX) * 180 / Math.PI
    // `pokerDegree` only for halo black circle and finger displaying
    this.pokerDegree = degree * (Math.PI / 180)

    this.setBackCornerShadow(direction, locationX, locationY, shadowDegree)
  },

  setBackSideShadow: function (direction) {
    const widget = this.backSideShadow.getComponent('cc.Widget')

    this.backSideShadow.x = 0
    this.backSideShadow.y = 0
    this.backSideShadow.setRotation(direction * 45)
    this.backSideShadow.active = true;

    ['left', 'up', 'right', 'down'].forEach((value) => {
      const isTop = value == 'up'
      const isBottom = value == 'down'
      const position = (isTop) ? 'top' : (isBottom) ? 'bottom' : value

      widget[position] = (isTop || isBottom) ? -100 : 0
      widget[`isAlign${capitalize(position)}`] = directions[direction] == value
    })
  },

  setBackCornerShadow: function (direction, x, y, degree) {
    const widget = this.backCornerShadow.getComponent('cc.Widget')
    const isLeft = direction == DIRECTION_UP_LEFT || direction == DIRECTION_DOWN_LEFT
    const isUp = direction == DIRECTION_UP_LEFT || direction == DIRECTION_UP_RIGHT

    this.backCornerShadow.x = (this.halfwidth * (isLeft ? -1 : 1) + x) / 2
    this.backCornerShadow.y = (this.halfheight * (isUp ? 1 : -1) + y) / 2
    this.backCornerShadow.setRotation(degree + 90 * (isUp ? 1 : -1))
    this.backCornerShadow.active = true
  },

  autoSquintFromSide: function (duration = 2, delay = 0) {
    if (this.isOpened) return

    const timer1$ = Rx.Observable.timer(0, 50).take(duration * 20)
    const timer2$ = Rx.Observable.timer(0, 50).take(duration * 20)
    const timer3$ = Rx.Observable.timer(0, 50).take(duration * 8)

    let direction1 = Math.floor((Math.random() * 4) + 1) * 2
    let direction2 = (direction1 == 6) ? 8 : (direction1 + 2) % 8
    let count = 0

    const timeout$ = Rx.Observable.timer(8000 + delay * 1000)
    const timerSubscription = timer1$.concat(timer2$).concat(timer3$).skip(1).delay(delay * 1200).takeUntil(timeout$).subscribe({
      next: (offset) => {
        if (offset == 0) {
          direction1 = direction2
          count++
        }

        const isLeft = (direction1 == DIRECTION_LEFT)
        const isRight = (direction1 == DIRECTION_RIGHT)
        const isUp = (direction1 == DIRECTION_UP)
        const isDown = (direction1 == DIRECTION_DOWN)

        if (count < 2) {
          offset /= 2
        } else {
          offset *= (isUp || isDown) ? 10 : 5
        }

        const padding = this.dragPadding + offset

        if (this.node.active) {
          const globalSpace = this.node.convertToWorldSpaceAR(this.node.x, this.node.y)

          const paddingX = globalSpace.x + (this.node.width / 2) * (isLeft ? -1 : 1) + 2 * padding * (isLeft ? 1 : -1)
          const paddingY = globalSpace.y + (this.node.height / 2) * (isDown ? -1 : 1) + 2 * padding * (isDown ? 1 : -1)

          this.showFinger(true, direction1, paddingX, paddingY)
        }

        this.squintFromSide(direction1, padding)
      },
      complete: () => {
        this.hideFinger()
        this.resetPoker()
        if (this.node.active) {
          this.isOpened = true
        }
      }
    })

    this.subscriptions.push(timerSubscription)
  },

  showFinger: function (isDragging, direction, x, y) {
    if (!this.finger1Node || !this.finger2Node || this.isOpened) return

    const isUp = (direction == DIRECTION_UP)
    const isUpOrDown = isUp || (direction == DIRECTION_DOWN)
    const isLeft = (direction == DIRECTION_LEFT)
    const isRight = (direction == DIRECTION_RIGHT)
    const isLeftOrRight = isLeft || (direction == DIRECTION_RIGHT)
    const isUpRight = (direction == DIRECTION_UP_RIGHT)
    const isDownLeft = (direction == DIRECTION_DOWN_LEFT)

    this.finger1Node.getChildByName('finger').scaleX = 1
    this.finger2Node.getChildByName('finger').scaleX = -1

    this.finger1Node.getChildByName('blackCircle').active = isDragging
    this.finger2Node.getChildByName('blackCircle').active = isDragging

    if (isDragging && (isUpOrDown || isLeftOrRight)) {
      this.finger1Node.active = direction != DIRECTION_CENTER
      this.finger2Node.active = direction != DIRECTION_CENTER

      const globalX = this.node.convertToWorldSpaceAR(this.node.x).x
      const globalY = this.node.convertToWorldSpaceAR(0, this.node.y).y

      const locationX = (isNegative = false) => {
        return isLeftOrRight ?
               x + 25 * (isLeft ? -1 : 1):
               (globalX + (this.node.width / 2) * (isNegative ? -1 : 1)  + 30 * (isNegative ? 1 : -1))
      }
      const locationY = (isNegative = false) => {
        return isUpOrDown ?
               y + 30 * (isUp ? 1 : -1):
               (globalY + (this.node.height / 2) * (isNegative ? -1 : 1) + 30 * (isNegative ? 1 : -1))
      }

      this.finger1Node.setPosition(locationX(true), locationY())
      this.finger2Node.setPosition(locationX(), locationY(true))

      if (isLeft) {
        this.finger1Node.getChildByName('finger').scaleX = -1
      } else if (isRight) {
        this.finger2Node.getChildByName('finger').scaleX = 1
      }
    } else {
      this.finger1Node.active = this.direction != DIRECTION_CENTER
      this.finger2Node.active = false

      if (isUpRight) {
        const degree = this.pokerDegree - 45 * (Math.PI / 180)
        this.finger1Node.setPosition(x + 40 * Math.cos(degree), y + 40 * Math.sin(degree))
      } else if (isDownLeft) {
        const degree = this.pokerDegree - 135 * (Math.PI / 180)
        this.finger1Node.setPosition(x + 40 * Math.cos(-degree), y + 40 * Math.sin(-degree))
      } else {
        this.finger1Node.setPosition(x, y)
      }
    }
  },

  hideFinger: function () {
    if (!this.finger1Node || !this.finger2Node) return

    this.finger1Node.active = false
    this.finger2Node.active = false
  }
})

const switchOneZero = (value) => {
  return (value == 0) ? 1 : 0
}
