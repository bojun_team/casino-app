/*
    功能描述：發送取消下注
    實作內容：
        1 把取消下注事件送出去給Model(Game)
        2 訂閱 Game 等待回應中的參數及 TableInfo 的 State 判斷是否要給玩家按。
*/ 
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component, 
    properties: {
        cancelBtn:{
            default:null,
            displayName:"[單台]cancelBtn",
            type:cc.Button,
        },
        cancelBetIFBtn:{
            default:null,
            displayName:"[多台]cancelBet_btn",
            type:require("IFButton"),
        },
    }, 
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },
    onLoad: function () {
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
        } else {
            // 多台時cancelBtn相等於cancelBetIFBtn
            this.cancelBtn = this.cancelBetIFBtn
        }
    },
    setTableData: function(tableName, tableID) {
        this.tableName = tableName
        this.tableID = tableID

        let table = Casino.Tables.getTableInfo(tableName, tableID)        
        this.cancelBtn.interactable = false
        this.betSum = Casino.Game.getSpotMgr(tableName, tableID).betSum
        this.gameStatus = table.status
        this.subscribes = []

        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                this.gameStatus = gameStatus
                this.updateBtnsInteractable()
            },  
        }); 
        let betSum$ = Casino.Game.getSpotMgr(tableName, tableID).propChange$.betSum
        this.subscribes[this.subscribes.length] = betSum$.subscribe({
            next: betSum => {     
                this.betSum = betSum
                this.updateBtnsInteractable()
            },  
        }); 

        // 設定IFButton點擊事件
        if(this.cancelBetIFBtn == null) return
        this.cancelBetIFBtn.addClickEvent(function(){
            this.onTouchCancelBet()
        }.bind(this))
    }, 
    onTouchCancelBet: function() {
        this.cancelBtn.interactable = false
        Casino.Game.cancelBet(this.tableName, this.tableID, function(resp){
            if (this.gameStatus == Config.gameState.CountDown) this.cancelBtn.interactable = true
            if (resp == Config.requireToServerError.timeOut){
                let titleDataID = Casino.Localize.AlertTitleWarning
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, null)
                return
            }
            if (resp != 0){
                let errorCode = resp
                let dataID = Casino.Localize.betFailMsg(errorCode)
                Casino.Tool.showToast(dataID, Config.TOAST_SHOW_TIME_DEFAULT, [errorCode])
            }
        }.bind(this))
    },
    // 更新按鈕狀態
    updateBtnsInteractable: function() {
        // 如果遊戲狀態是倒數中且有下注金額，才可以按按鈕
        let interactable = (this.gameStatus == Config.gameState.CountDown && this.betSum > 0)     
        this.cancelBtn.interactable = interactable
    },
});
