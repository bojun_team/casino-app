/*
    功能描述：記錄目前這輪開過的莊閒和次數
    實作內容：
        1 訂閱 Game(or Table) 中的莊閒和次數
*/ 
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: { 
        playerLab:cc.Label,
        bankererLab:cc.Label,
        tieLab:cc.Label, 
    },  
    onDestroy:function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },
    onLoad: function () {
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
        }
    },
    setTableData: function(tableName, tableID) {
        this.subscribes = []
        let table = Casino.Tables.getTableInfo(tableName, tableID)

        let player$ = table.propChange$.player
        this.subscribes[this.subscribes.length] = player$.subscribe({
            next: count => {     
                this.playerLab.string = count 
            },  
        });  
        let banker$ = table.propChange$.banker
        this.subscribes[this.subscribes.length] = banker$.subscribe({
            next: count => {     
                this.bankererLab.string = count 
            },  
        });
        let tie$ = table.propChange$.tie
        this.subscribes[this.subscribes.length] = tie$.subscribe({
            next: count => {     
                this.tieLab.string = count 
            },  
        });  
    },
});
