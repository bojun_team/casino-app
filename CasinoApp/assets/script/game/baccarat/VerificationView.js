
import Casino from '../../model/Casino'
import Config from '../../constants/Config'

/**  VerificationView */
cc.Class({
    extends: cc.Component,

    properties: {
        view: cc.Node,
        panel: cc.Node,
        btn_verification: cc.Node,
        btn_verification_gray: cc.Node,
        img_flash: cc.Node,
        label_number: cc.Label,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
        Casino.NumberKeyboard.isHideKeyboard = true;
    },

    // use this for initialization
    onLoad: function () {
        this.view.active = false;
        this.panel.active = false;
        this.enableButton();
        this.setupWidgetVerification();
        this.startBlink();

        this.subscribes = [];

        let tableInfo = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID);
        this.subscribes[this.subscribes.length] = tableInfo._propChange$.liveAuth.subscribe({
            next: liveAuth => {
                this.view.active = liveAuth;
            }
        });

        this.subscribes[this.subscribes.length] = Casino.NumberKeyboard.text$.subscribe({
            next: text => {
                 this.onTextChanged(text);
            },  
        })

        //------ 訂閱 isOpenSetting，當設定頁開關時，更新視訊狀態  
        let isOpenSetting$ = Casino.User.propChange$.isOpenSetting
        this.subscribes[this.subscribes.length] = isOpenSetting$.subscribe({
            next: isOpenSetting => {
                if(isOpenSetting) {
                    this.onBtnCancelClick();
                }
            }
        });
        
    },

    setupWidgetVerification: function () {
        let widget = this.btn_verification.getComponent(cc.Widget);
        let widget2 = this.btn_verification_gray.getComponent(cc.Widget);
        if (Config.JACKPOT_ENABLE == false) {
            // widget.isAlignRight = widget2.isAlignRight = false
            // widget.isAlignLeft = widget2.isAlignLeft = true
            // widget.left = widget2.left = widget.right
        }
    },

    startBlink: function () {
        const fadeDuration = 1
        var fadeIn = cc.fadeIn(fadeDuration / 2)
        var delay = cc.delayTime(0.2)
        var fadeOut = cc.fadeOut(fadeDuration / 2)
        var seq = cc.sequence(fadeIn, delay, fadeOut)
        var fadeInOutForever = cc.repeatForever(seq)
        this.img_flash.runAction(fadeInOutForever)
    },

    showPanel: function () {
        Casino.NumberKeyboard.text$.next("");
        Casino.NumberKeyboard.isHideKeyboard = false;
        this.panel.active = true
    },

    onEditingDidBegan: function () {
        Casino.NumberKeyboard.text$.next("");
        Casino.NumberKeyboard.isHideKeyboard = false;
        this.panel.active = true
    },

    onTextChanged: function (text) {
        this.label_number.string = text;
    },
    
    onBtnOkClick: function () {
        Casino.NumberKeyboard.isHideKeyboard = true;

        let text = this.label_number.string;
        let dataId = Casino.Localize.NoLocalize;
        if (text.length < 4) {
            Casino.Tool.showToast(dataId, 2, ["驗證失敗，請輸入四位數字"])
        } else {
            this.panel.active = false;
            this.unscheduleAllCallbacks();
            this.disableButton();
            this.scheduleOnce(this.enableButton, 5);
            this.liveAuth(text);
        }
    },

    liveAuth: function (text, index = 1) {
        let dataId = Casino.Localize.NoLocalize;
        Casino.User.liveAuth(Casino.Game.tableName, Casino.Game.tableID, text, (cmd, resp, error) => {
            let data = {
                cmd: cmd,
                resp: resp,
                error: error,
            }

            if (resp == Config.requireToServerError.timeOut) {
                Casino.Tool.showToast(dataId, 2, ["驗證失敗，連線逾時"]);
                return;
            }

            if (error != null) {
                let errMsg = "驗證失敗\n" + error;
                Casino.Tool.showToast(dataId, 2, [errMsg]);
                return;
            }

            if (resp != 0) {
                if (resp == 136) {
                    Casino.Tool.showToast(dataId, 2, ["驗證太頻繁"]);
                } else {
                    let errMsg = "驗證失敗，代碼:" + resp;
                    Casino.Tool.showToast(dataId, 2, [errMsg]);
                }
                return;
            }

            Casino.Tool.showToast(dataId, 2, ["已送出驗證碼 " + text]);
        });
    },

    onBtnCancelClick: function () {
        Casino.NumberKeyboard.isHideKeyboard = true;

        this.panel.active = false;
    },

    disableButton: function () {
        this.btn_verification.active = false;
        this.btn_verification_gray.active = true;
    },

    enableButton: function () {
        this.btn_verification.active = true;
        this.btn_verification_gray.active = false;
    }

});
