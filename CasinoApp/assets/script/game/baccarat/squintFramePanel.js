import Config from '../../constants/Config'
import Casino from '../../model/Casino'

var Card = require("Card") 
var FlipCard = require("FlipCard")
cc.Class({
    extends: cc.Component,

    properties: {
        ctrView:cc.Node,
        pokerCard:[Card],
        forceCard:cc.Node
    }, 
 
    onLoad: function () {
        this.nowSequence = 0

        this.isInBackGound = false
        cc.game.on(cc.game.EVENT_HIDE, function () {  
            this.isInBackGound = true
        }.bind(this)); 
        cc.game.on(cc.game.EVENT_SHOW , function () {  
            this.isInBackGound = false
        }.bind(this)); 
        
        // 初始化所有的牌
        let cardForceWorldPos = this.node.convertToWorldSpace(this.forceCard.getPosition());
        for (let i = 0 ; i < this.pokerCard.length ; i++ ) {
            this.pokerCard[i].resetCard()
            this.pokerCard[i].setCardIndex(i)
            this.pokerCard[i].listenSelect(this.onSelect.bind(this))
            this.pokerCard[i].listenMiCardFinish(this.onMiCardFinish.bind(this))
            this.pokerCard[i].setForceRotate(0) 
            this.pokerCard[i].setCardConvert(cardForceWorldPos)
            this.pokerCard[i].node.active = false
        }
        this.pokerCard[4].setForceRotate(-90) //閒家第三張補牌轉90度
        this.pokerCard[5].setForceRotate(-90) //莊家第三張補牌轉90度

        // ===== 訂閱註冊 =====
        this.subscribes = []
        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        let gameStatus$ = table.propChange$.gameStatus
        // 訂閱遊戲狀態
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                // 除了mi card 狀態外 this.ctrView 都是關閉的
                this.ctrView.active = (gameStatus == Config.gameState.MiCard || 
                                       gameStatus == Config.gameState.EndMiCardTime)
                if (gameStatus == Config.gameState.EndBetTime ||
                    gameStatus == Config.gameState.Computing) {
                    // 重置所有的牌
                    this.resetAllCard()
                } 
                else if (gameStatus == Config.gameState.EndMiCardTime){
                    // 打開所有有資料的牌
                    this.openMiCard(this.pokerCard.length)
                }
            },
        });
    },
    isMiCard:function(spotID) {
        let amount = Casino.Game.spotMgr.getSpot(spotID).chipAmount
        return amount > 0
    },
    // 重置所有的牌
    resetAllCard:function() {
        for (let i = 0 ; i < this.pokerCard.length; i++ ) {
            this.pokerCard[i].resetCard()
            this.pokerCard[i].node.active = false
        } 
    },
    // 開牌
    openMiCard:function(lastIndex) {
        for (let i = 0 ; i < lastIndex; i++ ) {
            if (this.pokerCard[i].getIsHaveData()) this.pokerCard[i].openCard()
            this.pokerCard[i].setForce(false, !this.isInBackGound)
        }
    },
    // 判斷是不是莊家的index
    isBankerIndex:function(sequence) {
        return (sequence == 0 || sequence == 1 || sequence == 4) 
    },
    // 切換下一張要咪的牌
    onSelect:function(index){
        for (let i = 0 ; i < this.pokerCard.length ; i++ ) {
            this.pokerCard[i].setForce(false, !this.isInBackGound)
        }
        this.pokerCard[index].setForce(true, !this.isInBackGound)
    },
    // 咪牌結束後尋找下一張要開的牌
    onMiCardFinish:function(index){
        this.pokerCard[index].setForce(false, !this.isInBackGound)
        this.searchNextCanMiCard()
    },
    // 尋找下一找有資料且還沒開過的牌
    searchNextCanMiCard:function(){
        for (let i = 0; i < this.pokerCard.length; i++) {
            if (this.pokerCard[i].getIsHaveData() && this.pokerCard[i].getLock() == false) { 
                this.pokerCard[i].setForce(true, !this.isInBackGound)
                return
            }
        }
    },
    onDestroy: function(){
        // 解除所有訂閱
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }  
    },
});