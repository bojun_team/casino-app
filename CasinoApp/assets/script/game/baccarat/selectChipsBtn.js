import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        btn:cc.Button,
    },
    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    },
    onLoad: function () {
         
        this.subscribes = []
        let currentLimitStakeIndex$ = Casino.Game.limitMgr.propChange$.currentLimitStakeIndex
        this.subscribes[this.subscribes.length] = currentLimitStakeIndex$.subscribe({
            next: index => {     
                this.chips = Config.chips
                const userLimitStake = Casino.Game.getCurrentLimitStak()
                this.haveChips = 0
                if (userLimitStake["_maxLimit"] == 0 ) this.btn.node.active = false //判斷是否為0-0範本
                // for (var i = 0;i<this.chips.length; i ++){
                //     if (userLimitStake["_minLimit"] <= this.chips[i] && userLimitStake["_maxLimit"] >= this.chips[i] ){
                //         this.haveChips = this.haveChips + 1
                //     }
                // }
                // if (this.haveChips <= 3 ) this.btn.node.active = false
            },  
        });  
    },

});
