/*
    功能描述：顯示目前遊戲狀態
    實作內容：從Table裡面訂閱目前的遊戲狀態。
    其它：倒數的UI先問達哥
*/ 

import Localize from '../../constants/Localize'
import Tool from "../../tool/Tool"
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: { 
        countdownInner:cc.Sprite,
        countdownStateLabel:require("LocalizedLabel"),
        countdownNumberLabel:cc.Label,
        isClockwise:true,//順時針
        stateWidget:cc.Widget // jp上線前暫時用來改變位置        
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
        this.unschedule(this.upateEverySec);
    }, 
    onLoad: function () {
        this.countdownInner.fillRange = 0
        
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
            // 當jackpot 關閉時改變位置
            if (!Config.JACKPOT_ENABLE){
                // this.stateWidget.isAlignRight = false
                // this.stateWidget.isAlignLeft = true
                // this.stateWidget.left = this.stateWidget.right
            }
            
        }
    },
    setTableData: function(tableName, tableID) {
        this.tableName = tableName
        this.tableID = tableID
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        this.totalTime = table.totalTime
        this.state = String(table.status)
        this.setState(this.state)

        // 訂閱
        this.subscribes = []

        // 訂閱 gameStatus
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                this.state = String(table.status) 
                this.setState(gameStatus)
            },
        });

        // 訂閱 countDownSec
        let countDownSec$ = table.propChange$.countDownSec
        this.subscribes[this.subscribes.length] = countDownSec$.subscribe({
            next: value => { 
                this.upateEverySec() 
            }
        }); 

        // 訂閱 totalTime
        let totalTime$ = table.propChange$.totalTime
        this.subscribes[this.subscribes.length] = totalTime$.subscribe({
            next: totalTime => { 
                this.totalTime = totalTime
            }
        });

        //每0.2秒更新一次
        this.schedule(this.upateEverySec, 0.2);
    },
    setState: function(state) {  
        if (state == String(Config.gameState.CountDown) ||
            state == String(Config.gameState.OpenBankerCardBet) ||
            state == String(Config.gameState.OpenPlayerCardBet)) {   
            this.setCountdownNumberLabel(true)
            this.upateEverySec()
        } else if (state == String(Config.gameState.MiCard)) { 
            this.processMiCardCountDown()
        } else {   
            this.setCountdownNumberLabel(false)
            this.updateCountDown(0)
            this.countdownStateLabel.dataID = Localize.gameStateDataID(state)
            // 維護時設定倒數環為０
            if(Number(state) == Config.gameState.Maintenance) {
                this.countdownInner.fillRange = 0
            }
            // 只有非多台才會跑 音效
            if(Casino.Game.tableName != Config.TableName.MultipleTable) {
                let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
                let isVivoVideo = table.videoProvider == Config.VideoProvider.Vivo ||
                                  table.videoProvider == Config.VideoProvider.VivoBigTable
                // 不播放中場語音
                if(state == String(Config.gameState.HalfTime) && isVivoVideo) return
                Tool.getMusicControl().gameStatus(state)
            }
        }
    },
    setCountdownNumberLabel: function(isOpen) {
        this.countdownNumberLabel.node.active = isOpen
        this.countdownStateLabel.node.active = !isOpen
    },
    upateEverySec: function() {    
        if (this.state == String(Config.gameState.CountDown) || 
            this.state == String(Config.gameState.OpenBankerCardBet) ||
            this.state == String(Config.gameState.OpenPlayerCardBet))
            this.processCountDown()
        else if (this.state == String(Config.gameState.MiCard))
            this.processMiCardCountDown()
    },
    processCountDown: function() {
        if(Casino.Tables == null) return
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        if (table.countDownEndDate == null) {
            return   
        } 
        let remainTime = table.countDownEndDate - (+new Date())  
        if(remainTime < 0) remainTime = 0 
        this.updateCountDown(remainTime)  
    },
    processMiCardCountDown: function() {
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        if (table.miCardCountDownEndDate == null) return  
        else this.countdownStateLabel.dataID = Localize.gameStateDataID(Config.gameState.MiCard)
        let remainTime = table.miCardCountDownEndDate - (+new Date())  
        if(remainTime < 0) remainTime = 0
        this.setCountdownNumberLabel(true)
        this.updateCountDown(remainTime) 
    },
    updateCountDown: function(sec) {
        if (this.totalTime == 0) return
        if(Casino.Game == null) return
        // 只有非多台才會跑 音效
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.countDownSound(sec)
        }
        
        this.countdownNumberLabel.string = String(Math.floor(sec / 1000))
        const proportion = sec / (this.totalTime * 1000)
        const degree = proportion * 360
        this.countdownInner.fillRange = proportion - (this.isClockwise ? 0 : 1)
    },
    countDownSound (sec) {
        if ((this.state == String(Config.gameState.CountDown)) || (this.state == String(Config.gameState.MiCard))){
            if (this.saveSec == null || this.saveSec != Math.floor(sec / 1000)){
                this.saveSec = Math.floor(sec / 1000)
                Tool.getMusicControl().countDown(Math.floor(sec / 1000))
            }
        }
    }
});
