import Config from '../../../constants/Config'
import Casino from '../../../model/Casino'

const WIN_DELAY_SHOW_SEC = 2
const WIN_FADE_IN_SEC = 0.5

cc.Class({
    extends: cc.Component,

    properties: { 
        playerPoker: require("PokerView"),
        bankerPoker: require("PokerView"),

        playerWin: require("WinnerView"),
        bankerWin: require("WinnerView"),
        tieWin: require("WinnerView"),

        playerLose:cc.Node,
        bankerLose:cc.Node,

        winNode:cc.Node,
        winLab:cc.Label,
    },

    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
        Casino.MusicControl.closeAction()
    }, 

    onLoad: function () {
        this.subscribes = []

        let gameStatus$ = Casino.Game.singleGame.tableInfo.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {     
                this.setState(gameStatus)
            },  
        });  
        
        let pokers$ = Casino.Game.singleGame.tableInfo.propChange$.pokers
        this.subscribes[this.subscribes.length] = pokers$.subscribe({
            next: pokers => {      
                this.setPoker(pokers)
            },   
        }); 

        let hitSpots$ = Casino.Game.singleGame.result.propChange$.hitSpots
        this.subscribes[this.subscribes.length] = hitSpots$.subscribe({
            next: hitSpots => {
                this.onHitSpotsUpdate(hitSpots)
            },  
        }); 
    },

    onHitSpotsUpdate: function(hitSpots) {
        this.resetBlinkNode()
                                
        for(let key in hitSpots) {    
            if (hitSpots[key] == Config.spotID.Banker) {
                this.blinkBanker()
            } else if (hitSpots[key] == Config.spotID.Player) { 
                this.blinkPlayer()
            } else if (hitSpots[key] == Config.spotID.Tie) { 
                this.blinkTie()
            }
        }

        this.showWinMoney()
    },

    showWinMoney: function() {
        let totalAmount = Casino.Game.singleGame.result.totalAmount
        if(totalAmount <= 0) return
        this.winLab.string = Casino.Tool.getThousandsOfBits(totalAmount)
        this.winNode.active = true
        this.winNode.opacity = 1
        this.winNode.runAction(cc.sequence(cc.delayTime(WIN_DELAY_SHOW_SEC), cc.fadeIn(WIN_FADE_IN_SEC)))
        Casino.MusicControl.winMoney()
    },

    setState: function(state) {
        let nodeActive = (
            state ==  "" + Config.gameState.Dealing || 
            state ==  "" + Config.gameState.Computing 
        )

        this.node.active = nodeActive

        if(nodeActive == false) {
            this.bankerPoker.reset() 
            this.playerPoker.reset()
            this.resetBlinkNode()
        }
    }, 

    resetBlinkNode:function(){
        this.winNode.active = false

        this.bankerWin.stopBlink()
        this.playerWin.stopBlink()
        this.tieWin.stopBlink()

        this.playerLose.active = false
        this.bankerLose.active = false
    },

    blinkBanker: function() {
        this.bankerWin.startBlink()
        this.playerLose.active = true
        Casino.MusicControl.baccaratWhichWin(Config.Spot.Banker, Casino.Game.singleGame.tableInfo.bankerTotal)
    },

    blinkPlayer: function() {
        this.playerWin.startBlink()
        this.bankerLose.active = true
        Casino.MusicControl.baccaratWhichWin(Config.Spot.Player, Casino.Game.singleGame.tableInfo.playerTotal)
    },

    blinkTie: function() {
        this.tieWin.startBlink()
        Casino.MusicControl.baccaratWhichWin(Config.Spot.Tie, Casino.Game.singleGame.tableInfo.playerTotal)
    },

    setPoker: function(pokers) {  
        var playerPokers = []
        var bankerPokers = []
        for (var i in pokers)
        {   
            if (i % 2 == 0){  
                this.bankerPoker.setPoker(i / 2 - 1, pokers[i].toLowerCase())
                bankerPokers[bankerPokers.length] = pokers[i]
            }
            else { 
                this.playerPoker.setPoker((parseInt(i) + 1) / 2 - 1, pokers[i].toLowerCase()) 
                playerPokers[playerPokers.length] = pokers[i]
            }
        }
        let game = Casino.Game
        let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
        this.playerPoker.setCountLabel(table.playerTotal)
        this.bankerPoker.setCountLabel(table.bankerTotal)
    },
});
