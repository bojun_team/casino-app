//---winnerBlink
var BlinkView = require("BlinkView"); 
cc.Class({
    extends: cc.Component,

    properties: {
        title:BlinkView,
        bar:BlinkView,
    },

    startBlink: function() {
        this.node.active = true
        this.title.startBlink()
        this.bar.startBlink()
    },
    stopBlink: function(){ 
        this.title.stopBlink()
        this.bar.stopBlink()
        this.node.active = false
    }
});
