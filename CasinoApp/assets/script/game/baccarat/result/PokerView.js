/*
    功能描述：控制目前該顯示出什麼牌及點數
    實作內容：
       1 poker換圖
       2 顯示點數 
*/

cc.Class({
    extends: cc.Component,

    properties: {
        countLab:cc.Label,
        pokerSprite:[cc.Sprite], 
        pokerAtlas: cc.SpriteAtlas 
    },

    // use this for initialization
    onLoad: function () {
        
        this.reset()
        var spriteFrame = this.spriteFrame;
        var textureURL = this.textureURL;
        if (this.imgsName == null) return
        for (var key in this.imgsName)
        {
            this.setPoker(key, this.imgsName[key])
        }
    },
    onDestroy: function(){

    },
    setPoker: function(index, imgName){  
        if(index < 0) return;
        if (this.imgsName == null) this.imgsName = []
        this.imgsName[index] = imgName
        this.pokerSprite[index].spriteFrame = this.pokerAtlas.getSpriteFrame(imgName)
        this.pokerSprite[index].node.active = true
    },
    setCountLabel: function(count)
    { 
        this.countLab.string =  "" + count
    },
    reset: function(){ 
        for(var key in this.pokerSprite)
        {
            this.pokerSprite[key].node.active = false
        }
    }
});
