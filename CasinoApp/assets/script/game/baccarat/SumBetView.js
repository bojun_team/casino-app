/*
    功能描述：總投注金額
    實作內容：從Game(or 其它model)裡面訂閱目前有投注的金額並加總。
*/ 
import Tool from "../../tool/Tool"
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        sumBetLab:cc.Label,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },

    // use this for initialization
    onLoad: function () {
        this.subscribes = []
        let betSum$ = Casino.Game.spotMgr.propChange$.betSum
        this.subscribes[this.subscribes.length] = betSum$.subscribe({
            next: betSum => {     
                this.setSumBet(betSum)
            },  
        }); 
        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {  
                this.setState(gameStatus) 
            },  
        }); 
    },

    setSumBet:function(betSum){
        this.sumBetLab.string = Tool.conversionNumber(betSum) 
    },
    
    setState:function(gameStatus){
        if ("6" == gameStatus){
            Casino.Game.betSum = 0
        }
    },

});
