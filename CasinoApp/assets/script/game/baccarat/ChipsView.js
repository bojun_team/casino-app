/*
    功能描述：玩家目前選擇的籌碼
    實作內容：
        1 於Game訂閱玩家目前選擇的籌碼，並顯示為按鈕，預設為最小的籌碼。
        2 被選擇的按鈕為隱藏，改成為站起的的籌碼圖片。
        3 更改籌碼的按鈕
*/ 
import Config from '../../constants/Config'
import RxJsMgr from '../../model/RxJsMgr'
import Casino from '../../model/Casino'

var ChipsViewItem = require("ChipsViewItem");
cc.Class({
    extends: cc.Component,

    properties: {
        chips:[ChipsViewItem]
    },
    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    },
    // use this for initialization
    onLoad: function () { 
        //設定按鈕的index
        for (var index in this.chips){
            this.chips[index].setIndex(index)
        }

        //設定目前選的籌碼
        const choseUseChips = Casino.Game.getChoseUseChips()
        this.setSelectChips(choseUseChips) 
        
        //------訂閱
        this.subscribes = []    
        let choseUseChips$ = Casino.Game.chipMgr.propChange$.choseUseChips
        this.subscribes[this.subscribes.length] = choseUseChips$.subscribe({
            next: choseUseChips => {   
                this.setSelectChips(choseUseChips)  
            },  
        });    
    },

    //--給creator拉的
    openChipPicker:function(){
        RxJsMgr.gameUIState.isHideChipPicker = false
    },

    // 設定選擇的籌碼
    setSelectChips: function(choseUseChips) {
        for (var key in this.chips){
            this.chips[key].setDisButton(false)
        }
        for(var index in choseUseChips){
            if (index > 2) break
            this.chips[index].setSprite(choseUseChips[index])
            this.chips[index].setChose(false)
            if (key == Casino.Game.getUseChipIndex()){
                this.chips[index].setChose(true)
            }

            this.chips[index].setDisButton(true)
        } 
        this.updateChipsPos() // 調整可使用的籌碼位置(靠右對齊)
    },
    updateChipsPos:function() { // 調整可使用的籌碼位置(靠右對齊)
        let noUseChipCount = 0
        for (var key in this.chips) {
            let chipsViewItem = this.chips[key]
            let isUseChip = chipsViewItem.button.interactable
            chipsViewItem.node.active = isUseChip
            if(isUseChip == false) {
                noUseChipCount++
            }
        }

        const offsetX = (Casino.Game.tableName != Config.TableName.MultipleTable) ? 210 : 168
        for (var key in this.chips) {
            let chipsViewItem = this.chips[key]
            let chipsViewItemPosY = chipsViewItem.node.getPosition().y
            chipsViewItem.node.setPosition(cc.v2(noUseChipCount*offsetX, chipsViewItemPosY));
        }
    }

});
