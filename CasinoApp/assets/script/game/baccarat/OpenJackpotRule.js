
// OpenJackpotRule.js
// 功能： 開啟Jackpot規則頁面
import localizeMgr from '../../tool/localize/LocalizeMgr'
import Config from '../../constants/Config'
import NativeConnect from '../../native/NativeConnect'
import RxJsMgr from '../../model/RxJsMgr'
import Tool from '../../tool/Tool'

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    onLoad: function () {
        this.node.on('click', this.onClickButton, this.node) 
    }, 
    onDestroy: function() {
        this.node.off('click', this.onClickButton, this.node) 
    },

    onClickButton() {
        let url = Config.getJackpotRuleURL() 
        let labelString = localizeMgr.getLocalizedString("JackpotRule", localizeMgr.getCurrentLanguage()) 
        NativeConnect.openFullScreenWebView(url, labelString)
    },
    
});
