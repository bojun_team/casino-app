/*
    功能描述：統計目前投注的莊閒比。
    實作內容：
        1 訂閱 BetingPool  處理目前投注的莊閒比, 人數, 及金額並顯示
        2 訂閱 betZoneType 當切換玩法時，更新成相對應的即時彩池
        3 訂閱 gameStatus  當中場時清除資料
*/ 
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        playerBar:cc.Sprite,
        bankerBar:cc.Sprite,
        playerBetCountLab:cc.Label,
        bankerBetCountLab:cc.Label,
        playerBetCreditLab:cc.Label,
        bankerBetCreditLab:cc.Label,
    },
    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },
    onLoad: function () {
        // 注區ID
        this.spotIdArray = [
            Config.spotID.Banker, 
            Config.spotID.Player, 
            Config.spotID.BankerNoWater, 
            Config.spotID.PlayerNoWater
        ]

        // 初始化資料
        this.subscribes = []
        this.betPoolData = []
        this.betZoneType = Config.betZoneType.Standard
        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        this.resetData() 

        // 處理spotIdArray裡所有注區ID的監聽事件
        for(let i=0; i<this.spotIdArray.length; i++) {
            let spotId = String(this.spotIdArray[i])
            let betPool = Casino.Game.getBetingPool(Casino.Game.tableName, Casino.Game.tableID, spotId)
            // 訂閱 spotPlayerCount， 用於畫面顯示目前下注人數
            let spotPlayerCount$ = betPool.propChange$.spotPlayerCount
            this.subscribes[this.subscribes.length] = spotPlayerCount$.subscribe({
                next: spotPlayerCount => {
                    // 根據注區ID，存放相對應的下注人數
                    this.betPoolData[spotId]["spotPlayerCount"] = spotPlayerCount
                    this.updateProgessBar()
                },  
            }); 
            // 訂閱 spotTotal，用於畫面顯示目前下注總金額
            let spotTotal$ = betPool.propChange$.spotTotal
            this.subscribes[this.subscribes.length] = spotTotal$.subscribe({
                next: spotTotal => {  
                    // 根據注區ID，存放相對應的下注總金額
                    this.betPoolData[spotId]["spotTotal"] = spotTotal
                    this.updateProgessBar()
                },  
            });
        }

        // 訂閱 注區玩法狀態，當切換玩法時，更新成相對應的即時彩池
        let betZoneType$ = Casino.Game.singleGame.propChange$.betZoneType
        this.subscribes[this.subscribes.length] = betZoneType$.subscribe({
            next: betZoneType => {
                this.betZoneType = betZoneType
                this.updateProgessBar()
            }
        });

        // 訂閱 遊戲狀態，當中場時清除資料
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {  
                if (gameStatus == "" + Config.gameState.HalfTime) {
                    this.resetData()
                    this.updateProgessBar()
                }
                
            },  
        });  
    },
    // 設定資料為預設值
    resetData: function() {
        // 初始化所有資料
        for(let i=0; i<this.spotIdArray.length; i++) {
            let spotId = this.spotIdArray[i]
             // 彩池資料(預設)
            let data = {
                spotPlayerCount:0,
                spotTotal:0
            }
            this.betPoolData[spotId] = data
        }
    },
    // 更新即時彩池
    updateProgessBar: function() { 
        const basisSize = 20
        const totalSize = cc.director.getWinSize().width - (basisSize * 2)
        
        // 根據現在是標準或免水，設定相對應的人數及總金額
        let bankerBetCount = 0
        let bankerBetCredit = 0
        let playerBetCount = 0
        let playerBetCredit = 0
        switch (this.betZoneType) {                    
            case Config.betZoneType.Standard:
            case Config.betZoneType.West:
                bankerBetCount = Number(this.betPoolData[Config.spotID.Banker]["spotPlayerCount"])
                bankerBetCredit = Number(this.betPoolData[Config.spotID.Banker]["spotTotal"])
                playerBetCount = Number(this.betPoolData[Config.spotID.Player]["spotPlayerCount"])
                playerBetCredit = Number(this.betPoolData[Config.spotID.Player]["spotTotal"])
                break
            case Config.betZoneType.NoWater:
                bankerBetCount = Number(this.betPoolData[Config.spotID.BankerNoWater]["spotPlayerCount"])
                bankerBetCredit = Number(this.betPoolData[Config.spotID.BankerNoWater]["spotTotal"])
                playerBetCount = Number(this.betPoolData[Config.spotID.PlayerNoWater]["spotPlayerCount"])
                playerBetCredit = Number(this.betPoolData[Config.spotID.PlayerNoWater]["spotTotal"])
                break
        }
        
        // 設定顯示文字
        this.bankerBetCountLab.string = String(bankerBetCount)
        this.bankerBetCreditLab.string = String(bankerBetCredit)
        this.playerBetCountLab.string = String(playerBetCount)
        this.playerBetCreditLab.string = String(playerBetCredit)

        // 設定即時彩池的莊閒比
        let allBetCount = bankerBetCount + playerBetCount
        // 如果相等，則各顯示一半
        if (bankerBetCount == playerBetCount){
            this.playerBar.node.width = basisSize + totalSize * 0.5
            this.bankerBar.node.width = basisSize + totalSize * 0.5
        }
        // 否則依照人數比例顯示
        else{
            this.bankerBar.node.width = basisSize + totalSize * (bankerBetCount/allBetCount)
            this.playerBar.node.width = basisSize + totalSize * (playerBetCount/allBetCount)
        }
    }
});
