import RxJsMgr from '../../model/RxJsMgr'
import Tool from '../../tool/Tool'
cc.Class({
    extends: cc.Component,

    properties: {
        prefabList:[cc.Prefab]
    },

    onLoad: function () {
        Tool.setKeepScreenOn(true)
        this.orderMap = {
            videoView:0,
            infoView:10,
            statesView:2,
            betZoneView:3,
            betingPoolView:4,
            chipsView:5,
            roadMapView:6,
            betInfoView:7,
            resultView:8,
            jackpotView:9,
            squintFramePanel:10,
            settingPage:11,
            chipPicker:12, 
            limitPicker:13,  
            VerificationView:9,
        }
        
        this.scheduleOnce(()=>{
            this.loadNode()
        }, 0.1)
    },
    
    updateLoading: function(index) {
        let maxCount = this.prefabList.length
        RxJsMgr.gameUIState.isHideLoading = (index / maxCount) >= 1
        RxJsMgr.gameUIState.loadingBarProgress = index / maxCount
    },

    loadNode: function(index=0) {
        this.updateLoading(index)
        if(index >= this.prefabList.length) return
        let prefab = this.prefabList[index] 
        let zOrder = this.orderMap[prefab.name]
        let newNode = cc.instantiate(prefab);
        this.node.addChild(newNode, zOrder);
        
        this.scheduleOnce(()=>{
            this.loadNode(++index)
        }, 0.1)
    }
});
