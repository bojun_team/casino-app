import Config from '../../constants/Config'
import SignalR from '../../native/SignalR'
import Casino from '../../model/Casino'
cc.Class({
    extends: cc.Component,

    properties: {
        backButton:cc.Button

    },

    onLoad: function() {
        let onGameSceneLeave = function() {
            Casino.Game = null
        }

        let succcessFun = function() { 
            Casino.Tool.goToLobbyScene(onGameSceneLeave)
        }

        let failFun = function(resp, error) {   
            if (resp == Config.requireToServerError.timeOut) {
                let titleDataID = Casino.Localize.AlertTitleError
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.User.logOut(false, false)
                    Casino.Tool.goToLoginScene(onGameSceneLeave)
                })
                return
            }
            SignalR.disconnect()
        } 

        this.backButton.node.on("click", function() {
            Casino.Game.leaveGame(succcessFun, failFun)
        }, this);
        
    }

});
