import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        standardBtn:cc.Button,
        noWaterBtn:cc.Button,
        westBtn:cc.Button,
        modeBlink:cc.Node,
    },
    
    onLoad: function () {
        this.subscribes = []

        // 訂閱注區玩法狀態
        var betZoneType$ = Casino.Game.singleGame.propChange$.betZoneType
        this.subscribes[this.subscribes.length] = betZoneType$.subscribe({
            next: betZoneType => {
                this.betZoneType = betZoneType
                switch (betZoneType) {                    
                    case Config.betZoneType.Standard:
                        this.standardBtn.node.active = true
                        this.noWaterBtn.node.active = false
                        this.westBtn.node.active = false
                        break
                    case Config.betZoneType.NoWater:
                        this.standardBtn.node.active = false
                        this.noWaterBtn.node.active = true
                        this.westBtn.node.active = false
                        break
                    case Config.betZoneType.West:
                        this.standardBtn.node.active = false
                        this.noWaterBtn.node.active = false
                        this.westBtn.node.active = true
                        break
                }
            }
        });

        this.betSum = 0
        this.gameStatus = Config.gameState.CountDown

        // 訂閱遊戲狀態，改變按鈕狀態(可否切換、閃爍等等)。
        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {  
                this.gameStatus = gameStatus 
                this.updateBtns()
            },  
        }); 

        // 訂閱下注金額，有下注不可切換。
        var betSum$ = Casino.Game.spotMgr.propChange$.betSum
        this.subscribes[this.subscribes.length] = betSum$.subscribe({
            next: betSum => {    
                this.betSum = betSum          
                this.updateBtns()
            },  
        });

        // 訂閱局號，用來判斷西洋是否超過30局
        let runNumber$ = table.propChange$.run
        this.subscribes[this.subscribes.length] = runNumber$.subscribe({
            next: runNumber => {     
                // 如果超過30局時，把西洋玩法切換回標準玩法
                if(runNumber > 30 && this.betZoneType == Config.betZoneType.West) {
                    Casino.Game.changeBetZoneType(Casino.Game.tableName, Casino.Game.tableID, Config.betZoneType.Standard, function(cmd, respData, error) { })
                }
            },  
        });
        
        // 標準 按鈕點擊時，切換玩法。
        this.standardBtn.node.on("click", (function () {
            Casino.Game.changeBetZoneType(Casino.Game.tableName, Casino.Game.tableID, Config.betZoneType.NoWater, function(cmd, respData, error) { })
        }), this);

        // 免水 按鈕點擊時，切換玩法。
        this.noWaterBtn.node.on("click", (function () {
            // 超過30局則切換為標準玩法
            if(table.run > 30) {
                Casino.Game.changeBetZoneType(Casino.Game.tableName, Casino.Game.tableID, Config.betZoneType.Standard, function(cmd, respData, error) { })
            } else {
                Casino.Game.changeBetZoneType(Casino.Game.tableName, Casino.Game.tableID, Config.betZoneType.West, function(cmd, respData, error) { })
            }
        }), this);

        // 西洋 按鈕點擊時，切換玩法。
        this.westBtn.node.on("click", (function () {
            Casino.Game.changeBetZoneType(Casino.Game.tableName, Casino.Game.tableID, Config.betZoneType.Standard, function(cmd, respData, error) { })
        }), this);
    },
    
    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    // 更新按鈕狀態
    updateBtns: function() {
        // 如果遊戲狀態不是為倒數中或是有下注金額，則不可以變更玩法           
        if(this.gameStatus != Config.gameState.CountDown ||
           this.betSum > 0) {
            this.standardBtn.interactable = false
            this.noWaterBtn.interactable = false
            this.westBtn.interactable = false
            this.stopBlink()
        } else {
            this.standardBtn.interactable = true
            this.noWaterBtn.interactable = true
            this.westBtn.interactable = true
            this.startBlink()
        }
    },

    startBlink: function(){
        this.stopBlink()
        this.modeBlink.active = true
        // 閃爍的週期時間
        const fadeDuration = 1
        var fadeIn = cc.fadeIn(fadeDuration / 2)
        var delay = cc.delayTime(0.2)
        var fadeOut = cc.fadeOut(fadeDuration / 2)
        var seq = cc.sequence(fadeIn, delay, fadeOut)
        var fadeInOutForever = cc.repeatForever(seq)
        this.modeBlink.runAction(fadeInOutForever)
    },

    stopBlink: function(){
        this.modeBlink.active = false 
        this.modeBlink.stopAllActions()
    },
});
