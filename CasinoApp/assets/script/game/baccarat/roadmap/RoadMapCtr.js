/*
    功能描述：用來控制 路圖 ui的切換
    實作內容：
        1 把三種路圖的關聯拉進來，然後在路圖上註冊回傳事件做切換。
    其它：
        待三種路圖做好才做這個。
*/ 

import Ramda from 'ramda'
var { append } = Ramda
import BaccaratRoad from '../../../tool/baccaratRoad'
var { mapToDownThreeRoad, mapToDragonRoad, mapToBigRoad, predictDownThreeRoadTypes, parserRoadMap, putRoadMapContainer } = BaccaratRoad
import Type from '../../../constants/type'
var { BANKER, PLAYER, TIE } = Type
import Config from '../../../constants/Config'
import Casino from '../../../model/Casino'

var BigRoadView = require("BigRoadView"); 
var DownThreeRoadView = require("DownThreeRoadView"); 
var CubeRoadView = require("CubeRoadView"); 
var DownThreeRoadItem = require("DownThreeRoadItem"); 

const COCKROACH = 2
const SMALL = 1
const BIG_EYE = 0

const BIG_ROAD_ID = 0
const CUBE_ROAD_ID = 1
const DOWN_THREE_ROAD_ID = 2

cc.Class({
    extends: cc.Component,

    properties: {  
        bigRoadView:BigRoadView,
        smallRoadView:DownThreeRoadView,
        downThreeRoad:cc.Node,
        cockroachRoadView:DownThreeRoadView,
        bigEyeRoadView:DownThreeRoadView,
        cubeRoadView:CubeRoadView,
        bankerAskRoadItem:[DownThreeRoadItem],
        playerAskRoadItem:[DownThreeRoadItem],
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
        if (this.changeRoadTimer != null) {
            clearTimeout(this.changeRoadTimer)
        }
    },
    // use this for initialization
    onLoad: function () {
        this.curCubeRoad = []
        this.isLoadingRoad = false 
        this.subscribes = []

        
        
        this.curSelectRoad = BIG_ROAD_ID
        this.roadNodes = []
        this.roadNodes[BIG_ROAD_ID] = this.bigRoadView.node
        this.roadNodes[CUBE_ROAD_ID] = this.cubeRoadView.node
        this.roadNodes[DOWN_THREE_ROAD_ID] = this.downThreeRoad  


        if (Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.creatRoadNode(Casino.Game.tableName, Casino.Game.tableID)
        }
    },
    getPredictRoadMap:function(cubeRoad, isPlayer){
        let nextType = PLAYER
        if (isPlayer) nextType = BANKER
        const nextBigRoad = mapToBigRoad(append({ type: nextType, value: 0 }, cubeRoad))
        const smallRoad = mapToDownThreeRoad(SMALL)(nextBigRoad)   
        const cockroachRoad = mapToDownThreeRoad(COCKROACH)(nextBigRoad)   
        const bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(nextBigRoad)  
        return { smallRoad:this.getLastRoadData(smallRoad), cockroachRoad:this.getLastRoadData(cockroachRoad), bigEyeRoad:this.getLastRoadData(bigEyeRoad)}
    },

    getLastRoadData:function(roadData){
        let wLength = roadData.length 
        if (wLength == 0) return
        let hLength = roadData[wLength - 1].length
        return roadData[wLength - 1][hLength - 1]
    },
    askBankerBigRoad:function(){ 
        if(this.curCubeRoad.length == 0) return
        const nextBigRoad = mapToBigRoad(append({ type: BANKER, value: 0 }, this.curCubeRoad))
        this.bigRoadView.startAskRoad(nextBigRoad)
    },
    askPlayerBigRoad:function(){ 
        if(this.curCubeRoad.length == 0) return
        const nextBigRoad = mapToBigRoad(append({ type: PLAYER, value: 0 }, this.curCubeRoad))
        this.bigRoadView.startAskRoad(nextBigRoad)
    },
    askBankerCubeRoad:function(){ 
        if(this.curCubeRoad.length == 0) return
        const nextCubeRoad = append({ type: BANKER, value: "" }, this.curCubeRoad)
        this.cubeRoadView.startAskRoad(nextCubeRoad)
    },
    askPlayerCubeRoad:function(){ 
        if(this.curCubeRoad.length == 0) return
        const nextCubeRoad = append({ type: PLAYER, value: "" }, this.curCubeRoad)
        this.cubeRoadView.startAskRoad(nextCubeRoad)
    },
    askPlayerDownThreeRoad:function(){ 
        if(this.curCubeRoad.length == 0) return
        const nextBigRoad = mapToBigRoad(append({ type: PLAYER, value: 0 }, this.curCubeRoad))
        var smallRoad = mapToDownThreeRoad(SMALL)(nextBigRoad)
        var cockroachRoad = mapToDownThreeRoad(COCKROACH)(nextBigRoad)
        var bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(nextBigRoad)

        this.smallRoadView.startAskRoad(smallRoad) 
        this.cockroachRoadView.startAskRoad(cockroachRoad) 
        this.bigEyeRoadView.startAskRoad(bigEyeRoad) 
    },
    askBankerDownThreeRoad:function(){ 
        if(this.curCubeRoad.length == 0) return
        const nextBigRoad = mapToBigRoad(append({ type: BANKER, value: 0 }, this.curCubeRoad))
        
        var smallRoad = mapToDownThreeRoad(SMALL)(nextBigRoad)   
        var cockroachRoad = mapToDownThreeRoad(COCKROACH)(nextBigRoad)   
        var bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(nextBigRoad)   
        
        this.smallRoadView.startAskRoad(smallRoad) 
        this.cockroachRoadView.startAskRoad(cockroachRoad) 
        this.bigEyeRoadView.startAskRoad(bigEyeRoad) 
    }, 
    changeRoad:function(){  
        // ===== 防止按鈕連點 
        if(this.isLoadingRoad) return
        this.isLoadingRoad = true  
        this.curSelectRoad++
        if (this.curSelectRoad >= this.roadNodes.length) 
            this.curSelectRoad = 0 
        
        this.updateCurrentRoad()
        // ===== 防止按鈕連點
        let delayTime = 500
        this.changeRoadTimer = setTimeout(() => {  
            this.isLoadingRoad = false
        }, delayTime)
    },
    // multiple使用
    setTableData:function(strTableName, strTableID){
        this.creatRoadNode(strTableName, strTableID)
        let table = Casino.Tables.getTableInfo(strTableName, strTableID)
        // 訂閱 gameStatus
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => { 
                this.node.active = (gameStatus == Config.gameState.CountDown || 
                                    gameStatus == Config.gameState.EndBetTime ||
                                    gameStatus == Config.gameState.Maintenance)
                                
            },
        });
    },
    updateCurrentRoad:function(){
        if (this.curCubeRoad == null) return
        for(var key in this.roadNodes){ 
            this.roadNodes[key].active = key == this.curSelectRoad
        } 
        switch (this.curSelectRoad) {
            case CUBE_ROAD_ID:
                this.cubeRoadView.setRoadMap(this.curCubeRoad) 
                break;
            case DOWN_THREE_ROAD_ID:
            {  
                var smallRoad = mapToDownThreeRoad(SMALL)(this.bigRoad)   
                var cockroachRoad = mapToDownThreeRoad(COCKROACH)(this.bigRoad)   
                var bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(this.bigRoad)  
                this.smallRoadView.setRoadMap(smallRoad) 
                this.cockroachRoadView.setRoadMap(cockroachRoad) 
                this.bigEyeRoadView.setRoadMap(bigEyeRoad) 
                if (Casino.Game.tableName != Config.TableName.MultipleTable){
                    const bankerPredictRoadMap = this.getPredictRoadMap(this.curCubeRoad, true) 
                    this.bankerAskRoadItem[0].setData(bankerPredictRoadMap.bigEyeRoad)
                    this.bankerAskRoadItem[1].setData(bankerPredictRoadMap.smallRoad)
                    this.bankerAskRoadItem[2].setData(bankerPredictRoadMap.cockroachRoad) 

                    const playerPredictRoadMap = this.getPredictRoadMap(this.curCubeRoad, false) 
                    this.playerAskRoadItem[0].setData(playerPredictRoadMap.bigEyeRoad)
                    this.playerAskRoadItem[1].setData(playerPredictRoadMap.smallRoad)
                    this.playerAskRoadItem[2].setData(playerPredictRoadMap.cockroachRoad) 
                }
            }
                break;
            default://大路 
                this.bigRoadView.setRoadMap(this.bigRoad ) 
                break;
        }

        
    },
    creatRoadNode:function(strTableName, strTableID){
        this.strTableName = strTableName
        this.strTableID = strTableID
        // 等待下一幀再執行
        this.scheduleOnce(this.delayToCreatRoadNode, 0)
    },
    delayToCreatRoadNode:function(strTableName, strTableID) {
        const tableName = this.strTableName
        const tableID = this.strTableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let roadmap$ = table.propChange$.roadmap
        this.subscribes[this.subscribes.length] = roadmap$.subscribe({
            next: cubeRoad => {  
                this.curCubeRoad = parserRoadMap(cubeRoad)
                if(this.curCubeRoad[0].type == null) {
                    this.curCubeRoad.shift()
                }
                this.bigRoad = mapToBigRoad(this.curCubeRoad) 
                if (cubeRoad.length == 0) {
                    this.bigEyeRoadView.resetRoadMap()
                    this.cubeRoadView.resetRoadMap()
                    this.smallRoadView.resetRoadMap()
                    this.cockroachRoadView.resetRoadMap()
                    this.bigRoadView.resetRoadMap()
                }   
                this.updateCurrentRoad()  
            }
        });    
    },
});
