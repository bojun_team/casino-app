
import Casino from '../../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad: function () {
        let button = this.node.getComponent(cc.Button)
        if(button == null) return
        
        this.subscribes = []

        let {tableName, tableID} = Casino.Game
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let roadmap = table.roadmap
        button.interactable = (roadmap.length > 0)

        let roadmap$ = table.propChange$.roadmap
        this.subscribes[this.subscribes.length] = roadmap$.subscribe({
            next: cubeRoad => {
                button.interactable = (cubeRoad.length > 0)
            }
        })  
    }

});
