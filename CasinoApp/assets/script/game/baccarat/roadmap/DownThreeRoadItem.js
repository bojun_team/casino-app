


cc.Class({
    extends: cc.Component, 
    properties: {
        bule:cc.Sprite,
        red:cc.Sprite, 
    }, 
    onLoad:function(){
        
    },
    setData: function(data)
    {   
        if (data == null) {
            this.bule.node.active = false
            this.red.node.active = false 
            return
        } 
        this.bule.node.active = data.type == 2
        this.red.node.active = data.type == 1
        this.data = data
        this.isHaveData = true 
    },
    startBlink: function(data){ 
        this.setData(data)
        this.isHaveData = false 
        const blinkDuration = 10
        const blinkCount = 20
        var seq = cc.sequence(cc.blink(blinkDuration, blinkCount), cc.callFunc(this.stopBlink, this))
        this.node.runAction(seq) 
    },
    stopBlink: function(){    
        this.node.stopAllActions()
        if (this.isHaveData)
        { 
            this.setData(this.data)
        }
        else
        { 
            this.bule.node.active = false
            this.red.node.active = false
        }  
    },
});
