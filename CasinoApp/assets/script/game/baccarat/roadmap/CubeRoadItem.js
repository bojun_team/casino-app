import Type from '../../../constants/type'
var { BANKER, PLAYER, TIE } = Type
//第三位:: 0:無對子 1:閒對, 2:莊對, 3:閒對+莊對
cc.Class({
    extends: cc.Component,

    properties: {
        bule:cc.Sprite,
        red:cc.Sprite,
        green:cc.Sprite,
        numLab:cc.Label,
        bankerPair:cc.Sprite,
        playerPair:cc.Sprite,
    }, 
    
    setData: function(data)
    {  
        if (data == null) {
            this.bule.node.active = false
            this.red.node.active = false
            this.green.node.active = false
            this.numLab.node.active = false
            this.playerPair.node.active = false
            this.bankerPair.node.active = false
            return
        } 
        this.bule.node.active = data.type == PLAYER
        this.red.node.active = data.type == BANKER
        this.green.node.active = data.type == TIE
        this.bankerPair.node.active = data.pairs == BANKER || data.pairs == 3
        this.playerPair.node.active = data.pairs == PLAYER || data.pairs == 3

        this.numLab.node.active = true
        this.numLab.string = data.value

        this.data = data
        this.isHaveData = true
    },
    startBlink: function(data){ 
        this.setData(data)
        this.isHaveData = false 
        const blinkDuration = 10
        const blinkCount = 20
        var seq = cc.sequence(cc.blink(blinkDuration, blinkCount), cc.callFunc(this.stopBlink, this))
        this.node.runAction(seq) 
    },
    stopBlink: function(){ 
        this.node.stopAllActions()
        if (this.isHaveData)
        { 
            this.setData(this.data)
        }
        else
        { 
            this.bule.node.active = false
            this.red.node.active = false
        }  
    },
});
