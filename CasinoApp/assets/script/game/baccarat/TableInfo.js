import Localize from '../../constants/Localize'
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        countNode: require("HistoryView"),
        tableNameLabel : require("LocalizedLabel"),
        tableIdLabel : cc.Label,
        tableModeLabel : require("LocalizedLabel"),
        roundNumberLabel : cc.Label,
        runNumberLabel : cc.Label,
        pinkColor:cc.Color,
        multipleAtlas: cc.SpriteAtlas,
        tableIdBg: cc.Sprite,
    },

    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    // use this for initialization
    onLoad: function () {
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
        }
    },

    setTableData : function (tableName, tableID) {
        // 設定莊閒和
        if(Casino.Game.tableName == Config.TableName.MultipleTable) {
            this.countNode.setTableData(tableName, tableID)            
        }

        this.setTableNameLabel(tableName)
        this.setTableIdLabel(tableID)

        // 訂閱
        this.subscribes = []
        let table = Casino.Tables.getTableInfo(tableName, tableID)

        if(Casino.Game.tableName == Config.TableName.MultipleTable) {
            if(table != null && table.isR18) {
                this.tableIdBg.spriteFrame = this.multipleAtlas.getSpriteFrame("tableNumTab_pink");
                this.tableIdLabel.node.color = this.pinkColor;
                this.tableModeLabel.node.color = this.pinkColor;
            }
        }

        // 訂閱 tableMode 用於更新 tableModeLabel
        let tableMode$ = table.propChange$.tableMode
        this.subscribes[this.subscribes.length] = tableMode$.subscribe({
            next: tableMode => {
                this.setTableModeLabel(tableMode)
            }
        });

        // 訂閱 round 用於更新 roundNumberLabel
        let round$ = table.propChange$.round
        this.subscribes[this.subscribes.length] = round$.subscribe({
            next: round => { 
                this.setRoundNumberLabel(round)
            }
        }); 

        // 訂閱 run 用於更新 roundNumberLabel
        let run$ = table.propChange$.run
        this.subscribes[this.subscribes.length] = run$.subscribe({
            next: run => { 
                this.setRunNumberLabel(run)
            }
        }); 
    },
    setTableNameLabel : function(tableName) { 
        if(this.tableNameLabel == null) return
        this.tableNameLabel.dataID = Localize.tableNameDataID(tableName)
    },
    setTableIdLabel : function(tableID) { 
        this.tableIdLabel.string = String(tableID)
    },
    setTableModeLabel : function(mode) { 
        this.tableModeLabel.dataID = mode
    },
    setRoundNumberLabel : function(round) { 
        this.roundNumberLabel.string = String(round) 
    },
    setRunNumberLabel : function(run) { 
        this.runNumberLabel.string = String(run) 
    },
});