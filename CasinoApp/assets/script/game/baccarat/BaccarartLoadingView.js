import RxJsMgr from '../../model/RxJsMgr'
import Casino from '../../model/Casino'
import JackpotAlert from "../../tool/JackpotAlert"

cc.Class({
    extends: cc.Component,

    properties: {

    },
    
    onDestroy: function() {
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    onLoad: function () {
        Casino.Tool.getLoadingView().loadingLabel.dataID = Casino.Localize.LoadingTable
        
         this.isPrefabLoaded = false
         this.isModeReady = false
         this.isTimeOut = false
        
         //------訂閱
         this.subscribes = []
         //------訂閱 isHideLimitPicker
         let isHideLoading$ = RxJsMgr.gameUIState.propChange$.isHideLoading
         this.subscribes[this.subscribes.length] = isHideLoading$.subscribe({
             next: isHide => {       
                 this.isPrefabLoaded = isHide
                 this.hideSelf()
             },  
         });       
        
         let loadingBarProgress$ = RxJsMgr.gameUIState.propChange$.loadingBarProgress
         this.subscribes[this.subscribes.length] = loadingBarProgress$.subscribe({
             next: loadingBarProgress => {
                Casino.Tool.getLoadingView().progressBar.progress = loadingBarProgress
             },  
         });   
          
         let isReady$ = Casino.Game.singleGame.propChange$.isReady
         this.subscribes[this.subscribes.length] = isReady$.subscribe({
             next: isReady => {      
                 this.isModeReady = isReady 
                 this.hideSelf()
             },  
         }); 
         this.scheduleOnce(function() { 
             if (this.isPrefabLoaded && this.isModeReady) return 
             this.isTimeOut = true
             let titleDataID = ""
             let msgDataID = Casino.Localize.LoadingGameDataTimeOut
             if (this.isPrefabLoaded && this.isModeReady)
                msgDataID = Casino.Localize.LoadingGameFail
             else if (this.isPrefabLoaded)
                msgDataID = Casino.Localize.LoadingGameDataTimeOut
             else
                msgDataID = Casino.Localize.LoadingGameUITimeOut

             Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                Casino.Game.leaveGame(function(){}, function(){})
                Casino.Tool.goToLobbyScene()
             })
         }.bind(this), 20);//讀取超過20秒跳回大廳
    }, 

    hideSelf: function() {
        if (this.isTimeOut) return
        if (this.isPrefabLoaded && this.isModeReady) {
            const delayTime = 0.1 //晚一點打開
            this.scheduleOnce(function() { 
                Casino.Tool.closeLoadingView()
                // 顯示JP Hint
                JackpotAlert.showBetHintAlert()
            }.bind(this), delayTime);
        }
    }
});
