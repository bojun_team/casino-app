/*
    功能描述：玩家目前選擇的限額
    實作內容：
        1 於Game訂閱玩家目前選擇的限額，並顯示為按鈕。 
        2 更改限額的按鈕文字
*/ 
import Tool from "../../tool/Tool"
import Config from '../../constants/Config'
import RxJsMgr from '../../model/RxJsMgr'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        LimitLab:cc.Label,
        button:cc.Button,
        banSprite:cc.Sprite,
        arrow:cc.Sprite,
    },
    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },
    // use this for initialization
    onLoad: function () {
        // 設定目前的按鈕上的文字
        this.setLimitLabel()

        // 當按鈕被按時
        this.button.node.on("click", function (event) {
            RxJsMgr.gameUIState.isHideLimitPicker = false
        })

        // ===== 訂閱 =====
        this.subscribes = []
       
        // 訂閱 currentLimitStakeIndex 改變範本時更新範本文字
        let currentLimitStakeIndex$ = Casino.Game.limitMgr.propChange$.currentLimitStakeIndex
        this.subscribes[this.subscribes.length] = currentLimitStakeIndex$.subscribe({
            next: index => {      
                this.setLimitLabel()
            },  
        }); 

        // 多台只監聽下注金額
        if(Casino.Game.tableName == Config.TableName.MultipleTable) {
            this.betSumList = []
            let isBet = function () {
                for(let index in this.betSumList) {
                    if(this.betSumList[index] > 0) {
                        return true
                    }
                }
                return false
            }.bind(this)
            for(let index in Casino.Game.games) {
                let game = Casino.Game.games[index]
                this.betSumList[index] = game.betSum
                // 訂閱 betSum 只有在沒有下注的時候可以改變範本
                let betSum$ = game.spotMgr.propChange$.betSum
                this.subscribes[this.subscribes.length] = betSum$.subscribe({
                    next: betSum => {     
                        this.betSumList[index] = betSum
                        this.setLock(isBet() == false)
                    },  
                }); 
                let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
                let gameStatus$ = table.propChange$.gameStatus
                this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
                    next: gameStatus => {  
                        if (gameStatus == Config.gameState.Maintenance){
                            game.betSum = 0
                        }
                    },  
                });
            }
            return
        }
 
        // 單台監聽遊戲狀態跟下注金額
        this.gameState = null
        this.sumBet = null
        let table = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID)
        
        // 訂閱 gameStatus 只有在倒數時可以改變範本
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {      
                this.gameState = gameStatus
                if (this.sumBet == null)  {
                    this.setLock(true)
                }
                else{
                    this.setLock(this.sumBet == 0 && this.gameState == Config.gameState.CountDown)
                }
            },  
        }); 
        
        // 訂閱 betSum 只有在沒有下注的時候可以改變範本
        let betSum$ = Casino.Game.spotMgr.propChange$.betSum
        this.subscribes[this.subscribes.length] = betSum$.subscribe({
            next: betSum => {     
                this.sumBet = betSum
                if (this.gameState == null) {
                    this.setLock(true)
                }
                else {
                    this.setLock(this.sumBet == 0 && this.gameState == Config.gameState.CountDown)
                }

            },  
        }); 
         
    },
    // 設定目前的按鈕上的文字
    setLimitLabel:function(){
        const userLimitStake = Casino.Game.getCurrentLimitStak() 
        this.LimitLab.string = Tool.conversionNumber(Number(userLimitStake.minLimit)) + " - " + Tool.conversionNumber(Number(userLimitStake.maxLimit))

    },
    // 設置按鈕狀態
    setLock:function(type){
        var isLock = !type
        const labLockColor = new cc.color(140,140,140,255)
        const labNoLockColor = new cc.color(229,178,0,255)
        if(isLock){
            this.LimitLab.node.color = labLockColor;
        }else{
            this.LimitLab.node.color = labNoLockColor;
        }
        this.banSprite.node.active = isLock
        this.button.interactable = !isLock
        this.arrow.node.active = !this.banSprite.node.active
    }, 
});
