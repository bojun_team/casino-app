/*
    功能描述：選擇限額
    實作內容：
        1 返回鍵
        2 訂閱可選擇的限額
*/
import Config from '../../constants/Config'
import RxJsMgr from '../../model/RxJsMgr'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        view: cc.Node,

        backButton:cc.Button,

        scrollView:cc.ScrollView,
        limitBtn:cc.Node, 
    },

    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    },
    
    // use this for initialization
    onLoad: function () {
        this.limitBtns = []
        this.limitLabs = []
        this.userLimitStake = Casino.Game.getUserLimitStakes()        

        for(let i=0; i<this.userLimitStake.length; i++) {
            let limitBtn = cc.instantiate(this.limitBtn)
            limitBtn.setPosition(cc.v2(0, -i*200))
            limitBtn.active = true
           
            this.scrollView.content.addChild(limitBtn)
            this.scrollView.content.height = 200 * (i + 1)
            let node = new cc.Node()
            this.limitBtns.push(limitBtn.getComponent(cc.Button))
            this.limitLabs.push(limitBtn.getComponentInChildren(cc.Label))
        } 

        this.setLimitBtn()
        this.setBackButton()
        //------訂閱
        this.subscribes = []
        //------訂閱 isHideLimitPicker
        let isHideLimitPicker$ = RxJsMgr.gameUIState.propChange$.isHideLimitPicker
        this.subscribes[this.subscribes.length] = isHideLimitPicker$.subscribe({
            next: isHide => {   
                this.view.active = !isHide
                if (isHide == false){
                }
            },  
        });        
    },
    //設定目前的按鈕上的文字
    setLimitBtn:function(){ 
        for (var key in this.userLimitStake){
            // if (key < 3)
            this.limitBtns[key].node.active = true
            this.limitLabs[key].string =  Casino.Tool.conversionNumber(Number(this.userLimitStake[key]["_minLimit"])) + " ~ " +  Casino.Tool.conversionNumber(Number(this.userLimitStake[key]["_maxLimit"]))
        }

        this.setLimitBtnClickEvn()
    },

    setLimitBtnClickEvn:function (){
        const labClickColor = new cc.color(38,38,38,255)
        const labCancelClickColor = new cc.color(140,140,140,255)
        for (var key in this.limitBtns){
            const index = key
            this.limitBtns[index].node.on("click", function(event){
                this.setChoseLimit(index)
            }.bind(this), this);
            //按鈕換字顏色
            this.limitBtns[index].node.on(cc.Node.EventType.TOUCH_START, function (event) { 
                 this.limitLabs[index].node.color = labClickColor;
                 return true
            }.bind(this));
            this.limitBtns[index].node.on(cc.Node.EventType.TOUCH_MOVE, function (event) { 
            
            }.bind(this));
            this.limitBtns[index].node.on(cc.Node.EventType.TOUCH_END, function (event) { 
                this.limitLabs[index].node.color = labCancelClickColor;
                // this.setChoseLimit(index)
            }.bind(this));
            this.limitBtns[index].node.on(cc.Node.EventType.TOUCH_CANCEL, function (event) { 
                this.limitLabs[index].node.color = labCancelClickColor;
            }.bind(this));
        }

       

    },

    setChoseLimit:function(index){
        var sendOkFun = function(cmd, resp, error){
            if (resp == Config.requireToServerError.timeOut){
                let titleDataID = Casino.Localize.AlertTitleWarning
                let msgDataID = Casino.Localize.GameTimeOutFail
                Casino.Tool.showAlert(titleDataID, msgDataID, null)
                return
            }
            if (error != null){
                let errorCode = String(resp)
                let errorMessage = String(error)                        
                let titleDataID = Casino.Localize.AlertTitleError
                let msgDataID = Casino.Localize.UnknownError
                Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                    Casino.NativeConnect.exit()
                }, [errorCode, errorMessage])
                return
            }
            RxJsMgr.gameUIState.isHideLimitPicker = true 
        }.bind(this)
        Casino.Game.sendLimitChoseIndex(index,sendOkFun)
    },

    setBackButton:function(){
        this.backButton.node.on("click", 
            function(){
                RxJsMgr.gameUIState.isHideLimitPicker = true
            }.bind(this)
            , this)
    },
});
