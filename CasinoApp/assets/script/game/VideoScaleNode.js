

// VideoScaleNode.js
cc.Class({
    extends: cc.Component,

    properties: {
        bigNode:cc.Node,
        smallNode:cc.Node
    },

    // use this for initialization
    onLoad: function () {

    },
    
    setVideoScale: function(isScaleToBig) {
        if(isScaleToBig == true) {
            this.bigNode.active = false
            this.smallNode.active = true
        }else {
            this.bigNode.active = true
            this.smallNode.active = false
        }
    },

});
