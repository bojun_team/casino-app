/*
    視訊控制

    功能列表：
        1.控制影片播放，暫停，回覆，停止等功能
        2.影片重整功能
        3.監聽 video callback
            a.失敗時重新讀取(2 sec)
            b.成功時顯示重整視訊按鈕
        4.Pop-up頁面跳出時，視訊顯示控制
            a.訂閱 isHideChipPicker，當籌碼選擇頁開關時，更新視訊狀態
            b.訂閱 isHideLimitPicker，當限額選擇頁開關時，更新視訊狀態
            c.訂閱 isOpenSetting，當設定頁開關時，更新視訊狀態 
        5.訂閱 gameStatus，當開始咪牌時，隱藏Video Reflash Btn
        6.訂閱 videoEnabled，當從設定頁開關視頻時，要處理視訊打開或關閉
*/ 
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import Tool from '../tool/Tool'
import RxJsMgr from '../model/RxJsMgr'
import Casino from '../model/Casino'

// baccaratVideo 百家樂 單桌 視訊參數
let _baccaratVideo = {
    x: 0, 
    y: 435, 
    w: 1080, 
    h: 651,
    clipW: 1080, 
    clipH: 600, 
    offsetY: 0, 
    isVideoMute: true
};

let _videoZoomIn = {
    scale: 2,     // 視訊放大時倍率
    offsetX: 0,   // 視訊放大時X位移量
    offsetY: 0    // 視訊放大時Y位移量
}

cc.Class({
    extends: cc.Component, 
    properties: {
        vivoVideoNode:cc.Node,
        vivoClipSizeNode:cc.Node,
        vivoBigVideoNode:cc.Node,
        waterCubeVideoNode:cc.Node,
        videoBg:cc.Node,
        closeVideoNode:cc.Node,
        videoCoverNode: cc.Node,
        videoCoverNode2: cc.Node,
    },
    onDestroy: function(){
        cc.game.off(cc.game.EVENT_SHOW, this.onAppResume, this)
        cc.game.off(cc.game.EVENT_HIDE, this.onAppPause, this)
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
        if (this.reloadVideoTimer != null) {
            clearTimeout(this.reloadVideoTimer)
        }
        this.stopVideo()
    },  
    onAppResume: function() {
        NativeConnect.setVideoMute(true)
        // NativeConnect.setVideoMute(!Casino.User.effectEnabled)
    },
    onAppPause: function() {
        NativeConnect.setVideoMute(true)
    },
    onLoad: function () {
        // 設定監聽到背景及返回App
        cc.game.on(cc.game.EVENT_SHOW, this.onAppResume, this)
        cc.game.on(cc.game.EVENT_HIDE, this.onAppPause, this)
        
        this.isHideLimitPicker = true
        this.isHideChipPicker = true
        this.isOpenSetting = false
        this.isVideoPrepared = false
        this.videoBg.active = false
        this.videoEnabled = Casino.User.videoEnabled
        this.videoCoverNode.active = false
 
        let setupVideoButton = function() {
            this.videoButton.onBigBtnClickEvent = ()=>{
                if(this.isVideoPrepared == false) return
                NativeConnect.setVideoScale(true, Casino.Game.tableID, _videoZoomIn.scale, _videoZoomIn.offsetX, _videoZoomIn.offsetY) ;
            } 
        
            this.videoButton.onSmallBtnClickEvent = ()=>{
                if(this.isVideoPrepared == false) return
                NativeConnect.setVideoScale(false, Casino.Game.tableID) 
            } 
            // ===== 縮放視訊功能 End =====
 
 
            // 設定 reflash button
            this.videoButton.onReflashBtnClickEvent = ()=> {
                if(this.isVideoPrepared == false) return
                this.onReflash()
            } 
 
            // 設定 close video button (點擊此處可開啟現場畫面)
            this.videoButton.onCloseNodeClickEvent = ()=> {
                this.updateVideoState()
            } 
 
        }.bind(this)
 
        // 讀取 VideoButton prefab ， 因為層次問題 ，所以用動態加載
        cc.loader.loadRes("prefab/Video/VideoButton", function (err, prefab) {
            if(err != null || prefab == null) return
 
            let newNode = cc.instantiate(prefab)
            this.videoButton = newNode.getComponent("VideoButton")
            cc.director.getScene().addChild(newNode)
            
            setupVideoButton()
        }.bind(this))
        
        
        // 設定 close video button (點擊此處可開啟現場畫面)
        this.closeVideoNode.on("click", (function () {
            Casino.User.videoEnabled = true
            this.updateVideoState()
        }), this);

        //------ 監聽 video callback
        NativeConnect.listenVideoInfo(function(cmd, info){  
            switch (cmd) {
                case "didPrepared":
                    this.onRecvVideoDidPrepared()
                    break
                case "VideoError":
                    this.onRecvVideoError(info)
                    break
                default:
                    break
            }
        }.bind(this)) 
        //------ 訂閱
        this.subscribes = []
        //------ 訂閱 isHideChipPicker，當籌碼選擇頁開關時，更新視訊狀態
        let isHideChipPicker$ = RxJsMgr.gameUIState.propChange$.isHideChipPicker
        this.subscribes[this.subscribes.length] = isHideChipPicker$.subscribe({
            next: isHide => {   
                this.isHideChipPicker = isHide
                this.updateVideoState()
            },  
        });  
        //------ 訂閱 isHideLimitPicker，當限額選擇頁開關時，更新視訊狀態
        let isHideLimitPicker$ = RxJsMgr.gameUIState.propChange$.isHideLimitPicker
        this.subscribes[this.subscribes.length] = isHideLimitPicker$.subscribe({
            next: isHide => {
                this.isHideLimitPicker = isHide
                this.updateVideoState()
            },
        });
        //------ 訂閱 isOpenSetting，當設定頁開關時，更新視訊狀態  
        let isOpenSetting$ = Casino.User.propChange$.isOpenSetting
        this.subscribes[this.subscribes.length] = isOpenSetting$.subscribe({
            next: isOpenSetting => {
                this.isOpenSetting = isOpenSetting
                this.updateVideoState()
            }
        });
        // ------ 訂閱videoEnabled，當從設定頁變更時，要處理視訊打開或關閉
        this.isReplayVideo = false
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                // 如果視訊開關從off->on, 則當設定頁關閉時，要重新播放視頻
                this.isReplayVideo = (!this.videoEnabled && videoEnabled) 
                this.videoEnabled = videoEnabled
                // 如果設定頁關閉視訊，則停止播放視訊
                if(!this.videoEnabled) this.stopVideo()
                // 設定視訊背景
                this.videoBg.active = !this.videoEnabled
            },
        });

        //------訂閱 isHideLimitPicker, 等關閉loading才開始顯示視頻
        let isHideLoading$ = RxJsMgr.gameUIState.propChange$.isHideLoading
        this.subscribes[this.subscribes.length] = isHideLoading$.subscribe({
            next: isHide => {  
                if(!isHide) return 
                    
                // 判斷是否播放視頻
                if(!this.videoEnabled) return
                this.playVideo()
            },  
        });

        // 監聽音效，控制影片聲音
        let effectEnabled$ = Casino.User.propChange$.effectEnabled
        this.subscribes.push(effectEnabled$.subscribe({
            next: effectEnabled => {
                // NativeConnect.setVideoMute(!effectEnabled)
                NativeConnect.setVideoMute(true)
            }
        }))

        // 監聽網路設定參數
        let tableInfo = Casino.Tables.getTableInfo(Casino.Game.tableName, Casino.Game.tableID);
        this.subscribes.push(tableInfo.propChange$.videoZoomIn.subscribe({next:(videoZoomIn) => {
            _videoZoomIn = videoZoomIn;
        }}));
        this.subscribes.push(tableInfo.propChange$.baccaratVideo.subscribe({next:(baccaratVideo) => {
            _baccaratVideo = baccaratVideo;
        }}));

        if (!cc.sys.isBrowser) return
        Casino.isResizeVideo$.subscribe({
            next: isResizeVideo => {
                if(isResizeVideo == true) {
                    if(window.resizeVideo == null) return
                    window.resizeVideo()
                }
            }
        })
    },
    // 處理影片讀取失敗
    onRecvVideoError:function(error) { 
        if (this.isReloadVideo) return
        const reloadTime = 200
        this.isReloadVideo = true
        
        this.reloadVideoTimer = setTimeout(() => {
            this.playVideo()   
            this.isReloadVideo = false
        }, reloadTime)
        NativeConnect.stopVideo()
    },
    // 處理影片讀取成功
    onRecvVideoDidPrepared:function() { 
        this.isVideoPrepared = true
        Casino.User.isReflashVideo = false
        this.updateCoverNode(true)
    },
    // 更新視訊狀態
    updateVideoState:function(){
        if (this.isHideChipPicker && this.isHideLimitPicker && !this.isOpenSetting){
           this.playOrContinueVideo()
        }
        else{
            this.pauseVideo()
        }
    },
    // 重新播放或是繼續播放
    playOrContinueVideo: function() {
        if(this.isReplayVideo) {
            this.isReplayVideo = false
            this.playVideo()
        } else {
            this.continueVideo()
        }
    },
    // 影片重整
    onReflash:function(){
        this.stopVideo()
        this.playVideo()
    },
    // 控制影片播放，暫停，回覆，停止
    playVideo:function (){
        Casino.User.isReflashVideo = true
        let url =  "rtmp://" + Casino.Game.getVideoURL(Casino.Game.tableName, Casino.Game.tableID);
        NativeConnect.playVideo(
            _baccaratVideo.x, _baccaratVideo.y, _baccaratVideo.w, _baccaratVideo.h, url, 
            _baccaratVideo.clipW, _baccaratVideo.clipH, _baccaratVideo.offsetY, _baccaratVideo.isVideoMute
        );
        NativeConnect.setVideoVisbile(true)
        this.isPauseVideo = false
        this.isPlayVideo = true
    },
    pauseVideo:function(){ 
        this.isPauseVideo = true
        NativeConnect.pauseVideo()
        NativeConnect.setVideoVisbile(false)
    },
    continueVideo:function(){
        if (!this.isPauseVideo) return
        this.isPauseVideo = false
        NativeConnect.continueVideo()
        NativeConnect.setVideoVisbile(true)
    },
    stopVideo:function(){
        Casino.User.isReflashVideo = false
        this.isVideoPrepared = false
        this.updateCoverNode(false)
        NativeConnect.stopVideo()

        // 設定禁用裝置自動休眠功能
        const delayTime = 1 //晚一點在設定
        this.scheduleOnce(function() { 
            Tool.setKeepScreenOn(true)
        }.bind(this), delayTime);
    },
    // 根據視頻提供商決定是否打開注區的背景蓋色
    updateCoverNode: function(active) {
        let game = Casino.Game
        if(game == null) return

        if(Casino.Tables == null) return

        let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
        if(table == null) return
        if(cc.sys.isBrowser) return
        let videoProvider = table.videoProvider
        
        this.videoCoverNode.active = !table.isR18
        this.videoCoverNode2.active = table.isR18
    } 
});
