/*
    功能描述：選擇籌碼
    實作內容：
        1 返回鍵
        2 選擇三個籌碼
        3 訂閱可選的籌碼並鎖定不可選的籌碼
*/ 
import Config from '../../constants/Config'
import RxJsMgr from '../../model/RxJsMgr'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: { 
        view: cc.Node,

        chipBtns:[require("ChipBtn")],
        acceptBtn:cc.Button,
        backButton:cc.Button,
    },
    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    },
    // use this for initialization
    onLoad: function () {
        // 一般版沒有１０元籌碼，所以移除第一顆，後面往前補，最後一顆位置不動
        if(Config.gameCountry == Config.Country.TW) {
            for(let i = this.chipBtns.length - 2; i > 0; i--) {
                this.chipBtns[i].node.setPosition(this.chipBtns[i-1].node.getPosition())
            }
            this.chipBtns[0].node.removeFromParent()
            this.chipBtns.shift()
        }
        else {
            let lastChipBtns = this.chipBtns[this.chipBtns.length - 1]
            lastChipBtns.node.active = false

            // 將50K籌碼放中間
            this.chipBtns[this.chipBtns.length - 2].node.x = -12
        }
        
        this.haveChips = 0 //有幾種籌碼
        this.choseBtnCount = 0
        this.chips = Config.chips
        this.setChipClickEvn()
        this.view.active = !RxJsMgr.gameUIState.isHideChipPicker
        this.setBackButton()
        //設定選完籌碼要做的事
        this.setAcceptBtnEvn()
        //------訂閱
        this.subscribes = []
        //------訂閱 isHideChipPicker
        let isHideChipPicker$ = RxJsMgr.gameUIState.propChange$.isHideChipPicker
        this.subscribes[this.subscribes.length] = isHideChipPicker$.subscribe({
            next: isHide => {   
                this.view.active = !isHide
                if (!isHide){
                   this.setCurrentSelectChips() 
                }
            },  
        }); 
        //------訂閱 currentLimitStakeIndex
        let currentLimitStakeIndex$ = Casino.Game.limitMgr.propChange$.currentLimitStakeIndex
        this.subscribes[this.subscribes.length] = currentLimitStakeIndex$.subscribe({
            next: index => {     
                this.setCurrentSelectChips() 
            },  
        });  
    }, 
    //設定目前選的籌碼
    setCurrentSelectChips: function(){
        const userLimitStake = Casino.Game.getCurrentLimitStak() 
        //先把所有的籌碼禁止掉
        for (var key in this.chipBtns){
            const index = key
            this.chipBtns[index].setDisabled(true)
            this.chipBtns[index].setChose(false)
            this.chipBtns[index].setLock(false)
        }
        //記錄有多少籌碼
        this.haveChips = 0
        //顯示可以選取的籌碼範圍
        for (var i = 0;i<this.chips.length; i ++){
            if (userLimitStake["_minLimit"] <= this.chips[i] && 
                userLimitStake["_maxLimit"] >= this.chips[i]) {
                this.haveChips = this.haveChips + 1
                this.chipBtns[i].setDisabled(false)
            }
        }
    
        // 設定目前選的籌碼 (只選前三個)
        this.choseBtnCount = 0
        const choseUseChips = Casino.Game.getChoseUseChips()
        for(var key in choseUseChips){
            if (key > 2) break
            const index = key
            for (var i = 0;i<this.chips.length; i ++){
                if (choseUseChips[index] == this.chips[i]){
                    this.chipBtns[i].setChose(true)
                    this.choseBtnCount = this.choseBtnCount + 1
                }
            }
        } 
        this.updateAcceptBtn(this.choseBtnCount)
    },

    setChipClickEvn:function(){
        for(var key in this.chipBtns){ 
            this.chipBtns[key].setClickFun(this.clickBtnEvn.bind(this)) 
        } 
    },

    clickBtnEvn:function(data){
        if (data){
            this.choseBtnCount = this.choseBtnCount  + 1
        }else{
            this.choseBtnCount = this.choseBtnCount  - 1
        }
        this.updateAcceptBtn(this.choseBtnCount)
    },

    updateAcceptBtn:function(count){
        this.acceptBtn.interactable = false
        if(count > 0) this.acceptBtn.interactable = true
        if (count >= 3){
            for(var key in this.chipBtns){
                if (!this.chipBtns[key].chose){
                    this.chipBtns[key].setLock(true)
                }
            }
        }else{
            for(var key in this.chipBtns){
                this.chipBtns[key].setLock(false)
            }
        }
    },

    setAcceptBtnEvn:function(){
         this.acceptBtn.node.on("click", 
            function(){
                var ansChips = []
                for(var key in this.chipBtns){
                    if (this.chipBtns[key].chose){
                        ansChips.push(this.chips[key])
                    }
                }   
                //改變儲存的資料
                var sendOkFun = function(resp, error){
                     if (resp == Config.requireToServerError.timeOut){
                        let titleDataID = Casino.Localize.AlertTitleWarning
                        let msgDataID = Casino.Localize.GameTimeOutFail
                        Casino.Tool.showAlert(titleDataID, msgDataID, null)
                        return
                    }

                    if (error != null){
                        let errorCode = String(resp)
                        let errorMessage = String(error)                        
                        let titleDataID = Casino.Localize.AlertTitleError
                        let msgDataID = Casino.Localize.UnknownError
                        Casino.Tool.showAlert(titleDataID, msgDataID, function() {
                            Casino.NativeConnect.exit()
                        }, [errorCode, errorMessage])
                        return
                    }
                    RxJsMgr.gameUIState.isHideChipPicker = true
                }.bind(this)
                Casino.Game.sendChipsChose(ansChips,sendOkFun)
            }.bind(this)
            , this)
    },
    setBackButton:function(){
        this.backButton.node.on("click", 
            function(){
                RxJsMgr.gameUIState.isHideChipPicker = true
            }.bind(this)
            , this)
    },

});