cc.Class({
    extends: cc.Component,

    properties: {
       button:cc.Button,
       choseSprite:cc.Sprite,
       disabledSprite:cc.Sprite,
    },

    // use this for initialization
    onLoad: function () {
        this.setBtnClickFun()
    },

    setBtnClickFun:function(){
        this.button.node.on("click", 
            function(){
                if (this.lock) return
                if (this.chose){
                    this.chose = false
                    const chose = this.chose
                    this.setChose(chose)
                    this.clickFun(chose)
                }else{
                    this.chose = true
                    const chose = this.chose
                    this.setChose(chose)
                    this.clickFun(chose)
                }
            }.bind(this)
            , this)
    },
    setDisabled:function(isDisabled){ 
        this.disabledSprite.node.active = isDisabled
        this.button.interactable = !isDisabled
    },

    setChose:function(isChose){
        this.chose = isChose
        this.choseSprite.node.active = isChose
    },

    setClickFun:function(clickFun){
        this.clickFun = clickFun
    },

    setLock:function(isLock){
        this.lock = isLock
    }
});
