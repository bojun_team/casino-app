import Localize from '../../constants/Localize'
import Config from '../../constants/Config'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        tableIdLabel : cc.Label,
        roundNumberLabel : cc.Label,
        runNumberLabel : cc.Label,
    },

    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    // use this for initialization
    onLoad: function () {
        // 只有非多台才會跑 onLoad
        if(Casino.Game.tableName != Config.TableName.MultipleTable) {
            this.setTableData(Casino.Game.tableName, Casino.Game.tableID)
        }
    },

    setTableData : function (tableName, tableID) {
        this.setTableIdLabel(tableID)

        // 訂閱
        this.subscribes = []
        let table = Casino.Tables.getTableInfo(tableName, tableID)

        // 訂閱 round 用於更新 roundNumberLabel
        let round$ = table.propChange$.round
        this.subscribes[this.subscribes.length] = round$.subscribe({
            next: round => { 
                this.setRoundNumberLabel(round)
            }
        }); 

        // 訂閱 run 用於更新 roundNumberLabel
        let run$ = table.propChange$.run
        this.subscribes[this.subscribes.length] = run$.subscribe({
            next: run => { 
                this.setRunNumberLabel(run)
            }
        }); 
    },
    setTableIdLabel : function(tableID) { 
        this.tableIdLabel.string = String(tableID)
    },
    setRoundNumberLabel : function(round) { 
        this.roundNumberLabel.string = String(round) 
    },
    setRunNumberLabel : function(run) { 
        this.runNumberLabel.string = String(run) 
    },
});