import Casino from '../../model/Casino'
import RxJsMgr from '../../model/RxJsMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        prefabList:[cc.Prefab]
    },

    onDestroy: function() {
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    onLoad: function () {
        Casino.Tool.setKeepScreenOn(true)
        this.orderMap = {
            videoView:0,
            infoView:1,
            statesView:2,
            betZoneView:3,
            betInfoView:4,
            videoButtonPanel:5,
            settingPage:6,
            chipPicker:7, 
            limitPicker:8,  
        }

        this.initLoadingPage()
    },

    initLoadingPage: function() {
        Casino.Tool.getLoadingView().loadingLabel.dataID = Casino.Localize.LoadingTable
        
        this.isPrefabLoaded = false
        this.isModeReady = false
        this.isTimeOut = false
       
        //------訂閱
        this.subscribes = []

        let loadingBarProgress$ = RxJsMgr.gameUIState.propChange$.loadingBarProgress
        this.subscribes[this.subscribes.length] = loadingBarProgress$.subscribe({
            next: loadingBarProgress => {
               Casino.Tool.getLoadingView().progressBar.progress = loadingBarProgress
            },  
        }); 

        //------訂閱 isHideLimitPicker
        let isHideLoading$ = RxJsMgr.gameUIState.propChange$.isHideLoading
        this.subscribes[this.subscribes.length] = isHideLoading$.subscribe({
            next: isHide => {       
                this.isPrefabLoaded = isHide
                this.hideSelf()
            },  
        });    
        
        let isReady$ = Casino.Game.singleGame.propChange$.isReady
        this.subscribes[this.subscribes.length] = isReady$.subscribe({
            next: isReady => {      
                this.isModeReady = isReady 
                this.hideSelf()
            },  
        }); 

        this.scheduleOnce(function() { 
            if (this.isPrefabLoaded && this.isModeReady) return 
            this.isTimeOut = true
            let titleDataID = ""
            let msgDataID = Casino.Localize.LoadingGameDataTimeOut
            if (this.isPrefabLoaded && this.isModeReady)
               msgDataID = Casino.Localize.LoadingGameFail
            else if (this.isPrefabLoaded)
               msgDataID = Casino.Localize.LoadingGameDataTimeOut
            else
               msgDataID = Casino.Localize.LoadingGameUITimeOut

            Casino.Tool.showAlert(titleDataID, msgDataID, function() {
               Casino.Game.leaveGame(function(){}, function(){})
               Casino.Tool.goToLobbyScene()
            })
        }.bind(this), 20);//讀取超過20秒跳回大廳

        this.scheduleOnce(()=>{
            this.loadNode()
        }, 0.1)
    },
    
    updateLoading: function(index) {
        let maxCount = this.prefabList.length
        RxJsMgr.gameUIState.isHideLoading = (index / maxCount) >= 1
        RxJsMgr.gameUIState.loadingBarProgress = index / maxCount
    },

    loadNode: function(index=0) {
        this.updateLoading(index)
        if(index >= this.prefabList.length) return
        let prefab = this.prefabList[index] 
        let zOrder = this.orderMap[prefab.name]
        let newNode = cc.instantiate(prefab);
        this.node.addChild(newNode, zOrder);
        
        this.scheduleOnce(()=>{
            this.loadNode(++index)
        }, 0.1)
    },

    hideSelf: function() {
        if (this.isTimeOut) return
        if (this.isPrefabLoaded && this.isModeReady) {
            const delayTime = 0.1 //晚一點打開
            this.scheduleOnce(function() { 
                Casino.Tool.closeLoadingView()
            }.bind(this), delayTime);
        }
    }
});
