
import Casino from '../../model/Casino'

let _testHis = []

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // use this for initialization
    onLoad: function () {

    },

    onResetClick: function() {
        _testHis = []
        let tableName = Casino.Game.tableName
        let tableID = Casino.Game.tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let history$ = table.propChange$.history
        history$.next(_testHis)
    },

    onRedClick: function() {
        let red = [1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36]
        let number = parseInt(18*Math.random())
        _testHis.push(red[number])
        let tableName = Casino.Game.tableName
        let tableID = Casino.Game.tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let history$ = table.propChange$.history
        history$.next(_testHis)
    },

    onBlackClick: function() {
        let black = [2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35]
        let number = parseInt(18*Math.random())
        _testHis.push(black[number])
        let tableName = Casino.Game.tableName
        let tableID = Casino.Game.tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let history$ = table.propChange$.history
        history$.next(_testHis)
    },

    onZeroClick: function() {
        _testHis.push(0)
        let tableName = Casino.Game.tableName
        let tableID = Casino.Game.tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let history$ = table.propChange$.history
        history$.next(_testHis)
    },
});
