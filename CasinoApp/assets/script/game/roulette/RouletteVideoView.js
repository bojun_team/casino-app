import Config from '../../constants/Config'
import NativeConnect from '../../native/NativeConnect'
import Tool from '../../tool/Tool'
import RxJsMgr from '../../model/RxJsMgr'
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        videoNode:cc.Node,
        clipSizeNode:cc.Node,
        videoBg:cc.Node,
        closeVideoNode:cc.Node,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
        if (this.reloadVideoTimer != null) {
            clearTimeout(this.reloadVideoTimer)
        }
        this.stopVideo()
    }, 

    onLoad: function () {
        this.url = Casino.Game.getVideoURL(Casino.Game.tableName, Casino.Game.tableID)
        this.isHideLimitPicker = true
        this.isHideChipPicker = true
        this.isOpenSetting = false
        this.isVideoPrepared = false
        this.videoBg.active = false
        this.videoEnabled = Casino.User.videoEnabled
        NativeConnect.setVideoMute(true)
 
        RxJsMgr.gameUIState.videoButtonCallback.onBigBtnClickEvent = ()=>{
            if(this.isVideoPrepared == false) return
            NativeConnect.setVideoScale(true, Casino.Game.tableID) 
        } 
        
        RxJsMgr.gameUIState.videoButtonCallback.onSmallBtnClickEvent = ()=>{
            if(this.isVideoPrepared == false) return
            NativeConnect.setVideoScale(false, Casino.Game.tableID) 
        } 
 
        RxJsMgr.gameUIState.videoButtonCallback.onReflashBtnClickEvent = ()=> {
            if(this.isVideoPrepared == false) return
            this.onReflash()
        } 
        
        RxJsMgr.gameUIState.videoButtonCallback.oncloseBtnClickEvent = ()=> {
            Casino.User.videoEnabled = false
            this.updateVideoState()
        } 

        RxJsMgr.gameUIState.videoButtonCallback.onCloseNodeClickEvent = ()=> {
            Casino.User.videoEnabled = true
            this.updateVideoState()
        }

        // 設定 close video button (點擊此處可開啟現場畫面)
        this.closeVideoNode.on("click", (function () {
            Casino.User.videoEnabled = true
            this.updateVideoState()
        }), this);

        //------ 監聽 video callback
        NativeConnect.listenVideoInfo(function(cmd, info){  
            switch (cmd) {
                case "didPrepared":
                    this.onRecvVideoDidPrepared()
                    break
                case "VideoError":
                    this.onRecvVideoError(info)
                    break
                default:
                    break
            }
        }.bind(this)) 
        //------ 訂閱
        this.subscribes = []
        //------ 訂閱 isHideChipPicker，當籌碼選擇頁開關時，更新視訊狀態
        let isHideChipPicker$ = RxJsMgr.gameUIState.propChange$.isHideChipPicker
        this.subscribes[this.subscribes.length] = isHideChipPicker$.subscribe({
            next: isHide => {   
                this.isHideChipPicker = isHide
                this.updateVideoState()
            },  
        });  
        //------ 訂閱 isHideLimitPicker，當限額選擇頁開關時，更新視訊狀態
        let isHideLimitPicker$ = RxJsMgr.gameUIState.propChange$.isHideLimitPicker
        this.subscribes[this.subscribes.length] = isHideLimitPicker$.subscribe({
            next: isHide => {
                this.isHideLimitPicker = isHide
                this.updateVideoState()
            },
        });
        //------ 訂閱 isOpenSetting，當設定頁開關時，更新視訊狀態  
        let isOpenSetting$ = Casino.User.propChange$.isOpenSetting
        this.subscribes[this.subscribes.length] = isOpenSetting$.subscribe({
            next: isOpenSetting => {
                this.isOpenSetting = isOpenSetting
                this.updateVideoState()
            }
        });
        // ------ 訂閱videoEnabled，當從設定頁變更時，要處理視訊打開或關閉
        this.isReplayVideo = false
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                // 如果視訊開關從off->on, 則當設定頁關閉時，要重新播放視頻
                this.isReplayVideo = (!this.videoEnabled && videoEnabled) 
                this.videoEnabled = videoEnabled
                // 如果設定頁關閉視訊，則停止播放視訊
                if(!this.videoEnabled) this.stopVideo()
                // 設定視訊背景
                this.videoBg.active = !this.videoEnabled
            },
        });

        //------訂閱 isHideLoading, 等關閉loading才開始顯示視頻
        let isHideLoading$ = RxJsMgr.gameUIState.propChange$.isHideLoading
        this.subscribes[this.subscribes.length] = isHideLoading$.subscribe({
            next: isHide => {  
                if(!isHide) return 
                    
                // 判斷是否播放視頻
                if(!this.videoEnabled) return
                this.playVideo()
            },  
        });

        if (!cc.sys.isBrowser) return
        Casino.isResizeVideo$.subscribe({
            next: isResizeVideo => {
                if(isResizeVideo == true) {
                    if(window.resizeVideo == null) return
                    window.resizeVideo()
                }
            }
        })
    },

    // 處理影片讀取失敗
    onRecvVideoError:function(error) { 
        if (this.isReloadVideo) return
        const reloadTime = 200
        this.isReloadVideo = true
        
        this.reloadVideoTimer = setTimeout(() => {
            this.playVideo()   
            this.isReloadVideo = false
        }, reloadTime)
        NativeConnect.stopVideo()
    },
    // 處理影片讀取成功
    onRecvVideoDidPrepared:function() { 
        this.isVideoPrepared = true
        Casino.User.isReflashVideo = false
    },
    // 更新視訊狀態
    updateVideoState:function(){
        if (this.isHideChipPicker && this.isHideLimitPicker && !this.isOpenSetting){
           this.playOrContinueVideo()
        }
        else{
            this.pauseVideo()
        }
    },
    // 重新播放或是繼續播放
    playOrContinueVideo: function() {
        if(this.isReplayVideo) {
            this.isReplayVideo = false
            this.playVideo()
        } else {
            this.continueVideo()
        }
    },
    // 影片重整
    onReflash:function(){
        this.stopVideo()
        this.playVideo()
    },
    // 控制影片播放，暫停，回覆，停止
    playVideo:function () {
        Casino.User.isReflashVideo = true
        let isVideoMute = true
        let game = Casino.Game
        let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
        let videoProvider = table.videoProvider
        let clipNode = this.clipSizeNode
        let videoNode = this.videoNode
        
        let posY = clipNode.getPositionY()
        let videoW = videoNode.width
        let videoH = videoNode.height
        let clipW = clipNode.width
        let clipH = clipNode.height 
        let url =  "rtmp://" + this.url
        NativeConnect.playVideo(0, posY, videoW, videoH, url, clipW, clipH, this.videoOffsetY, isVideoMute)
        NativeConnect.setVideoVisbile(true)
        this.isPauseVideo = false
        this.isPlayVideo = true
    },
    pauseVideo:function(){ 
        this.isPauseVideo = true
        NativeConnect.pauseVideo()
        NativeConnect.setVideoVisbile(false)
    },
    continueVideo:function(){
        if (!this.isPauseVideo) return
        this.isPauseVideo = false
        NativeConnect.continueVideo()
        NativeConnect.setVideoVisbile(true)
    },
    stopVideo:function(){
        Casino.User.isReflashVideo = false
        this.isVideoPrepared = false
        NativeConnect.stopVideo()

        // 設定禁用裝置自動休眠功能
        const delayTime = 1 //晚一點在設定
        this.scheduleOnce(function() { 
            Tool.setKeepScreenOn(true)
        }.bind(this), delayTime);
    },
});
