import Casino from '../../model/Casino'
import RxJsMgr from '../../model/RxJsMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        view: cc.Node,

        bigButton: cc.Node,
        smallButton: cc.Node,
        closeButton: cc.Node,
        reflashButton: cc.Node,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
    },
    
    onLoad: function () {
        this.view.on("click", this.onViewClick, this)
        this.bigButton.on('click', this.onBigBtnClick, this);
        this.smallButton.on('click', this.onSmallBtnClick, this);
        this.closeButton.on('click', this.oncloseBtnClick, this);
        this.reflashButton.on('click', this.onReflashBtnClick, this);

        this.subscribes = []
        //------ 訂閱 isHideVideoButtonPannel
        let isHideVideoButtonPannel$ = RxJsMgr.gameUIState.propChange$.isHideVideoButtonPannel
        this.subscribes[this.subscribes.length] = isHideVideoButtonPannel$.subscribe({
            next: isHideVideoButtonPannel => {   
                this.view.active = !isHideVideoButtonPannel
            },  
        });  
    },

    onViewClick: function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = true
    },
    
    onBigBtnClick: function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = true
        RxJsMgr.gameUIState.videoButtonCallback.onBigBtnClickEvent()
    },

    onSmallBtnClick: function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = true
        RxJsMgr.gameUIState.videoButtonCallback.onSmallBtnClickEvent()
    },

    oncloseBtnClick: function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = true
        RxJsMgr.gameUIState.videoButtonCallback.oncloseBtnClickEvent()
    },

    onReflashBtnClick: function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = true
        RxJsMgr.gameUIState.videoButtonCallback.onReflashBtnClickEvent()
    },
});
