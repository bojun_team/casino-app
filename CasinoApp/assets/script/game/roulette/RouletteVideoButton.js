import Casino from '../../model/Casino'
import RxJsMgr from '../../model/RxJsMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        closeNode: cc.Node,

        openNode: cc.Node,
        openCircle: cc.Node,
        openReflash: cc.Node,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
    },

    // use this for initialization
    onLoad: function () {
        // 註冊按鈕事件
        this.closeNode.on("click", this.onCloseNodeClick, this)
        this.openNode.on("click", this.onOpenNodeClick, this)
        
        this.subscribes = []
        // 註冊視訊開關
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                this.closeNode.active = !videoEnabled
                this.openNode.active = videoEnabled
            },
        })

        // 重整視訊開關
        let isReflashVideo$ = Casino.User.propChange$.isReflashVideo
        this.subscribes[this.subscribes.length] = isReflashVideo$.subscribe({
            next: isReflashVideo => {
                if(isReflashVideo == true) {
                    this.openReflashArrow()
                } else {
                    this.hideReflashArrow()
                }
            },
        })
        //------ 訂閱 isHideLimitPicker，當限額選擇頁開關時，更新視訊狀態
        let isHideVideoButtonPannel$ = RxJsMgr.gameUIState.propChange$.isHideVideoButtonPannel
        this.subscribes[this.subscribes.length] = isHideVideoButtonPannel$.subscribe({
            next: isHideVideoButtonPannel => {
                this.delayToEnableButton()
            },
        });
    },

    onCloseNodeClick:function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = true 
        Casino.User.videoEnabled = true
        RxJsMgr.gameUIState.videoButtonCallback.onCloseNodeClickEvent()
    },

    onOpenNodeClick:function() {
        RxJsMgr.gameUIState.isHideVideoButtonPannel = false
    },

    delayToEnableButton: function() {
        this.node.active = false
        const changeVideoDisabelDuration = 1000
        setTimeout(() => {
            if (this.node != null) {
                if(RxJsMgr.gameUIState.propChange$.isHideLimitPicker.getValue() == false) return
                if(RxJsMgr.gameUIState.propChange$.isHideChipPicker.getValue() == false) return
                if(Casino.User.propChange$.isOpenSetting.getValue() == true) return
                this.node.active = true
            }
                
        }, changeVideoDisabelDuration)
    },

    openReflashArrow: function() {
        this.openReflash.active = true
        this.openCircle.active = false
    },

    hideReflashArrow: function() {
        this.openReflash.active = false
        this.openCircle.active = true
    },
});
