
import Casino from '../../model/Casino'

let HISTORY_TYPE_ZERO = 'ZERO'
let HISTORY_TYPE_RED = 'RED'
let HISTORY_TYPE_BLACK = 'BLACK'
let HISTORY_TYPE = [
    'ZERO', 
    'RED','BLACK','RED',
    'BLACK','RED','BLACK',
    'RED','BLACK','RED',
    'BLACK','BLACK','RED',
    'BLACK','RED','BLACK',
    'RED','BLACK','RED',
    'RED','BLACK','RED',
    'BLACK','RED','BLACK',
    'RED','BLACK','RED',
    'BLACK','BLACK','RED',
    'BLACK','RED','BLACK',
    'RED','BLACK','RED']

cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: cc.ScrollView,
        red:cc.Node,
        zero:cc.Node,
        black:cc.Node,
        arrow:cc.Node,
        content:cc.Node,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
    },
    
    onLoad: function () {
       this.scheduleOnce(this.initView, 0.1)
    },
    
    resetView: function() {
        let node = new cc.Node()

        this.content.removeAllChildren()
        this.content.width = this.node.width-1

        let arrow = cc.instantiate(this.arrow)
        arrow.active = true
        this.content.addChild(arrow)
        arrow.width = this.content.width
    },

    initView: function() {
        let tableName = Casino.Game.tableName
        let tableID = Casino.Game.tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let history$ = table.propChange$.history

        this.subscribes = []
        this.subscribes[this.subscribes.length] = history$.subscribe({
            next: history => {
                this.resetView()
                this.drawHistory(history)
            }
        });    
    },

    drawHistory: function(history) {
        let baseX = 130
        let nextX = 80

        for(let index=0; index<history.length; index++) {
            let number = history[index]
            let newNode = cc.instantiate(this.getHistoryItem(number))
            newNode.active = true
            let label = newNode.getComponentInChildren(cc.Label)
            label.string = String(number)
            newNode.setPosition(baseX + index*nextX, 0)
            this.content.addChild(newNode)
            
            if(newNode.getPositionX() + nextX > this.content.width) {
                this.content.width = this.content.width + nextX
                let arrow = this.content.children[0]
                arrow.width = this.content.width
                this.scrollView.scrollToRight()
            }

            
            if(index > 0) {
                let beforeNode = this.content.children[index]
                let label = beforeNode.getComponentInChildren(cc.Label)
                if(label.string != '0') {
                    beforeNode.width = 10
                    beforeNode.height = 10
                }
            }
        }

    },

    getHistoryItem: function(number) {
        let type = HISTORY_TYPE[number]
        if(type == HISTORY_TYPE_ZERO) return this.zero
        else if(type == HISTORY_TYPE_BLACK) return this.black
        else if(type == HISTORY_TYPE_RED) return this.red
    },
    
});
