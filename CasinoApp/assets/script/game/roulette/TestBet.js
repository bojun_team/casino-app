cc.Class({
    extends: cc.Component,

    properties: {
        bet0Node:cc.Node,
        bet30Node:cc.Node,
        bet6Node:cc.Node,

    },

    // use this for initialization
    onLoad: function () {

    },

    onClick0Bet: function() {
        this.bet0Node.active = !this.bet0Node.active
    },

    onClick3Bet: function() {
        this.bet30Node.active = !this.bet30Node.active
    },

    onClick6Bet: function() {
        this.bet6Node.active = !this.bet6Node.active
    },
    
});
