import Casino from '../model/Casino'
import RxJsMgr from '../model/RxJsMgr'
import Config from '../constants/Config'

// VideoButton.js

cc.Class({
    extends: cc.Component,

    properties: {
        closeNode: cc.Node,

        openNode: cc.Node,
        openCircle: cc.Node,
        openReflash: cc.Node,

        buttonsPanpel: cc.Node,

        bigButton: cc.Node,
        smallButton: cc.Node,
        closeButton: cc.Node,
        reflashButton: cc.Node,

        panelOffset: cc.Node,
    },

    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
    },

    onLoad: function () {
        // 註冊按鈕事件
        this.onCloseNodeClickEvent = () => {}
        this.closeNode.on("click", (function () {
            this.openVideo()
            this.onCloseNodeClickEvent()
        }), this)
        this.openNode.on("click", (function () {
            this.buttonsPanpel.active = true
            // 多台位置位移
            if(Casino.Game.tableName == Config.TableName.MultipleTable) {
                this.panelOffset.setPositionY(160)
            }
        }), this)
        this.buttonsPanpel.on("click", (function () {
            this.buttonsPanpel.active = false
        }), this)

        this.buttonsPanpel.on("click", (function () {
            this.buttonsPanpel.active = false
        }), this)

        this.onBigBtnClickEvent = () => {}
        this.onSmallBtnClickEvent = () => {}
        this.oncloseBtnClickEvent = () => {}
        this.onReflashBtnClickEvent = () => {}

        this.bigButton.on('click', this.onBigBtnClick, this);
        this.smallButton.on('click', this.onSmallBtnClick, this);
        this.closeButton.on('click', this.oncloseBtnClick, this);
        this.reflashButton.on('click', this.onReflashBtnClick, this);

        

        this.subscribes = []
        // 註冊視訊開關
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                this.closeNode.active = !videoEnabled
                this.openNode.active = videoEnabled
            },
        })

        // 重整視訊開關
        let isReflashVideo$ = Casino.User.propChange$.isReflashVideo
        this.subscribes[this.subscribes.length] = isReflashVideo$.subscribe({
            next: isReflashVideo => {
                if(isReflashVideo == true) {
                    this.openReflashArrow()
                } else {
                    this.hideReflashArrow()
                }
            },
        })

        //------ 訂閱 isHideChipPicker，當籌碼選擇頁開關時，更新視訊狀態
        let isHideChipPicker$ = RxJsMgr.gameUIState.propChange$.isHideChipPicker
        this.subscribes[this.subscribes.length] = isHideChipPicker$.subscribe({
            next: isHide => {   
                this.node.active = isHide
            },  
        });  
        //------ 訂閱 isHideLimitPicker，當限額選擇頁開關時，更新視訊狀態
        let isHideLimitPicker$ = RxJsMgr.gameUIState.propChange$.isHideLimitPicker
        this.subscribes[this.subscribes.length] = isHideLimitPicker$.subscribe({
            next: isHide => {
                this.node.active = isHide
            },
        });
        //------ 訂閱 isOpenSetting，當設定頁開關時，更新視訊狀態  
        let isOpenSetting$ = Casino.User.propChange$.isOpenSetting
        this.subscribes[this.subscribes.length] = isOpenSetting$.subscribe({
            next: isOpenSetting => {
                this.node.active = !isOpenSetting
            }
        });
    },

    delayToEnableButton: function() {
        this.node.active = false
        const changeVideoDisabelDuration = 1000
        setTimeout(() => {
            if (this.node != null) {
                if(RxJsMgr.gameUIState.propChange$.isHideLimitPicker.getValue() == false) return
                if(RxJsMgr.gameUIState.propChange$.isHideChipPicker.getValue() == false) return
                if(Casino.User.propChange$.isOpenSetting.getValue() == true) return
                this.node.active = true
            }
                
        }, changeVideoDisabelDuration)
    },

    openVideo:function() { 
        this.delayToEnableButton()
        Casino.User.videoEnabled = true
    },

    onBigBtnClick: function() {
        this.buttonsPanpel.active = false
        this.onBigBtnClickEvent()
    },

    onSmallBtnClick: function() {
        this.buttonsPanpel.active = false
        this.onSmallBtnClickEvent()
    },

    oncloseBtnClick: function() {
        this.delayToEnableButton()
        Casino.User.videoEnabled = false
        this.buttonsPanpel.active = false
        this.oncloseBtnClickEvent()
    },

    onReflashBtnClick: function() {
        this.buttonsPanpel.active = false
        this.onReflashBtnClickEvent()
    },

    openReflashArrow: function() {
        this.openReflash.active = true
        this.openCircle.active = false
    },

    hideReflashArrow: function() {
        this.openReflash.active = false
        this.openCircle.active = true
    },
    
});
