import Debug from '../../debugPanel/DebugFunction'
import { timeout } from 'rxjs/operator/timeout';
import Config from "../../constants/Config"
import Casino from '../../model/Casino'
cc.Class({
    extends: cc.Component,

    properties: {
        dynamicOddMsg:cc.Label,
    },
    // use this for initialization
    onLoad: function () {
        this.COLOR_BET = new cc.Color(229, 178, 0);
        this.COLOR_STOP_BET = new cc.Color(105, 105, 105);
        this.MOVE_GAP = 80

        // 訂閱
        this.subscribes = []
        this.tableName = Casino.Game.tableName
        this.tableID = Casino.Game.tableID
        let table = Casino.Tables.getTableInfo(this.tableName, this.tableID)
        

        // 訂閱 gameStatus
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                const isFirst = this.gameState == null
                this.gameState = gameStatus
                switch (gameStatus) {
                    case Config.gameState.CountDown:
                        this.setMassage("主\n要\n投\n注", this.COLOR_BET, !isFirst)
                        break;
                    case Config.gameState.OpenBankerCardBet:
                        this.setMassage("閒\n家\n發\n牌", this.COLOR_BET, !isFirst)
                        break;
                    case Config.gameState.OpenPlayerCardBet:
                        this.setMassage("莊\n家\n發\n牌", this.COLOR_BET, !isFirst)
                        break;
                    case Config.gameState.EndBetTime:
                    case Config.gameState.OpenBankerCardBetEnd:
                    case Config.gameState.OpenPlayerCardBetEnd:
                        this.setMassage("停\n止\n投\n注", this.COLOR_STOP_BET, !isFirst)
                        break;
                    default:
                        break;
                }
            },
        });
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    }, 
    setMassage:function(msg = '', color = new cc.Color(0, 0, 0), isAnimate = true){
        if (!isAnimate){
            this.dynamicOddMsg.string = msg  
            this.dynamicOddMsg.node.color = color 
            this.dynamicOddMsg.node.opacity = 255 
            return;
        }
        let scaleAct = cc.scaleTo(0.3, 1.3)
        let fadeOut = cc.fadeOut(0.3)
        let scaleAndFadeOut = cc.spawn(scaleAct, fadeOut)  
        var setMsgAct = cc.callFunc(function () { 
            this.dynamicOddMsg.string = msg  
            this.dynamicOddMsg.node.color = color 
            this.dynamicOddMsg.node.opacity = 255
            this.dynamicOddMsg.node.scale = 1 
            let posX = this.dynamicOddMsg.node.getPositionX()
            this.dynamicOddMsg.node.setPositionX(posX + this.MOVE_GAP)
        }, this); 
        let moveIn = cc.moveBy(0.3, -this.MOVE_GAP, 0)
        let seqAct = cc.sequence(scaleAndFadeOut, setMsgAct, moveIn)
        this.dynamicOddMsg.node.runAction(seqAct) 
    },
    onBanker:function(){
        cc.log("onBanker"); 
        Debug.setDynamicOddsState("A", Config.gameState.OpenBankerCardBet)
        setTimeout(() => {
            Debug.setDynamicOddsState("A", Config.gameState.OpenBankerCardBetEnd)
        }, 20000)
    },
    onPlayer:function(){
        cc.log("onPlayer");
        Debug.setDynamicOddsState("A", Config.gameState.OpenPlayerCardBet)
        setTimeout(() => {
            Debug.setDynamicOddsState("A", Config.gameState.OpenPlayerCardBetEnd)
        }, 20000)
    }, 
    onMainBet:function(){
        // this.setMassage("主\n要\n投\n注", this.COLOR_BET)
        cc.log("onMainBet");
        Debug.Openanewbureau("A")
        setTimeout(() => {
            Debug.setDynamicOddsState("A", Config.gameState.EndBetTime)
        }, 20000)
    }
    
    // Stateopenpoker
});
