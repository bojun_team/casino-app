
// MultipleTableReturnMoney.js
/*
    功能描述：處理多台的維護畫面訊息開關
    實作內容：
        1 訂閱 gameStatus來開關維護訊息
*/ 
import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        returnMoney: {
            default: null,
            type: cc.Node,
        },

        acceptBtn: {
            default: null,
            type: cc.Node,
        },
    },

    // use this for initialization
    onLoad: function () {

    },
    
    onDestroy: function() {
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    }, 
    onLoad: function() {

    },
    setTableData: function(tableName, tableID) {
        this.tableName = tableName
        this.tableID = tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)

        this.subscribes = []

        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {
                if(gameStatus == Config.gameState.CancelRun) {
                    this.returnMoney.active = true
                }
            },
        });

        this.acceptBtn.on('click', ()=> {
            this.returnMoney.active = false
        })
    },
    
});
