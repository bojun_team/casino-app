/*
    功能描述：處理多台的開牌顯示
    實作內容：
        2 訂閱 sequence 來顯示撲克牌
        3 訂閱 gameStatus 來開關結果頁
        4 訂閱 hitSpots 來顯示輸贏及贏錢動畫
*/ 
import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: { 

        playerPoker: require("PokerView"),
        playerWin: require("WinnerView"),
        playerWinTitle: require("BlinkView"),
        playerLose:cc.Node,
        playerBg:cc.Sprite,

        bankerPoker: require("PokerView"),
        bankerWin: require("WinnerView"),
        bankerWinTitle: require("BlinkView"),
        bankerLose:cc.Node,
        bankerBg:cc.Sprite,

        tie: require("WinnerView"),
        tieTitle: require("BlinkView"),
        tieBg:cc.Sprite,

        winNode:cc.Node,
        winLab:cc.Label,
        winBlinkNode: require("BlinkView"),

    },
    onDestroy: function(){
        for (let key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    }, 
    // use this for initialization
    onLoad: function () {
    },

    setTableData:function(tableName, tableID){
        let game = Casino.Game.getGame(tableName, tableID)
        
        this.subscribes = []
        let gameStatus$ = game.tableInfo.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {     
                this.setState(gameStatus)
            },  
        });  
        
        let pokers$ = game.tableInfo.propChange$.pokers
        this.subscribes[this.subscribes.length] = pokers$.subscribe({
            next: pokers => {      
                this.setPoker(pokers)
                this.playerPoker.setCountLabel(game.tableInfo.playerTotal)
                this.bankerPoker.setCountLabel(game.tableInfo.bankerTotal)
            },   
        }); 

        let hitSpots$ = game.result.propChange$.hitSpots
        this.subscribes[this.subscribes.length] = hitSpots$.subscribe({
            next: hitSpots => {
                this.onHitSpotsUpdate(hitSpots)

                let totalAmount = game.result.totalAmount
                if(totalAmount <= 0) return
                this.winLab.string = Casino.Tool.getThousandsOfBits(totalAmount)
                this.winNode.active = true
                this.winBlinkNode.startBlink()
            },  
        }); 
    },

    onHitSpotsUpdate: function(hitSpots) {
        this.resetBlinkNode()

        for(var key in hitSpots){    
            if (hitSpots[key] == Config.spotID.Banker || hitSpots[key] == Config.spotID.BankerNoWater) { 
                this.bankerWin.node.active = true
                this.bankerWinTitle.startBlink()
                this.playerLose.active = true
            }
            else if (hitSpots[key] == Config.spotID.Player || hitSpots[key] == Config.spotID.PlayerNoWater) { 
                this.playerWin.node.active = true
                this.playerWinTitle.startBlink()
                this.bankerLose.active = true
            }
            else if (hitSpots[key] == Config.spotID.Tie || hitSpots[key] == Config.spotID.TieNoWater) {   
                this.tieBg.node.active = true
                this.tie.node.active = true
                this.tieTitle.startBlink()
            }
        }
    },

    setState: function(state) {
        let nodeActive = (
            state ==  "" + Config.gameState.Dealing || 
            state ==  "" + Config.gameState.Computing 
        )

        this.node.active = nodeActive

        if(nodeActive == false) {
            this.bankerPoker.reset() 
            this.playerPoker.reset()
            this.resetBlinkNode()
        }
    }, 

    resetBlinkNode:function() {
        this.playerWinTitle.stopBlink()
        this.playerWin.node.active = false
        this.playerLose.active = false
        this.playerBg.node.active = true

        this.bankerWinTitle.stopBlink()
        this.bankerWin.node.active = false
        this.bankerLose.active = false
        this.bankerBg.node.active = true

        this.tieTitle.stopBlink()
        this.tie.node.active = false
        this.tieBg.node.active = false

        this.winBlinkNode.stopBlink()
        this.winNode.active = false
    },

    setPoker: function(pokers) {
        let playerPokers = []
        let bankerPokers = []
        for (let i in pokers) {   
            if (pokers[i] != null && pokers[i] != '') {  
                if (i % 2 == 0){  
                    this.bankerPoker.setPoker(i / 2 - 1, pokers[i].toLowerCase())
                    bankerPokers[bankerPokers.length] = pokers[i]
                }
                else { 
                    this.playerPoker.setPoker((parseInt(i) + 1) / 2 - 1, pokers[i].toLowerCase()) 
                    playerPokers[playerPokers.length] = pokers[i]
                }
            } 
        }
    },
    
});

