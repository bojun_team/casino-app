/*
    功能描述：
        控制多台視訊開關
    實作內容：
        1 因為按鈕的的背景要會轉所以分兩個按鈕做。
*/
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        onButton:cc.Button,
        offButton:cc.Button
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    },  
    // use this for initialization
    onLoad: function () { 
        // 註冊按鈕事件
        this.offButton.node.on("click", (function () {
            this.onSwitchVideo()
        }), this)
        this.onButton.node.on("click", (function () {
            this.onSwitchVideo()
        }), this)
        
        this.subscribes = []
        // 註冊視訊開關
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                this.onButton.node.active = videoEnabled
                this.offButton.node.active = !videoEnabled
            },
        }) 
        
    },
    // 收到視訊關或開的事件
    onSwitchVideo:function(){
        this.node.active = false
        const changeVideoDisabelDuration = 1000
        setTimeout(() => {
            if (this.node != null)
                this.node.active = true
        }, changeVideoDisabelDuration) 
        Casino.User.videoEnabled = !Casino.User.videoEnabled 
    }
});
