/*
    功能描述：
        當視訊開關時，畫面要跟著改變位置。
    實作內容：
        1 監聽視訊開關時，畫面切換位置。
*/
import Casino from '../../model/Casino'
import Config from '../../constants/Config'
cc.Class({
    extends: cc.Component,

    properties: {
        widget:cc.Widget,
        topWhenOpen:0, 
        topWhenClose:0, 
    },
    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    }, 
    onLoad: function () {
        this.subscribes = []
        // 監聽視訊開關時，畫面切換位置。
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                if (videoEnabled) {
                    this.widget.top = this.topWhenOpen
                }
                else{
                    //目前大陸版無jackpot，等jackpot上架後再拿掉。
                    if (Config.JACKPOT_ENABLE){
                        this.widget.top = this.topWhenClose 
                    }
                    else{
                        this.widget.top = this.topWhenClose - 125
                    }
                    
                }
                this.widget.enabled = true  
            },
        });
    },
});
