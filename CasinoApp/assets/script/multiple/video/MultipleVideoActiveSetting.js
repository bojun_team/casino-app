/*
    功能描述：
        視訊開關時關或開的元件
    實作內容：
        監聽視訊開關，並改變元件Active為關或開
*/ 
import Casino from '../../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        isActiveWhenVideoOpen:false, 
    },
    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    }, 

    // use this for initialization
    onLoad: function () {
        this.subscribes = [] //
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => { 
                if (videoEnabled) 
                    this.node.active = !this.isActiveWhenVideoOpen
                else
                    this.node.active = this.isActiveWhenVideoOpen
                
            },
        });
    }, 
});
