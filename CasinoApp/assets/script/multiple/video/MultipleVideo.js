/*
    功能描述：
        多桌專用的視訊播放器
        
    功能列表：
        1 切換各桌視訊
        2 切換頁面時，關閉視訊
        3 關閉視訊
        4 重整視訊
    實作:
        狀態模式
*/ 
import Config from '../../constants/Config'
import NativeConnect from '../../native/NativeConnect'
import RxJsMgr from '../../model/RxJsMgr'
import Tool from "../../tool/Tool"
import Casino from '../../model/Casino'

//-----------基本的狀態------------
var State = function () {};
State.prototype.playVideo = function () {};
State.prototype.pauseVideo = function () {};
State.prototype.changeVideo = function(){};
State.prototype.toOtherPage = function () {};
State.prototype.onError = function(errorData, url){};
State.prototype.onRecvVideoDidPrepared = function(){};
State.prototype.stopVideo = function(){};
State.prototype.enableMaintain = function(isMaintain){};

//------------Class 宣告-----------
var LoadingState = function (multiVideo) {  
    State.apply(this);
    this.multiVideo = multiVideo
};
var PlayState = function (multiVideo) { 
    State.apply(this);
    this.multiVideo = multiVideo
};
var HideState = function (multiVideo) {  
    State.apply(this);
    this.multiVideo = multiVideo
};
var OtherPageState = function (multiVideo, isPlayVideo, isMaitain) {   
    State.apply(this);
    this.multiVideo = multiVideo
    this.isPlayVideoDefault = isPlayVideo
    this.isPlayVideo = isPlayVideo
    this.isMaitainDefault = isMaitain
    this.isMaintain = isMaitain
}; 
var MaintainState = function (multiVideo) {  
    State.apply(this);
    this.multiVideo = multiVideo
    this.multiVideo.stopVideo()
    this.multiVideo.enableMaintain(true)
}; 
//--------------視訊讀取狀態時的處理---------
LoadingState.prototype = new State();
LoadingState.prototype.playVideo = function (url) { 
    this.url = url
    this.multiVideo.playVideo(url)
};
LoadingState.prototype.pauseVideo = function () {
    if (this.reloadVideoTimer != null) {
        clearTimeout(this.reloadVideoTimer)
    }
    this.multiVideo.stopVideo()
    return new HideState(this.multiVideo)
};
LoadingState.prototype.toOtherPage = function () { 
    NativeConnect.pauseVideo()
    NativeConnect.setVideoVisbile(false)
    return new OtherPageState(this.multiVideo, true)
};
LoadingState.prototype.onRecvVideoDidPrepared = function () { 
    return new PlayState(this.multiVideo)
}; 
LoadingState.prototype.changeVideo = function (url) {
    this.url = url 
    this.multiVideo.stopVideo()
    this.multiVideo.playVideo(url)
};
LoadingState.prototype.onError = function(errorData, url){
    const reloadTime = 1000
    this.reloadVideoTimer = setTimeout(() => {
        this.multiVideo.playVideo(url)
    }, reloadTime)
    this.multiVideo.stopVideo()
};
LoadingState.prototype.stopVideo = function(errorData){
    if (this.reloadVideoTimer != null) {
        clearTimeout(this.reloadVideoTimer)
    }
    this.multiVideo.stopVideo()
};
LoadingState.prototype.enableMaintain = function(isMaintain){
    if (!isMaintain) return
    let maintainState = new MaintainState(this.multiVideo)
    return maintainState
}
//--------------播放狀態時的處理----------
PlayState.prototype = new State();
PlayState.prototype.pauseVideo = function () {
    this.multiVideo.stopVideo()
    return new HideState(this.multiVideo)
}
PlayState.prototype.toOtherPage = function () {
    NativeConnect.pauseVideo()
    NativeConnect.setVideoVisbile(false)
    return new OtherPageState(this.multiVideo, true)
}
PlayState.prototype.changeVideo = function (url) { 
    this.multiVideo.stopVideo()
    let loadingState = new LoadingState(this.multiVideo)
    loadingState.playVideo(url)
    return loadingState
}
PlayState.prototype.enableMaintain = function(isMaintain){ 
    if (!isMaintain) return
    let maintainState = new MaintainState(this.multiVideo)
    return maintainState
}
//-----------------去設定 籌碼 及 限額頁 狀態時的處理-------------------
OtherPageState.prototype = new State(); 
OtherPageState.prototype.toOtherPage = function () {//回到主頁面時的處理
    if (this.isMaintain && this.isPlayVideo) { //維護中及視訊播放中時 去 MaintainState
        let maintainState = new MaintainState(this.multiVideo)
        return maintainState
    }
    if (this.isPlayVideoDefault){ 
        if (this.isPlayVideo){ 
            //預設是視訊播放中時，回來也是播放，且進入時是維護中，則重讀視訊
            if (this.isMaitainDefault) 
            {
                let loadingState = new LoadingState(this.multiVideo) 
                loadingState.playVideo(this.multiVideo.curUrl)
                return loadingState    
            }
            else //預設是視訊播放中時，回來也是播放，且進入時非維護中，則繼續播視訊
            {
                NativeConnect.continueVideo()
                NativeConnect.setVideoVisbile(true)
                return new PlayState(this.multiVideo)    
            }
        }
        else //預設是視訊播放中時，回來也是關閉視訊，則停止視訊
        {
            this.multiVideo.stopVideo()
            let hideState = new HideState(this.multiVideo)        
            if (this.isMaintain) hideState.enableMaintain(this.isMaintain)
            return hideState
        }
    }
    else 
    {
        if (this.isPlayVideo){//預設是關閉視訊，回來是視訊播放中，則重讀視訊
            let loadingState = new LoadingState(this.multiVideo)
            loadingState.playVideo(this.url)
            return loadingState     
        }
        else //預設是關閉視訊，回來是關閉視訊，則回到停止視訊狀態
        {
            let hideState = new HideState(this.multiVideo)        
            if (this.isMaintain) hideState.enableMaintain(this.isMaintain)
            return hideState
        }
    } 
}
OtherPageState.prototype.playVideo = function (url) {
    this.isPlayVideo = true
    this.url = url
}
OtherPageState.prototype.pauseVideo = function () {
    this.isPlayVideo = false
}
OtherPageState.prototype.enableMaintain = function(isMaintain){  
    this.isMaintain = isMaintain
}
//-------------關閉 狀態時的處理-----------
HideState.prototype = new State();
HideState.prototype.playVideo = function (url) {
    if (this.isMaintain) {
        let maintainState = new MaintainState(this.multiVideo)
        return maintainState
    }
    let loadingState = new LoadingState(this.multiVideo)
    loadingState.playVideo(url)
    return loadingState
}; 
HideState.prototype.toOtherPage = function () { 
    let otherPageState = new OtherPageState(this.multiVideo, false)
    if (this.isMaintain) otherPageState.enableMaintain(this.isMaintain)
    return otherPageState
}, 
HideState.prototype.enableMaintain = function(isMaintain){
    this.isMaintain = isMaintain
}
//-------------維護 狀態時的處理-----------
MaintainState.prototype = new State(); 
MaintainState.prototype.changeVideo = function (url) { 
    this.multiVideo.stopVideo()
    this.multiVideo.enableMaintain(false)
    let loadingState = new LoadingState(this.multiVideo)
    loadingState.playVideo(url)
    return loadingState
}
MaintainState.prototype.enableMaintain = function(isMaintain, url){
    this.isMaintain = isMaintain
    if (this.isMaintain == false){
        this.multiVideo.enableMaintain(false)
        let loadingState = new LoadingState(this.multiVideo)
        loadingState.playVideo(url)
        return loadingState
    }
}
MaintainState.prototype.pauseVideo = function () {
    this.multiVideo.enableMaintain(false)
    let hideState = new HideState(this.multiVideo)
    hideState.enableMaintain(true)
    return hideState
}
MaintainState.prototype.toOtherPage = function () {
    this.multiVideo.enableMaintain(false)
    let otherPageState = new OtherPageState(this.multiVideo, true, true)
    otherPageState.enableMaintain(true)
    return otherPageState
}
// VideoContext.prototype

cc.Class({
    extends: cc.Component,

    properties: {
        switchVideoNode:cc.Node,
        switchBtnSrollView:require("IFScrollView"), 
        videoSizeNode:cc.Node,
        reflashVideoBtn:cc.Button,
        maintainNode:cc.Node
    },
    onDestroy: function(){
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        }
        if (this.reloadVideoTimer != null) {
            clearTimeout(this.reloadVideoTimer)
        }
        this.stopVideo()
    },  
    // use this for initialization
    onLoad: function () { 
        this.scale = {};
        this.offsetX = {};
        this.offsetY = {};
        this.videoSettings = {};

        for(let i in Casino.Game.games) {
            let game = Casino.Game.games[i]
            this.scale[game.tableID] = 2;
            this.offsetX[game.tableID] = 70;
            this.offsetY[game.tableID] = 0;
        }

        this.statusSwitchBtns = {}

        // 取得視訊列表
        this.videoURL = []
        let games = Casino.Game.games
        for(let i = 0; i < games.length; i ++) {
            let game = games[i]
            this.videoURL[game.tableID] = game.videoURL
        }
        
        // 建立切換視訊按鈕
        let switchBtnHeight = this.switchVideoNode.height
        let count = 0
        let firstTableID = ""
        for (var tableID in this.videoURL) {
            let videoBtnNode = this.switchVideoNode
            if (count == 0){
                firstTableID = tableID
            }
            videoBtnNode = cc.instantiate(this.switchVideoNode);
            this.switchVideoNode.parent.addChild(videoBtnNode) 
            this.setSwitchBtnLabel("normalNode", videoBtnNode, tableID)
            this.setSwitchBtnLabel("pressedNode", videoBtnNode, tableID)
            this.setSwitchBtnLabel("disabledNode", videoBtnNode, tableID)
            this.statusSwitchBtns[tableID] = videoBtnNode.getComponent("IFButton")  
            //加入事件 
            this.addCallBack(this.statusSwitchBtns[tableID], tableID)  
            count++
        }
        this.switchVideoNode.active = false
        this.switchBtnSrollView.content.height = switchBtnHeight * count

        // 開始註冊
        this.subscribes = []
        
        // ------ 訂閱 網路設定參數 videoZoomIn
        for(let i = 0; i < games.length; i ++) {
            let game = games[i]
            let tableInfo = Casino.Tables.getTableInfo(game.tableName, game.tableID);
            this.subscribes.push(tableInfo.propChange$.videoZoomIn.subscribe({next:(videoZoomIn) => {
                this.scale[tableInfo.tableID] = videoZoomIn.scale;
                this.offsetX[tableInfo.tableID] = videoZoomIn.offsetX;
                this.offsetY[tableInfo.tableID] = videoZoomIn.offsetY;
            }}));  
            this.subscribes.push(tableInfo.propChange$.baccaratVideo.subscribe({next:(baccaratVideo) => {
                this.videoSettings[tableInfo.tableID] = baccaratVideo;
            }})); 

        } 
        
        //------ 訂閱 isHideChipPicker，當籌碼選擇頁開關時，更新視訊狀態
        let isHideChipPicker$ = RxJsMgr.gameUIState.propChange$.isHideChipPicker
        this.subscribes[this.subscribes.length] = isHideChipPicker$.subscribe({
            next: isHide => {   
                this.setIsOtherPage(isHide, "isHideChipPicker")
            },  
        });  
        //------ 訂閱 isHideLimitPicker，當限額選擇頁開關時，更新視訊狀態
        let isHideLimitPicker$ = RxJsMgr.gameUIState.propChange$.isHideLimitPicker
        this.subscribes[this.subscribes.length] = isHideLimitPicker$.subscribe({
            next: isHide => {
                this.setIsOtherPage(isHide, "isHideLimitPicker")
            },
        });
        //------ 訂閱 isOpenSetting，當設定頁開關時，更新視訊狀態  
        let isOpenSetting$ = Casino.User.propChange$.isOpenSetting
        this.subscribes[this.subscribes.length] = isOpenSetting$.subscribe({
            next: isOpenSetting => {
                this.setIsOtherPage(isOpenSetting, "isOpenSetting")
            }
        });
        //------ 監聽 video native 的 callback
        NativeConnect.listenVideoInfo(function(cmd, info){  
            switch (cmd) {
                case "didPrepared":
                    this.onRecvVideoDidPrepared()
                    break
                case "VideoError":
                    this.onRecvVideoError(info)
                    break
                default:
                    break
            }
        }.bind(this))

        //將第一個視訊按鈕亮起來
        this.statusSwitchBtns[firstTableID].interactable = false 
        this.curSelectVideoID = firstTableID
        this.curSelectVideoTableName = "Baccarat"
        //------ 訂閱 isHideVideo，當限額選擇頁開關時，更新視訊狀態
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => {
                this.switchBtnSrollView.node.active = videoEnabled 
                // 第一次進來的處理是否開關視訊
                if (this.videoEnabled == null){
                    this.videoEnabled = videoEnabled
                    if (this.videoEnabled){
                        this.state = new LoadingState(this)
                        this.state.playVideo(this.videoURL[firstTableID]) 
                    } 
                    else {
                        this.state = new HideState(this)
                        this.curUrl = this.videoURL[firstTableID]
                    } 
                    return
                }   
                // 之後每一次開關視訊都由此處理
                this.videoEnabled = videoEnabled
                if (this.videoEnabled){
                    let state = this.state.playVideo(this.curUrl)
                    if (state != null) this.state = state 
                }
                else{ 
                    let state = this.state.pauseVideo()
                    if (state != null) this.state = state
                }
            },
        }); 
         
        let setupVideoButton = function() {
            // ===== 縮放視訊功能 Start ===== 
            this.videoButton.onBigBtnClickEvent = ()=>{
                NativeConnect.setVideoScale(true, this.curSelectVideoID, this.scale[this.curSelectVideoID], this.offsetX[this.curSelectVideoID], this.offsetY[this.curSelectVideoID]);
            } 
        
            this.videoButton.onSmallBtnClickEvent = ()=>{
                NativeConnect.setVideoScale(false, this.curSelectVideoID)
            } 
            // ===== 縮放視訊功能 End =====
 
 
            // 設定 reflash button
            this.videoButton.onReflashBtnClickEvent = ()=> {
                this.onReflashVideo()
            } 
        }.bind(this)
 
        // 讀取 VideoButton prefab ， 因為層次問題 ，所以用動態加載
        cc.loader.loadRes("prefab/Video/VideoButton", function (err, prefab) {
            if(err != null || prefab == null) return
 
            let newNode = cc.instantiate(prefab)
            this.videoButton = newNode.getComponent("VideoButton")
            cc.director.getScene().addChild(newNode)
            
            setupVideoButton()
        }.bind(this))


        this.tables = {}
        // 訂閱每一桌的狀態是否為維護中
        for(let i = 0; i < games.length; i ++) {
            let game = games[i]
            let table = Casino.Tables.getTableInfo(game.tableName, game.tableID)
            if (this.tables[game.tableName] == null) {
                this.tables[game.tableName] = {}
            }
            
            this.tables[game.tableName][game.tableID] = table
             // 訂閱 gameStatus
            let tableStatus$ = table.propChange$.tableStatus
            this.subscribes[this.subscribes.length] = tableStatus$.subscribe({
                next: tableStatus => {                     
                    if (game.tableID != this.curSelectVideoID) return 
                    const url = this.videoURL[game.tableID]
                    let state = this.state.enableMaintain(tableStatus != 0, url)
                    if(state != null) this.state = state
                },
            });
        }
    },
    // 開關去其它頁面
    // isOpenOtherPage: 開還是關
    // name: 該頁面的名子
    setIsOtherPage: function(isOpenOtherPage, name){
        if (this[name] == null) {
            this[name] = isOpenOtherPage
            return
        }
        if (this[name] == isOpenOtherPage) return
        this[name] = isOpenOtherPage   
        let state = this.state.toOtherPage()
        if (state != null) this.state = state
    },
    // 對按鈕加入事件
    // btn: 要加入事件的按鈕
    // customEventData: 事件呼叫時可以帶個檔案
    addCallBack: function (btn, customEventData) {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; //这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "MultipleVideo";//这个是代码文件名
        clickEventHandler.handler = "onChangeVideo";
        clickEventHandler.customEventData = customEventData; 
        btn.clickEvents.push(clickEventHandler);
    },
    // 設定多台選單的名字
    // lableName: 要設定lable的名字
    // text: 文字內容
    setSwitchBtnLabel: function(lableName, btnNode, text){
        let labelBtn = btnNode.getChildByName(lableName)
        let labelNode = labelBtn.getChildByName("label")
        let videoBtnLabel = labelNode.getComponent(cc.Label)
        videoBtnLabel.string = text
    },
    // 重整視訊 
    onReflashVideo:function(){ 
        let state = this.state.changeVideo(this.curUrl)
        if (state != null) this.state = state
    },
    onChangeVideo: function(event, customEventData){ 
        // 防止連按
        this.switchBtnSrollView.node.active = false
        const changeVideoDisabelDuration = 1000
        setTimeout(() => {
            if (this.switchBtnSrollView != null) {
                let videoEnabled = Casino.User.videoEnabled
                this.switchBtnSrollView.node.active = videoEnabled
            }
        }, changeVideoDisabelDuration) 
        // 設定現在的視ID
        this.curSelectVideoID = customEventData 
        const tableStatus = this.tables[this.curSelectVideoTableName][this.curSelectVideoID].tableStatus 
        if (tableStatus != 0){ // 維護 or 關閉
            let state = this.state.enableMaintain(true)
            if (state != null) this.state = state
        }
        else{ // 切換視訊
            let state = this.state.changeVideo(this.videoURL[customEventData])
            if (state != null) this.state = state
        }
        for (var key in this.statusSwitchBtns) {
            this.statusSwitchBtns[key].interactable = true 
        }
        this.statusSwitchBtns[customEventData].interactable = false 
        
    },
    // 處理影片讀取成功
    onRecvVideoDidPrepared:function() {  
        Casino.User.isReflashVideo = false
        let state = this.state.onRecvVideoDidPrepared()
        if (state != null) this.state = state 
    },
    // 處理影片讀取失敗
    onRecvVideoError:function(error) { 
        let state = this.state.onError(error, this.curUrl);
        if (state != null) this.state = state 
    }, 
    // 控制影片播放，暫停，回覆，停止
    playVideo:function (url){
        Casino.User.isReflashVideo = true
        this.curUrl = url
        let rtmpUrl = "rtmp://" + url
        let settings = this.videoSettings[this.curSelectVideoID];
        NativeConnect.playVideo(
            settings.x, settings.m_y, settings.w, settings.m_h, rtmpUrl, 
            settings.clipW, settings.m_clipH, settings.offsetY, settings.isVideoMute
        );
        NativeConnect.setVideoVisbile(true) 
    },  
    stopVideo:function(){ 
        Casino.User.isReflashVideo = false
        NativeConnect.stopVideo() 
        // 設定禁用裝置自動休眠功能
        const delayTime = 0.5 //晚一點在設定
        this.scheduleOnce(function() { 
            Tool.setKeepScreenOn(true)
        }.bind(this), delayTime);
    },
    enableMaintain:function(isMaintain){
        this.maintainNode.active = isMaintain
    }
});
