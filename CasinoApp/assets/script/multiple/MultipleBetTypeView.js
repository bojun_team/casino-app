/*
    功能描述：處理多台標準，免水按鈕的狀態
    實作內容：
        1 訂閱注區玩法狀態，更新按鈕選中狀態
        2 訂閱遊戲狀態，不是為倒數中不可切換
        3 訂閱下注金額，有下注不可切換
*/ 
import Config from '../constants/Config'
import Casino from '../model/Casino'
cc.Class({
    extends: cc.Component,

    properties: {
        betZoneType:{
            default:Config.betZoneType.Standard,
            type:cc.Integer,
            tooltip: "1:標準, 2:西洋, 3:免水",
        },
        ifBtn:{
            type:require("IFButton"),
            default:null,
            visible:false,
        },
        gameModeBtnNormalSpriteFrame:{
            default:null,
            displayName:"gameModeBtn_n",
            type:cc.SpriteFrame,
        },
        gameModeBtnClickSpriteFrame:{
            default:null,
            displayName:"gameModeBtn_c",
            type:cc.SpriteFrame,
        },
    },
    onDestroy: function(){
        for (let key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    }, 
    onLoad: function () {
        this.ifBtn = this.node.getComponent("IFButton")
    },
    setTableData: function(tableName, tableID) {
        this.betSum = 0
        this.gameStatus = Config.gameState.CountDown
        this.subscribes = []

        // 訂閱注區玩法狀態，更新按鈕選中狀態
        let betZoneType$ = Casino.Game.getGame(tableName, tableID).propChange$.betZoneType
        this.subscribes[this.subscribes.length] = betZoneType$.subscribe({
            next: betZoneType => {
                this.updateBetZoneTypeButtonStatus(betZoneType)
            }
        });

        // 訂閱遊戲狀態，改變按鈕狀態(可否切換)
        let table = Casino.Tables.getTableInfo(tableName, tableID)
        let gameStatus$ = table.propChange$.gameStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: gameStatus => {  
                this.gameStatus = gameStatus 
                this.updateBtnsInteractable()
            },  
        }); 

        // 訂閱下注金額，有下注不可切換
        let betSum$ = Casino.Game.getSpotMgr(tableName, tableID).propChange$.betSum
        this.subscribes[this.subscribes.length] = betSum$.subscribe({
            next: betSum => {    
                this.betSum = betSum          
                this.updateBtnsInteractable()
            },  
        });

        // 設定玩法按鈕
        this.ifBtn.addClickEvent(function(){
            Casino.Game.changeBetZoneType(tableName, tableID, this.betZoneType, function(cmd, respData, error) { })
        }.bind(this))
    },
    // 更新按鈕選中狀態
    updateBetZoneTypeButtonStatus(betZoneType) {
        let btnNormalSprite = this.ifBtn.normalNode.getComponent(cc.Sprite)
        btnNormalSprite.spriteFrame = (Number(betZoneType) == this.betZoneType) ? (this.gameModeBtnClickSpriteFrame) : (this.gameModeBtnNormalSpriteFrame)
    },
    // 更新按鈕狀態
    updateBtnsInteractable: function() {
        // 如果遊戲狀態不是為倒數中或是有下注金額，則不可以變更玩法 
        let interactable = (this.gameStatus == Config.gameState.CountDown && this.betSum == 0)     
        this.ifBtn.interactable = interactable
    },
});
