const MultipleTableBaccaratItem = cc.Class({
    name: 'MultipleTableBaccaratItem',
    properties: {
        prefab: cc.Prefab,
        order: cc.Integer,
        scriptName: cc.String
    }
});

module.exports = MultipleTableBaccaratItem;
