/*
    功能描述：處理多台的讀取頁面
    實作內容：
        1 訂閱isHideLoading 來移除讀取頁
        2 loadingBarProgress 來更新讀取條
*/ 
import RxJsMgr from '../model/RxJsMgr'
import JackpotAlert from "../tool/JackpotAlert"
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {

    },
    onDestroy: function(){
        for (var key in this.subscribes)
        {
            this.subscribes[key].unsubscribe()
        }
    },

    onLoad: function () {
        Casino.Tool.setKeepScreenOn(true)

        let loadingLabel = Casino.Tool.getLoadingView().loadingLabel
        loadingLabel.string = Casino.Localize.LoadingGetImage
        
        //------訂閱
        this.subscribes = []
        let isHideLoading$ = RxJsMgr.gameUIState.propChange$.isHideLoading
        this.subscribes[this.subscribes.length] = isHideLoading$.subscribe({
            next: isHide => {        
                if (isHide) {
                    Casino.Tool.closeLoadingView()
                    // 顯示JP Hint
                    JackpotAlert.showBetHintAlert()
                }  
                loadingLabel.dataID = Casino.Localize.LoadingTable
            },  
        });  

        let loadingBarProgress$ = RxJsMgr.gameUIState.propChange$.loadingBarProgress
        this.subscribes[this.subscribes.length] = loadingBarProgress$.subscribe({
            next: loadingBarProgress => {  
                let loadingBar = Casino.Tool.getLoadingView().progressBar   
                loadingBar.progress = loadingBarProgress
                if (loadingBar.progress >= 1)
                    loadingLabel.dataID = Casino.Localize.LoadingGetRoadMap
            },  
        });  
    },
});
