/*
    功能描述：處理多台的滑動頁面內各桌檯內容讀取
    實作內容：
        1 依照 讀取桌檯的 prefab ， 並加到場景上。
        2 監聽 Casino.User 的 videoEnabled 來調整 scrollView 的長度
*/ 
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import Casino from '../model/Casino'

cc.Class({ 
    extends: cc.Component,
    properties: {
        content:cc.Node,
        tablesScrollView:require("IFScrollView"),
        tablesWidget:cc.Widget,
        tablesMaskWidget:cc.Widget,

        baccaratTablePrefab:cc.Prefab,
        baccaratItemList:[require("MultipleTableBaccaratItem")]
    },
    onDestroy: function(){
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        } 
    }, 
    onLoad: function () {
        this.subscribes = [] 

        // 監聽 Casino.User 的 videoEnabled 來調整 scrollView 的長度
        let videoEnabled$ = Casino.User.propChange$.videoEnabled
        this.subscribes[this.subscribes.length] = videoEnabled$.subscribe({
            next: videoEnabled => { 
                const videoEnabledTop = 832
                // 判斷是否有jackpot來決定用那一個videoDisabledTop
                let videoDisabledTop = 447.9 
                if (!Config.JACKPOT_ENABLE){
                    videoDisabledTop = 325     
                }                 
                if (videoEnabled) this.tablesWidget.top = videoEnabledTop
                else this.tablesWidget.top = videoDisabledTop
                this.tablesWidget.enabled = true
                this.tablesMaskWidget.enabled = true 
            },
        });
        
        this.tablePrefabList = {
            Baccarat: {
                prefab: this.baccaratTablePrefab,
                scriptName: 'multipleBaccaratTable'
            }
        }

        const createTableDelayTime = 0.1
        this.scheduleOnce(function(){
            this.createTable()
        }.bind(this), createTableDelayTime)
    }, 

    addTable: function(tableName, tableID, index) { 
        let {prefab, scriptName} = this.tablePrefabList[tableName] 
        let tableNode = cc.instantiate(prefab) 
        tableNode.name = tableName + tableID; 
 
        let tableNodeHeight = tableNode.height 
        this.content.addChild(tableNode) 
        this.content.height = ((index+1) * tableNodeHeight) 
        let script = tableNode.getComponent(scriptName)         
        script.instantiateTableItems(this.baccaratItemList, tableName, tableID) 
    },  
  
    createTable: function(index=0) { 
        let game = Casino.Game.games[index] 
        let tableName = game.tableName 
        let tableID = game.tableID 
        this.addTable(tableName, tableID, index); 
  
        if(++index < Casino.Game.games.length) this.createTable(index)  
        else this.subscribesTableChange(); 
    } 
  
    , subscribesTableChange() {
        let games = Casino.Game.games;
        // ------ 訂閱 網路設定參數 videoZoomIn
        for(let i = 0; i < games.length; i ++) {
            let game = games[i]
            let tableInfo = Casino.Tables.getTableInfo(game.tableName, game.tableID);
            this.subscribes.push(tableInfo.propChange$.tableStatus.subscribe({next:(tableStatus) => {
                if(tableStatus == 0) {
                    let tableNode = (this.content).getChildByName(tableInfo.tableName+tableInfo.tableID);
                    if(tableNode == null) {
                       this.addTable(tableInfo.tableName, tableInfo.tableID, this.content.childrenCount);
                    }
                } else {
                    let tableNode = (this.content).getChildByName(tableInfo.tableName+tableInfo.tableID);
                    if(tableNode) {
                        let tableNodeHeight = tableNode.height
                        this.content.height = this.content.height - tableNode.height;
                        tableNode.removeFromParent(true);
                    }
                }
            }}));  
        }   
    }  
      

    
    , removeTable: function(finishCallback) {
        let table = this.content.getComponentInChildren("multipleBaccaratTable")
        if(table) {
            let node = table.node;
            table.node.removeFromParent(true)
            table.node.destroy()
        }

        if(this.content.childrenCount == 0) {
            finishCallback()
            return
        }
        
        this.scheduleOnce(function() {
            this.removeTable(finishCallback)
        }.bind(this), 0.2)
    }
})
