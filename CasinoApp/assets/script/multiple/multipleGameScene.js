/*
    功能描述：處理多台的物件讀取
    實作內容：
        1 依照 orderMap 讀取 prefab ， 並加到場景上。
*/ 
import Config from '../constants/Config'
import NativeConnect from '../native/NativeConnect'
import RxJsMgr from '../model/RxJsMgr'
import Tool from "../tool/Tool"

cc.Class({
    extends: cc.Component,

    properties: {
        prefabList:[cc.Prefab]
    },
    
    onLoad: function () {
        Tool.setKeepScreenOn(true)
        this.orderMap = {
            multipleGamesContainer:1,
            multipleGamesInfoNode:2,
            multipleGamesVideoNode:3,
            multipleGamesLimitsNode:4,
            jackpotView:5,
            multipleGamesChipsNode:6,
            settingPage:7,
            limitPicker:8,
            chipPicker:9, 
        }
        
        this.scheduleOnce(()=>{
            this.loadNode()
        }, 0.1)
    },
    
    updateLoading: function(index) {
        let maxCount = this.prefabList.length
        RxJsMgr.gameUIState.isHideLoading = (index / maxCount) >= 1
        RxJsMgr.gameUIState.loadingBarProgress = index / maxCount
    },

    loadNode: function(index=0) {
        this.updateLoading(index)
        if(index >= this.prefabList.length) return

        let prefab = this.prefabList[index]
        let zOrder = this.orderMap[prefab.name]
        let newNode = cc.instantiate(prefab);
        this.node.addChild(newNode, zOrder);
        
        this.scheduleOnce(()=>{
            this.loadNode(++index)
        }, 0.1)
    }

})
