/*
    功能描述：處理多台的百家樂桌檯內物件的讀取
    實作內容：
        1 依照 baccaratItem.order 讀取桌檯內的 prefab 各物件， 並加到場景上。
*/ 
import Casino from '../model/Casino'
cc.Class({
    extends: cc.Component,

    properties: {
        multipleAtlas: cc.SpriteAtlas,
        bg: cc.Sprite
    },

    onLoad: function () {
    },

    instantiateTableItems:function(baccaratItemList, tableName, tableID, index=0) {
        const table = Casino.Tables.getTableInfo(tableName, tableID);
        if(table != null && table.isR18) {
            this.bg.spriteFrame = this.multipleAtlas.getSpriteFrame("table_bg_pink");
        }

        let baccaratItem = baccaratItemList[index]
        let newNode = cc.instantiate(baccaratItem.prefab)
        this.node.addChild(newNode, baccaratItem.order)
        //傳tableID tableName 進去
        let script = newNode.getComponent(baccaratItem.scriptName)
        script.setTableData(tableName, tableID)

        if(++index < baccaratItemList.length) this.instantiateTableItems(baccaratItemList, tableName, tableID, index)
    }
 
});
