/*
    功能描述：大廳上面的進入按鈕，在一般情況時會閃爍
    實作內容：
        1 繼承 IFButton , 並加上閃爍功能
*/ 
cc.Class({
    extends: require("IFButton"),

    onLoad: function () {
        this._super()
        this.startBlink()
    },
    setToPressed: function () {
        this._super()
        this.stopBlink()
    },
    setToNormal : function () {
        this._super()
        this.startBlink()
    },
    startBlink: function() {
        this.pressedNode.stopAllActions()
        this.pressedNode.opacity = 0
        this.pressedNode.active = true

        // 閃爍的週期時間
        const fadeDuration = 1
        var fadeIn = cc.fadeIn(fadeDuration / 2)
        var delay = cc.delayTime(0.2)
        var delay2 = cc.delayTime(0.75)
        var fadeOut = cc.fadeOut(fadeDuration / 2)
        var seq = cc.sequence(delay2, fadeIn, delay, fadeOut, delay2)
        var fadeInOutForever = cc.repeatForever(seq)
        this.pressedNode.runAction(fadeInOutForever)
    },
    stopBlink: function() {
        this.pressedNode.stopAllActions()
        this.pressedNode.opacity = 255
    },
});
