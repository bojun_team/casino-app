/*
    功能描述：處理多台的維護畫面訊息開關
    實作內容：
        1 訂閱 gameStatus來開關維護訊息
*/ 
import Config from '../constants/Config'
import Casino from '../model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        maintain: {
            default: null,
            type: cc.Node,
        },
        maintainLabel: require("LocalizedLabel"),
        closeNode: cc.Node,
        timeLabel:cc.Label,
    },
    onDestroy: function() {
        for(var key in this.subscribes){ 
            this.subscribes[key].unsubscribe()
        } 
    }, 
    onLoad: function() {

    },
    setTableData: function(tableName, tableID) {
        this.tableName = tableName
        this.tableID = tableID
        let table = Casino.Tables.getTableInfo(tableName, tableID)

        this.subscribes = []

        // 訂閱 gameStatus, 等於Maintenance時開啟this.maintain
        this.updateMaintainActive(table.status)
        let gameStatus$ = table.propChange$.tableStatus
        this.subscribes[this.subscribes.length] = gameStatus$.subscribe({
            next: status => {
                this.updateMaintainActive(status)
            },
        });

        //---------------- 設定現場開放時間及關閉時間
        let openingTimes$ = table.propChange$.openingTimes;
        this.subscribes[this.subscribes.length] = openingTimes$.subscribe({
            next: openingTimes => {
                this.updateOpeningTimes(openingTimes.startingAt, openingTimes.closingAt);
            }
        });
    },
    updateMaintainActive(status) {
        if(status == Config.tableStatus.Normal) {
            this.maintain.active = false;
            this.closeNode.active = false;
        } else if((status & Config.tableStatus.Closed) == Config.tableStatus.Closed) {
            this.maintain.active = true;
            this.maintainLabel.dataID = "TableClose";
            this.closeNode.active = true;
        } else if((status & Config.tableStatus.Suspend) == Config.tableStatus.Suspend) {
            this.maintain.active = true;
            this.maintainLabel.dataID = "TableMaintenance";
            this.closeNode.active = false;
        } else {
            this.maintain.active = true;
            this.closeNode.active = false;
        }
    },
    /** 更新桌檯開放時間 */
    updateOpeningTimes: function(startingAt, closingAt) {
        if(startingAt == null || closingAt == null) return;
        if(startingAt.length > 0 && closingAt.length > 0) {
            let startTime = new Date(startingAt);
            let closeTime = new Date(closingAt);
            let startAM = (startTime.getHours() >= 12) ? ("PM") : ("AM");
            let closeAM = (closeTime.getHours() >= 12) ? ("PM") : ("AM");
            let startHour = pdNum(startTime.getHours(), 2, true);
            let closeHour = pdNum(closeTime.getHours(), 2, true);
            let startMinute = pdNum(startTime.getMinutes(), 2);
            let closeMinute = pdNum(closeTime.getMinutes(), 2);
            
            this.timeLabel.string = `${startAM} ${startHour}:${startMinute} - ${closeAM} ${closeHour}:${closeMinute}`;
        }
    }, 
});

//填充截取法 加 24小時轉12小時
function pdNum(num, length, isHour=false) {
    num = (isHour && num > 12) ? (num-12): num;
    return (Array(length).join("0") + num).slice(-length);
}
