import Rx from 'rxjs'

const _updateState = {
    UNCHECKED: -1,
    ERROR_NO_LOCAL_MANIFEST: 4000,
    ERROR_DOWNLOAD_MANIFEST: 4001,
    ERROR_PARSE_MANIFEST: 4002,
    NEW_VERSION_FOUND: 4003,
    ALREADY_UP_TO_DATE: 4004,
    UPDATE_PROGRESSION: 4005,
    ASSET_UPDATED: 4006,
    ERROR_UPDATING: 4007,
    UPDATE_FINISHED: 4008,
    UPDATE_FAILED: 4009,
    ERROR_DECOMPRESS: 4010
}

var _propChange$ = {
    localVersion: new Rx.BehaviorSubject('0'),
    updateState: new Rx.BehaviorSubject(_updateState.UNCHECKED),
    updateProgress: new Rx.BehaviorSubject(0),
}

var _am = null
var _updating = false
var _checkListener = null
var _updateListener = null

var _deleteAm = function () {
    if(_updateListener != null) {
        cc.eventManager.removeListener(_updateListener);
        _updateListener = null;
    }

    _updating = false

    if (_am != null && !cc.sys.ENABLE_GC_FOR_NATIVE_OBJECTS) {
        _am.release()
        _am = null
    }

}

// This value will be retrieved and appended to the default search path during game startup,
// please refer to samples/js-tests/main.js for detailed usage.
// !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
var _restartGame = function() {
    console.log('jun _restartGame')
    let searchPaths = jsb.fileUtils.getSearchPaths()
    let newPaths = _am.getLocalManifest().getSearchPaths()
    Array.prototype.unshift(searchPaths, newPaths);
    cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths))
    jsb.fileUtils.setSearchPaths(searchPaths)

    console.log('_restartGame : ' + JSON.stringify(newPaths))
    _deleteAm()
    cc.game.restart()

}

var _projectManifest = null

var _versionCompareHandle = function (versionA, versionB) {
    let vA = versionA.split('.');
    let vB = versionB.split('.');
    for (let i = 0; i < vA.length; ++i) {
        let a = parseInt(vA[i]);
        let b = parseInt(vB[i] || 0);
        if (a === b) {
            continue;
        }
        else {
            return a - b;
        }
    }
    if (vB.length > vA.length) {
        return -1;
    }
    else {
        return 0;
    }
}

var HotUpdate = {
    
    UpdateState: _updateState,
    
    propChange$: _propChange$,

    init(projectManifest) {
        _projectManifest = projectManifest
        
        // Hot update is only available in Native build
        if (!cc.sys.isNative) {
            return;
        }

        let storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'remote-asset')
        if (_am != null && !cc.sys.ENABLE_GC_FOR_NATIVE_OBJECTS) {
            _am.release()
            _am = null
        } 
        _am = new jsb.AssetsManager(projectManifest, storagePath);
        if (!cc.sys.ENABLE_GC_FOR_NATIVE_OBJECTS) _am.retain();

        
        if(_am.getLocalManifest().isLoaded() == true) {
            let localVersion = _am.getLocalManifest().getVersion()
            _propChange$.localVersion.next(localVersion)
            console.log('jun HotUpdate getVersion:', _am.getLocalManifest().getVersion())
            console.log('jun HotUpdate getManifestFileUrl:', _am.getLocalManifest().getManifestFileUrl())
            console.log('jun HotUpdate getVersionFileUrl:', _am.getLocalManifest().getVersionFileUrl())
        }
        
        // Setup your own version compare handler, versionA and B is versions in string
        // if the return value greater than 0, versionA is greater than B,
        // if the return value equals 0, versionA equals to B,
        // if the return value smaller than 0, versionA is smaller than B.
        _am.setVersionCompareHandle(_versionCompareHandle)
        
        // Some Android device may slow down the download process when concurrent tasks is too much.
        // The value may not be accurate, please do more test and find what's most suitable for your game.
        if (cc.sys.os === cc.sys.OS_ANDROID) _am.setMaxConcurrentTask(2);

    },

    isNewVersion(callback=(eventCode=_updateState.UNCHECKED, remotelVersion='') => {}) {
        // Hot update is only available in Native build
        if (!cc.sys.isNative) {
            callback(_updateState.ALREADY_UP_TO_DATE)
            return
        }
        
        let storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'remote-asset')
        let am = new jsb.AssetsManager(_projectManifest, storagePath);
        am.setVersionCompareHandle(_versionCompareHandle)

        let checkListener = new jsb.EventListenerAssetsManager(am, (event) => {
            let eventCode = _updateState.UNCHECKED
            switch (event.getEventCode()) {
                case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                    eventCode = _updateState.ERROR_NO_LOCAL_MANIFEST
                    break
                case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
                    eventCode = _updateState.ERROR_DOWNLOAD_MANIFEST
                    break
                case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                    eventCode = _updateState.ERROR_PARSE_MANIFEST
                    break
                case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                    eventCode = _updateState.ALREADY_UP_TO_DATE
                    break
                case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                    eventCode = _updateState.NEW_VERSION_FOUND
                    break
                case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                    return
            }

            let remotelVersion = am.getRemoteManifest().getVersion()
        
            cc.eventManager.removeListener(checkListener);
            if (!cc.sys.ENABLE_GC_FOR_NATIVE_OBJECTS) am.release()

            callback(eventCode, remotelVersion)
        })
        
        cc.eventManager.addListener(checkListener, 1)
        am.checkUpdate()
    },
    
    checkUpdate() {
        if(_am == null) {
            console.log('jun checkUpdate _am == null')
            _propChange$.updateState.next(_updateState.ERROR_AM_NULL)
            return
        }

        if(_updating == true) {
            console.log('jun checkUpdate _updating == true')
            return
        }
        _updating = true

        if(_checkListener != null) {
            cc.eventManager.removeListener(_checkListener)
        }

        _checkListener = new jsb.EventListenerAssetsManager(_am, (event) => {
            let isRemoveListener = false
            let eventCode = _updateState.UNCHECKED
            switch (event.getEventCode()) {
                case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                    console.log('jun checkUpdate  ERROR_NO_LOCAL_MANIFEST')
                    eventCode = _updateState.ERROR_NO_LOCAL_MANIFEST
                    isRemoveListener = true
                    break
                case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
                    console.log('jun checkUpdate  ERROR_DOWNLOAD_MANIFEST')
                    eventCode = _updateState.ERROR_DOWNLOAD_MANIFEST
                    isRemoveListener = true
                    break
                case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                    console.log('jun checkUpdate  ERROR_PARSE_MANIFEST')
                    eventCode = _updateState.ERROR_PARSE_MANIFEST
                    isRemoveListener = true
                    break
                case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                    console.log('jun checkUpdate  ALREADY_UP_TO_DATE')
                    eventCode = _updateState.ALREADY_UP_TO_DATE
                    isRemoveListener = true
                    break
                case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                    console.log('jun checkUpdate  NEW_VERSION_FOUND')
                    eventCode = _updateState.NEW_VERSION_FOUND
                    isRemoveListener = true
                    break
                case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                    return
            }

            if(isRemoveListener == true) {
                cc.eventManager.removeListener(_checkListener);
                _checkListener = null;
                _updating = false;
            }
            _propChange$.updateState.next(eventCode)
        })
        cc.eventManager.addListener(_checkListener, 1)
        _am.checkUpdate()

    },

    hotUpdate() {
        if(_am == null) {
            console.log('jun hotUpdate _am == null')
            return
        }
        
        if(_updating == true) {
            console.log('jun hotUpdate _updating == true')
            return
        }
        _updating = true
        if(_updateListener != null) {
            cc.eventManager.removeListener(_checkListener)
        }

        _updateListener = new jsb.EventListenerAssetsManager(_am, (event) => {
            switch (event.getEventCode()) {
                case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                    _propChange$.updateProgress.next((event.getPercentByFile() / 100))
                    break
                case jsb.EventAssetsManager.UPDATE_FINISHED:
                    console.log('jun hotUpdate UPDATE_FINISHED')
                    _restartGame()
                    break
                case jsb.EventAssetsManager.UPDATE_FAILED:
                    console.log('jun hotUpdate UPDATE_FAILED')
                    _am.downloadFailedAssets()
                    break
            }
        })
        cc.eventManager.addListener(_updateListener, 1)
        _am.update()

    },
    
    restartGame() {
        _restartGame()
    }
}

module.exports = HotUpdate