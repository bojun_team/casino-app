import HotUpdate from '../module/HotUpdate'
import LocalizeMgr from '../../../script/tool/localize/LocalizeMgr'
import Config from '../../../script/constants/Config'
import Casino from '../../../script/model/Casino'

cc.Class({
    extends: cc.Component,

    properties: {
        projectManifest: cc.RawAsset,
        loadingView: require('LoadingView')
    },

    onDestroy: function() {
        if(this.subscribes == null) return
        for (var key in this.subscribes) {
            this.subscribes[key].unsubscribe()
        }
    },

    onLoad: function () {
        // 全域錯誤監聽
        window.__errorHandler = function (filename, lineNumber, message) {
            let errData = {
                msg: message
            }
            Casino.AppWebAPI.logRecord("Error", JSON.stringify(errData));
        };
        
        cc.director.setDisplayStats(false)

        // 設定現在跑的版本
        Config.gameCountry = Casino.NativeConnect.country
        Config.gamePlatform = Casino.NativeConnect.gamePlatform

        // 初始化多國語言
        let language = Config.Language.ZH
        if(Config.gameCountry == Config.Country.CN) {
            language = Config.Language.CN
        }
        let languageList = [
            Config.Language.ZH,
            Config.Language.CN,
        ]
        let csvPathList = [
            "/base_res/localize/common",
            "/base_res/localize/messageApp"
        ]
        LocalizeMgr.init(language, languageList, csvPathList)


        let runLoginScene = () => {
            cc.director.preloadScene('Login', (error) => {
                if(error != null) {
                    console.log('jun no login scene')
                    HotUpdate.restartGame()
                    return
                }
                cc.director.loadScene("Login")
            })
        }

        //------訂閱
        this.subscribes = []  
        this.subscribes[this.subscribes.length] = HotUpdate.propChange$.updateState.subscribe({
            next: updateState => {
                switch (updateState) {
                    case HotUpdate.UpdateState.UNCHECKED:
                        break
                    case HotUpdate.UpdateState.ALREADY_UP_TO_DATE:
                        this.loadingView.progressBar.progress = 1.0 
                        runLoginScene()
                        break
                    case HotUpdate.UpdateState.NEW_VERSION_FOUND:
                        this.loadingView.loadingLabel.dataID = Casino.Localize.LoadingAssetsUpdate
                        HotUpdate.hotUpdate()
                        break
                    case HotUpdate.UpdateState.ERROR_NO_LOCAL_MANIFEST:
                        this.showFailAlert(HotUpdate.UpdateState.ERROR_NO_LOCAL_MANIFEST)
                        break
                    case HotUpdate.UpdateState.ERROR_DOWNLOAD_MANIFEST:
                        this.showFailAlert(HotUpdate.UpdateState.ERROR_DOWNLOAD_MANIFEST)
                        break
                    case HotUpdate.UpdateState.ERROR_PARSE_MANIFEST:
                        this.showFailAlert(HotUpdate.UpdateState.ERROR_PARSE_MANIFEST)
                        break
                }                
            }
        })
        
        this.subscribes[this.subscribes.length] = HotUpdate.propChange$.updateProgress.subscribe({
            next: updateProgress => {
                this.loadingView.progressBar.progress = updateProgress               
            }
        })

        HotUpdate.init(this.projectManifest)
        
        // 手機網頁版 不跑熱更新
        if(cc.sys.isBrowser) {
            console.log('jun web mobile no hot update')

            cc.game.setFrameRate(24)
            if(cc._renderType === cc.game.RENDER_TYPE_CANVAS){  
                cc.renderer.enableDirtyRegion(false)
            }

            // 手機網頁版 將web gl 設成透明畫布 for video
            if(cc._renderType === cc.game.RENDER_TYPE_WEBGL){  
                cc.rendererWebGL._clearColor = cc.color(0,0,0,0);
            }
            

            cc.director.loadScene("Login")
            return 
        }
        
        // Creator 不能跑熱更新
        if (cc.sys.os != cc.sys.OS_IOS && cc.sys.os != cc.sys.OS_ANDROID) {
            cc.director.loadScene("Login")
            return 
        }
        this.checkGameVersion()
    },
    
    // 版本檢查
    checkGameVersion: function() {
        let versionUpdate = function(strVersion, url) {
            let titleDataID = Casino.Localize.AlertTitleNewVersion
            let msgDataID = Casino.Localize.VersionIsOld
            Casino.Tool.closeAllAlert()
            Casino.Tool.showAlert(titleDataID, msgDataID, function() {  
                Casino.NativeConnect.updateApp(url)
            }.bind(this), [strVersion])
        }
        let success = function(haveNewVersion, newVersion, newURL) {
            if(haveNewVersion == true)
                versionUpdate(newVersion, newURL)
            else
                HotUpdate.checkUpdate()
        }
        let fail = function(){ 
            Casino.NativeConnect.exit()
        }
        this.loadingView.loadingLabel.dataID = Casino.Localize.LoadingGetVersion
        Casino.AppWebAPI.checkGameVersion(Casino.NativeConnect.gameVersion, success.bind(this), fail.bind(this))
    },

    showFailAlert: function(errorCode) {
        let titleDataID = Casino.Localize.AlertTitleError
        let msgDataID = Casino.Localize.NoGetVersion
        let failFun = () => {
            Casino.NativeConnect.exit()
        }
        Casino.Tool.showAlert(titleDataID, msgDataID, failFun, [errorCode]) 
    },


});
