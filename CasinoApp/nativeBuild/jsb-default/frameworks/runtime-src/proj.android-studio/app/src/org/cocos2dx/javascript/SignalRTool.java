package org.cocos2dx.javascript;

import android.util.ArrayMap;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;

/**
 * Created by fan on 2017/1/24.
 */

public class SignalRTool {
    public static HubProxy proxy;
    public static HubConnection conn;

    private final static String TAG = "SignalRTool";

    public static void connect(String connectUrl){
        //Log.w("connectUrl ",connectUrl);
        conn = new HubConnection(connectUrl);
        // Create the hub proxy
        proxy = conn.createHubProxy("casinoHub");

        // 暫時沒用到
//        proxy.subscribe(SignalRTool.class);
//        proxy.subscribe(new Object() {
//            @SuppressWarnings("unused")
//            public void messageReceived(String name, String message) {
//                Log.w("ww connect ok",name);
//                Log.w("ww connect ok",message);
//                System.out.println(name + ": " + message);
//            }
//        });
//        proxy.subscribe(SignalRTool.class);

        // Subscribe to the error event
        conn.error(new ErrorCallback() {

            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
                System.out.println("ConnectionError");
                JSConnect.onRecvConnectionInfo("-1", error.getMessage());
                //Log.w("conn error",error.getMessage());

            }
        });

        // Subscribe to the connected event
        conn.connected(new Runnable() {

            @Override
            public void run() {
                System.out.println("Connecting Open");
                JSConnect.onRecvConnectionInfo("1","");
            }
        });

        // Subscribe to the closed event 使用自己網路會走這
        conn.closed(new Runnable() {

            @Override
            public void run() {
                System.out.println("Connection Closed");
                JSConnect.onRecvConnectionInfo("3","");
            }
        });

        // Start the connection
        SignalRFuture<Void> done = conn.start()
                .done(new Action<Void>() {

                    @Override
                    public void run(Void obj) throws Exception {
                        System.out.println("Connecting Strat!");
                        JSConnect.onRecvConnectionInfo("ConnectionStrat","");

                    }
                });

        // Subscribe to the reconnecting event
        conn.reconnecting(new Runnable() {
            @Override
            public void run() {
                System.out.println("Connection Reconnecting");
//                JSConnect.onRecvConnectionInfo("2","");
                conn.disconnect();
            }
        });

        // Subscribe to the connectionSlow event
        conn.connectionSlow(new Runnable() {
            @Override
            public void run() {
                System.out.println("Connection Slow");
                JSConnect.onRecvConnectionInfo("3","");
            }
        });

        // Subscribe to the received event
        conn.received(new MessageReceivedHandler() {
            @Override
            public void onMessageReceived(JsonElement json) {
                System.out.println("RAW received message: " + json.toString());
                try {
                    JSConnect.recvServerData(json.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


        //直接選定要接收的回傳
//        proxy.on("gameStatusUpdateMessageReceived", new SubscriptionHandler1<Object>() {
//            @Override
//            public void run(Object object) {
//                Log.w("wwSubscriptionHandler1", object.toString());
//            }
//        }, Object.class);


    }

    public static void requireServerWithCmd(String cmd, String jsonS, int cbName) throws JSONException, ExecutionException, InterruptedException {
        //Log.w("requireServerWit cmd",cmd);
        //Log.w("requireServerWit jsonS ",jsonS);

        sendInvoke(cmd, jsonParse(jsonS), String.valueOf(cbName));

    }


    public static void sendInvoke(final String cmd, final Object[] arg, final String cbName) throws ExecutionException, InterruptedException, JSONException {
        proxy.invoke(Object.class, cmd, arg).done(new Action<Object>() {
            @Override
            public void run(Object res) throws Exception {
                //Log.w(TAG, "invoke ok");
                //"{"data":res}
                String ansStr = "";
                char[] Char = res.toString().substring(0,1).toCharArray();
                char OneChar=Char[0];
                if(OneChar=='{') {
                    Map<String, Object> resMap = (Map<String, Object>)res;
                    //Log.w("sendInvoke ","6" + resMap.toString());
                    JSONObject obj = new JSONObject(resMap);
                    ansStr = obj.toString();
                }
                else if (OneChar == '[')
                {
                    //Log.w("sendInvoke ","5 res " + res + " " + cmd);
                    ArrayList<Object> resArrayList = (ArrayList<Object>)res;
                    JSONArray jsonArray = new JSONArray();
                    for(Object resArrayItem: resArrayList) {
                        Map<String, Object> resArrayItemMap = (Map<String, Object>)resArrayItem;
                        JSONObject jsonObject = new JSONObject(resArrayItemMap);
                        jsonArray.put(jsonObject);
                    }
                    //Log.w("sendInvoke ","5 jsonArray " + jsonArray.toString() + " " + cmd);
                    ansStr = jsonArray.toString();
                    //Log.w("sendInvoke ","5 ansStr" + ansStr + " " + cmd);
                }
                else
                {
                    //Log.w("sendInvoke ","4");
                    ansStr = res.toString();
                }
                String sendRes = "{" + "\"data\"" + ":" + ansStr + "}";
                if (null == res) {
                    JSConnect.onRecvRequireServerWtihCmd(cmd, "{" + "\"data\"" + ": 0 }", null, cbName);
                } else {
                    JSConnect.onRecvRequireServerWtihCmd(cmd, sendRes, null, cbName);
                }
            }
        }).onError(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                //Log.w(TAG, "invoke error");
                //Log.w("invoke return",throwable.toString());
                try {
                    if (null == throwable) {
                        JSConnect.onRecvRequireServerWtihCmd(cmd, "{" + "\"data\"" + ": 1 }", null, cbName);
                    } else {
                        JSConnect.onRecvRequireServerWtihCmd(cmd, "{" + "\"data\"" + ": 1 }", throwable.toString(), cbName);
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public static Object[] jsonParse(String jsonStr) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonStr);
        Object objs[] = new Object[jsonArray.length()];
        if (jsonArray != null) {
            int len = jsonArray.length();
            for (int i=0;i<len;i++){
                String nextStr = jsonArray.get(i).toString();
                if (nextStr.equals("")){
                    objs[i] = jsonArray.get(i);
                }else{
                    char[] Char = nextStr.substring(0,1).toCharArray();
                    char OneChar=Char[0];
                    if(OneChar=='{') {
                        ArrayMap<String, Object> arg = new ArrayMap<String, Object>();
                        JSONObject obj = new JSONObject();
                        obj = jsonArray.getJSONObject(i);
                        Iterator iterator = obj.keys();
                        while(iterator.hasNext()){
                            String key = (String)iterator.next();
//                            arg.put(key, obj.opt(key));
                            // 判斷value是什麼值
                            String objStr = obj.opt(key).toString();
//                            Log.w("objStr",objStr);
                            if (objStr.equals("")){
                                arg.put(key, obj.opt(key));
                            }else{
                                char[] _Char = objStr.substring(0,1).toCharArray();
                                char _OneChar= _Char[0];
                                if (_OneChar=='{'){
                                    //todo 如果是這種
                                }else if (_OneChar == '['){
                                    arg.put(key, SignalRTool.jsonParse(objStr));
                                }else{
                                    arg.put(key, obj.opt(key));
                                }
                            }
                        }
                        objs[i] = arg;
                    }
                    else if (OneChar == '[')
                    {
                        objs[i] = SignalRTool.jsonParse(jsonArray.get(i).toString());

                    }
                    else
                    {
                        objs[i] = jsonArray.get(i);
                    }

                }

            }
        }else{
            //Log.w("requireServerWi","error");
        }

        return objs;
    }

    public static void disconnect(){
        if(conn == null) return;
        conn.disconnect();
        conn = null;
    }

}
