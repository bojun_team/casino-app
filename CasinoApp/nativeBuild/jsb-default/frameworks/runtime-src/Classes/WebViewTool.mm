//
//  WebViewTool.m
//  HoldemPokerApp
//
//  Created by Bean on 2016/11/16.
//
//

#import "WebViewTool.h"
#import "ScriptingCore.h"
@implementation WebViewTool
static WebViewTool* _WebViewTool = nil;
+ (WebViewTool *)sharedWebView
{
    if (!_WebViewTool) {
        
        if( [ [WebViewTool class] isEqual:[self class]] )
            _WebViewTool = [[WebViewTool alloc] init];
        else
            _WebViewTool = [[self alloc] init];
    }
    return _WebViewTool;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        m_webView = nil;
        m_fullScreenView = nil;
    }
    return self;
}
- (void)dealloc{
    [super dealloc];
    [m_rootView release];
}
//-----------------------------JS to OC-------------------------
+ (void)openSafariWithURL:(NSString*)url
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] ];
}
+ (void)enableWebViewWithCreatorTop:(NSNumber*)top
                               down:(NSNumber*)down
                               left:(NSNumber*)left
                              right:(NSNumber*)right
                                url:(NSString*)url{
    WebViewTool* webTool = [WebViewTool sharedWebView];
    [webTool addWebViewWithCreatorTop:top
                                 down:down
                                 left:left
                                right:right url:url];
}
+ (void)enableWebViewWithX:(NSNumber*)x y:(NSNumber*)y width:(NSNumber*)w
                    height:(NSNumber*)h url:(NSString*)url
{
    WebViewTool* webTool = [WebViewTool sharedWebView];
    [webTool addWebViewWithX:x y:y width:w height:h url:url];
}
+ (void)enableFullScreenWebViewWithUrl:(NSString*)url title:(NSString*)title
{
    WebViewTool* webTool = [WebViewTool sharedWebView];
    [webTool addFullScreenWebViewWithURL:url title:title];
}
+ (void)disableWebView
{
    WebViewTool* webTool = [WebViewTool sharedWebView];
    [webTool removeWebView];
}
//-----------------------------public function-------------------------
- (void)setRootView:(UIView*)rootView
{
    m_rootView = [rootView retain];
}
//-----------------------------private function-------------------------
- (void)addWebViewWithCreatorTop:(NSNumber*)top
                            down:(NSNumber*)down
                            left:(NSNumber*)left
                           right:(NSNumber*)right
                             url:(NSString*)url{
    CGSize winSize = [m_rootView frame].size;
    float widthVar = winSize.width / 1080;
    float heightVar = winSize.height / 1920;
    
    float y = [top floatValue] * heightVar;
    float x = [left floatValue] * widthVar;
    float width = winSize.width - [right floatValue] * widthVar - x;
    float height = winSize.height - [down floatValue] * heightVar - y;
    CGRect rect = CGRectMake(x, y, width, height);
    //做webView
    [self createWebViewWithRect:rect url:url];
}
- (void)createWebViewWithRect:(CGRect)rect url:(NSString*)url{
    UIWebView* webView = [[[UIWebView alloc] initWithFrame:rect] autorelease];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [webView loadRequest:request];
    [m_rootView addSubview:webView];
    m_webView = webView;
    [m_webView setBackgroundColor:[UIColor blackColor]];
    [m_webView setDelegate:self];
}
- (void)addWebViewWithX:(NSNumber*)x y:(NSNumber*)y width:(NSNumber*)w
                 height:(NSNumber*)h url:(NSString*)url
{
    //轉成ios的大小
    CGSize winSize = [m_rootView frame].size;
    float widthVar = winSize.width / 1080;
    float heightVar = winSize.height / 1920;
    CGRect rect = CGRectMake([x floatValue] * widthVar,
                             [y floatValue] * heightVar,
                             [w floatValue] * widthVar,
                             [h floatValue] * heightVar);
    
    //做webView
    [self createWebViewWithRect:rect url:url];
}
- (void)addFullScreenWebViewWithURL:(NSString*)url title:(NSString*)title{
    if(m_fullScreenView != nil) return;
    
    CGSize winSize = [m_rootView frame].size;
    CGSize titleSize = CGSizeMake(winSize.width, winSize.height / 10);
    CGSize btnSize = CGSizeMake(titleSize.height, titleSize.height);
    CGRect webRect = CGRectMake(0, titleSize.height, winSize.width, winSize.height - titleSize.height);
    CGRect btnRect = CGRectMake(titleSize.width - btnSize.width, 0, btnSize.width, btnSize.height);
    CGRect titleRect = CGRectMake(0, 0, titleSize.width, titleSize.height);
    //close btn
    UIButton* closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setFrame:btnRect];
    [closeBtn addTarget:self
                 action:@selector(onCloseFullScreenWebView)
       forControlEvents:UIControlEventTouchDown];
    [closeBtn setImage:[UIImage imageNamed:@"webBackBtn.png"]
              forState:UIControlStateNormal];
    [closeBtn setBackgroundColor:[UIColor blackColor]];
    
    
    //title label
    UILabel* label = [[UILabel alloc] initWithFrame:titleRect];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:title];
    [label setTextColor:[UIColor colorWithRed:0.9
                                        green:0.7
                                         blue:0
                                        alpha:1]];
    [label setFont:[UIFont boldSystemFontOfSize:20]];
    
    //做webView
    UIWebView* webView = [[[UIWebView alloc] initWithFrame:webRect] autorelease];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [webView loadRequest:request];
    
    UIView* bg = [[[UIView alloc] initWithFrame:m_rootView.frame] autorelease];
    [bg setBackgroundColor:[UIColor blackColor]];
    [bg addSubview:closeBtn];
    [bg addSubview:webView];
    [bg addSubview:label];
    [m_rootView addSubview:bg];
    m_fullScreenView = bg;
    [webView setDelegate:self];
}
- (void)onCloseFullScreenWebView{
    [self removeWebView];
}
- (void)removeWebView
{
    if (m_webView != nil) {
        [m_webView removeFromSuperview];
        m_webView = nil;
    }
    if (m_fullScreenView != nil) {
        [m_fullScreenView removeFromSuperview];
        m_fullScreenView = nil;
    }
}

//-----------------------------uiweb view delegate-------------------------
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"url = %@", [[request URL] absoluteString]);
    NSString* params = [[request URL] absoluteString];
    NSString *returnMessage = [NSString stringWithFormat:@"NativeConnect.onChangeWebUrl(\"%@\")",params];
    ScriptingCore::getInstance()->evalString([returnMessage UTF8String]);
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}
@end
