//
//  SignalRTool.h
//  CasinoApp
//
//  Created by Bean on 2017/1/23.
//
//

#import <Foundation/Foundation.h>
#import "SignalR.h"
@interface SignalRTool : NSObject <SRConnectionDelegate>
{
    SRHubProxy *hub;
    SRHubConnection *hubConnection;
}
+ (SignalRTool *)sharedSignalRTool;
- (void)connectToServerWithURL:(NSString*)url;
- (void)requireServerWithCmd:(NSString*)cmd arg:(NSArray*)arg callBack:(NSString*)cbName;
- (void)disconnect;
@end
