//
//  JSConnect.m
//  CasinoApp
//
//  Created by Bean on 2017/1/23.
//
//

#import "JSConnect.h"
#import "ScriptingCore.h"
#import "SignalRTool.h"
#import "WebViewTool.h"
#import "KSYStreamVideo.h"
#import "AppUpdataTool.h"
@implementation JSConnect
static JSConnect* _jsConnect = nil;

//private static boolean mIsCocosCreatorRunning = false;
//private static Intent mIntent = null;
static BOOL mIsCocosCreatorRunning = FALSE;
static NSURL *mUrl = NULL;
+ (JSConnect *)sharedJSConnect{
    if (!_jsConnect) {
        if( [ [JSConnect class] isEqual:[self class]] )
            _jsConnect = [[JSConnect alloc] init];
        else
            _jsConnect = [[self alloc] init];
    }
    return _jsConnect;
}
- (instancetype)init{ 
    self = [super init];
    return self;
}
+ (void)recvServerData:(NSString*)json{
    NSString *encodeMessage = [json stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *msg = [NSString stringWithFormat:@"NativeConnect.onRecvServerData(\"%@\")",encodeMessage];
    const char *utf8Msg = [msg UTF8String];
    ScriptingCore::getInstance()->evalString(utf8Msg);
}
+ (void)connectToServerWithURL:(NSString*)url{
    [[SignalRTool sharedSignalRTool] connectToServerWithURL:url];
}
+ (void)disconnect{
    [[SignalRTool sharedSignalRTool] disconnect];
}
+ (void)sendConnectInfoWithState:(NSString*)state info:(NSString*)info
{
    NSLog(@"JSConnect sendConnectInfoWithState = %@", state);
    NSString *msg = [NSString stringWithFormat:@"NativeConnect.onRecvConnectionInfo(\"%@\",\"%@\")", state, info];
    const char *utf8Msg = [msg UTF8String];
    ScriptingCore::getInstance()->evalString(utf8Msg);
    
}
+ (void)requireServerWithCmd:(NSString*)cmd jsonData:(NSString*)jsData callBackName:(NSString*)cbName{
    NSLog(@"IFLOG:cmd = %@", cmd);
    NSLog(@"IFLOG:jsData = %@", jsData);
    NSLog(@"IFLOG:cbName = %@", cbName);
    
    NSError *jsonError;
    NSData *objectData = [jsData dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:objectData
                                              options:NSJSONReadingMutableContainers
                                                error:&jsonError];
    [[SignalRTool sharedSignalRTool] requireServerWithCmd:cmd arg:json callBack:cbName];
}
+ (void)onRecvRequireServerWtihCmd:(NSString*)cmd data:(NSString*)data error:(NSString*)errorMsg callBackName:(NSString*)cbName
{
    NSString *encodeMessage = [data stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *msg = [NSString stringWithFormat:@"NativeConnect.onRecvRequireServer(\"%@\",\"%@\",\"%@\",\"%@\")", cmd, encodeMessage, errorMsg, cbName];
    const char *utf8Msg = [msg UTF8String];
    ScriptingCore::getInstance()->evalString(utf8Msg);
}

+ (void)nlogTag:(NSString *)strTag info:(NSString *)strInfo
{
    if(strInfo!=nil)
    {
        NSLog(@"IFLOG:javaScript Log === \"%@\" : [%@]", strTag, strInfo);
    }
}
+ (void)playVideoWithX:(NSNumber*)x y:(NSNumber*)y w:(NSNumber*)w h:(NSNumber*)h
                   url:(NSString*)url
                 clipW:(NSNumber*)clipW
                 clipH:(NSNumber*)clipH
               offsetY:(NSNumber*)offsetY
           isVideoMute:(BOOL)isVideoMute
{
    CGSize cocosWinSize = CGSizeMake(1080, 1920);
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    float wFactor = screenSize.width / cocosWinSize.width;
    float hFactor = screenSize.height / cocosWinSize.height;
    
    CGPoint centerPos = CGPointMake(screenSize.width / 2, screenSize.height / 2 - hFactor * [y floatValue]);
    CGSize size = CGSizeMake(wFactor * [w floatValue],
                             hFactor * [h floatValue]);
    
    CGSize clipSize = CGSizeMake(wFactor * [clipW intValue], hFactor * [clipH intValue]);
    float _offsetY = [offsetY floatValue]*hFactor;
    [[KSYStreamVideo sharedStreamVideoTool] playVideoWithCenter:centerPos size:size url:url clipSize:clipSize offsetY:[NSNumber numberWithFloat:_offsetY] isVideoMute:isVideoMute];

}
+ (void)stopVideo{
    NSLog(@"stop");
    [[KSYStreamVideo sharedStreamVideoTool] stopVideo];
}
+ (void)continueVideo{
    [[KSYStreamVideo sharedStreamVideoTool] continueVideo];
}
+ (void)setVideoVisible:(BOOL)isVisible{
    NSLog(@"hidden %d", isVisible);
    [[KSYStreamVideo sharedStreamVideoTool] setVideoVisible:isVisible];
}
+ (void)setVideoScaleWithIsScale:(BOOL)isScaleToBig tableID:(NSString*)tableID{
    [[KSYStreamVideo sharedStreamVideoTool] setVideoScaleWithIsScale:isScaleToBig tableID:tableID];
}
+ (void) setVideoMute:(BOOL)isVideoMute {
    NSLog(@"setVideoMute %d", isVideoMute);
    [[KSYStreamVideo sharedStreamVideoTool] setVideoMute:isVideoMute];
}
+ (void)pauseVideo{
    NSLog(@"pause");
    [[KSYStreamVideo sharedStreamVideoTool] pauseVideo];
}
+ (void)sendVideoDataWithCmd:(NSString*)cmd andInfo:(NSString*)info {
    NSString *msg = [NSString stringWithFormat:@"NativeConnect.onVideoInfo(\"%@\",\"%@\")", cmd, info];
    const char *utf8Msg = [msg UTF8String];
    ScriptingCore::getInstance()->evalString(utf8Msg);
}
+ (void)seekTo:(NSNumber*)n{
    [[KSYStreamVideo sharedStreamVideoTool] seekTo:n];
}
//--------------------------Web
+ (void)openWebWithTop:(NSNumber*)top
                  down:(NSNumber*)down
                  left:(NSNumber*)left
                 right:(NSNumber*)right
                   url:(NSString*)url{
    [WebViewTool enableWebViewWithCreatorTop:top
                                        down:down
                                        left:left
                                       right:right url:url];
}
+ (void)openWebWithX:(NSNumber*)x y:(NSNumber*)y w:(NSNumber*)w h:(NSNumber*)h url:(NSString*)url{
    [WebViewTool enableWebViewWithX:x y:y width:w height:h url:url];
}
+ (void)closeWeb{
    [WebViewTool disableWebView];
}
+ (void)openWebOnOtherApp:(NSString*)url{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
+ (void)openWebFullScreenWithUrl:(NSString*)url title:(NSString*)title{
    [WebViewTool enableFullScreenWebViewWithUrl:url title:title];
}
//--------------------------Skype
+ (void)openSkypeWithCompanyId:(NSString*)company_id {
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"skype:"]];
    if(installed)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"skype:sytepoker?chat"]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:company_id]];
    }
}
//--------------------------Line
+ (void)openLineWithCompanyId:(NSString*)company_id {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:company_id]];
}
//--------------------------QQ
+ (void)openQQWithCompanyId:(NSString*)company_id {
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq:"]];
    
    if(installed)
    {
        NSString* urlStr = [NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web", company_id];
        NSURL *url = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/tw/app/qq/id444934666?mt=8"]];
    }
}
//-------------------------FB
+ (void)openFacebookMessagerWithCompanyId:(NSString*)company_id
{
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"fb-messenger://"]]) {
        NSString* path = [NSString stringWithFormat:@"fb-messenger://user-thread/%@", company_id];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:path]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/tw/app/messenger/id454638411?l=zh&mt=8"]];
    }
}
//------------------app更新
+ (void)updataAppWithURL:(NSString*)url
{
    [[AppUpdataTool sharedAppUpdataTool] updataAppWithURL:url];
}
+ (NSString*)getGameVersion{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}
+ (NSString*)getCountry{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"AppCountry"];
}
+ (NSString*)getBusinessType{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"BusinessType"];
}
//---------------------system
+ (void)onRecvDeepLinkData:(NSURL *)url {
    if(mIsCocosCreatorRunning == FALSE) {
        mUrl = url;
        [mUrl retain];
        return;
    }
    
    
    NSMutableDictionary* jsonDic = [NSMutableDictionary dictionary];
    [jsonDic setObject:[url scheme] forKey:@"scheme"];
    id host = [url host];
    if (host == nil) return;

    [jsonDic setObject:[url host] forKey:@"host"];
    NSMutableDictionary* queryList = [NSMutableDictionary dictionary];
    NSArray *urlComponents = [[url query] componentsSeparatedByString:@"&"];
    for (NSString *keyValuePair in urlComponents) {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [pairComponents firstObject];
        NSString *value = @"";
        for(int i=1; i<[pairComponents count]; i++) {
            NSString *addStr = (i != 1) ? (@"=") : (@"");
            value = [[value stringByAppendingString:addStr] stringByAppendingString:[pairComponents objectAtIndex:i]];
        }
        [queryList setObject:value forKey:key];
    }
    [jsonDic setObject:queryList forKey:@"queryList"];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDic options:0 error:&err];
    NSString * jsonStr = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
    NSString *encodeMessage = [jsonStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *msg = [NSString stringWithFormat:@"NativeConnect.onRecvDeepLinkData(\"%@\")", encodeMessage];
    const char *utf8Msg = [msg UTF8String];
    ScriptingCore::getInstance()->evalString(utf8Msg);
}

+ (void)setCocosCreatorRunning {
    mIsCocosCreatorRunning = TRUE;
    if(mUrl != NULL) {
        [JSConnect onRecvDeepLinkData:mUrl];
        [mUrl release];
        mUrl = NULL;
    }
}

+ (void)exit{
    exit(0);
}


@end
