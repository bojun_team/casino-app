//
//  KSYStreamVideo.m
//  CasinoApp
//
//  Created by Bean on 2017/4/20.
//
//

#import "KSYStreamVideo.h"
#import "JSConnect.h"


@implementation KSYStreamVideo
static KSYStreamVideo* _streamVideoTool = nil;
static CGPoint videoOriginPos;
+ (KSYStreamVideo *)sharedStreamVideoTool
{
    if (!_streamVideoTool) {
        if( [ [KSYStreamVideo class] isEqual:[self class]] )
            _streamVideoTool = [[KSYStreamVideo alloc] init];
        else
            _streamVideoTool = [[self alloc] init];
    }
    return _streamVideoTool;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        m_reloadTimer = nil;
    }
    return self;
}
- (void)setContainerView:(UIView*)view{
    m_containerView = [view retain];
}

- (void)playVideoWithCenter:(CGPoint)center size:(CGSize)size url:(NSString*)url clipSize:(CGSize)clipSize offsetY:(NSNumber*)offsetY isVideoMute:(BOOL)isVideoMute{
    if (m_player != nil){
        [m_player setUrl:[NSURL URLWithString:url]];
        [m_player prepareToPlay];
    }
    else
    {
        NSURL* nurl = [NSURL URLWithString:url];
        m_player = [[KSYMoviePlayerController alloc] initWithContentURL:nurl fileList:nil sharegroup:nil];
        m_player.scalingMode = MPMovieScalingModeFill;
        [m_player setShouldMute:isVideoMute]; // 設置播放器禁音
        [m_player setVolume:0.75f rigthVolume:0.75f];
        [self setupObservers];
        m_player.logBlock = ^(NSString *logJson){
            NSLog(@"logJson is %@",logJson);
        };
        
        m_videoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, clipSize.width, clipSize.height)];
        m_videoView.clipsToBounds = true;
        m_videoView.center = center;
        [m_containerView insertSubview:m_videoView atIndex:0];
        
        [m_player.view setFrame: CGRectMake(0, 0, size.width, size.height)];  // player's frame must match parent's
        videoOriginPos = CGPointMake(clipSize.width / 2, m_player.view.center.y - [offsetY doubleValue]);
        [m_player.view setCenter:videoOriginPos];
        [m_videoView addSubview:m_player.view];
        [m_player prepareToPlay];
    }
    
    //add loading view
    m_loadingView =  [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
    m_loadingView.center = CGPointMake(clipSize.width / 2, clipSize.height / 2);
    [m_loadingView setHidesWhenStopped:YES];
    [m_loadingView startAnimating];
    [m_videoView addSubview:m_loadingView];
    m_URL = url;
    m_isReload = NO;
}
- (void)setVideoVisible:(BOOL)isVisible{
    if (m_player == nil) return;
    [m_videoView setHidden:!isVisible];
}
- (void)setVideoScaleWithIsScale:(BOOL)isScaleToBig tableID:(NSString*)tableID{
    if (m_player == nil) return;
    float scale = 2.0f;
    float offsetX = 0.0f;
    float offsetY = 70.0f;
    if([tableID isEqualToString:@"A"]) {
        scale = 2.0f;
        offsetX = 15.0f;
        offsetY = 90.0f;
    } else if([tableID isEqualToString:@"B"]) {
        scale = 3.5f;
        offsetX = 30.0f;
        offsetY = 150.0f;
    } else if([tableID isEqualToString:@"C"]) {
        scale = 2.0f;
        offsetX = 5.0f;
        offsetY = 68.0f;
    } else if([tableID isEqualToString:@"D"]) {
        scale = 1.8f;
        offsetX = 5.0f;
        offsetY = 90.0f;
    } else {
        scale = 1.3f;
        offsetY = 20.0f;
    }
    
    if(isScaleToBig == true) {
        m_player.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, scale, scale);
        [m_player.view setCenter:CGPointMake(videoOriginPos.x + offsetX, videoOriginPos.y - offsetY)];
    } else {
        m_player.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
        [m_player.view setCenter:videoOriginPos];

    }
}
- (void)setVideoMute:(BOOL)isVideoMute {
    if (m_player == nil) return;
    [m_player setShouldMute:isVideoMute];
}
- (void)stopVideo{
    if (m_player == nil) return;
    [m_player stop];
    [self releaseObservers];
    [m_player.view removeFromSuperview];
    [m_videoView removeFromSuperview];
    [m_player release];
    [m_videoView release];
    m_player = nil;
}
- (void)pauseVideo{
    if (m_player) {
        [m_player pause];
    }
}
- (void)continueVideo{
    if (m_player) {
        [m_player play];
    }
}
- (void)seekTo:(NSNumber*)n{
    
}
- (void)setupObservers
{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMediaPlaybackIsPreparedToPlayDidChangeNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerPlaybackStateDidChangeNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerPlaybackDidFinishNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerLoadStateDidChangeNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMovieNaturalSizeAvailableNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerFirstVideoFrameRenderedNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerFirstAudioFrameRenderedNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerSuggestReloadNotification)
                                              object:m_player];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handlePlayerNotify:)
                                                name:(MPMoviePlayerPlaybackStatusNotification)
                                              object:m_player];
}

- (void)releaseObservers
{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerPlaybackDidFinishNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerLoadStateDidChangeNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMovieNaturalSizeAvailableNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerFirstVideoFrameRenderedNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerFirstAudioFrameRenderedNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerSuggestReloadNotification
                                                 object:m_player];
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:MPMoviePlayerPlaybackStatusNotification
                                                 object:m_player];
}
-(void)handlePlayerNotify:(NSNotification*)notify
{
    if (!m_player) {
        return;
    }
    if (MPMediaPlaybackIsPreparedToPlayDidChangeNotification ==  notify.name) {
        m_isReload = YES;
        NSLog(@"MPMediaPlaybackIsPreparedToPlayDidChangeNotification");
        [m_loadingView stopAnimating];
        [JSConnect sendVideoDataWithCmd:@"didPrepared" andInfo:@""];
    }
    if (MPMoviePlayerPlaybackStateDidChangeNotification ==  notify.name) {
        NSLog(@"MPMoviePlayerPlaybackStateDidChangeNotification");
        NSLog(@"------------------------");
        NSLog(@"player playback state: %ld", (long)m_player.playbackState);
        NSLog(@"------------------------");
    }
    if (MPMoviePlayerLoadStateDidChangeNotification ==  notify.name) {
        NSLog(@"MPMoviePlayerLoadStateDidChangeNotification");
        NSLog(@"player load state: %ld", (long)m_player.loadState);
        if (MPMovieLoadStateStalled & m_player.loadState) {
            NSLog(@"player start caching");
        }
        
        if (m_player.bufferEmptyCount &&
            (MPMovieLoadStatePlayable & m_player.loadState ||
             MPMovieLoadStatePlaythroughOK & m_player.loadState)){
                NSLog(@"player finish caching");
            }
    }
    if (MPMoviePlayerPlaybackDidFinishNotification ==  notify.name) {
        NSLog(@"MPMoviePlayerPlaybackDidFinishNotification");
        int reason = [[[notify userInfo] valueForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
        NSString* videoMsg = @"";
        if (reason ==  MPMovieFinishReasonPlaybackEnded) {
            videoMsg = [NSString stringWithFormat:@"player finish"];
        }else if (reason == MPMovieFinishReasonPlaybackError){
            videoMsg = [NSString stringWithFormat:@"player Error : %@", [[notify userInfo] valueForKey:@"error"]];
            [JSConnect sendVideoDataWithCmd:@"VideoError" andInfo:[NSString stringWithFormat:@"%@", [[notify userInfo] valueForKey:@"error"]]];
        }else if (reason == MPMovieFinishReasonUserExited){
            videoMsg = [NSString stringWithFormat:@"player userExited"];
        }
        NSLog(@"videoMsg = %@", videoMsg);
    }
    if (MPMovieNaturalSizeAvailableNotification ==  notify.name) {
        NSLog(@"MPMovieNaturalSizeAvailableNotification");
        NSLog(@"video size %.0f-%.0f", m_player.naturalSize.width, m_player.naturalSize.height);
    }
    if (MPMoviePlayerFirstVideoFrameRenderedNotification == notify.name)
    {
        NSLog(@"MPMoviePlayerFirstVideoFrameRenderedNotification");
        NSLog(@"first video frame show, cost time : ms!\n" );
    }
    
    if (MPMoviePlayerFirstAudioFrameRenderedNotification == notify.name)
    {
        NSLog(@"MPMoviePlayerFirstAudioFrameRenderedNotification");
        NSLog(@"first audio frame render, cost time : ms!\n");
    }
    
    if (MPMoviePlayerSuggestReloadNotification == notify.name)
    {
        NSLog(@"MPMoviePlayerSuggestReloadNotification");
        NSLog(@"suggest using reload function!\n");
        if(!m_isReload)
        {
            m_isReload = YES;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(){
                if (m_player) {
                    NSLog(@"reload stream");
                    NSURL* nurl = [NSURL URLWithString:m_URL];
                    [m_player reload:nurl flush:YES mode:MPMovieReloadMode_Accurate];
                }
            });
        }
    }
    
    if(MPMoviePlayerPlaybackStatusNotification == notify.name)
    {
        NSLog(@"MPMoviePlayerPlaybackStatusNotification");
        int status = [[[notify userInfo] valueForKey:MPMoviePlayerPlaybackStatusUserInfoKey] intValue];
        if(MPMovieStatusVideoDecodeWrong == status)
        {
            NSLog(@"Video Decode Wrong!\n");
        }
        else if(MPMovieStatusAudioDecodeWrong == status)
        {
            NSLog(@"Audio Decode Wrong!\n");
        }
        else if (MPMovieStatusHWCodecUsed == status )
        {
            NSLog(@"Hardware Codec used\n");
        }
        else if (MPMovieStatusSWCodecUsed == status )
        {
            NSLog(@"Software Codec used\n");
        }
    }
}

@end
