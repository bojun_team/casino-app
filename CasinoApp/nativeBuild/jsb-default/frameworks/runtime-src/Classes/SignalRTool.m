//
//  SignalRTool.m
//  CasinoApp
//
//  Created by Bean on 2017/1/23.
//
//

#import "SignalRTool.h"
#import "JSConnect.h"
@implementation SignalRTool
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
static SignalRTool* _signalRTool = nil;
+ (SignalRTool *)sharedSignalRTool
{
    if (!_signalRTool) {
        if( [ [SignalRTool class] isEqual:[self class]] )
            _signalRTool = [[SignalRTool alloc] init];
        else
            _signalRTool = [[self alloc] init];
    }
    return _signalRTool;
}
- (void)connectToServerWithURL:(NSString*)url
{
    // Connect to the service
    hubConnection = [[SRHubConnection connectionWithURLString:url] retain];
    
//    SRHubConnection *hubConnection = [SRHubConnection connectionWithURLString:@"http://54.169.214.140:8880/signalr"];
    // Create a proxy to the chat service
    hub = [hubConnection createHubProxy:@"casinoHub"];
    //    [chat on:@"addMessage" perform:self selector:@selector(addMessage:)];
    hubConnection.delegate = self;
    // Register for connection lifecycle events
    [hubConnection setStarted:^{
        NSLog(@"IFLOG:Connection Started");
    }];
    [hubConnection setReceived:^(NSString *message) {
        //NSLog(@"IFLOG:Connection Recieved Data: %@",message);
    }];
    [hubConnection setConnectionSlow:^{
        NSLog(@"IFLOG:Connection Slow");
    }];
    [hubConnection setReconnecting:^{
        NSLog(@"IFLOG:Connection Reconnecting");
    }];
    [hubConnection setReconnected:^{
        NSLog(@"IFLOG:Connection Reconnected");
    }];
    [hubConnection setClosed:^{
        NSLog(@"IFLOG:Connection Closed");
    }];
    [hubConnection setError:^(NSError *error) {
        NSLog(@"IFLOG:Connection Error %@",error);
    }];
    // Start the connection
    [hubConnection start];

}

- (void)disconnect{
    if(hubConnection == NULL) return;
    [hubConnection stop];
    [hubConnection release];
    hubConnection = nil;
}
- (void)requireServerWithCmd:(NSString*)cmd arg:(NSArray*)arg callBack:(NSString*)cbName
{
    __block BOOL isSend = NO;
    [hub invoke:cmd withArgs:arg completionHandler:^(id response, NSError *error){
        if (error)
        { 
            NSString* errorStr = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:[error code]]];
            [JSConnect onRecvRequireServerWtihCmd:cmd data:errorStr error:[error localizedDescription] callBackName:cbName];
            isSend = YES;
            return;
        }
        if (!isSend){
            isSend = YES;
        }
        else
        {
            NSError * err;
            NSMutableDictionary* responseDict = [NSMutableDictionary dictionary];
            [responseDict setObject:response forKey:@"data"];
            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:responseDict options:0 error:&err];
            NSString * jsonStr = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
            //NSString * replaceStr = [jsonStr stringByReplacingOccurrencesOfString: @"\"" withString: @"\\\""];
            //NSLog(@"requireServerWithCmd fake Str = %@", replaceStr);
            [JSConnect onRecvRequireServerWtihCmd:cmd
                                             data:jsonStr
                                            error:@"null"
                                     callBackName:cbName];
        }
    }];
}
//----------------------imp SRConnectionDelegate
- (void)SRConnectionDidOpen:(id <SRConnectionInterface>)connection{
    NSLog(@"IFLOG:SRConnectionDidOpen"); 
}
- (void)SRConnectionWillReconnect:(id <SRConnectionInterface>)connection{
    NSLog(@"IFLOG:SRConnectionWillReconnect");
    [self disconnect];
}
- (void)SRConnectionDidReconnect:(id <SRConnectionInterface>)connection{
    NSLog(@"IFLOG:ConnectionDidReconnect"); 
}
- (void)SRConnection:(id <SRConnectionInterface>)connection didReceiveData:(id)data{
    NSLog(@"IFLOG:SRConnection data %@",data);
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&err];
    NSString * jsonStr = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
    //NSString * replaceStr = [jsonStr stringByReplacingOccurrencesOfString: @ "\"" withString: @ "\\\""];
    //NSLog(@"fake Str = %@", replaceStr);
    [JSConnect recvServerData:jsonStr];
}
- (void)SRConnectionDidClose:(id <SRConnectionInterface>)connection{
    NSLog(@"IFLOG:SRConnectionDidClose connection %@",connection);
    NSString* state = [NSString stringWithFormat:@"%d", disconnected];
    [JSConnect sendConnectInfoWithState:state info:nil];
}
- (void)SRConnection:(id <SRConnectionInterface>)connection didReceiveError:(NSError *)error{
    NSLog(@"IFLOG:SRConnection error = %@",error);
    NSString* errorStr = [NSString stringWithFormat:@"error = %@",error];
    [JSConnect sendConnectInfoWithState:@"-1" info:errorStr];
}

- (void)SRConnection:(id <SRConnectionInterface>)connection didChangeState:(connectionState)oldState newState:(connectionState)newState{
    NSLog(@"IFLOG:SRConnection newState %d", newState);
    NSString* state = [NSString stringWithFormat:@"%d", newState];
    [JSConnect sendConnectInfoWithState:state info:nil];
}
- (void)SRConnectionDidSlow:(id <SRConnectionInterface>)connection{
    NSLog(@"IFLOG:SRConnectionDidSlow connection %@",connection);
}
@end
