//
//  KSYStreamVideo.h
//  CasinoApp
//
//  Created by Bean on 2017/4/20.
//
//

#import <Foundation/Foundation.h>
#import <KSYMediaPlayer/KSYMediaPlayer.h>
#import <KSYMediaPlayer/KSYMoviePlayerController.h>

@interface KSYStreamVideo : NSObject
{
    UIView* m_videoView;
    KSYMoviePlayerController *m_player;
    UIView* m_containerView;
    BOOL m_isReload;
    NSString* m_URL;
    NSTimer* m_reloadTimer;
    UIActivityIndicatorView* m_loadingView;
}
+ (KSYStreamVideo *)sharedStreamVideoTool;
- (void)setContainerView:(UIView*)view;

- (void)playVideoWithCenter:(CGPoint)center size:(CGSize)size url:(NSString*)url clipSize:(CGSize)clipSize offsetY:(NSNumber*)offsetY isVideoMute:(BOOL)isVideoMute;
- (void)setVideoVisible:(BOOL)isVisible;
- (void)setVideoScaleWithIsScale:(BOOL)isScaleToBig tableID:(NSString*)tableID;
- (void)setVideoMute:(BOOL)isVideoMute;
- (void)stopVideo;
- (void)pauseVideo;
- (void)continueVideo;
- (void)seekTo:(NSNumber*)n;
@end
