cc.Class({
    extends: cc.Component,

    properties: {
        spriteAtlas:cc.SpriteAtlas,
        testSprite: cc.Sprite
    },

    // use this for initialization
    onLoad: function () {
        this.testSprite.spriteFrame = this.spriteAtlas.getSpriteFrame("account")
    },
});
