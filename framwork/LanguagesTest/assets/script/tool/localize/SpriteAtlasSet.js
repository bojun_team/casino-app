const SpriteAtlasSet = cc.Class({
    name: 'SpriteAtlasSet',
    properties: {
        language: '',
        spriteAtlas: cc.SpriteAtlas
    }
});

module.exports = SpriteAtlasSet;
