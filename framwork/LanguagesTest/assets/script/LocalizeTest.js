import Config from './Config'
import localizeMgr from './tool/localize/LocalizeMgr'

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // use this for initialization
    onLoad: function () {
        let language = Config.Language.CN
        let languageList = [
            Config.Language.ZH,
            Config.Language.CN,
        ]
        let csvPathList = [
            "/localize/CasinoLocalize - Common"
        ]
        localizeMgr.init(language, languageList, csvPathList)
    },

    onClickZHButton: function() {
        localizeMgr.changeLanguage(Config.Language.ZH)
    },

    onClickCNButton: function() {
        localizeMgr.changeLanguage(Config.Language.CN)
    },
});
