import { clone, defaultTo, drop, last, pipe, reduce, zipObj, split, map } from 'ramda'

const BANKER = 1
const PLAYER = 2
const TIE = 3

const countContinuous = (value, data) => {
  let count = 0
  for (var index in data) {
    if (data[index].type != value) {
      break
    }

    count++
  }

  return (count == data.length) ? count - 1 : count
}

const twoDimensionalArray = (inner, outter, init) => {
  let unit = []
  let result = []
  times(() => unit.push(init), inner)
  times(() => result.push(clone(unit)), outter)

  return result
}

const parserRoadMap = (s) => {
  const formatResultString = zipObj(['type', 'value','pairs'])
  var resolveResult = pipe(defaultTo(''), split(','), map(formatResultString))
  return resolveResult(s)
}

// NOTICE: bigRoadReducer is NOT a pure function
const bigRoadReducer = (initTie = 0) => (now, current) => {
  current.value = 0

  if (initTie != 0) {
    current.value = initTie
    initTie = 0
  }

  const lastRoad = now[now.length - 1]
  const lastRoadType = lastRoad ? lastRoad[0].type : 0
  const lastCircle = lastRoad ? lastRoad[lastRoad.length - 1] : current

  if (current.type == TIE) {
    lastCircle.value++

    if (now.length > 0) return now
  }

  if (lastRoadType == current.type) {
    lastRoad.push(current)
  } else {
    now.push([current])
  } 
  return now
}

const mapToBigRoad = (results) => {
  const firstTies = countContinuous(TIE, results)
  return pipe(clone, drop(firstTies), reduce(bigRoadReducer(firstTies), []))(results)
}

const mapToDownThreeRoad = (padding) => (results) => {
  const roads = []

  results.forEach((col, x) => {
    if (x <= padding) return

    col.forEach((result, y) => {
      if (x <= 1 + padding && y < 1) return

      const lastRoad = roads[roads.length - 1]
      const lastRoadType = lastRoad ? lastRoad[0].type : 0
      let type = 0

      if (y == 0) {
        type = (results[x - 1].length == results[x - 2 - padding].length) ? BANKER : PLAYER
      } else {
        type = (results[x - 1 - padding][y] || !results[x - 1 - padding][y - 1]) ? BANKER : PLAYER
      }

      if (lastRoadType == type) {
        lastRoad.push({ type })
      } else {
        roads.push([{ type }])
      }
    })
  })

  return roads
} 
const putRoadMapContainer = (maxY, roadData, isShowLog) => {
  maxY = maxY
  var roadMapContainer = []
  const putRoadMap = function(container, data, x, y)
  {
      if (container[x] == null){
          container[x] = []
          roadMapContainer[x].length = maxY
      } 
      container[x][y] = data 
      return container
  }
  var posTemp = new cc.Vec2(0, 0) 
  let startPointX = 0
  for(var x = 0; x < roadData.length; x++)
  {
      posTemp = new cc.Vec2(startPointX, 0)
      let isTurn = false
      for(var y = 0; y < roadData[x].length; y++)
      { 
          // 下一格不是空的往右邊畫一格
          if (isTurn){
              posTemp = new cc.Vec2(posTemp.x + 1, posTemp.y)
          }
          else if (roadMapContainer[posTemp.x] != null &&
                   roadMapContainer[posTemp.x][posTemp.y + 1] != null){
              posTemp = new cc.Vec2(posTemp.x + 1, posTemp.y)
              isTurn = true
          }
          else if (posTemp.y + 1 > maxY)//下一行是空的往右邊劃
          { 
              posTemp = new cc.Vec2(posTemp.x + 1, posTemp.y)
          }
          else{ 
              posTemp = new cc.Vec2(posTemp.x, posTemp.y + 1)
          } 
          roadMapContainer = putRoadMap(roadMapContainer, roadData[x][y], posTemp.x, posTemp.y) 
          
          if (posTemp.y == 1 && posTemp.x >= startPointX){
              startPointX++;
          }
      } 
      isTurn = false
  }
  return roadMapContainer
}
 
const putCuteRoadMapContainer = (maxY, roadData) => {
    maxY = maxY + 1
    var newRoad = []
    for(var i = 0; i < roadData.length; i++)
    {
        const x = Math.floor(i / maxY)
        const y = i % maxY + 1
        if (newRoad[x] == null) newRoad[x] = []
        newRoad[x][y] = roadData[i]
    }
    return newRoad
}

export default {
  parserRoadMap: parserRoadMap,
  mapToBigRoad: mapToBigRoad,
  mapToDownThreeRoad: mapToDownThreeRoad,
  
  putRoadMapContainer: putRoadMapContainer, 
  putCuteRoadMapContainer: putCuteRoadMapContainer
}
