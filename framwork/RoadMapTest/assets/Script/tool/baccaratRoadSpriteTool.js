const BANKER = 1
const PLAYER = 2
const TIE = 3

const BIG_EYE = 0
const SMALL = 1
const COCKROACH = 2

const getCubeRoadItemSpriteName = (type, value, pairs) => {
    let spriteName = ""
    // 贏的種類處理 莊 閒 和 三種
    switch(String(type)) {
        case String(BANKER): 
            spriteName = "Banker" 
            break
        case String(PLAYER): 
            spriteName = "Player" 
            break
        case String(TIE): 
            spriteName = "Tie" 
            break
    }
    // 對子總類處理 閒對 莊對 莊閒對 無對
    switch(String(pairs)) {
        case "1": 
            spriteName = spriteName + "PlayerPair" 
            break
        case "2": 
            spriteName = spriteName + "BankerPair" 
            break
        case "3": 
            spriteName = spriteName + "Pair" 
            break
        default:
            break
    }
    // 贏的點數處理
    spriteName = spriteName + String(value)
    return spriteName
}

const getBigRoadItemSpriteName = (type) => {
    let spriteName = "big_"
    // 贏的種類處理 莊 閒 和 三種
    switch(String(type)) {
        case String(BANKER): 
            spriteName = spriteName + "red" 
            break
        case String(PLAYER): 
            spriteName = spriteName + "bule" 
            break
        case String(TIE): 
            spriteName = spriteName + "green" 
            break
    }
    spriteName = spriteName + "_circle"
    return spriteName
}

const getDownThreeRoadItemSpriteName = (winType, downThreeRoadType) => {
    let spriteName = "small_"
    // 贏的種類處理 莊 閒 二種
    switch(String(winType)) {
        case String(BANKER): 
            spriteName = spriteName + "red" 
            break
        case String(PLAYER): 
            spriteName = spriteName + "bule" 
            break
    }
    // 下三路處理 三種
    switch(String(downThreeRoadType)) {
        case String(BIG_EYE): 
            spriteName = spriteName + "_circle" 
            break
        case String(SMALL): 
            spriteName = spriteName + "_solid_circle" 
            break
        case String(COCKROACH): 
            spriteName = spriteName.replace("_red", "") + "_line" 
            break
    }
    return spriteName
}


export default {
  getCubeRoadItemSpriteName: getCubeRoadItemSpriteName,
  getBigRoadItemSpriteName: getBigRoadItemSpriteName,
  getDownThreeRoadItemSpriteName: getDownThreeRoadItemSpriteName,
}