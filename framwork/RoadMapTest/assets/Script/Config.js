var Config = {
    gameState: {
        CountDown : 1, //倒數
        Dealing : 2, //開牌
        Computing : 3, //結算
        HalfTime : 4, //中場
        Maintenance : 5, // 維修
        NewRoundRun : 6, // 新輪新局
        ComputingFinish : 7, //結算完畢
        EndBetTime : 8, //下注時間結束
        MiCard : 9, // 眯牌
        EndMiCardTime : 10, // 眯牌時間結束
        CancelRun : 11, // 取消此局
    },
    betZoneType: {
        Standard: 1, //標準
        West: 2, //西洋
        NoWater: 3, //免水
    },
    BaccaratGameMode:{
        BaccaratSpeed:"BaccaratSpeed",
        BaccaratStandard:"BaccaratStandard",
        BaccaratMiCard:"BaccaratMiCard"
    },
    BaccaratGameModeDescription:{
        BaccaratMiCard:"眯牌",
        BaccaratSpeed:"極速",
        BaccaratStandard:"先發"
    },
    GameType:{
        Baccarat:"百家樂"
    },
    TableName : {
        Baccarat:"Baccarat",	// 百家樂
        Roulette:"Roulette",	// 輪盤
        DragonTiger:"DragonTiger",	// 	龍虎
        MultipleTable:"MultipleTable",	// 多桌
    },
    gameStateMsg:function(state){
        switch(parseInt(state))
        {
            case this.gameState.CountDown: return "倒數中"; 
            case this.gameState.Dealing: return "開牌中";
            case this.gameState.Computing: return "結算中";
            case this.gameState.HalfTime: return "中場";
            case this.gameState.Maintenance: return "維修中";
            case this.gameState.NewRoundRun: return "準備中";
            case this.gameState.ComputingFinish: return "結算完畢"
            case this.gameState.EndBetTime: return "停止下注"
            case this.gameState.MiCard: return "眯牌中"
            case this.gameState.EndMiCardTime: return "眯牌結束"
            default: return ""; 
        }
    },
    
    
    kickState:{
        default:0,
        kick:1, // 沒原因踢人
        noAuthority:2, //停用踢人
        relogin:3, // 重複登入踢人
    },
    miCardState: {
        // 應已收到需要咪的牌，並且確認是否需要補牌
        StartAndCheck : 1,      // 準備咪牌 : "必須"確認玩家是否有咪牌資格
        BaseCard : 2,           // 開始咪牌 
        OpenBaseCard : 3,       // 開牌    ： 第一階段咪牌結束 － 開牌
        PlayerAddCard : 4,      // 補牌    ： 閒補牌
        OpenPlayerAddCard : 5,  // 開牌    ： 閒補牌開牌
        BankerAddCard : 6,      // 補牌    ： 莊補牌
        OpenBankerAddCard : 7,  // 開牌    ： 莊補牌開牌
        EndAndReset: 0,         // 咪牌結束 : 關閉咪牌頁面 並且重置所有牌位置
    },
    videoErrorState: {
        VideoError : 1,
        VideoDownloadRate : 2,
    },
    spotDecription :{
        player: "閒家",
        playerPair: "閒對",
        banker: "莊家",
        bankerPair: "莊對",
        tie: "和",
    },
    // curruentDNS: "test",
    curruentDNS: "stage",
    // curruentDNS: "release",
    
    version: "2.1.2",    
    appType: 2,
    apiBreakTime: 10000,
    iosVideoDelayTime: 0,
    gameBreakTime: 5000,
    DNS:{
        test: "http://192.168.119.72:8880",
        stage:"http://52.77.218.2:8880",
        release:"http://54.65.16.37:8880",
    },
    PLATFORM:{
        test: "http://casino-test.greatvirtue.com.tw:8000",
        stage:"http://casino.greatvirtue.com.tw",
        release:"http://app.sytebet.com/",
    },
    signalR_URL:function(){
        return this.DNS[this.curruentDNS] + "/signalr"
    },
    casinoGetSpot_URL:function(){
        return this.DNS[this.curruentDNS] + "/api/CasinoGetSpot"
    }, 
    casinoGetChipSet_URL:function(){
        return this.DNS[this.curruentDNS] + "/api/CasinoGetChipSet"
    }, 
    
    // casinoGetSpot_URL: Config.DNS[Config.curruentDNS] + "/api/CasinoGetSpot",
    // casinoGetChipSet_URL: Config.DNS[Config.curruentDNS] + "/api/CasinoGetChipSet",
    casinoGetReportURL:function(){
        return this.PLATFORM[this.curruentDNS] + "/casino-api/app-report?report="
    }, 
    casinoGetRuleURL:function(){
        return this.PLATFORM[this.curruentDNS] + "/casino-api/app-rule?rule="
    }, 
    casinoGetVersionCheckURL:function(){
        return this.PLATFORM[this.curruentDNS] + "/casino-api/app-version-check?"
    },
    casinoGetServerStateURL:function(){
        return this.PLATFORM[this.curruentDNS] + "/casino-api/app-maintain-check"
    },
    casinoGetLoginURL:function(){
        return this.PLATFORM[this.curruentDNS] + "/casino-api/app-login?"
    },
    // gameStateDecription = function(state) {
    //     //todo: gameState的中文
    //     return "sss"
    // }

    //WebApiTool 額外參數設定
    webApiToolTimeOut: -1,

    titleMsg:{
        Warning:"警告",
        Error:"錯誤",
        Hint:"提示",
        TimeOut:"連線逾時",
        NewVersion:"版本更新"
    },
    
    errorMsg:{
        VersionIsOld:"請更新至最版本：",
        ConnectClose:"無法連線至Server，請檢查網路是否正確連線",
        CheckVersionTimeOut:"版本檢查逾時，請檢查網路是否正確連線",
        getChipInfoTimeOut:"取得籌碼資訊逾時，請檢查網路是否正確連線",
        getSpotInfoTimeOut:"取得注區資訊逾時，請檢查網路是否正確連線",
        getServerStateTimeOut:"取得Server狀態逾時，請檢查網路是否正確連線",
        NoChipInfo:"無法取得籌碼資訊",
        NoSpotInfo:"無法取得注區資訊",
        NoServerState:"無法取得Server狀態",
        passwordAccountError:"帳號密碼錯誤，請注意大小寫",
        loginFail:"登入失敗，請重新登入",
        TableMaintenance:"該桌維護中",
        RemindNoBetFive:"提醒您，若5局未下注則會於\n本局結束後自動返回大廳",
        NoBetFive:"提醒您，您已經5局未下注，\n將自動返回大廳",
        kickUser:"此帳號已被踢除，遊戲即將中斷連線",
        kickUserNoAuthority:"此帳號已被停用，遊戲即將中斷連線",
        kickUserTypeRelogin:"此帳號已被重複登入，遊戲即將中斷連線",
        ConnectError:"連線錯誤, 請檢查網路是否正確連線",
        ConnectSlow:"網路不穩,請檢查網路是否正確連線",
        redLimit: "該注區已達最高下注額度，下注失敗",
        loseLimit: "下注失敗，超過限額下限，請與上層聯絡",
        winLimit: "下注失敗，超過限額上限，請與上層聯絡",
        lessMoney: "帳戶額度不足",
        lockBill: "下注失敗，請聯絡您的上層",
        underLimit: "低於該注區押注限額:0",
        overLimit: "超過該注區限額，已押注最大額度:0",
        noAmount: "無可用額度",
        cancelBetFail: "開始發牌，無法取消下注",
        betFail: "下注失敗",
        areaLimitFull: "該注區限額已滿",
        noAuthority: "無下注權限",
        cancelRun: "此局已取消",
        serverError: "遊戲伺服器斷線系統緊急維修中",
        relax: "中場休息，請稍待",
        start: "開始新局，請點選籌碼下注",
        stop: "停止下注",
        overFive: "超過五場未下注，請重新連線",
        offLine: "遊戲斷線，請重新連線",
        leaveGameFail: "離開遊戲失敗：遊戲即將關閉",
        CannotGetVersion: "無法取得版本資訊，請檢查網路：遊戲即將關閉",
        unknowHttpPostError: "未知的錯誤 代碼：",
        loginWebApiFail:"登入失敗，請重新登入",
        errorCode: "錯誤代碼：",
        seatErrorLoseLimit: "超過限額下限，請聯絡您的上層",
        seatErrorWinLimit: "超過限額上限，請聯絡您的上層",
        seatErrorNoAuthority: "入桌失敗，無指定遊戲權限",
        UnknowError:"錯誤，遊戲即將關閉\n代碼：",
        videoReflash:"視訊重整完成",
        loadingGameDataTimeOut:"讀取遊戲資料逾時，請重新進入",
        loadingGameUITimeOut:"讀取畫面逾時，請重新進入",
        loadingGameFail:"讀取逾時，請重新進入",
        gameTimeOutFail:"您的網路過慢將可能影響牌局流程，\n建議您重整視訊或重新登入遊戲",
        gameLeaveTimeOutFail:"您的網路過慢將可能影響牌局流程，\n請重新登入遊戲",
        inBackGroundLongTime:"閒置過久，已斷線",
        gameServerMaintain:"伺服器維護中，請稍候",
        gameServerMaintainCloseGame:"伺服器維護中：遊戲即將關閉",
        defaultBetZoneIsNotStandard:"目前手機版尚未開放西洋注區投注，\n請稍待結算完進入新局即可進桌。"
    }, 
    requireToServerError:{
        timeOut:-10001,
        signalR:-10002,
    },
    seatFailMsg:function(failId){
        switch(parseInt(failId))
        {
            case Config.requireToServerError.timeOut: return "逾時，遊戲即將關閉"; 
            case Config.requireToServerError.signalR: return "失敗，遊戲即將關閉"; 
            case 110: return "超過限額上限，請聯絡您的上層"; 
            case 111: return "超過限額下限，請聯絡您的上層"; 
            case 116: return "入桌失敗，無指定遊戲權限";
            case 122: return "該桌已有同IP玩家在進行遊戲，請更換其他桌檯，謝謝";
            case 123: return "尚有桌台未結算完畢，請稍候再入桌";
            case 124: return "尚有桌台未結算完畢，請稍候再入桌";
            default: return "入桌失敗，未知的錯誤 代碼：" + failId; 
        }
    },
    betErrorId:{
        overBetMaxLimit:112,
        lowerBetMiniLimit:113, 
        rebetLowerBetMiniLimit:-20001,
        betIsMax:-20002,
        betOverCredit:30001,//重複下注超出玩家金額
        betOverLimit:30002,//超過補滿下注
    },
    betFailMsg:function(failId, value){
        cc.log("bean asdadad")
        switch(parseInt(failId))
        { 
            case 1: return "開始發牌，\n無法取消下注或下注";
            case 100: return "機器錯誤"; 
            case 101: return "無此桌";
            case 102: return "沒登入";
            case 103: return "沒入坐";
            case 104: return lessMoney;
            case 105: return "無使用者";
            case 106: return "下注鎖單";
            case 107: return "下注失敗，\n該注區已達最高下注額度";
            case 108: return "已達到限注條件";
            case 109: return "玩家額度更新錯誤";
            case 110: return "已達到當週限贏";
            case 111: return "已達到當週限輸";
            case Config.betErrorId.overBetMaxLimit: return "高於該注區押注限額" 
            case Config.betErrorId.lowerBetMiniLimit: return "低於該注區押注限額"
            case Config.betErrorId.rebetLowerBetMiniLimit: return "下注金額低於注區押注限額"
            case Config.betErrorId.betIsMax: return "該注區限額已滿"
            case 114: return "限紅資料不能為空"; 
            case 115: return "賠率未設定無法計算當週限贏限輸";
            case 116: return "玩家沒有權限開啟";
            case 117: return "玩家沒有下注";
            case Config.betErrorId.betOverCredit: return "帳戶額度不足";
            case Config.betErrorId.betOverLimit: return "超過該注區限額，\n已下滿本注區最大額度";
            default: return "未知的錯誤 代碼：" + failId; 
        }
    },
    httpGetMsgSuccessState:200,
    httpGetMsgSuccessReadyState:4,
    httpPostErrorTimeOutID:-1,
    webApiMsgID:{
        cantParserRequest:-2,
        parmError:-1,
        loginFail:0,
        newVersion:1,
        noNewVersion:2,
    },

    postLoginWebApiMsgID:{
        parmError:-1,
        success:1,
    },
    webApiErrorMsg:function(failId){
        switch(parseInt(failId))
        {
            case this.webApiMsgID.cantParserRequest: return "Server無法解析Request"; 
            case this.webApiMsgID.parmError: return "參數錯誤";
            case this.webApiMsgID.loginFail: return "登入失敗";
            default: return "未知的錯誤 代碼：" + failId; 
        }   
    },

    saveName:{
        userName:"username",
        password:"password",
        isAutoLogin:"isAutoLogin",
        lastVersion:"lastVersion",
        origin:"origin",
        userToken:"userToken",
        videoEnable:"videoEnable",  // 視訊開關
    },
    saveAudio:{
        music:"playBgMusic",
        effect:"playEffect",
    },
    saveData:{
        isShowFirstBetingTypeHint:"isShowFirstBetingTypeHint", // 有沒有秀過 百家樂玩法(標準, 免水)教學
    }, 
    spotID:{
        Banker:"11001",
        Player:"11002",
        Big:"11003",
        Small:"11004",
        PlayerJQK:"11005",
        BankerJQK:"11006",
        PlayerOdd:"11007",
        PlayerEven:"11008",
        BankerOdd:"11009",
        BankerEven:"11010",
        PlayerBankerJQK:"11011",
        PlayerA:"11012",
        BankerA:"11013",
        Tie:"11014",
        PlayerPair:"11015",
        BankerPair:"11016",
        PlayerJQKA:"11017",
        BankerJQKA:"11018",
        PlayerBankerA:"11019",
        PlayerBankerJQKA:"11020",
        BankerNoWater:"11021",
        PlayerNoWater:"11022",
        TieNoWater:"11023",
        BankerPairNoWater:"11024",
        PlayerPairNoWater:"11025",
        Super6:"11026", 
    },

    // origin =======
    companyId:{
        pharaoh:1,
        signatureClub:100,
        lameixisi:6,
        sands:5,
        ubenClub:101
    }, 
    
    companyName:{
        pharaoh:"法老王",
        signatureClub:"皇璽會",
        lameixisi:"拉美西斯",
        sands:"金沙",
        ubenClub:"御匾會"
    }, 

    customerId:{
        iwin168:1,
        iwinbet:2,
        iwin999:3,
    },
    
    customerServiceFB:{
        iwin168:"1667047676851255",
        iwinbet:"583118081838441",
        iwin999:"382365508771795",
    },
    customerServiceQQ:{
        iwin168:"2080595885",
        iwinbet:"1608584476",
        iwin999:"2914291857",
    },
    customerServiceLine:{
        iwin168:"http://line.naver.jp/ti/p/MHnVgS6_UU",
        iwinbet:"http://line.naver.jp/ti/p/j-irCViiWz",
        iwin999:"http://line.me/ti/p/aqSNGLkRao",
    },
    connectState:{
        connecting:0,
        connected:1,
        reconnecting:2,
        disconnected:3,
        error:-1
    },

    deviceStates:{
        revert:0, //重新
        foreground:1, //前景
        background:2, //背景
    },

    chips:[50,100,200,500,1000,2000,5000,10000,50000,100000],

    GameCenterErrorCode: {
        OperationSuccess: 0,
    },
}