/*
    功能描述：處理下三路路圖
    實作內容：
        1 訂閱 Table 中的路圖並分析顯示
        2 加入可註冊點擊後的事件
*/ 

import { putRoadMapContainer } from '../tool/baccaratRoad'  
var DownThreeRoadItem = require("DownThreeRoadItem"); 
var RoadView = require("RoadView"); 

cc.Class({
    extends: RoadView,

    properties: {
        downThreeRoadItem:cc.Prefab
    }, 


    // 回傳RoadMapContainer
    getRoadMapContainer:function(data) { 
        return putRoadMapContainer(this.roadmapItemHightCount, data)
    },


    //設定路圖 
    setRoadMap:function(data){
        this.stopAskRoad()
        this._super(data);
    },


    //重置路圖
    resetRoadMap:function(){ 
        this.stopAskRoad()
        this._super()
    }, 
 
 
    createRoadMapItems: function(pos) { 
        var newNode = cc.instantiate(this.downThreeRoadItem)
        this.roadMapNode.addChild(newNode)
        var item = newNode.getComponent(DownThreeRoadItem)
        newNode.setPosition(pos)
        newNode.active = true
        return item
    }

});
