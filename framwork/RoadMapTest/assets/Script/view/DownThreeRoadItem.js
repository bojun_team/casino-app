
import { getDownThreeRoadItemSpriteName } from '../tool/baccaratRoadSpriteTool'

const BIG_EYE = 0
const SMALL = 1
const COCKROACH = 2

cc.Class({
    extends: cc.Component, 
    properties: {
        downThreeRoadType:cc.Integer
    }, 
    onLoad:function(){
        this.sprite = this.getComponent(cc.Sprite)
    },
    setData: function(data, roadMapSpriteAtlas) {  
        let name = getDownThreeRoadItemSpriteName(data.type, this.downThreeRoadType)
        this.sprite.spriteFrame = roadMapSpriteAtlas.getSpriteFrame(name)
        this.data = data
        this.isHaveData = true 
    },
    startBlink: function(data, roadMapSpriteAtlas){ 
        this.setData(data, roadMapSpriteAtlas)
        this.isHaveData = false 
        const blinkDuration = 10
        const blinkCount = 20
        var seq = cc.sequence(cc.blink(blinkDuration, blinkCount), cc.callFunc(this.stopBlink, this))
        this.node.runAction(seq) 
    },
    stopBlink: function(){    
        this.node.stopAllActions()
        if (this.isHaveData) { 
            this.setData(this.data)
        } else  { 
            this.sprite.spriteFrame = null
        }  
    },
});
