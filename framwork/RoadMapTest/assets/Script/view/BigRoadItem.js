const BANKER = 1
const PLAYER = 2
const TIE = 3

import { getBigRoadItemSpriteName } from '../tool/baccaratRoadSpriteTool'

cc.Class({
    extends: cc.Component,

    properties: {
        
    }, 

    onLoad: function () {
        
        this.sprite = this.getComponent(cc.Sprite)
        this.numLabel = this.node.getChildByName("numLabel").getComponent(cc.Label)
    },

    setData: function(data, roadMapSpriteAtlas) { 
        if(data == null) {
            this.sprite.spriteFrame = null
            return
        }

        let name = getBigRoadItemSpriteName(data.type)
        this.sprite.spriteFrame = roadMapSpriteAtlas.getSpriteFrame(name)

        this.numLabel.node.active = (data.value != 0)
        this.numLabel.string = data.value

        this.data = data
        this.isHaveData = true 
    }, 
    startBlink: function(data, roadMapSpriteAtlas){ 
        this.setData(data, roadMapSpriteAtlas)
        this.isHaveData = false 
        const blinkDuration = 10
        const blinkCount = 20
        var seq = cc.sequence(cc.blink(blinkDuration, blinkCount), cc.callFunc(this.stopBlink, this))
        this.node.runAction(seq) 
    },
    stopBlink: function(){ 
        this.node.stopAllActions()
        if (this.isHaveData) { 
            this.setData(this.data)
        } else { 
            this.sprite.spriteFrame = null
        }  
    },
});
