
var Tool = {
    printData:function(data, spaceCount)
    { 
        if (spaceCount == null) spaceCount = 0
        var space = ""
        for (var i = 0; i < spaceCount; i++) 
            space = "    " + space 
        var str = "[\n"
        for (var key in data)
        {
            if (typeof(data[key]) == "object"){
                str = str + space + key + ":" + this.printData(data[key], spaceCount + 1) + ",\n"
            } 
            else{
                str = str + space + key + ":" + data[key] + ",\n"
            }
        }
        var space = ""
        for (var i = 0; i < spaceCount - 1; i++) 
            space = "    " + space 
        return str + space +"]"
    },

    printAll:function(data, spaceCount)
    { 
        if (spaceCount == null) spaceCount = 0
        var space = ""
        for (var i = 0; i < spaceCount; i++) 
            space = "    " + space 
        var str = ""
        for (var key in data)
        {
            if (typeof(data[key]) == "object"){
                this.printAll(data[key], spaceCount + 1)
            }
            else{
                cc.log(key + ":" + data[key])
            }
        } 
    },

    setSpritFrame:function(_sprit, plistUrl, pngName){
        cc.loader.loadRes(plistUrl, cc.SpriteAtlas, function (err, atlas) {
            var frame = atlas.getSpriteFrame(pngName)
            _sprit.spriteFrame = frame;
            cc.loader.releaseRes(plistUrl);
        });
    },
    //用來取數字轉成字串
    conversionNumber:function(number){
        var strNumber = ""
        var number_integer = Math.floor(number)  // 取整數部分
        
        var getStrSecond = function(str_second){
            for (var i = 3 ; i > 0 ; i -- ){
                var isReturn = false
                if (0 != Number(str_second.substring(i - 1, i))){
                    str_second = str_second.substring(0, i)
                    isReturn = true
                } 
                if (isReturn) break
            }

            if (0 == Number(str_second)) str_second = ""
            return str_second
        }

        var strN = String(number_integer)
        var iLen = strN.length
        if (number_integer %10 == 0 && 3 < iLen){
            if (number_integer % 10000 == 0){
                if (6 < iLen){
                    var str_first = strN.substring(0, iLen-6)
                    var str_second = strN.substring(str_first.length, iLen-3)
                    str_second = getStrSecond(str_second)
                    if (0 != str_second.length){
                        strNumber = str_first + "." + str_second + "M"
                    }else{
                        strNumber = str_first + "M"
                    }
                }else{
                    var str_first = strN.substring(0, iLen-3)
                    strNumber = str_first + "K"
                }
            }else{
                var str_first = strN.substring(0, iLen-3)
                var str_second = strN.substring(str_first.length, iLen)
                str_second = getStrSecond(str_second)
                if (0 != str_second.length) {
                    strNumber = str_first + "." + str_second + "K"
                }else{
                    strNumber = str_first + "K"
                }
            }
        }else{
            strNumber = String(number_integer) 
        }
        return strNumber
    },
    //設定sprite圖片
    setSpritFrame:function(_sprit, plistUrl, pngName){
        cc.loader.loadRes(plistUrl, cc.SpriteAtlas, function (err, atlas) {
            var frame = atlas.getSpriteFrame(pngName)
            _sprit.spriteFrame = frame;
        });
    },
    showAlert:function(title, msg, acceptFun){ 
        if (Tool.alertViewPrefab == null)
        { 
            Tool.AlertView = require("AlertView")
            cc.loader.loadRes("prefab/alertView", function (err, prefab) 
            {
                Tool.alertViewPrefab = prefab 
                Tool.showAlert(title, msg, acceptFun) 
            })
        }
        else
        { 
            var newNode = cc.instantiate(Tool.alertViewPrefab); 
            cc.director.getScene().addChild(newNode)
            var alertView = newNode.getComponent(Tool.AlertView)  
            alertView.addAlert(title, msg, acceptFun)
        }   
    },
    showRequireFailAlert:function(resp, error){
        Tool.showAlert(Config.titleMsg.Error, Config.errorMsg.UnknowError+ " " + resp + "\n" + error, function(){
            NativeConnect.exit()
        })
        return
    },
    // 使用後會不會自動移除(轉場景使用)
    // 
    showNextSceneLockView:function(callBack){ 
        if (Tool.lockViewPrefab == null)
        {  
            cc.loader.loadRes("prefab/lockView", function (err, prefab) 
            {
                Tool.lockViewPrefab = prefab 
                Tool.showNextSceneLockView(callBack) 
            })
        }
        else
        { 
            var lockView = cc.instantiate(Tool.lockViewPrefab); 
            cc.director.getScene().addChild(lockView)
            if(callBack!=null) callBack(lockView) 
        }
    },
    saveData:function(key, value){
        cc.sys.localStorage.setItem(key, value);
    },
    loadData:function(key, defaultValue){
        const value = cc.sys.localStorage.getItem(key)
        if (value == null) return defaultValue
        else return value
    },

    showToast:function(strMessage){
        if (Tool.toastViewPrefab == null){
            Tool.ToastView = require("ToastView")
            cc.loader.loadRes("prefab/ToastView", function (err, prefab) 
            {
                NativeConnect.nlog("showToast","load")
                Tool.toastViewPrefab = prefab 
                Tool.showToast(strMessage) 
            })
        }else{
            var newNode = cc.instantiate(Tool.toastViewPrefab); 
            cc.director.getScene().addChild(newNode)
            var toastView = newNode.getComponent(Tool.ToastView)
            toastView.addToast(strMessage)

        }
    },

    dailyQuotaUpdateView:function(){
        if (Tool.dailyQuotaUpdatePrefab == null){
            Tool.QuotaUpdateView = require("DailyQuotaUpdateView")
            cc.loader.loadRes("prefab/dailyQuotaUpdateView", function (err, prefab) 
            {
                Tool.dailyQuotaUpdatePrefab = prefab 
                Tool.dailyQuotaUpdateView()  
            })
        }else{
            Tool.dailyQuotaUpdateNode = cc.instantiate(Tool.dailyQuotaUpdatePrefab); 
            cc.director.getScene().addChild(Tool.dailyQuotaUpdateNode)
        }
    },

    removeDailyQuotaUpdateView:function(){
        if (Tool.dailyQuotaUpdateNode) cc.director.getScene().removeChild(Tool.dailyQuotaUpdateNode)
    },

    addMusicNode:function(){
        if (Tool.MusicPrefab == null){
            Tool.MusicControl = require("MusicControl")
            cc.loader.loadRes("prefab/MusicNode", function (err, prefab) 
            {
                Tool.MusicPrefab = prefab 
                Tool.addMusicNode() 
            })
        }else{
            Tool.MusicNode = cc.instantiate(Tool.MusicPrefab)
            cc.director.getScene().addChild(Tool.MusicNode)
            cc.game.addPersistRootNode(Tool.MusicNode) // 跳場景不會關閉node
            
        }
    },

    saveBgMusicData:function(isPlay){
        Tool.saveData(Config.saveAudio.music, String(isPlay))
        Tool.playBgMusic(isPlay)
    },

    saveEffectData:function(isPlay){
        Tool.playEffect()
        Tool.saveData(Config.saveAudio.effect,String(isPlay))
    },

    playBgMusic:function(isPlay){
        var playMusic = 1
        var closeMusic = 0
        if (Tool.MusicNode){
            var musicControl = Tool.MusicNode.getComponent(Tool.MusicControl)
            if (isPlay){
                musicControl.setBgMusicVolume(playMusic)
            }else{
                musicControl.setBgMusicVolume(closeMusic)
            }
        }
    },

    playEffect:function(){
        if ("true" == Tool.loadData(Config.saveAudio.effect, "true")){
            if (Tool.MusicNode){
                var musicControl = Tool.MusicNode.getComponent(Tool.MusicControl)
                musicControl.playBtnEffect()
            }
        }
    },

    getMusicControl:function(){
        if (this.musicControl != null) return this.musicControl
        else{
          this.musicControl = Tool.MusicNode.getComponent(Tool.MusicControl)
          return this.musicControl
        }
    },

    getThousandsOfBits:function(credit){
        //3為千分位
        const digital = 3
        const creditString = "" + credit
        const digtalCount = creditString.length 
        var finalStr = ""
        for(var n = creditString.length; n > 0; n-=digital)
        {
            if (n - digital <= 0)
                finalStr = creditString.substring(n - digital, n) + finalStr
            else
                finalStr =  "," + creditString.substring(n - digital, n) + finalStr
        }
        return "$" + finalStr
    },

    addDeviceStatesNode:function(){
        if (Tool.DeviceStatesPrefab == null){
            Tool.MonitorDeviceStates = require("MonitorDeviceStates")
            cc.loader.loadRes("prefab/DeviceStatesNode", function (err, prefab) 
            {
                Tool.DeviceStatesPrefab = prefab 
                Tool.addDeviceStatesNode() 
            })
        }else{
            Tool.DeviceStatesNode = cc.instantiate(Tool.DeviceStatesPrefab)
            cc.director.getScene().addChild(Tool.DeviceStatesNode)
            cc.game.addPersistRootNode(Tool.DeviceStatesNode) // 跳場景不會關閉node
            
        }
    },
    
} 