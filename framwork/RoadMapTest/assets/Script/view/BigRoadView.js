/*
    功能描述：處理大路路圖
    實作內容：
        1 訂閱 Table 中的路圖並分析顯示
        2 加入可註冊點擊後的事件
*/ 

// import { putRoadMapContainer} from '../tool/baccaratRoad'
import { putRoadMapContainer} from '../tool/baccaratRoad'

var BigRoadItem = require("BigRoadItem"); 
var RoadView = require("RoadView"); 

cc.Class({
    extends: RoadView, 

    properties: { 
        bigRoadItem:cc.Prefab
    }, 


    // 回傳RoadMapContainer
    getRoadMapContainer:function(data) {  

        // let DragonRoad = mapToDragonRoad(6, 80,  { type: null, value: null }, data)
        // cc.log("Tool.printData(data) = " + Tool.printData(data)) 
        let DragonRoad =  putRoadMapContainer(this.roadmapItemHightCount , data)    
        return DragonRoad
    },


    //設定路圖 
    setRoadMap:function(data){
        this.stopAskRoad()
        this._super(data);
    },


    //重置路圖
    resetRoadMap:function(){
        this.stopAskRoad()
        this._super()
    },


    createRoadMapItems: function(pos) { 
        var newNode = cc.instantiate(this.bigRoadItem)
        this.roadMapNode.addChild(newNode)
        var item = newNode.getComponent(BigRoadItem)
        newNode.setPosition(pos)
        newNode.active = true
        return item
    }

});
