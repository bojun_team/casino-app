import { getCubeRoadItemSpriteName } from '../tool/baccaratRoadSpriteTool'

cc.Class({
    extends: cc.Component,

    properties: {
        
    }, 
    
    setData: function(data, roadMapSpriteAtlas) {  
        let name = getCubeRoadItemSpriteName(data.type, data.value, data.pairs)
        this.getComponent(cc.Sprite).spriteFrame = roadMapSpriteAtlas.getSpriteFrame(name)
    },
});
  