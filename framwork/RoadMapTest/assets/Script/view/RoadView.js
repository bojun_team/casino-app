import { putRoadMapContainer } from '../tool/baccaratRoad' 

cc.Class({
    extends: cc.Component,

    properties: {
        roadMapNode:cc.Node, 
        scrollContent:cc.Node,
        scrollView:cc.ScrollView,

        bgWidget:cc.Widget,

        roadmapItemSize:80, 
        roadmapItemHightCount:6,
        roadMapSpriteAtlas:cc.SpriteAtlas,
    },


    genDefaultSize:function(){
        if (this.defaultSize == null) {
            this.defaultSize = cc.v2(this.node.width, this.node.height)
        }
    },


    getMiniCuount:function(){
        if (this.miniCount == null)
            this.miniCount = this.defaultSize.x / this.roadmapItemSize
        return this.miniCount
    },


    // 交給子類別去覆寫
    getRoadMapContainer:function(data) {
        return null
    },


    //設定路圖 
    setRoadMap:function(data){
        this.genDefaultSize()
        
        const xOffset = this.roadmapItemSize * 0.5 + 10
        var yOffset = this.roadmapItemSize * 0.5
 

        // 放入6 x 6的陣列中
        var roadMapContainer = this.getRoadMapContainer(data)
        this.currentRoadData = roadMapContainer
        if (this.roadMapItems == null) this.roadMapItems = []
        // 如果目前的路圖不足把填滿roadMapNode，就把roadMapNode畫滿 
        const minCount = this.getMiniCuount() 
        const xLength = minCount > roadMapContainer.length ? minCount : roadMapContainer.length + 1

        // 將路圖放入roadMapNode
        for(var x = 0; x < xLength; x++)
        {
            if (this.roadMapItems[x] == null) this.roadMapItems[x] = [] 
            for(var y = 0; y < this.roadmapItemHightCount + 1; y++)//+1是為了多畫一行格線
            {  
                if (roadMapContainer.length > x && roadMapContainer[x].length > y && roadMapContainer[x][y] != null){
                    const pos = new cc.Vec2(this.roadmapItemSize * x + xOffset, -this.roadmapItemSize * y + yOffset)
                    if(this.roadMapItems[x][y] == null || this.roadMapItems[x][y].node == null) {
                        this.roadMapItems[x][y] = this.createRoadMapItems(pos)  
                    }
                    this.roadMapItems[x][y].node.setPosition(pos)
                    this.roadMapItems[x][y].setData(roadMapContainer[x][y], this.roadMapSpriteAtlas) 
                }
            }
        }
        //設定scrollContent的寬，+1是為了預留空間 
        this.scrollContent.width = (roadMapContainer.length + 1) * this.roadmapItemSize
        //路圖太空就是預留大小
        if (this.scrollContent.width < this.defaultSize.x){
            this.scrollContent.width = this.defaultSize.x
        } else {
            if (this.scrollView != null)
                this.scrollView.scrollToRight()
        }
        //按鈕大小跟著scrollContent改變，才能被按到 
        this.bgWidget.enabled = true
    }, 

    
    //重置路圖
    resetRoadMap:function(){
        this.currentRoadData = []
        this.roadMapNode.removeAllChildren()
        this.roadMapItems = null
    }, 

    
    //莊問路
    startAskRoad:function(data){
        this.genDefaultSize()
        this.stopAskRoad()
        const xOffset = this.roadmapItemSize * 0.5 + 10 // x 位移
        const yOffset = this.roadmapItemSize * 0.5 // y 位移Ｓ
        var roadMapContainer = putRoadMapContainer(this.roadmapItemHightCount, data)
        if (this.roadMapItems == null) this.roadMapItems = []
        if (this.currentRoadData == null) this.currentRoadData = []    
        for(var x = 0; x < roadMapContainer.length; x++)
        {
            if (this.roadMapItems[x] == null) this.roadMapItems[x] = []
            if (this.currentRoadData[x] == null) this.currentRoadData[x] = []
            for(var y = 0; y < roadMapContainer[x].length; y++)
            {  
                if (roadMapContainer[x][y] != null && 
                    (this.currentRoadData[x][y] == null || 
                     roadMapContainer[x][y].type != this.currentRoadData[x][y].type))
                {
                    const pos = new cc.Vec2(this.roadmapItemSize * x + xOffset, -this.roadmapItemSize * y + yOffset)
                    this.blinkRoad = this.createRoadMapItems(pos)
                    this.blinkRoad.node.setPosition(pos)  
                    this.blinkRoad.startBlink(roadMapContainer[x][y], this.roadMapSpriteAtlas)
                }
            }
        }

        let scrollContentWidth = (roadMapContainer.length) * this.roadmapItemSize
        if (scrollContentWidth > this.defaultSize.x){
            if (this.scrollView != null)
                this.scrollView.scrollToRight()
        }
    },


    stopAskRoad:function(){
        if (this.blinkRoad != null) {
            this.blinkRoad.stopBlink()
            this.blinkRoad.node.removeFromParent() 
            this.blinkRoad = null
        } 
    }

});
