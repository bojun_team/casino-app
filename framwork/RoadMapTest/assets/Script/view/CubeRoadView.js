/*
    功能描述：處理珠路路圖
    實作內容：
        1 訂閱 Table 中的路圖並分析顯示
        2 加入可註冊點擊後的事件
*/

var CubeRoadItem = require("CubeRoadItem");
var RoadView = require("RoadView"); 

import { putCuteRoadMapContainer } from '../tool/baccaratRoad'

cc.Class({
    extends: RoadView,

    properties: {
        cubeRoadItem:cc.Prefab
    },
    

    // 回傳RoadMapContainer
    getRoadMapContainer:function(data) {  
        const DragonRoad = putCuteRoadMapContainer(this.roadmapItemHightCount - 1, data) 
        return DragonRoad
    },


    //設定路圖 
    setRoadMap:function(data){
        this._super(data);
    },


    //重置路圖
    resetRoadMap:function(){ 

        this._super()
    }, 


    createRoadMapItems: function(pos) {
        var newNode = cc.instantiate(this.cubeRoadItem)
        this.roadMapNode.addChild(newNode)
        var item = newNode.getComponent(CubeRoadItem)
        newNode.setPosition(pos)
        newNode.active = true
        return item
    }

});

