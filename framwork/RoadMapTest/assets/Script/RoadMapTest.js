import { append} from 'ramda'
import { parserRoadMap, mapToBigRoad, mapToDownThreeRoad} from './tool/baccaratRoad'  

const BANKER = 1
const PLAYER = 2
const TIE = 3

const BIG_EYE = 0
const SMALL = 1
const COCKROACH = 2

cc.Class({
    extends: cc.Component,

    properties: {
        cubeRoadView:require("CubeRoadView"),
        bigRoadView:require("BigRoadView"),
        bigEyeRoadView:require("DownThreeRoadView"),
        smallRoadView:require("DownThreeRoadView"),
        cockroachRoadView:require("DownThreeRoadView"),
    },

    // use this for initialization
    onLoad: function () {
        cc.log("bean Config.gameState = ", + Config.betFailMsg(1, 2))
        this.data = ""

        this.editBox = this.node.getChildByName("editBox").getComponent(cc.EditBox)
        this.sendButton = this.node.getChildByName("sendButton")
        this.resetButton = this.node.getChildByName("resetButton")
        this.askBankerButton = this.node.getChildByName("askBankerButton")
        this.askPlayerButton = this.node.getChildByName("askPlayerButton")

        this.sendButton.on("click", (function () {
            this.checkStr()
            this.data = (this.data == "") ? (this.editBox.string) : (this.data + "," + this.editBox.string)
            this.parserRoadMapData(this.data)
        }), this);

        this.resetButton.on("click", (function () {
            this.data = []
            this.cubeRoadView.resetRoadMap()
            this.bigRoadView.resetRoadMap()
            this.bigEyeRoadView.resetRoadMap()
            this.smallRoadView.resetRoadMap()
            this.cockroachRoadView.resetRoadMap()
        }), this);

        this.askBankerButton.on("click", (function () {
            let nextBigRoad = mapToBigRoad(append({ type: BANKER, value: 0 }, this.curCubeRoad))
            this.bigRoadView.startAskRoad(nextBigRoad)

            let bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(nextBigRoad)   
            let smallRoad = mapToDownThreeRoad(SMALL)(nextBigRoad)   
            let cockroachRoad = mapToDownThreeRoad(COCKROACH)(nextBigRoad)   
            this.bigEyeRoadView.startAskRoad(bigEyeRoad) 
            this.smallRoadView.startAskRoad(smallRoad) 
            this.cockroachRoadView.startAskRoad(cockroachRoad) 
        }), this);

        this.askPlayerButton.on("click", (function () {
            let nextBigRoad = mapToBigRoad(append({ type: PLAYER, value: 0 }, this.curCubeRoad))
            this.bigRoadView.startAskRoad(nextBigRoad)

            let bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(nextBigRoad)
            let smallRoad = mapToDownThreeRoad(SMALL)(nextBigRoad)
            let cockroachRoad = mapToDownThreeRoad(COCKROACH)(nextBigRoad)
            this.bigEyeRoadView.startAskRoad(bigEyeRoad)
            this.smallRoadView.startAskRoad(smallRoad) 
            this.cockroachRoadView.startAskRoad(cockroachRoad) 
        }), this);

        this.setOtherButtons()
    },
    
    parserRoadMapData(data) {
        this.curCubeRoad = parserRoadMap(data)
        let bigRoad = mapToBigRoad(this.curCubeRoad)
        let bigEyeRoad = mapToDownThreeRoad(BIG_EYE)(bigRoad)  
        let smallRoad = mapToDownThreeRoad(SMALL)(bigRoad)   
        let cockroachRoad = mapToDownThreeRoad(COCKROACH)(bigRoad)   

        this.cubeRoadView.setRoadMap(this.curCubeRoad)
        this.bigRoadView.setRoadMap(bigRoad)
        this.bigEyeRoadView.setRoadMap(bigEyeRoad) 
        this.smallRoadView.setRoadMap(smallRoad) 
        this.cockroachRoadView.setRoadMap(cockroachRoad) 
    },

    checkStr: function() {
        if(this.editBox.string.length > 3) {
            this.editBox.string = this.editBox.string.substring(0, 3)
        } else if(this.editBox.string.length == 2) {
           this.editBox.string = this.editBox.string + "0"
        }else if(this.editBox.string.length == 1) {
            this.editBox.string = this.editBox.string + "00"
        }else if(this.editBox.string.length == 0) {
            this.editBox.string = "100"
        }

        if(this.editBox.string.substring(0, 1) > 3 || this.editBox.string.substring(0, 1) <1) {
            this.editBox.string = "3" + this.editBox.string.substring(1, 3)
        }
    }, 

    setOtherButtons: function() {
        let btns = ["bankerWinButton", "playerWinButton", "tieWinButton", "bankerPairButton", "playerPairButton"]
        for(let index in btns) {
            this.node.getChildByName(btns[index]).on("click", (function () {
                let type = Math.floor(Math.random() * 3) + 1  
                let value = Math.floor(Math.random() * 9) + 0
                let pair = Math.floor(Math.random() * 4) + 0 
                let nums = [
                    ("1" + String(value) + String(pair)),
                    ("2" + String(value) + String(pair)),
                    ("3" + String(value) + String(pair)),
                    (String(type) + String(value) + "2"),
                    (String(type) + String(value) + "1"),
                ]
                this.editBox.string = nums[index]
                this.data = (this.data == "") ? (this.editBox.string) : (this.data + "," + this.editBox.string)
                this.parserRoadMapData(this.data)
            }), this);
        }
    },
});  
